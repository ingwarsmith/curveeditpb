#include "gcelogiclayer.h"

#include "imports/importeddataindexer.h"
#include "base/importmoduleinterface.h"
#include "base/variablecontainer.h"
#include <QDir>
#include <QDataStream>
#include <QPluginLoader>
#include <QCoreApplication>
#include <QVector>
#include <map>

GceLogicLayer::GceLogicLayer()
{
    ocpInited = false;
    indexerImports = new ImportedDataIndexer(this);
}

GceLogicLayer::~GceLogicLayer()
{
    if (indexerImports && indexerImports->parent() == nullptr)
    {
        delete indexerImports;
    }
}

QList<uint32_t> GceLogicLayer::getSourcesIds() const
{
    QList<uint32_t> res;
    for (auto &ds : sources)
    {
        res.push_back(ds->getSourceID());
    }
    return res;
}

QList<QPair<uint32_t,uint32_t> > GceLogicLayer::getCurvesIdsWithSourcesIds() const
{
    QList<QPair<uint32_t,uint32_t> > res;
    for (auto &ds : sources)
    {
        auto sourceId = ds->getSourceID();
        QList<uint32_t> curvesIds;
        ds->getCurvesIDs(curvesIds);
        for (uint32_t curveID : curvesIds)
        {
            QPair<uint32_t,uint32_t> p;
            p.first = curveID;
            p.second = sourceId;
            res.append(p);
        }
    }
    return res;
}

QString GceLogicLayer::getCurveStringIdByCurveId(uint32_t srcId, uint32_t curveId, bool &success)
{
    success = false;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return QString();
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return QString();
    }
    success = true;
    return QString::fromStdString(crv.second->name);
}

QString GceLogicLayer::getCurveDescriptionStringByCurveId(uint32_t srcId, uint32_t curveId, bool &success)
{
    success = false;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return QString();
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return QString();
    }
    success = true;
    return QString::fromStdString(crv.second->description);
}

bool GceLogicLayer::isLoggingCurveLoaded(uint32_t srcId, uint32_t curveId, bool &exists)
{
    exists = false;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return false;
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return false;
    }
    exists = true;
    return crv.second->loaded;
}

std::pair<bool, double> GceLogicLayer::nullValueExistAndValue(uint32_t srcId, uint32_t curveId, bool &existsCurve)
{
    std::pair<bool, double> ans(false, 0);
    existsCurve = false;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return ans;
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return ans;
    }
    existsCurve = true;
    ans.first = crv.second->null_value_exist;
    if (ans.first) ans.second = crv.second->null_value;
    return ans;
}

QVector<double> GceLogicLayer::getValuesX(uint32_t srcId, uint32_t curveId, int64_t msecsProjectTimeShift, int64_t msecsDataSourceTimeShift, bool &success)
{
    success = false;
    auto msecsFullShift = msecsProjectTimeShift + msecsDataSourceTimeShift;
    auto msecsShiftDbl = static_cast<double>(msecsFullShift);
    auto secsShiftDbl = msecsShiftDbl * 0.001;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return QVector<double>();
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return QVector<double>();
    }
    success = true;
    return src__->getTimesAsXFromCurve(curveId, secsShiftDbl);
}

QVector<double> GceLogicLayer::getValuesY(uint32_t srcId, uint32_t curveId, bool &success)
{
    success = false;
    auto src__ = getDataSource(srcId);
    if (!src__)
    {
        return QVector<double>();
    }
    auto crv = src__->getLoggingCurve(curveId);
    if (!crv.first)
    {
        return QVector<double>();
    }
    auto excludedTimeData = src__->getExcludedTimeData();
    QVector<double> y_n;
    if (excludedTimeData.size())
    {
        for (size_t i = 0; i < crv.second->values.size(); ++i)
        {
            auto it = std::find(excludedTimeData.begin(), excludedTimeData.end(), i);
            if (it != excludedTimeData.end())
            {
                continue;
            }
            auto vl = crv.second->values.at(i);
            if (crv.second->null_value_exist && qFuzzyCompare(crv.second->null_value, vl))
            {
                continue;
            }
            y_n.push_back(vl);
        }
    }
    else
    {
        if (crv.second->null_value_exist)
        {
            for (size_t i = 0; i < crv.second->values.size(); ++i)
            {
                auto vl = crv.second->values.at(i);
                if (qFuzzyCompare(crv.second->null_value, vl))
                {
                    continue;
                }
                y_n.push_back(vl);
            }
        }
        else
        {
            QVector<double> tmp_vec(crv.second->values.begin(), crv.second->values.end());
            y_n.swap(tmp_vec);
        }
    }

    success = true;
    return y_n;
}

std::vector<std::pair<double, double>> GceLogicLayer::getNoDataBlocksAtrtsWithEndsForSource(uint32_t srcId)
{
    return getDataSource(srcId)->getAllBlocksStartsWithEnds();
}

QVector<QObject *> *GceLogicLayer::getObjectsContainerPointer()
{
    return objContainerPtr;
}

void GceLogicLayer::setObjectsContainerPointer(QVector<QObject *> *ocp)
{
    ocpInited = true;
    objContainerPtr = ocp;
}

QObject *GceLogicLayer::getObjectFromObjectContainerByObjName(QString nm)
{
    for (decltype (objContainerPtr->size()) i_o = 0; i_o < objContainerPtr->size(); ++i_o)
    {
        auto o = objContainerPtr->at(i_o);
        if (o->objectName() == nm)
        {
            return o;
            break;
        }
    }
    return nullptr;
}

bool GceLogicLayer::existObjectInObjectContainerByObjName(QString nm)
{
    auto res = false;
    for (decltype (objContainerPtr->size()) i_o = 0; i_o < objContainerPtr->size(); ++i_o)
    {
        auto o = objContainerPtr->at(i_o);
        if (o->objectName() == nm)
        {
            res = true;
            break;
        }
    }
    return res;
}

uint32_t GceLogicLayer::importByFileName(bool &importCompletedSuccessfull,
                                         QStringList &errorsIfExist,
                                         QString projectHeaderPath,
                                         QString fileNameForImport)
{
    importCompletedSuccessfull = false;
    auto formatNames = QStringList();
    auto appDirPath = qApp->applicationDirPath();
    auto importModulesDirPath = QString().append(appDirPath).append("/import.modules");

    QDir importModulesDir(importModulesDirPath);
    if (importModulesDir.entryInfoList(QDir::Files).size() == 0)
    {
        return 0;
    }

    for (QFileInfo &fi : importModulesDir.entryInfoList(QDir::Files))
    {
        auto extension = fi.suffix().toUpper();
        auto completeExtension = fi.completeSuffix().toUpper();
        if (extension == "DLL" || completeExtension.split(".").first() == "SO")
        {
            QPluginLoader loader_local;
            loader_local.setFileName(fi.absoluteFilePath());
            if(!loader_local.isLoaded())
            {
                if(!loader_local.load())
                {
                    continue;
                }
            }
            QObject *obj = loader_local.instance();
            if (obj == nullptr)
            {
                continue;
            }
            ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(obj);
            if (iFace == nullptr)
            {
                loader_local.unload();
                continue;
            }
            QString formatName;
            iFace->getFormatName(formatName);
            QStringList extensionsList;
            iFace->getFileExtensionsList(extensionsList);
            importModulesFI_byExtensionsList.insert(extensionsList, fi);
            formatName.append("|").append(extensionsList.join(";;"));
            formatNames.append(formatName);
            loader_local.unload();
        }
    }

    QString pathToImported = fileNameForImport;

    ImportModuleInterface *selectedIFace = nullptr;
    QString selectedFileFormat;
    for (QFileInfo &fi : importModulesFI_byExtensionsList.values())
    {
        QPluginLoader loader_local;
        loader_local.setFileName(fi.absoluteFilePath());
        if(!loader_local.isLoaded())
        {
            if(!loader_local.load())
            {
                continue;
            }
        }
        QObject *obj = loader_local.instance();
        if (obj == nullptr)
        {
            continue;
        }
        ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(obj);
        iFace->setDataFileIndexed(pathToImported);
        QStringList errors;
        iFace->getErrors(errors);
        if (errors.size())
        {
            if (errors.contains(tr("Incorrect file format")))
            {
                loader_local.unload();
            }
        }
        else
        {
            selectedIFace = iFace;
            iFace->getFormatName(selectedFileFormat);
            break;
        }
    }

    if (selectedIFace == nullptr)
    {
        return 0;
    }

    uint32_t dsIdNext = 0;

    for (auto &ds : sources)
    {
        if (ds->getSourceID() >= dsIdNext)
        {
            dsIdNext = ds->getSourceID() + 1;
        }
    }

    AbscissParameter x_type;
    selectedIFace->getAbscissType(x_type);
    auto newDS = new GceDataSource(dsIdNext, x_type);
    newDS->generateMagicWord(); // только при импорте новых данных в датасорц

    indexerImports->setDirCurrentProjectPath(QString(projectHeaderPath).remove(".ceproj"));
    ImportedDataIndexer::DescriptionImport di =
            indexerImports->createIndexFromImport(pathToImported,
                                                  newDS->magicWord_DS,
                                                  selectedFileFormat);

    newDS->setSourceInfo(di.importDateTime.toString("yyyy_MM_dd"),
                         di.originalFileName, di.originalExtension,
                         di.originalPath);

    auto ok = false;
    QString fPath = indexerImports->prepareIndexedFileName(di.magicWord_Indexer, ok);

    ImportModuleInterface *iFace = selectedIFace;
    iFace->setDataFileIndexed(fPath);
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        indexerImports->removeDIbyIndex(di.index);
        delete newDS;
        return -2;
    }

    QStringList namesCurves, descriptionsCurves;
    iFace->getNamesOfCurves(namesCurves, descriptionsCurves);
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        indexerImports->removeDIbyIndex(di.index);
        delete newDS;
        return -2;
    }

    QString deviceInformation;
    iFace->getParametersOfDevice(deviceInformation);
    newDS->setAdditionalInfoSpec(deviceInformation);

    QVector<HierarchyMember> hierarhyInfo;
    iFace->getHierarchyInfo(hierarhyInfo);
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        indexerImports->removeDIbyIndex(di.index);
        delete newDS;
        return -2;
    }

    for (HierarchyMember &hMmb : hierarhyInfo)
    {
        newDS->addHierarchyMember(hMmb);
    }

    for (QString &dEl : namesCurves)
    {
        auto idxNameCurve = namesCurves.indexOf(dEl);
        QString description;
        if (descriptionsCurves.size() > idxNameCurve)
        {
            description = descriptionsCurves.at(idxNameCurve);
        }
        newDS->addCurveName(dEl);
        newDS->addLoggingDataRaw(x_type, dEl, description);
    }

    newDS->index_forImportedDataIndexer = di.index;

    QStringList existDevicesNames;
    for (auto &ds : sources)
    {
        existDevicesNames << ds->getDeviceName();
    }

    QString newDeviceName = di.originalFileName;
    QString devNm;
    iFace->getDeviceName(devNm);
    if (devNm != "-" && !devNm.isEmpty())
    {
        newDeviceName = devNm;
    }
    if (existDevicesNames.contains(newDeviceName))
    {
        int maxCountAlmostSameNames = 1;
        auto notExist = false;
        while (!notExist)
        {
            QString numb = QString::number(maxCountAlmostSameNames);
            if (numb.size() == 1)
            {
                numb.prepend("0");
            }
            QString prepareName = QString().append(newDeviceName).append("_").append(numb);
            if (!existDevicesNames.contains(prepareName))
            {
                notExist = true;
                newDeviceName = prepareName;
            }
            else
            {
                maxCountAlmostSameNames++;
            }
        }
    }

    newDS->setDeviceName(newDeviceName);
    sources.push_back(std::unique_ptr<GceDataSource>(newDS));
    importCompletedSuccessfull = true;

    return di.index;
}

uint32_t GceLogicLayer::callImportDialog(bool &importCompletedSuccessfull, QString projectHeaderPath)
{
    QPluginLoader local_loader;
    auto formatNames = QStringList();
    auto appDirPath = qApp->applicationDirPath();
    auto importModulesDirPath = QString().append(appDirPath).append("/import.modules");

    QDir importModulesDir(importModulesDirPath);
    if (importModulesDir.entryInfoList(QDir::Files).size() == 0)
    {
        return 0;
    }

    std::map<std::string, AbscissParameter> mapAbsiccParamsByFormatName;
    std::map<std::string, std::string> mapModulesPathsByFormatName;
    for (QFileInfo &fi : importModulesDir.entryInfoList(QDir::Files))
    {
        auto extension = fi.suffix().toUpper();
        auto fullPath = fi.absoluteFilePath();
        auto completeExtension = fi.completeSuffix().toUpper();
        if (extension == "DLL" || completeExtension.split(".").first() == "SO")
        {
            QObject *obj = nullptr;
            local_loader.setFileName(fi.absoluteFilePath());
            local_loader.load();
            obj = local_loader.instance();

            if (obj == nullptr)
            {
                continue;
            }
            ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(obj);
            QString formatName;
            iFace->getFormatName(formatName);
            QStringList extensionsList;
            iFace->getFileExtensionsList(extensionsList);
            importModulesFI_byExtensionsList.insert(extensionsList, fi);
            auto fmtFull = QString(formatName).append("|").append(extensionsList.join(";;"));
            formatNames.append(fmtFull);
            AbscissParameter ap;
            iFace->getAbscissType(ap);
            mapAbsiccParamsByFormatName.insert(std::make_pair(formatName.toStdString(), ap));
            mapModulesPathsByFormatName.insert(std::make_pair(formatName.toStdString(), fi.absoluteFilePath().toStdString()));
            local_loader.unload();
        }
    }

    if (formatNames.empty()) return -1;

    importCompletedSuccessfull = false;
    QString pathToImported = module.showDialog(formatNames);

    if (!module.isSuccessfull())
    {
        return 0;
    }
    auto formatSelected = module.selectedFileFormat();
    auto absciss_type = mapAbsiccParamsByFormatName.find(formatSelected.toStdString())->second;
    auto moduleAbsPathStd = mapModulesPathsByFormatName.find(formatSelected.toStdString())->second;
    QString moduleAbsPath = QString::fromStdString(moduleAbsPathStd);
    uint32_t dsIdNext = 0;

    for (auto &ds : sources)
    {
        if (ds->getSourceID() >= dsIdNext)
        {
            dsIdNext = ds->getSourceID() + 1;
        }
    }
    auto newDS = std::make_unique<GceDataSource>(dsIdNext, absciss_type);

    newDS->generateMagicWord(); // только при импорте новых данных в датасорц

    indexerImports->setDirCurrentProjectPath(QString(projectHeaderPath).remove(".ceproj"));
    ImportedDataIndexer::DescriptionImport di =
            indexerImports->createIndexFromImport(pathToImported,
                                                  newDS->magicWord_DS,
                                                  module.selectedFileFormat());

    newDS->setSourceInfo(di.importDateTime.toString("yyyy_MM_dd"),
                         di.originalFileName, di.originalExtension,
                         di.originalPath);

    auto ok = false;
    QString fPath = indexerImports->prepareIndexedFileName(di.magicWord_Indexer, ok);

    if (!ok)
    {
        indexerImports->removeDIbyIndex(di.index);
        return -1;
    }

    ok = false;

//    for (QFileInfo &fi_impMod : importModulesFI_byExtensionsList.values())
//    {
//        loader.setFileName(fi_impMod.absoluteFilePath());
////        ifz
//        if(!loader.isLoaded())
//        {
//            if(!loader.load())
//            {
//                continue;
//            }
//        }
//        ImportModuleInterface *iFace__ = qobject_cast<ImportModuleInterface*>(loader.instance());
//        QString formatNameString;
//        iFace__->getFormatName(formatNameString);
//        auto uloadOk = loader.unload();
//        if (formatNameString == di.dataType)
//        {
//            ok = true;
//            break;
//        }
//    }

    if (loader.isLoaded())
    {
        loader.unload();
    }
    loader.setFileName(moduleAbsPath);
    if (!loader.load())
    {
        indexerImports->removeDIbyIndex(di.index);
        return -1;
    }
    ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(loader.instance());
    QString formatNameString;
    iFace->getFormatName(formatNameString);
    ok = formatNameString == di.dataType;
    if (!ok)
    {
        loader.unload();
        indexerImports->removeDIbyIndex(di.index);
        return -1;
    }

    iFace->setDataFileIndexed(fPath);
    QStringList errorsIfExist;
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        loader.unload();
        module.showErrorsLoadingData(errorsIfExist.join("\n"));
        indexerImports->removeDIbyIndex(di.index);
        return -2;
    }

    AbscissParameter x_type;
    iFace->getAbscissType(x_type);

    QStringList namesCurves, descriptionsCurves;
    iFace->getNamesOfCurves(namesCurves, descriptionsCurves);
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        module.showErrorsLoadingData(errorsIfExist.join("\n"));
        indexerImports->removeDIbyIndex(di.index);
        return -2;
    }

    QString deviceInformation;
    iFace->getParametersOfDevice(deviceInformation);
    newDS->setAdditionalInfoSpec(deviceInformation);

    QVector<HierarchyMember> hierarhyInfo;
    iFace->getHierarchyInfo(hierarhyInfo);
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        module.showErrorsLoadingData(errorsIfExist.join("\n"));
        indexerImports->removeDIbyIndex(di.index);
        return -2;
    }

    for (HierarchyMember &hMmb : hierarhyInfo)
    {
        newDS->addHierarchyMember(hMmb);
    }

    for (QString &dEl : namesCurves)
    {
        auto idxNameCurve = namesCurves.indexOf(dEl);
        QString description;
        if (descriptionsCurves.size() > idxNameCurve)
        {
            description = descriptionsCurves.at(idxNameCurve);
        }
        newDS->addCurveName(dEl);
        newDS->addLoggingDataRaw(x_type, dEl, description);
    }

    newDS->index_forImportedDataIndexer = di.index;

    QStringList existDevicesNames;
    for (auto &ds : sources)
    {
        existDevicesNames << ds->getDeviceName();
    }

    QString newDeviceName = di.originalFileName;
    QString devNm;
    iFace->getDeviceName(devNm);
    if (devNm != "-" && !devNm.isEmpty())
    {
        newDeviceName = devNm;
    }
    if (existDevicesNames.contains(newDeviceName))
    {
        int maxCountAlmostSameNames = 1;
        auto notExist = false;
        while (!notExist)
        {
            QString numb = QString::number(maxCountAlmostSameNames);
            if (numb.size() == 1)
            {
                numb.prepend("0");
            }
            QString prepareName = QString().append(newDeviceName).append("_").append(numb);
            if (!existDevicesNames.contains(prepareName))
            {
                notExist = true;
                newDeviceName = prepareName;
            }
            else
            {
                maxCountAlmostSameNames++;
            }
        }
    }

    newDS->setDeviceName(newDeviceName);
    sources.push_back(std::move(newDS));
    importCompletedSuccessfull = true;

    return di.index;
}

bool GceLogicLayer::checkExistingImportedFileForDataSource(uint32_t srcID)
{
    auto res = false;

    auto dataSrc = getDataSource(srcID);
    if (dataSrc == nullptr)
    {
        return res;
    }

    auto descriptionImported = indexerImports->getDIbyIndex(dataSrc->index_forImportedDataIndexer);

    auto ok = false;
    QString fPath = indexerImports->prepareIndexedFileName(descriptionImported.magicWord_Indexer, ok);

    if (ok)
    {
        QFile flIndexed(fPath);
        res = flIndexed.exists();
    }

    return res;
}

void GceLogicLayer::loadGraphicsDataFromImportedFiles(uint32_t srcID,
                                                      QStringList curveNames,
                                                      QStringList &curveNamesIgnored)
{
    if (curveNames.isEmpty())
    {
        return;
    }

    emit sigOpenStartProgressBarWindow(tr("Loading data ..."));

    auto dataSrc = getDataSource(srcID);
    if (dataSrc == nullptr)
    {
        emit sigStopCloseProgressBarWindow();
        curveNamesIgnored = curveNames;
        return;
    }
    auto ok = false;

    auto descriptionImported = indexerImports->getDIbyIndex(dataSrc->index_forImportedDataIndexer);
//    // точка с расширением обязаны быть вместе в описании формата:
//    QString exstension_indexed = QString(".").append(descriptionImported.originalExtension);
//    QString lower_exstension_indexed = exstension_indexed.toLower();
//    foreach (QStringList extLst, importModulesFI_byExtensionsList.keys())
//    {
//        foreach (QString ext_fmt, extLst)
//        {
//            QString lower_ext_fmt = ext_fmt.toLower();
//            if (lower_ext_fmt.contains(lower_exstension_indexed))
//            {
//                QString moduleAbsPath = importModulesFI_byExtensionsList.value(extLst).absoluteFilePath();
//                loader.setFileName(moduleAbsPath);
//                ok = true;
//                break;
//            }
//        }

//        if (ok) break;
//    }

    for (QFileInfo &fi_impMod : importModulesFI_byExtensionsList.values())
    {
        loader.setFileName(fi_impMod.absoluteFilePath());
        ImportModuleInterface *iFace__ = qobject_cast<ImportModuleInterface*>(loader.instance());
        QString formatNameString;
        iFace__->getFormatName(formatNameString);
        if (formatNameString == descriptionImported.dataType)
        {
            ok = true;
            break;
        }
        else
        {
            loader.unload();
        }
    }

    ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(loader.instance());

    if (!iFace)
    {
        emit sigStopCloseProgressBarWindow();
        curveNamesIgnored = curveNames;
        return;
    }
    QString fPath = indexerImports->prepareIndexedFileName(descriptionImported.magicWord_Indexer, ok);
    iFace->setDataFileIndexed(fPath);
    QStringList errorsIfExist;
    iFace->getErrors(errorsIfExist);
    if (!errorsIfExist.isEmpty())
    {
        module.showErrorsLoadingData(errorsIfExist.join("\n"));

        emit sigStopCloseProgressBarWindow();
        curveNamesIgnored = curveNames;
        return;
    }
    if (dataSrc->dataEmpty())
    {
        iFace->setMaximumPeriodOfNonmonotonicIntervalMSesc(timeLoopPeriodMilliseconds);
        std::unordered_map<uint32_t, TimeLogElement> timesDt;
        iFace->getTimesData(timesDt, dataSrc->getExcludedTimeData());
        dataSrc->setTimesData(timesDt);
        iFace->getLastOperationResult(ok);
        if (!ok)
        {
            QStringList errMsgs;
            iFace->getErrors(errMsgs);
            errMsgs.prepend("Error reading time data from indexed source!");
            module.showErrorsLoadingData(errMsgs.join("\n"));
            emit sigStopCloseProgressBarWindow();
            curveNamesIgnored = curveNames;
            return;
        }

//        dataSrc->makeBlocksInfo();
//        auto linkedBlocksExist = false;
//        auto blocksExist = false;
//        dataSrc->formallyJoinUnlinkedBlocksAfterLinkedBlocks(linkedBlocksExist, blocksExist);

//        if (!linkedBlocksExist && blocksExist)
//        {
//            QDateTime d_t_ = module.showDialogForDateTime(); // вызов диалога установки UTC-времени
//            dataSrc->formallyJoinUnlinkedBlocksToTimeAndDate(d_t_);
//        }
    }

    std::vector<LoggingCurve*> curves;
    QStringList supportedCurves, descriptionsCurves;
    iFace->getNamesOfCurves(supportedCurves, descriptionsCurves); // список поддерживаемых источником данных кривых
    for (QString &curveName : curveNames)
    {
        if (!supportedCurves.contains(curveName))
        {
            curveNamesIgnored.append(curveName);
        }
    }
    for (QString &ignoredCurveName : curveNamesIgnored)
    {
        curveNames.removeOne(ignoredCurveName);
    }

    if (curveNames.isEmpty())
    {
        for (auto it = dataSrc->getCurves().begin(); it != dataSrc->getCurves().end(); ++it)
        {
            curves.push_back(&it->second);
        }
    }
    else
    {
        for (QString &crvName : curveNames)
        {
            auto crvId = dataSrc->getCurveIdByCurveName(crvName);
            curves.push_back(&dataSrc->getCurves().at(crvId));
        }
    }
    iFace->getCurvesData(curves, curveNames);
    iFace->getLastOperationResult(ok);

    if (!ok)
    {
        QStringList errMsgs;
        iFace->getErrors(errMsgs);
        errMsgs.prepend("Error reading time data from indexed source!");
        module.showErrorsLoadingData(errMsgs.join("\n"));
    }

    emit sigStopCloseProgressBarWindow();
}

void GceLogicLayer::saveSourceData(uint32_t srcID, QString headerProjectFileName)
{
    QString projectDirName = headerProjectFileName.remove(".ceproj");
    QDir projectDir(projectDirName);
    if (!projectDir.exists())
    {
        QStringList tmp = projectDirName.split("/");
        QString fnDir = tmp.last();
        tmp.removeLast();
        projectDir.setPath(tmp.join("/"));
        projectDir.mkdir(fnDir);
        projectDir.cd(fnDir);
    }
    QDir srcDir(projectDir.path().append("/").append(QString::number(srcID)));
    if (!srcDir.exists())
    {
        projectDir.mkdir(QString::number(srcID));
    }

    auto source = getDataSource(srcID);
    QDataStream dstreamout;
    QFile infoDat(projectDir.path().append("/").append(QString::number(srcID)).append("/i.data"));
    auto opened = infoDat.open(QFile::WriteOnly);
    if (opened)
    {
        dstreamout.setDevice(&infoDat);
        dstreamout << static_cast<qint32>(source->getAbscissAxisType());
        QDate dv = source->getSourceDate();
        dstreamout << dv;
        QString v = source->getSourceFileName();
        dstreamout << v;
        v = source->getSourceFileType();
        dstreamout << v;
        v = source->getAdditionalInfo();
        dstreamout << v;
        v = source->getSourceFilePath();
        dstreamout << v;
        v = source->getDeviceName();
        dstreamout << v;
        uint32_t iv = source->getSourceID();
        dstreamout << iv;
        dstreamout << source->index_forImportedDataIndexer;
        dstreamout << source->magicWord_DS;

        VariableContainer vcontainer;

        // записать что-то в контейнер ...
//        int64_t timeShift = 0L;
//        source->getTimeShiftMilliseconds(timeShift);
//        vcontainer.set_value<int64_t>("time_shift_source", timeShift);

        dstreamout << vcontainer.read_container_hash();

        infoDat.close();

        source->saveDataSource(projectDir.path().append("/").append(QString::number(srcID)));
    }    
}

bool GceLogicLayer::openSourceData(uint32_t srcId, QString headerProjectFileName,
                                   QString &errorKind,
                                   int projectVersionMajor, int projectVersionMinor)
{
    (void)projectVersionMajor, (void)projectVersionMinor;
    QString projectDirName = headerProjectFileName.remove(".ceproj");
    QDir projectDir(projectDirName);
    if (!projectDir.exists())
    {
        errorKind += tr("Project data is damaged").append(": project internal directory not found\n");
        return false;
    }


    QFile infoDat(projectDir.path().append("/").append(QString::number(srcId)).append("/i.data"));
    auto opened = infoDat.open(QFile::ReadOnly);
    if (opened)
    {
        try
        {
            QDataStream dstreamin(&infoDat);
            QDate srcDate;
            QString srcFileName, srcFileType, srcAdditionalInfo, srcPath, srcDevName;
            uint32_t srcID4check;
            qint32 atIntValue;
            dstreamin >> atIntValue;
            dstreamin >> srcDate;
            dstreamin >> srcFileName;
            dstreamin >> srcFileType;
            dstreamin >> srcAdditionalInfo;
            dstreamin >> srcPath;
            dstreamin >> srcDevName;
            dstreamin >> srcID4check;

            if (srcDevName.isEmpty() || srcFileName.isEmpty()
                    || srcFileType.isEmpty() )
            {
                errorKind += tr("Project data is damaged").append(": file i.data is incorrect\n");
                return false;
            }

            AbscissParameter absciss_axis_type = static_cast<AbscissParameter>(atIntValue);
            auto dsNew = std::make_unique<GceDataSource>(srcId, absciss_axis_type);

            dstreamin >> dsNew->index_forImportedDataIndexer;
            dstreamin >> dsNew->magicWord_DS;

            VariableContainer vcontainer;

            auto data_ = vcontainer.read_container_hash();

            dstreamin >> data_;

            vcontainer.write_container_hash(data_);

            // прочитать из контейнера...

            infoDat.close();

            if (srcID4check != srcId)
            {
                errorKind += tr("Project data may be incorrect: source ID checking fall").append("\n");
            }

            dsNew->setSourceInfo(srcDate.toString("yyyy_MM_dd"), srcFileName, srcFileType, srcPath, srcAdditionalInfo);
            dsNew->setDeviceName(srcDevName);

            auto ok = dsNew->openDataSource(projectDir.path().append("/").append(QString::number(srcId)));
            if (!ok)
            {
                errorKind += tr("Project data is damaged").append(": special files are broken\n");
                return false;
            }

            sources.push_back(std::move(dsNew));
        }
        catch (...)
        {
            errorKind += tr("Project data is damaged").append(": file i.data is incorrect\n");
            return false;
        }


    }
    else
    {
        errorKind += tr("Project data is damaged").append(": file i.data not found\n");
        return false;
    }

    //newDS->makeBlocksInfo();
    //auto linkedBlocksExist = false;
    //newDS->formallyJoinUnlinkedBlocksAfterLinkedBlocks(linkedBlocksExist);

    //if (!linkedBlocksExist)
    //{
    //    QDateTime d_t_ = module.showDialogForDateTime(); // вызов диалога установки UTC-времени
    //    newDS->formallyJoinUnlinkedBlocksToTimeAndDate(d_t_);
    //}

    //sources.insert(dsNew->getSourceID(), dsNew);

    return true;
}

void GceLogicLayer::removeAllDataOnClose()
{
    sources.clear();
}

void GceLogicLayer::removeOneSourceData(uint32_t srcID)
{
    auto it_ds_for_remove = sources.begin();
    for (; it_ds_for_remove != sources.end(); ++it_ds_for_remove)
    {
        if (it_ds_for_remove->get()->getSourceID() == srcID) break;
    }
    if (it_ds_for_remove == sources.end())
    {
        return;
    }

    auto removedDataSource = it_ds_for_remove->get();

    auto ok = false;
    QString indexedDSfileName = indexerImports->prepareIndexedFileName(removedDataSource->magicWord_DS, ok);
    if (!ok)
    {
        return;
    }

    QFile flIndexed4del(indexedDSfileName);
    flIndexed4del.remove();
    sources.erase(it_ds_for_remove);
}

void GceLogicLayer::saveIndexedDataHeader(QString headerProjectFileName)
{
    QString projectDirName = headerProjectFileName.remove(".ceproj");
    QDir prjDir(projectDirName);
    if (!prjDir.exists())
    {
        prjDir.cdUp();
        prjDir.mkdir("project");
    }
    QDir indexDir(QString().append(projectDirName).append("/index"));
    if (!indexDir.exists())
    {
        indexDir.cdUp();
        indexDir.mkdir("index");
    }
    QString indexedDataHeader = QString().append(projectDirName).append("/index/_HEAD_");
    QFile fileIDH(indexedDataHeader);
    if (!fileIDH.open(QFile::WriteOnly))
    {
        return;
    }
    QDataStream dstreamout(&fileIDH);
    dstreamout << (quint64)sources.size(); // число источников данных и число индексированных файлов совпадают
    for (auto &src : sources)
    {
        dstreamout << src->magicWord_DS;
        auto descriptionIndexer = indexerImports->getDIbyMagicWord(src->magicWord_DS);
        dstreamout << descriptionIndexer.dataType;
        dstreamout << descriptionIndexer.importDateTime;
        dstreamout << descriptionIndexer.index;
        dstreamout << descriptionIndexer.magicWord_Indexer;
        dstreamout << descriptionIndexer.indexedFileName;
        dstreamout << descriptionIndexer.originalExtension;
        dstreamout << descriptionIndexer.originalFileName;
        dstreamout << descriptionIndexer.originalPath;

        VariableContainer vcontainer;

        // записать что-то в контейнер ...

        dstreamout << vcontainer.read_container_hash();
    }

    VariableContainer vcontainer;

    // записать что-то в контейнер ...

    dstreamout << vcontainer.read_container_hash();

    fileIDH.close();
}

bool GceLogicLayer::openIndexedDataHeader(QString headerProjectFileName, QString &errorKind)
{
    QString projectDirName = headerProjectFileName.remove(".ceproj");
    indexerImports->setDirCurrentProjectPath(projectDirName);

    QDir projectDir(projectDirName);
    if (!projectDir.exists())
    {
        errorKind += tr("Project data is damaged").append(": project internal directory not found\n");
        return false;
    }

    QString indexedDataHeader = QString().append(projectDirName).append("/index/_HEAD_");
    QFile fileIDH(indexedDataHeader);
    if (!fileIDH.exists())
    {
        errorKind += tr("Imported files metadata file not found: _HEAD_\n");
        return false;
    }
    if (!fileIDH.open(QFile::ReadOnly))
    {
        errorKind += tr("Imported files metadata file can't read: _HEAD_\n");
        return false;
    }

    try
    {
        QDataStream dstreamin(&fileIDH);
        quint64 cnt;
        dstreamin >> cnt;
        for (quint64 i = 0; i < cnt; ++i)
        {
            QString magicWord;
            dstreamin >> magicWord;
            ImportedDataIndexer::DescriptionImport di;
            dstreamin >> di.dataType;
            dstreamin >> di.importDateTime;
            dstreamin >> di.index;
            dstreamin >> di.magicWord_Indexer;
            dstreamin >> di.indexedFileName;
            dstreamin >> di.originalExtension;
            dstreamin >> di.originalFileName;
            dstreamin >> di.originalPath;

            QFile indexedDatFile(QString().append(projectDirName).append("/index/")
                                 .append(di.indexedFileName));

            if (!indexedDatFile.exists())
            {
                errorKind += tr("Imported file data file can't read: ")
                        .append(di.indexedFileName).append("\n");
            }

            VariableContainer vcontainer;

            auto data_ = vcontainer.read_container_hash();

            dstreamin >> data_;

            vcontainer.write_container_hash(data_);

            // прочитать из контейнера...

            indexerImports->addDescriptionImportOnProjectOpen(di);
        }

        VariableContainer vcontainer;

        auto data_ = vcontainer.read_container_hash();

        dstreamin >> data_;

        vcontainer.write_container_hash(data_);

        // прочитать из контейнера...

        fileIDH.close();
    }
    catch (...)
    {
        errorKind += tr("Imported files metadata file corrupt: _HEAD_\n");
        fileIDH.close();
        return false;
    }

    return true;
}

bool GceLogicLayer::scanImportModules(QString &errorKind)
{
    QPluginLoader loader;
    auto formatNames = QStringList();
    auto appDirPath = qApp->applicationDirPath();
    auto importModulesDirPath = QString().append(appDirPath).append("/import.modules");

    QDir importModulesDir(importModulesDirPath);
    if ( !importModulesDir.exists()
            || importModulesDir.entryInfoList(QDir::Files).size() == 0 )
    {
        errorKind = tr("There is not any import module\n");
        return false;
    }

    for (QFileInfo &fi : importModulesDir.entryInfoList(QDir::Files))
    {
        auto extension = fi.suffix().toUpper();
        if (extension == "DLL" || extension == "SO")
        {
            loader.setFileName(fi.absoluteFilePath());
            QObject *obj = loader.instance();
            if (obj == nullptr)
            {
                continue;
            }
            ImportModuleInterface *iFace = qobject_cast<ImportModuleInterface*>(obj);
            QString formatName;
            iFace->getFormatName(formatName);
            QStringList extensionsList;
            iFace->getFileExtensionsList(extensionsList);
            importModulesFI_byExtensionsList.insert(extensionsList, fi);
            formatName.append("|").append(extensionsList.join(";;"));
            formatNames.append(formatName);
            loader.unload();
        }
    }

    if (importModulesFI_byExtensionsList.isEmpty())
    {
        errorKind = tr("There is not any import module\n");
        return false;
    }

    return true;
}

HierarchyMember GceLogicLayer::getHierarchyRootElement(uint32_t srcID)
{
    return getDataSource(srcID)->getHierarchyRootElement();
}

QVector<HierarchyMember> GceLogicLayer::getHierarchySubElementsOf(uint32_t srcID, HierarchyMember elementHierarchy)
{
    return getDataSource(srcID)->getSubElementsOf(elementHierarchy);
}

const QDate GceLogicLayer::getSourceDate(uint32_t srcID)
{
    return getDataSource(srcID)->getSourceDate();
}

QString GceLogicLayer::getSourceFileName(uint32_t srcID)
{
    return getDataSource(srcID)->getSourceFileName();
}

QString GceLogicLayer::getSourceFileType(uint32_t srcID)
{
    return getDataSource(srcID)->getSourceFileType();
}

QString GceLogicLayer::getSourceFilePath(uint32_t srcID)
{
    return getDataSource(srcID)->getSourceFilePath();
}

QString GceLogicLayer::getAdditionalInfo(uint32_t srcID)
{
    return getDataSource(srcID)->getAdditionalInfo();
}

QDateTime GceLogicLayer::getSourceDateTime(uint32_t srcID)
{
    auto indexImported = getDataSource(srcID)->index_forImportedDataIndexer;
    auto dataIndex =  indexerImports->getDIbyIndex(indexImported);
    return dataIndex.importDateTime;
}

uint32_t GceLogicLayer::getSourceIDbyDevNameOfSource(QString devName, bool &success)
{
    uint32_t val = 0;
    success = false;
    for (decltype (sources.size()) i = 0; i < sources.size(); i++)
    {
        if (sources.at(i)->getDeviceName() == devName)
        {
            val = sources.at(i)->getSourceID();
            success = true;
            break;
        }
    }
    return val;
}

uint32_t GceLogicLayer::getCurveIDbySourceIDandCurveName(uint32_t sourceID, QString curveName, bool &success)
{
    uint32_t val = 0;
    success = false;
    for (decltype (sources.size()) i = 0; i < sources.size(); i++)
    {
        if (sources.at(i)->getSourceID() == sourceID)
        {
            val = sources.at(i)->getCurveIdByCurveName(curveName);
            if (val != -1) success = true;
            break;
        }
    }
    return val;
}

GceDataSource *GceLogicLayer::getDataSource(uint32_t srcID)
{
    for (decltype (sources.size()) i = 0; i < sources.size(); i++)
    {
        if (sources.at(i)->getSourceID() == srcID)
        {
            return sources[i].get();
        }
    }
    return nullptr;
}

QStringList GceLogicLayer::getCurvesNamesForDataSource(uint32_t srcID)
{
    auto ds = getDataSource(srcID);
    if (ds == nullptr) return QStringList();
    QStringList ans;
    for (auto &stdstr : ds->getNamesOfCurves_correct())
    {
        ans.push_back(QString::fromStdString(stdstr));
    }
    return ans;
}

int64_t GceLogicLayer::getTimeShiftOfDataSource(uint32_t srcID)
{
    int64_t v = 0LL;
    if (getDataSource(srcID) != nullptr)
    {
        getDataSource(srcID)->getTimeShiftMilliseconds(v);
    }
    return v;
}

void GceLogicLayer::setTimeShiftOfDataSource(uint32_t srcID, int64_t newTimeShiftMilliseconds)
{
    if (getDataSource(srcID) == nullptr)
    {
        return;
    }
    int64_t oldValueTimeShift;
    getDataSource(srcID)->getTimeShiftMilliseconds(oldValueTimeShift);
    if (oldValueTimeShift != newTimeShiftMilliseconds)
    {
        getDataSource(srcID)->setTimeShiftMilliseconds(newTimeShiftMilliseconds);
        emit sigDrawOrRedrawGraphic(srcID, -1);
    }
}

void GceLogicLayer::setExcludedTimesForNullValues(uint32_t srcID, uint32_t curveID,
                                                  bool setNulls, double valueForNull)
{
    if (getDataSource(srcID) == nullptr)
    {
        return;
    }
    getDataSource(srcID)->setExcludedTimesForNullValues(curveID, setNulls, valueForNull);
    emit sigDrawOrRedrawGraphic(srcID, curveID);
}

void GceLogicLayer::getSettingOfNullValue(uint32_t srcID, uint32_t curveID,
                                          bool &settedNulls, double &valueForNull)
{
    if (getDataSource(srcID) == nullptr)
    {
        return;
    }
    getDataSource(srcID)->getSettingOfNullValue(curveID, settedNulls, valueForNull);
}

const QString &GceLogicLayer::getDeviceName(uint32_t srcID)
{
    return getDataSource(srcID)->getDeviceName();
}

void GceLogicLayer::setDeviceName(uint32_t srcID, QString nm)
{
    getDataSource(srcID)->setDeviceName(nm);
}

ObjectsPack::ObjectsPack(QObject *parent)
    : QObject(parent)
{}
