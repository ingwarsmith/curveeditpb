#ifndef VARIABLECONTAINER_H
#define VARIABLECONTAINER_H

#include <QVariant>
#include <QHash>

class VariableContainer  //класс динамического массива на основе QVariant
{
public:

    template< typename T >
    void set_value( const QString& key, const T& value )
    {
        m_values[ key ] = QVariant::fromValue( value );
    }

    template< typename T >
    void unset_value( const QString& key )
    {
        m_values.remove( key );
    }

    template< typename T >
    T get_value( const QString& key, const T& getDefaultValue = T() )
    {
        if ( m_values.contains( key ) )
        {
            return unpack< T >( m_values[ key ], getDefaultValue);
        }
        return getDefaultValue;
    }

    template< typename T >
    void clear_values()
    {
        for (QString &key : m_values.keys())
        {
            m_values.remove(key);
        }
    }

    template< typename T >
    void copy_data_from( const VariableContainer& another )
    {
        m_values.clear();
        m_values = another.m_values;
    }

    QHash< QString, QVariant > read_container_hash()
    {
        return m_values;
    }

    void write_container_hash( const QHash< QString, QVariant > from_data )
    {
        m_values = from_data;
    }

    bool keyExist(QString key)
    {
        return m_values.keys().contains(key);
    }

private:
    QHash< QString, QVariant > m_values;

    template< typename T >
    T unpack( const QVariant& var, const T& defaultValue = T() )
    {
        if ( var.isValid() && var.canConvert< T >() )
        {
            return var.value< T >();
        }
        return defaultValue;
    }
};

#endif // VARIABLECONTAINER_H

//VariableContainer::VariableContainer(QObject *parent) : QObject(parent) {}
