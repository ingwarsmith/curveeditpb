#ifndef SPECIMPORTTYPES_H
#define SPECIMPORTTYPES_H

enum class ImportDataType
{
    Folder = 1,
    StandardCsv,
    Bin,
    Ftm,
    Gti,
    CustomText,
    UncompatibleWontImported
};

#endif // SPECIMPORTTYPES_H
