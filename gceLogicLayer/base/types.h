#ifndef TYPES_H
#define TYPES_H

#include "basedatatypes.h"

constexpr int32_t TS_NONE = -1; // это может означать только интерполяцию
constexpr int64_t UTC_NONE = -1; // аналогично
constexpr double V_NONE = -111777000000001.99932523;
constexpr uint32_t NULL_ID = -1; // пустой идентификатор для кривых и источников данных

#endif // TYPES_H
