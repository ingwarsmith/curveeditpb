#ifndef IMPORTMODULEINTERFACE_H
#define IMPORTMODULEINTERFACE_H

#define __BLI_PRESTRING                 "CurveEdit.import.module.interfaces.v1."

#define BLI_CONCAT_IID(interfaceName)  __BLI_PRESTRING

#include <QtPlugin>
#include <QStringList>

#include "types.h"

class ImportModuleInterface : public QObject
{
public:
    virtual ~ImportModuleInterface() {}
    virtual void getFormatName(QString &dataSourceFormatName) = 0;
    virtual void getFileExtensionsList(QStringList &dataSourceFileExtensions) = 0;
    virtual void getAbscissType(AbscissParameter &abscissType) = 0;
    // Ниже в dataSourceFilePath не должно быть проверки на правильность расширения файла (!!!),
    // т.к. в структуре каталогов проекта индексированный файл-источник данных переименовывается и получает
    // произвольное расширение.
    // Проверка расширения файла производится в клиентском коде по результатам функции getFileExtensionsList
    virtual void setDataFileIndexed(QString dataSourceFilePath) = 0;
    // Если проверка отключена, periodMSecs = 0:
    virtual void setMaximumPeriodOfNonmonotonicIntervalMSesc(int periodMSecs) = 0;
    // after data file set:
    virtual void getDeviceName(QString &dev_name) = 0;
    virtual void getParametersOfDevice(QString &data_dev) = 0;
    virtual void getNamesOfCurves(QStringList &crvNames, QStringList &crvDescriptions) = 0;
    virtual void getTimesData(std::unordered_map<uint32_t, TimeLogElement> &times, std::vector<uint32_t> &excludedTimeIds) = 0;
    virtual void getDepthsData(std::unordered_map<uint32_t, DepthLogElement> &depths) = 0;
    virtual void getParameterValues(std::vector<double> &param_values, QString &parameter_name) = 0;
    virtual void getCurvesData(std::vector<LoggingCurve*> &curves, QStringList selectedCurvesNames = QStringList()) = 0;
    virtual void getHierarchyInfo(QVector<HierarchyMember> &hierarchyMembers) = 0;
    // характеристика успешного / неуспешного выполнения операции; будет проверяться после вызовов
    // getNamesOfCurves, getTimesData, getCurvesData, getHierarchyInfo
    // если success = false, то getErrors возвращает ошибки для последнего вызова одной из 4х указаных функций
    virtual void getLastOperationResult(bool &success) = 0;
    virtual void getErrors(QStringList &errors) = 0;
};
#define importModuleInterface_iid BLI_CONCAT_IID(ImportModuleInterface)
Q_DECLARE_INTERFACE(ImportModuleInterface,importModuleInterface_iid)

#endif // IMPORTMODULEINTERFACE_H
