#ifndef BASEDATATYPES_H
#define BASEDATATYPES_H

#include <vector>
#include <unordered_map>
#include <string>
#include <list>
#include <memory>

//struct LoggingDataTimeElement
//{
//    uint32_t    id;             // это номер строки в массиве строк данных - для точной привязки!!!
//    int32_t     timestamp_raw;  // для привязки к елементу данных, = -1 для точек интерполяции
//    int64_t     UTC_millisecondsSinceEpoch;
//};

//struct TimeLogElement
//{
//    uint32_t    id;
//    int64_t     time_utc_milliseconds_since_epoch;
//};

//struct DepthLogElement
//{
//    uint32_t    id;
//    double      depth;
//};

using id_32bit = uint32_t;
using time_utc_milliseconds_since_epoch = int64_t;
using TimeLogElement = time_utc_milliseconds_since_epoch;
using DepthLogElement = double;

//using  TimeElementIterator = std::vector<LoggingDataTimeElement>::iterator;

enum class SourceFileKind // тип файла-источника данных
{
    Binary = 1,     // двоичный
    Text            // текстовый
};

enum class AbscissParameter // описатель параметра по оси абсцисс
{
    ExactThrougthTimeStamp = 1,      // через таймстампу (относительное время)
    AbsoluteTime_UTC = 2,           // абсолютное время (UTC)
    Depth = 3,                      // глубина, м
    ParameterValue = 4              // значение параметра
};

struct DependenciesCalc // зависимости при вычислении
{
    std::vector<uint32_t>           parents_curvesIDs; // родители (наборы данных, от которых зависит текущий набор данных)
    std::vector<uint32_t>           children_curvesIDs; // дети (наборы данных, зависимые от текущего)
};

struct LoggingDatasBlock
{
    uint32_t            blockID;
    uint32_t            sourceID;
    bool                isShifted; // производился сдвиг по оси времени
    std::vector<double> shiftings_time;
    std::vector<uint32_t> elements_ids_interpolated;
    size_t                first_element_position;
    size_t                last_element_position;
    bool                linkedByTime;
};

using BlockIterator = std::vector<LoggingDatasBlock>::iterator;

struct LoggingCurve
{
    uint32_t            curve_id;
    uint32_t            source_id;
    std::vector<uint32_t>  ids_x;
    std::vector<double> values;
    bool                loaded;
    bool                calculated_curve; // расчетная кривая
    std::string         name;
    std::string         description;
    AbscissParameter    x_values_type;
    DependenciesCalc    dependencies;
    bool                null_value_exist;
    double              null_value;
    std::vector<uint32_t> excluded_time_ids;      // исключенные времена кривой
};

struct HierarchyMember // включает только один элемент с именем Root (корень иерархии) и с memberId = 0, и любое число подчиненных элементов
{
    int32_t             member_id;
    std::string             name;
    std::list<std::string>  curves_names;
    int32_t             depend_on_id; // для элемента Root = -1, если не от кого не зависит (является корнем)
    bool                can_drag;
    bool                is_flag;
    std::string         structure_number;
    HierarchyMember() { can_drag = false; is_flag = false; structure_number = "-1"; }
};

struct SourceData
{
    uint32_t                        source_id;
    std::vector<HierarchyMember>        hierarchy_members;
    std::vector<LoggingDatasBlock>      blocks;
    std::unordered_map<uint32_t, LoggingCurve>    curves;   // набор кривых, ключ - curve_id
    std::vector<uint32_t>               excludedIdsAxisX;      // исключенные времена / глубины
};

struct SourceDataTimes : SourceData
{
    // набор данных по времени:
    std::unordered_map<uint32_t, TimeLogElement> times;
};

struct SourceDataDepths : SourceData
{
    // набор данных по глубинам:
    std::unordered_map<uint32_t, DepthLogElement> depths;
};

#endif // BASEDATATYPES_H
