#ifndef EQUATIONS_H
#define EQUATIONS_H

#include <QMap>
#include <QPair>

enum class ElementaryArithmeticOperations
{
    Sum = 0, // суммирование
    Diff,
    Mul,
    Div
};

//enum class FunctionElements
//{
//    Constanta = 0,
//    PowerFunction,
//    TrigonometricFunction,
//};

struct Variable
{
    QString title_name;
    double  value;
    unsigned char isConst : 1;
};

struct ExpressionElement
{
    Variable                            var;
    ElementaryArithmeticOperations      operationWithVar;
};

class FormulaBase
{
    //QMap< int, QPair<ElementaryArithmeticOperations, Variable> >
};

#endif // EQUATIONS_H
