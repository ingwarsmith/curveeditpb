#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T15:17:38
#
#-------------------------------------------------

QT       -= gui

CONFIG += c++17

TARGET = gceLogicLayer
TEMPLATE = lib

DEFINES += GCELOGICLAYER_LIBRARY

NAME = CurveEditor Logic Layer

QMAKE_TARGET_DESCRIPTION = CurveEditor Logic Layer

CONFIG += skip_target_version_ext

#VERSION = 0.5.2.0 #
VERSION = 1.1.3.10
DEFINES += VERSION=\\\"$$VERSION\\\"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        gcelogiclayer.cpp \
    gcedatasource.cpp \
    imports/importeddataindexer.cpp

HEADERS += \
        gcelogiclayer.h \
        gcelogiclayer_global.h \ 
    gcedatasource.h \
    base/basedatatypes.h \
    base/equations.h \
    imports/importeddataindexer.h \
    base/specimporttypes.h \
    base/importmoduleinterface.h \
    base/types.h \
    base/variablecontainer.h

#INCLUDEPATH += ../include

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

INCLUDEPATH += $$PWD/../gceImportModule
DEPENDPATH += $$PWD/../gceImportModule

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../gceImportModule/release/ -lgceImportModule
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../gceImportModule/debug/ -lgceImportModule

unix: LIBS += -L$$OUT_PWD/../gceImportModule -lgceImportModule

unix {
    TARGET_EXT = .so
#    CONFIG(release, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/release/libgceLogicLayer.so $$OUT_PWD/../sys/libgceLogicLayer.so
#    else: CONFIG(debug, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/debug/libgceLogicLayer.so $$OUT_PWD/../sys/libgceLogicLayer.so
    QMAKE_POST_LINK = \
        mkdir -p $$OUT_PWD/../sys && \
        cp $$OUT_PWD/libgceLogicLayer.so $$OUT_PWD/../sys/libgceLogicLayer.so.1.1.2 && \
        if ! [ -f $$OUT_PWD/../sys/libgceLogicLayer.so.1 ]; then \
        ln -s $$OUT_PWD/../sys/libgceLogicLayer.so.1.1.2 $$OUT_PWD/../sys/libgceLogicLayer.so.1.1; \
        ln -s $$OUT_PWD/../sys/libgceLogicLayer.so.1.1.2 $$OUT_PWD/../sys/libgceLogicLayer.so.1; fi
#        cp $$OUT_PWD/libgceLogicLayer.so $$OUT_PWD/../sys/libgceLogicLayer.so.1.0.0 && \
#        ln -s $$OUT_PWD/../sys/libgceLogicLayer.so.1.0.0 $$OUT_PWD/../sys/libgceLogicLayer.so.1
}
win32 {
    DLLDESTDIR = $$OUT_PWD/../sys
}
