#ifndef GCELOGICLAYER_H
#define GCELOGICLAYER_H

#include "gcelogiclayer_global.h"
#include <QObject>
#include <QVector>
#include <QMap>
#include "gcedatasource.h"
#include "../gceImportModule/gceimportmodule.h"
#include <QFileInfo>
#include <QPluginLoader>

namespace ObjectsNames {
const QString objNameProjectManager = "ProjectManager";
const QString objNameProjectsMap = "ProjectsMap";
const QString objNameMainWindow = "MainWindow";
const QString objNameGceDataView = "GceDataView";
const QString objNameAppManager = "ApplicationManager";
const QString objNameTreeElementAddedCurve = "TreeElementAddedCurve";
const QString objNameAddedCurve = "AddedCurve";
const QString objNameAddedCurves_ = "AddedCurves";
const QString objNameConfiguration = "Configuration";
const QString objNameConfigurationDefaultLanguage = "ConfigurationDefaultLanguage";
const QString objNameConfigurationDefaultDeleteNonmonotonicInterval = "ConfigurationDefaultDeleteNonmonotonicInterval";
const QString objNameConfigurationDefaultNonmonotonicIntervalMaximum = "ConfigurationDefaultNonmonotonicIntervalMaximum";
const QString objNameConfigurationDefaultRecentProjectsCount = "ConfigurationDefaultRecentProjectsCount";
const QString objNameConfigurationDefaultAutoLoadData = "ConfigurationDefaultAutoLoadData";
const QString objNameCurveIDs = "CurveIDs";
const QString objNameSeveralCurvesIDs4pack = "SeveralCurvesIDs";
const QString objNameTextEditForViewGraphicsValuesMode = "TextEditForViewGraphicsValuesMode";
const QString objNameLinesVectorPtrForViewGraphicsValuesMode = "LinesVectorPtrForViewGraphicsValuesMode";
const QString objNameDialogDimensionMode = "DimensionModeDialog";
const QString objNameDialogDimensionModeTextEdit = "DimensionModeDialogTE";
}

class GCELOGICLAYERSHARED_EXPORT ObjectsPack : public QObject
{
    Q_OBJECT

public:
    explicit ObjectsPack(QObject *parent = nullptr);
    QMap<int, QObject*> pack;
};

class ImportedDataIndexer;

class GCELOGICLAYERSHARED_EXPORT GceLogicLayer : public QObject
{
    Q_OBJECT

protected:
    GceLogicLayer();

public:
    ~GceLogicLayer();
    static GceLogicLayer& getInstance()
    {
        static GceLogicLayer instanse;
        return instanse;
    }
    // получить идентификаторы (уникальные номера) всех наборов данных
    QList<uint32_t>       getSourcesIds() const;
    // получить идентификаторы (уникальные номера) всех наборов данных, по которым загружены кривые
    QList<QPair<uint32_t,uint32_t>> getCurvesIdsWithSourcesIds() const;
    // получить строку-идентификатор набора данных (кривой) по его уникальному номеру
    QString               getCurveStringIdByCurveId(uint32_t srcId, uint32_t curveId, bool &success);
    QString               getCurveDescriptionStringByCurveId(uint32_t srcId, uint32_t curveId, bool &success);
    bool                  isLoggingCurveLoaded(uint32_t srcId, uint32_t curveId, bool &exists);
    std::pair<bool, double> nullValueExistAndValue(uint32_t srcId, uint32_t curveId, bool &existsCurve);
    QVector<double>       getValuesX(uint32_t srcId, uint32_t curveId, int64_t msecsProjectTimeShift, int64_t msecsDataSourceTimeShift, bool &success);
    QVector<double>       getValuesY(uint32_t srcId, uint32_t curveId, bool &success);
    std::vector<std::pair<double, double>> getNoDataBlocksAtrtsWithEndsForSource(uint32_t srcId);
    QVector<QObject*>*    getObjectsContainerPointer();
    void                  setObjectsContainerPointer(QVector<QObject*> *ocp);
    QObject               *getObjectFromObjectContainerByObjName(QString nm);
    bool                  existObjectInObjectContainerByObjName(QString nm);
    uint32_t              importByFileName(bool &importCompletedSuccessfull,
                                           QStringList &errorsIfExist, QString projectHeaderPath,
                                           QString fileNameForImport);
    uint32_t              callImportDialog(bool &importCompletedSuccessfull, QString projectHeaderPath);
    bool                  checkExistingImportedFileForDataSource(uint32_t srcID);
    void                  loadGraphicsDataFromImportedFiles(uint32_t srcID, QStringList curveNames, QStringList &curveNamesIgnored);
    void                  saveSourceData(uint32_t srcID, QString headerProjectFileName);
    bool                  openSourceData(uint32_t srcId, QString headerProjectFileName, QString &errorKind, int projectVersionMajor, int projectVersionMinor);
    void                  removeAllDataOnClose();
    void                  removeOneSourceData(uint32_t srcID);
    void                  saveIndexedDataHeader(QString headerProjectFileName);
    bool                  openIndexedDataHeader(QString headerProjectFileName, QString &errorKind);
    bool                  scanImportModules(QString &errorKind);
    HierarchyMember       getHierarchyRootElement(uint32_t srcID);
    QVector<HierarchyMember> getHierarchySubElementsOf(uint32_t srcID, HierarchyMember elementHierarchy);
    const QDate getSourceDate(uint32_t srcID);
    QString     getSourceFileName(uint32_t srcID);
    QString     getSourceFileType(uint32_t srcID);
    QString     getSourceFilePath(uint32_t srcID);
    QString     getAdditionalInfo(uint32_t srcID);
    QDateTime   getSourceDateTime(uint32_t srcID);
    uint32_t    getSourceIDbyDevNameOfSource(QString devName, bool &success);
    uint32_t    getCurveIDbySourceIDandCurveName(uint32_t sourceID, QString curveName, bool &success);

    GceDataSource* getDataSource(uint32_t srcID);
    QStringList           getCurvesNamesForDataSource(uint32_t srcID);
    int64_t               getTimeShiftOfDataSource(uint32_t srcID);
    void                  setTimeShiftOfDataSource(uint32_t srcID, int64_t newTimeShiftMilliseconds);

    void                  setExcludedTimesForNullValues(uint32_t srcID, uint32_t curveID, bool setNulls, double valueForNull);
    void                  getSettingOfNullValue(uint32_t srcID, uint32_t curveID, bool &settedNulls, double &valueForNull);

    auto        getTimeLoopPeriodMilliseconds() { return timeLoopPeriodMilliseconds; }
    void        setTimeLoopPeriodMilliseconds(int newValue)
    {
        timeLoopPeriodMilliseconds = newValue;
    }
    const QString & getDeviceName(uint32_t srcID);
    void setDeviceName(uint32_t srcID, QString nm);

private:
    std::vector<std::unique_ptr<GceDataSource>>   sources;
    QVector<QObject*>           *objContainerPtr;
    bool                        ocpInited;
    GceImportModule             module;
    ImportedDataIndexer         *indexerImports;
    QMap<QStringList, QFileInfo>    importModulesFI_byExtensionsList;
    int                             timeLoopPeriodMilliseconds;
    QPluginLoader loader;

signals:
    void                    sigOpenStartProgressBarWindow(QString title);
    void                    sigStopCloseProgressBarWindow();
    void                    sigErrorMsgs(QStringList msgs);
    void                    sigDrawOrRedrawGraphic(uint32_t srcId, uint32_t curveId);
};

#endif // GCELOGICLAYER_H
