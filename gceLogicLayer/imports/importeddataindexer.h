#ifndef IMPORTEDDATAINDEXER_H
#define IMPORTEDDATAINDEXER_H

#include <QObject>
#include <QMap>
#include <QDateTime>

class ImportedDataIndexer : public QObject
{
    Q_OBJECT
public:
    struct DescriptionImport
    {
        QString originalFileName;
        QString originalPath;
        QString originalExtension;
        uint32_t index;
        //bool    copiedToProjectData;
        QDateTime importDateTime;
        QString indexedFileName;
        QString dataType;
        QString magicWord_Indexer;
        QString TEMPORARY_indexFolderOfProject;
    };
    explicit ImportedDataIndexer(QObject *parent = 0);
    void                setDirCurrentProjectPath(QString projectDirPath);
    void                scanIndexes();
    DescriptionImport   createIndexFromImport(QString fullDataPath, QString magicWord, QString dataType_Format);
    QString             getFileNameInIndex(uint32_t index);
    DescriptionImport   getDIbyIndex(uint32_t index);

    DescriptionImport   getDIbyMagicWord(QString magicWord);

    QString             prepareIndexedFileName(QString magicWord, bool &success);

    //void                loadDataFileHeader(QString fnameIndexed, int32_t dataType);

    //void                loadStandardTextCsvFileHeader(QString fnameIndexed);

    void                addDescriptionImportOnProjectOpen(DescriptionImport diNew)
    {
        descriptionsByIndex.insert(diNew.index, diNew);
    }

    void                removeDIbyIndex(uint32_t keyIdx);

    uint32_t            nextIndex();

private:
    QString             projectDirectoryIndexPath;
    QMap<uint32_t, DescriptionImport> descriptionsByIndex;

signals:

public slots:
};

#endif // IMPORTEDDATAINDEXER_H
