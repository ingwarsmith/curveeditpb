#include "importeddataindexer.h"
//#include "../gceImportModule/importdialog.h"
#include "base/specimporttypes.h"
#include <QFile>
#include <QTextStream>
#include <QDir>

ImportedDataIndexer::ImportedDataIndexer(QObject *parent)
    : QObject(parent)//,
      //nextIndex(1)//,
      //projectDirectroryIndexPath(QString(projectDirPath).append("/index"))
{
    //
}

void ImportedDataIndexer::setDirCurrentProjectPath(QString projectDirPath)
{
    projectDirectoryIndexPath = QString(projectDirPath).append("/index");
}

//void ImportedDataIndexer::scanIndexes()
//{
//    QDir indexDir(projectDirectoryIndexPath);
//    foreach (QFileInfo fi, indexDir.entryInfoList())
//    {
//        if (fi.suffix().toUpper() == "DATA")
//        {
//            nextIndex++;
//        }
//    }
//}

ImportedDataIndexer::DescriptionImport ImportedDataIndexer::createIndexFromImport(QString fullDataPath,
                                                                                  QString magicWord,
                                                                                  QString dataType_Format)
{
    QFile fData(fullDataPath);
    QStringList tmp = fullDataPath.split("/");
    QString fileNameWithExt = tmp.takeLast();
    QString filePath = tmp.join("/");
    tmp = fileNameWithExt.split(".");
    QString fileExtension = tmp.takeLast();
    QString fileName;
    if (tmp.size()>1)
    {
        fileName = tmp.join(".");
    }
    else
    {
        fileName = tmp.at(0);
    }
    DescriptionImport di;
    di.dataType = dataType_Format;
    di.index = nextIndex();
    di.magicWord_Indexer = magicWord;
    di.originalPath = filePath;
    di.originalFileName = fileName;
    di.originalExtension = fileExtension;
    di.importDateTime = QDateTime::currentDateTime();
    QString indexedName = QString::number(di.index);
    int digitsCount = indexedName.size();
    int zeroesCount = 4 - digitsCount;
    QString zeroesStr;
    while (zeroesCount>0)
    {
        zeroesStr.append("0");
        zeroesCount--;
    }
    indexedName.prepend(zeroesStr);
    indexedName.prepend("idx");
    indexedName.append(".data");
    di.indexedFileName = indexedName;
    di.TEMPORARY_indexFolderOfProject = projectDirectoryIndexPath;
    QDir idxPath(projectDirectoryIndexPath);
    if (!idxPath.exists())
    {
        tmp = projectDirectoryIndexPath.split("/");
        tmp.removeLast();
        QDir work(tmp.join("/"));
        if (!work.exists())
        {
            QString base = tmp.takeLast();
            QDir bsDir(tmp.join("/"));
            bsDir.mkdir(base);
        }
        work.mkdir("index");
    }

    fData.copy(QString(projectDirectoryIndexPath).append("/").append(indexedName));

    descriptionsByIndex.insert(di.index, di);

    return di;
}

QString ImportedDataIndexer::getFileNameInIndex(uint32_t index)
{
    return descriptionsByIndex.value(index).indexedFileName;
}

ImportedDataIndexer::DescriptionImport ImportedDataIndexer::getDIbyIndex(uint32_t index)
{
    return descriptionsByIndex.value(index);
}

ImportedDataIndexer::DescriptionImport ImportedDataIndexer::getDIbyMagicWord(QString magicWord)
{
    DescriptionImport di;
    for (DescriptionImport &di_ : descriptionsByIndex.values())
    {
        if (di_.magicWord_Indexer == magicWord)
        {
            di = di_;
            break;
        }
    }
    return di;
}

QString ImportedDataIndexer::prepareIndexedFileName(QString magicWord, bool &success)
{
    DescriptionImport di;
    success = false;
    for (DescriptionImport &di_ : descriptionsByIndex.values())
    {
        if (di_.magicWord_Indexer == magicWord)
        {
            di = di_;
            success = true;
            break;
        }
    }
    if (!success)
    {
        return QString();
    }
    QString pathBegin;
    if (!di.TEMPORARY_indexFolderOfProject.isEmpty())
    {
        pathBegin = di.TEMPORARY_indexFolderOfProject;
    }
    else
    {
        pathBegin = projectDirectoryIndexPath;
        pathBegin.append("/");
    }
    QString ret = QString().append(pathBegin).append("/")
            .append(di.indexedFileName);
    ret.replace("//", "/");
    return ret;
}

void ImportedDataIndexer::removeDIbyIndex(uint32_t keyIdx)
{
    DescriptionImport di = descriptionsByIndex.value(keyIdx);
    QString pathBegin;
    if (!di.TEMPORARY_indexFolderOfProject.isEmpty())
    {
        pathBegin = di.TEMPORARY_indexFolderOfProject;
    }
    else
    {
        pathBegin = projectDirectoryIndexPath;
        pathBegin.append("/");
    }
    QString fn = QString().append(pathBegin).append("/")
            .append(di.indexedFileName);
    fn.replace("//", "/");

    QFile fl(fn);
    fl.remove();
    descriptionsByIndex.remove(keyIdx);
}

uint32_t ImportedDataIndexer::nextIndex()
{
    if (descriptionsByIndex.size() == 0)
    {
        return 1;
    }
    QList<uint32_t> indexes = descriptionsByIndex.keys();
    std::sort(indexes.begin(), indexes.end());
    return indexes.last() + 1;
}

//void ImportedDataIndexer::loadDataFileHeader(QString fnameIndexed, int32_t dataType)
//{
//    ImportDataType idt = static_cast<ImportDataType>(dataType);

//    switch (idt)
//    {
//    case ImportDataType::StandardCsv:
//    {
//        loadStandardTextCsvFileHeader(fnameIndexed);
//        break;
//    }
//    }
//}

//void ImportedDataIndexer::loadStandardTextCsvFileHeader(QString fnameIndexed)
//{
//    QFile fl_MAIN(fnameIndexed);
//    bool ok = fl_MAIN.open(QFile::ReadOnly|QFile::Text);
//    if (!ok)
//    {
//        //QMessageBox::critical(nullptr, tr("Error"), tr("Can't open file CSV "); //.append("\n").append(absPathToImport));
//        return;
//    }

//    QString dataHeader;

//    QTextStream stream(&fl_MAIN);
//    bool dataBegin = false;
//    while (!stream.atEnd())
//    {
//        QString line = stream.readLine();
//        if (dataBegin)
//        {
//            break;
//        }
//        else
//        {
//            if (line.split(";").at(0) == "ts")
//            {
//                dataHeader = line;
//                dataBegin = true;
//                dataHeaderElements = dataHeader.split(";");
//                dataHeaderElements.removeFirst();
//                if (dataHeaderElements.first() == "UTC")
//                {
//                    dataHeaderElements.removeFirst();
//                }
//            }
//        }
//    }

//    fl_MAIN.close();
//}


