#include "gcedatasource.h"
//#include "imports/importeddataindexer.h"
#include <memory>

#include <QDate>
#include <QFile>
#include <QDataStream>

#include "base/variablecontainer.h"

struct GceDataSource::Impl
{
    Impl(uint32_t source_id, AbscissParameter ranging_type)
    {
        switch (ranging_type) {
        case AbscissParameter::AbsoluteTime_UTC:
        case AbscissParameter::ExactThrougthTimeStamp:
        {
            srcDat = std::make_unique<SourceDataTimes>();
            break;
        }
        case AbscissParameter::Depth:
            srcDat = std::make_unique<SourceDataDepths>();
            break;
        default:
            return; // non-valid Impl
        }

        data_ranging_type = ranging_type;
        srcDat->source_id = source_id;

        interpolatedElementsExist = false;
        next_id_element = 1UL;
        blocksNotLinkedByTimeInRawDataExist = false;
        timeShiftMilliseconds = static_cast<int64_t>(0);
        millisecondsMaximumPeriodOfNonmonotonicInterval = 0;
        next_id_block = -1;
    }
    bool valid()
    {
        return srcDat != nullptr;
    }
    bool timeRangingData()
    {
        return data_ranging_type == AbscissParameter::AbsoluteTime_UTC ||
                data_ranging_type == AbscissParameter::ExactThrougthTimeStamp;
    }
    bool depthRangingData()
    {
        return data_ranging_type == AbscissParameter::Depth;
    }
    AbscissParameter data_ranging_type;
    std::unique_ptr<SourceData> srcDat;
    QString     dataSourceName_Device;
    QStringList namesCurves;
    QString     DateString_yyyy_mm_dd;
    QString     pathFileImported;
    QString     nameFileImported;
    QString     extensionFileImported;
    QString     information;
    uint32_t    next_id_element;
    uint32_t    next_id_block;
    bool        interpolatedElementsExist;
    bool        blocksNotLinkedByTimeInRawDataExist;

    // for time-ranged data:
    int64_t     timeShiftMilliseconds;
    int         millisecondsMaximumPeriodOfNonmonotonicInterval;
};

GceDataSource::GceDataSource(uint32_t srcId, AbscissParameter type_for_ranging_data)
    : impl(std::make_unique<Impl>(srcId, type_for_ranging_data))
{}

AbscissParameter GceDataSource::getAbscissAxisType() const
{
    return impl->data_ranging_type;
}

void GceDataSource::saveDataSource(QString dirProjectSaving)
{
    QDataStream dstreamout;

    if (impl->timeRangingData())
    {
        QFile timesDat(QString(dirProjectSaving).append("/t.data"));
        dstreamout.setDevice(&timesDat);
        auto opened = timesDat.open(QFile::WriteOnly);
        if (opened)
        {
            auto srcDataTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());

            std::size_t szTimes = srcDataTimes->times.size();
            dstreamout << static_cast<quint64>(szTimes);

            for (auto it = srcDataTimes->times.begin(); it != srcDataTimes->times.end(); ++it)
            {
                dstreamout << it->first;
                dstreamout << static_cast<qint64>(it->second);
            }

            dstreamout << static_cast<qint64>(impl->timeShiftMilliseconds);
            dstreamout << static_cast<quint64>(srcDataTimes->excludedIdsAxisX.size());
            for (auto it = srcDataTimes->excludedIdsAxisX.begin();
                 it != srcDataTimes->excludedIdsAxisX.end(); ++it)
            {
                dstreamout << *it;
            }

            //VariableContainer vcontainer;

            // записать что-то в контейнер ...
//            vcontainer.set_value<decltype(impl->timeShiftMilliseconds)>("time_shift_source", impl->timeShiftMilliseconds);
//            vcontainer.set_value<decltype(srcDataTimes->excludedIdsAxisX.size())>("excluded_times_sz",
//                                                                                  srcDataTimes->excludedIdsAxisX.size());

//            auto cnt = 0;
//            for (auto it = srcDataTimes->excludedIdsAxisX.begin(); it != srcDataTimes->excludedIdsAxisX.end(); ++it)
//            {
//                auto excludedTime = *it;
//                QString sign = QString::number(cnt).prepend("excluded_time_");
//                vcontainer.set_value<decltype (excludedTime)>(sign, excludedTime);
//                cnt++;
//            }

//            dstreamout << vcontainer.read_container_hash();

            timesDat.close();
        }
    }
    else if (impl->depthRangingData())
    {
        QFile depthsDat(QString(dirProjectSaving).append("/d.data"));
        dstreamout.setDevice(&depthsDat);
        auto opened = depthsDat.open(QFile::WriteOnly);
        if (opened)
        {
            auto srcDataDepths = static_cast<SourceDataDepths*>(impl->srcDat.get());

            std::size_t szDepths = srcDataDepths->depths.size();
            dstreamout << static_cast<quint64>(szDepths);

            for (auto it = srcDataDepths->depths.begin(); it != srcDataDepths->depths.end(); ++it)
            {
                dstreamout << it->first; //id
                dstreamout << it->second; //depth
            }

            dstreamout << static_cast<quint64>(srcDataDepths->excludedIdsAxisX.size());
            for (auto it = srcDataDepths->excludedIdsAxisX.begin();
                 it != srcDataDepths->excludedIdsAxisX.end(); ++it)
            {
                dstreamout << *it;
            }

//            VariableContainer vcontainer;

//            vcontainer.set_value<decltype (srcDataDepths->excludedIdsAxisX.size())>("excluded_depths_sz",
//                                                                                    srcDataDepths->excludedIdsAxisX.size());

//            auto cnt = 0;
//            for (auto it = srcDataDepths->excludedIdsAxisX.begin(); it != srcDataDepths->excludedIdsAxisX.end(); ++it)
//            {
//                auto excludedDepth = *it;
//                QString sign = QString::number(cnt).prepend("excluded_depth_");
//                vcontainer.set_value<decltype (excludedDepth)>(sign, excludedDepth);
//                cnt++;
//            }

//            dstreamout << vcontainer.read_container_hash();

            depthsDat.close();
        }
    }

    QFile curvesDat(QString(dirProjectSaving).append("/c.data"));
    dstreamout.setDevice(&curvesDat);
    auto opened = curvesDat.open(QFile::WriteOnly);
    if (opened)
    {
        int32_t keysCount = impl->srcDat->curves.size();
        dstreamout << keysCount;
        for (auto it = impl->srcDat->curves.begin(); it != impl->srcDat->curves.end(); ++it)
        {
            auto crv = it->second;
            dstreamout << crv.curve_id;
            dstreamout << static_cast<quint64>( crv.ids_x.size() );
            for (auto it_t = crv.ids_x.cbegin();
                 it_t != crv.ids_x.cend(); ++it_t)
            {
                dstreamout << *it_t;
            }

            dstreamout << static_cast<quint64>( crv.values.size() );
            for (auto it_v = crv.values.cbegin(); it_v != crv.values.cend(); ++it_v )
            {
                dstreamout << *it_v;
            }

            dstreamout << QString::fromStdString( crv.name );
            dstreamout << QVector<uint32_t>( crv.dependencies.children_curvesIDs.begin(),
                                             crv.dependencies.children_curvesIDs.end() );
            dstreamout << QVector<uint32_t>( crv.dependencies.parents_curvesIDs.begin(),
                                             crv.dependencies.parents_curvesIDs.end() );

            dstreamout << crv.calculated_curve;
            dstreamout << crv.source_id;
            dstreamout << static_cast<int32_t>(crv.x_values_type);
            dstreamout << crv.loaded;

            dstreamout << QString::fromStdString( crv.description );
            dstreamout << crv.null_value_exist;
            dstreamout << crv.null_value;

//            VariableContainer vcontainer;

//            // записать что-то в контейнер ...
//            QString cds = QString::fromStdString(crv.curveDescriptionString);
//            vcontainer.set_value<QString>("curve_description", cds);
//            vcontainer.set_value<bool>("curve_exist_nulls", crv.nullValueExist);
//            vcontainer.set_value<double>("curve_null_value", crv.nullValue_value);
//            //vcontainer.set_value< QVector<uint32_t> >("vector_excluded_times_curve", curve->excludedTimeIdsForCurve);
//            int32_t sz_excl_tm = crv.excludedTimeIdsForCurve.size();
//            vcontainer.set_value<int32_t>("vector_excluded_times_curve_size", sz_excl_tm);
//            QString base("vector_excluded_times_curve_element");
//            auto iet = 0;
//            for (auto it_ev = crv.excludedTimeIdsForCurve.cbegin();
//                 it_ev != crv.excludedTimeIdsForCurve.cend(); ++it_ev, ++iet)
//            {
//                QString nm_ = QString(base).append("_").append(QString::number(iet));
//                vcontainer.set_value<uint32_t>(nm_, *it_ev);
//            }

//            dstreamout << vcontainer.read_container_hash();
        }

//        VariableContainer vcontainer;
//        // записать что-то в контейнер ...
//        dstreamout << vcontainer.read_container_hash();

        curvesDat.close();
    }

    QFile blocksDat(QString(dirProjectSaving).append("/b.data"));
    dstreamout.setDevice(&blocksDat);
    opened = blocksDat.open(QFile::WriteOnly);
    if (opened)
    {
        uint32_t szBlocks = impl->srcDat->blocks.size();
        dstreamout << szBlocks;
        for (auto it_b = impl->srcDat->blocks.cbegin(); it_b != impl->srcDat->blocks.cend(); ++it_b) //for (int32_t iB = 0; iB < szBlocks; ++iB)
        {
            dstreamout << it_b->blockID;
            dstreamout << (quint64)it_b->first_element_position;
            dstreamout << (quint64)it_b->last_element_position;
            dstreamout << it_b->isShifted;
            dstreamout << it_b->linkedByTime;
            dstreamout << (quint64)it_b->shiftings_time.size();
            for (std::size_t i = 0; i < it_b->shiftings_time.size(); ++i)
            {
                dstreamout << it_b->shiftings_time[i];
            }
            dstreamout << it_b->sourceID;
            quint64 sz = it_b->elements_ids_interpolated.size();
            dstreamout << sz;
            for (quint64 t = 0; t < sz; ++t)
            {
                dstreamout << it_b->elements_ids_interpolated[t];
            }

//            VariableContainer vcontainer;
//            // записать что-то в контейнер ...
//            dstreamout << vcontainer.read_container_hash();
        }

//        VariableContainer vcontainer;
//        // записать что-то в контейнер ...
//        dstreamout << vcontainer.read_container_hash();

        blocksDat.close();
    }

    QFile hierarchyDat(QString(dirProjectSaving).append("/h.data"));
    dstreamout.setDevice(&hierarchyDat);
    opened = hierarchyDat.open(QFile::WriteOnly);
    if (opened)
    {
        uint32_t szHierarchy = impl->srcDat->hierarchy_members.size();
        dstreamout << szHierarchy;
        for (auto it_hm = impl->srcDat->hierarchy_members.cbegin();
             it_hm != impl->srcDat->hierarchy_members.cend(); ++it_hm) //for (int32_t iH = 0; iH < szHierarchy; ++iH)
        {
            dstreamout << it_hm->member_id;
            dstreamout << it_hm->depend_on_id;
            dstreamout << QString::fromStdString(it_hm->name);
            QStringList sl_out;
            for (auto it_s = it_hm->curves_names.begin(); it_s != it_hm->curves_names.end(); ++it_s)
            {
                auto qs = QString::fromStdString(*it_s);
                sl_out.push_back(qs);
            }
            dstreamout << sl_out;
            dstreamout << it_hm->is_flag;
            dstreamout << it_hm->can_drag;
            dstreamout << QString::fromStdString(it_hm->structure_number);

//            VariableContainer vcontainer;

//            // записать что-то в контейнер ...
//            vcontainer.set_value<decltype(it_hm->isFlag)>("isFlag", it_hm->isFlag);
//            vcontainer.set_value<decltype(it_hm->canDrag)>("canDrag", it_hm->canDrag);
//            vcontainer.set_value<QString>("structureNumber", QString::fromStdString(it_hm->structureNumber));

//            dstreamout << vcontainer.read_container_hash();
        }

//        VariableContainer vcontainer;
//        // записать что-то в контейнер ...
//        dstreamout << vcontainer.read_container_hash();

        hierarchyDat.close();
    }
}

bool GceDataSource::openDataSource(QString dirProjectSaved)
{
    QDataStream dstreamin;
    QFile timesDat(QString(dirProjectSaved).append("/t.data"));
    QFile depthsDat(QString(dirProjectSaved).append("/d.data"));
    if (depthsDat.exists())
    {
        impl->srcDat = std::make_unique<SourceDataDepths>();
        auto srcDataDepths = static_cast<SourceDataDepths*>(impl->srcDat.get());

        dstreamin.setDevice(&depthsDat);
        auto opened = depthsDat.open(QFile::ReadOnly);
        if (opened)
        {
            try
            {
                quint64 szDepths = 0;
                dstreamin >> szDepths;
                srcDataDepths->depths.reserve(szDepths);

                for (uint64_t itm = 0; itm < szDepths; ++itm)
                {
                    id_32bit id;
                    DepthLogElement depth;
                    dstreamin >> id;
                    dstreamin >> depth;
                    srcDataDepths->depths[id] = depth;
                }

                quint64 szExcluded = 0;
                dstreamin >> szExcluded;
                if (szExcluded)
                {
                    srcDataDepths->excludedIdsAxisX.resize(szExcluded);
                    for (uint64_t itm = 0; itm < szDepths; ++itm)
                    {
                        dstreamin >> srcDataDepths->excludedIdsAxisX[itm];
                    }
                }

                //VariableContainer vcontainer;
//                auto data_ = vcontainer.read_container_hash();
//                dstreamin >> data_;
//                vcontainer.write_container_hash(data_);

                depthsDat.close();
            }
            catch (...)
            {
                depthsDat.close();
                return false;
            }
            depthsDat.close();
        }
        else
        {
            return false;
        }
    }
    else if (timesDat.exists())
    {
        impl->srcDat = std::make_unique<SourceDataTimes>();
        auto srcDataTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());

        dstreamin.setDevice(&timesDat);
        auto opened = timesDat.open(QFile::ReadOnly);

        if (opened)
        {
            try
            {
                quint64 szTimes = 0;
                dstreamin >> szTimes;
                srcDataTimes->times.reserve(szTimes);
                for (quint64 itm = 0; itm < szTimes; ++itm)
                {
                    id_32bit id;
                    dstreamin >> id; //srcDataTimes->times[itm].id;
                    qint64 time_utc;
                    dstreamin >> time_utc;
                    srcDataTimes->times[id] = time_utc;
                }

                qint64 tshift;
                dstreamin >> tshift;
                impl->timeShiftMilliseconds = tshift;
                quint64 exclIdsSz;
                dstreamin >> exclIdsSz;
                srcDataTimes->excludedIdsAxisX.resize(exclIdsSz);
                for (std::size_t et = 0; et < exclIdsSz; ++et)
                {
                    dstreamin >> srcDataTimes->excludedIdsAxisX[et];
                }

//                VariableContainer vcontainer;
//                auto data_ = vcontainer.read_container_hash();
//                dstreamin >> data_;
//                vcontainer.write_container_hash(data_);

//                // прочитать из контейнера...
//                timeShiftMilliseconds = vcontainer.get_value<decltype(timeShiftMilliseconds)>("time_shift_source", 0LL);
//                auto sz_excludedTimes = vcontainer.get_value<decltype(srcDat.excludedTimeIds.size())>("excluded_times_sz");
//                for (decltype (sz_excludedTimes) t_e = 0; t_e < sz_excludedTimes; ++t_e)
//                {
//                    QString sign = QString::number(t_e).prepend("excluded_time_");
//                    auto excludedTime = vcontainer.get_value<uint32_t>(sign);
//                    srcDat.excludedTimeIds.push_back(excludedTime);
//                }

                timesDat.close();
            }
            catch (...)
            {
                timesDat.close();
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    QFile curvesDat(QString(dirProjectSaved).append("/c.data"));
    dstreamin.setDevice(&curvesDat);
    auto opened = curvesDat.open(QFile::ReadOnly);
    if (opened)
    {
        try
        {
            int32_t keysCount = 0;
            dstreamin >> keysCount;
            impl->srcDat->curves.reserve(keysCount);
            for (int32_t k = 0; k < keysCount; ++k)
            {
                LoggingCurve curve;
                dstreamin >> curve.curve_id;

                quint64 timesSz;
                dstreamin >> timesSz;
                curve.ids_x.resize(timesSz);
                for (std::size_t i = 0; i < timesSz; ++i)
                {
                    dstreamin >> curve.ids_x[i];
                }

                quint64 valuesSz;
                dstreamin >> valuesSz;
                curve.values.resize(valuesSz);
                for (std::size_t i = 0; i < valuesSz; ++i)
                {
                    dstreamin >> curve.values[i];
                }

                QString tmp_s;
                dstreamin >> tmp_s;
                curve.name = tmp_s.toStdString();
                QVector<uint32_t> vdc;
                dstreamin >> vdc;
                curve.dependencies.children_curvesIDs = std::vector<unsigned int>(vdc.begin(), vdc.end());
                vdc.clear();
                dstreamin >> vdc;
                curve.dependencies.parents_curvesIDs = std::vector<unsigned int>(vdc.begin(), vdc.end());

                dstreamin >> curve.calculated_curve;
                dstreamin >> curve.source_id;
                int32_t x_value_type;
                dstreamin >> x_value_type;
                curve.x_values_type = static_cast<AbscissParameter>(x_value_type);
                dstreamin >> curve.loaded;

                dstreamin >> tmp_s;
                curve.description = tmp_s.toStdString();
                dstreamin >> curve.null_value_exist;
                dstreamin >> curve.null_value;

//                VariableContainer vcontainer;

//                auto data_ = vcontainer.read_container_hash();
//                dstreamin >> data_;
//                vcontainer.write_container_hash(data_);
//                // прочитать из контейнера...
//                curve.curveDescriptionString = vcontainer.get_value<QString>("curve_description").toStdString();
//                curve.nullValueExist = vcontainer.get_value<bool>("curve_exist_nulls");
//                curve.nullValue_value = vcontainer.get_value<double>("curve_null_value");
//                //curve->excludedTimeIdsForCurve = vcontainer.get_value< QVector<uint32_t> >("vector_excluded_times_curve");
//                int32_t sz_excl_tm = vcontainer.get_value<int32_t>("vector_excluded_times_curve_size");
//                QString base("vector_excluded_times_curve_element");
//                for (int32_t iet = 0; iet < sz_excl_tm; ++iet)
//                {
//                    QString nm_ = QString(base).append("_").append(QString::number(iet));
//                    curve.excludedTimeIdsForCurve.push_back(vcontainer.get_value<uint32_t>(nm_));
//                }

                //impl->srcDat->curves[curve.curve_id] = curve; //std::move(curve); ////????
                auto ci = curve.curve_id;
                impl->srcDat->curves.insert(std::make_pair(ci, std::move(curve)));
            }

//            VariableContainer vcontainer;
//            auto data_ = vcontainer.read_container_hash();
//            dstreamin >> data_;
//            vcontainer.write_container_hash(data_);

            // прочитать из контейнера...

            curvesDat.close();
        }
        catch (...)
        {
            curvesDat.close();
            return false;
        }
    }
    else
    {
        return false;
    }

    QFile blocksDat(QString(dirProjectSaved).append("/b.data"));
    dstreamin.setDevice(&blocksDat);
    opened = blocksDat.open(QFile::ReadOnly);
    if (opened)
    {
        try
        {
            uint32_t szBlocks = 0;
            dstreamin >> szBlocks;
            impl->srcDat->blocks.resize(szBlocks);
            for (uint32_t iB = 0; iB < szBlocks; ++iB)
            {
                LoggingDatasBlock ldblock;
                dstreamin >> impl->srcDat->blocks[iB].blockID;
                quint64 posTimeFirst = 0, posTimeLast = 0;
                dstreamin >> posTimeFirst;
                dstreamin >> posTimeLast;
                impl->srcDat->blocks[iB].first_element_position = posTimeFirst;
                impl->srcDat->blocks[iB].last_element_position = posTimeLast;
                quint64 stsz;
                dstreamin >> stsz;
                impl->srcDat->blocks[iB].shiftings_time.resize(stsz);
                for (quint64 t = 0; t < stsz; ++t)
                {
                    dstreamin >> impl->srcDat->blocks[iB].shiftings_time[t];
                }

//                VariableContainer vcontainer;
//                auto data_ = vcontainer.read_container_hash();
//                dstreamin >> data_;
//                vcontainer.write_container_hash(data_);

                // прочитать из контейнера...
            }

//            VariableContainer vcontainer;
//            auto data_ = vcontainer.read_container_hash();
//            dstreamin >> data_;
//            vcontainer.write_container_hash(data_);

            // прочитать из контейнера...

            blocksDat.close();
        }
        catch (...)
        {
            blocksDat.close();
            return false;
        }
    }
    else
    {
        return false;
    }

    QFile hierarchyDat(QString(dirProjectSaved).append("/h.data"));
    dstreamin.setDevice(&hierarchyDat);
    opened = hierarchyDat.open(QFile::ReadOnly);
    if (opened)
    {
        try
        {
            uint32_t szHierarchy = 0;
            dstreamin >> szHierarchy;
            impl->srcDat->hierarchy_members.resize(szHierarchy);
            for (uint32_t iH = 0; iH < szHierarchy; ++iH)
            {
                dstreamin >> impl->srcDat->hierarchy_members[iH].member_id;
                dstreamin >> impl->srcDat->hierarchy_members[iH].depend_on_id;
                QString nmt;
                dstreamin >> nmt;
                impl->srcDat->hierarchy_members[iH].name = nmt.toStdString();
                QStringList ql_in;
                dstreamin >> ql_in;
                for (auto& qs : ql_in)
                {
                    impl->srcDat->hierarchy_members[iH].curves_names.push_back(qs.toStdString());
                }
                dstreamin >> impl->srcDat->hierarchy_members[iH].is_flag;
                dstreamin >> impl->srcDat->hierarchy_members[iH].can_drag;
                QString sn;
                dstreamin >> sn;
                impl->srcDat->hierarchy_members[iH].structure_number = sn.toStdString();

//                VariableContainer vcontainer;
//                auto data_ = vcontainer.read_container_hash();
//                dstreamin >> data_;
//                vcontainer.write_container_hash(data_);

//                // прочитать из контейнера...
//                dtElem.isFlag = vcontainer.get_value<decltype(dtElem.isFlag)>("isFlag", false);
//                dtElem.canDrag = vcontainer.get_value<decltype(dtElem.canDrag)>("canDrag", false);
//                dtElem.structureNumber = vcontainer.get_value<QString>("structureNumber", "").toStdString();
            }

//            VariableContainer vcontainer;
//            auto data_ = vcontainer.read_container_hash();
//            dstreamin >> data_;
//            vcontainer.write_container_hash(data_);

            // прочитать из контейнера...

            hierarchyDat.close();
        }
        catch (...)
        {
            hierarchyDat.close();
            return false;
        }
    }
    else
    {
        return false;
    }
    return true;
}

void GceDataSource::setTimeLoopMaxPeriodMSecs(int timeLoopMSecs)
{
    if (impl->data_ranging_type == AbscissParameter::AbsoluteTime_UTC
            || impl->data_ranging_type == AbscissParameter::ExactThrougthTimeStamp)
    {
        impl->millisecondsMaximumPeriodOfNonmonotonicInterval = timeLoopMSecs;
    }
}

//void GceDataSource::makeBlocksInfo()
//{
//    impl->srcDat->blocks.clear();
//    auto srcDatTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());
//    if (srcDatTimes->times.empty())
//    {
//        return;
//    }
//    auto nextFirstTimeElement = srcDatTimes->times.begin();
//    auto it = nextFirstTimeElement;
//    ++it;

//    // OpenMP?
//    for (; it != srcDatTimes->times.end(); ++it)  //for (int t = 1; t < srcDat.times.size(); ++t)
//    {
//        if (it->timestamp_raw == TS_NONE) // интерполяция
//        {
//            continue;
//        }
//        LoggingDatasBlock block;
//        auto endBlock = false, endData = false;
//        if (it->timestamp_raw == TS_NONE)
//        {
//            auto it_backward = it;
//            it_backward--;
//            while (it_backward >= srcDat.times.begin())
//            {
//                if (it_backward->timestamp_raw != TS_NONE)
//                {
//                    if (it->timestamp_raw < it_backward->timestamp_raw)
//                    {
//                        endBlock = true;
//                    }
//                    break;
//                }
//                it_backward--;
//            }
//        }
//        else
//        {
//            auto current = it;
//            auto previos = current;
//            previos--;
//            if (current->timestamp_raw < previos->timestamp_raw)
//            {
//                endBlock = true;
//            }
//            //if ((t+1) == srcDat->times.size())
//            auto next = it;
//            next++;
//            if (next == srcDat.times.end())
//            {
//                endData = true;
//            }
//        }
//        if (endBlock)
//        {
//            auto previos = it;
//            previos--;
//            //&srcDat->times[t-1];
//            block.firstTimeElement = nextFirstTimeElement;
//            block.lastTimeElement = previos;
//            nextFirstTimeElement = it; //&srcDat->times[t];//lastTimeElement+1;
//            block.blockID = srcDat.blocks.size() + 1;
//            block.sourceID = srcDat.sourceID;
//            block.isShifted = false;
//            block.linkedByTime = previos->UTC_millisecondsSinceEpoch != UTC_NONE; //!qFuzzyCompare(srcDat->times.at(t-1).UTC_orExactTimeAny, UTC_NONE);
//            srcDat.blocks.push_back(block);
//        }
//        if (endData)
//        {
//            block.firstTimeElement = nextFirstTimeElement;
//            block.lastTimeElement = it;
//            block.blockID = srcDat.blocks.size() + 1;
//            block.sourceID = srcDat.sourceID;
//            block.isShifted = 0;
//            block.linkedByTime = it->UTC_millisecondsSinceEpoch != UTC_NONE; //!qFuzzyCompare(srcDat->times.last().UTC_orExactTimeAny, UTC_NONE);
//            srcDat.blocks.push_back(block);
//            next_id_block = block.blockID + 1;
//        }
//    }
//}

//void GceDataSource::formallyJoinUnlinkedBlocksAfterLinkedBlocks(bool &linkedBlocksExist, bool &blocksExist)
//{
//    linkedBlocksExist = false;

//    if (srcDat.blocks.size() == 0)
//    {
//        return;
//    }

//    blocksExist = true;

//    if (srcDat.blocks.size() == 1 && srcDat.blocks.front().linkedByTime)  //.first().linkedByTime)
//    {
//        linkedBlocksExist = true; // достаточно
//        return;
//    }

//    // какой по счету блок - первый с привязкой?
//    auto it_FirstEncounteredLinkedBlock = srcDat.blocks.begin();
//    int32_t idx_FELB = 0L; // index First Encountered Linked Block
//    for (; it_FirstEncounteredLinkedBlock != srcDat.blocks.end(); ++it_FirstEncounteredLinkedBlock)
//    {
//        if (it_FirstEncounteredLinkedBlock->linkedByTime)
//        {
//            linkedBlocksExist = true;
//            break;
//        }
//        idx_FELB++;
//    }

//    if (!linkedBlocksExist)
//    {
//        return;
//    }

//    auto FELB_atBegin = false, FELB_atEnd = false, FELB_inside = false;

//    if (it_FirstEncounteredLinkedBlock == srcDat.blocks.begin())
//    {
//        FELB_atBegin = true;
//    }
//    else
//    {
//        auto itBlockEnd = srcDat.blocks.end(); // т.к. итератор end() в STL указывает на воображаемый элемент, следующий за последним в контейнере
//        itBlockEnd--;
//        if (it_FirstEncounteredLinkedBlock == itBlockEnd) // признак принадлежности последнему блоку первых данных с привязкой по времени
//        {
//            FELB_atEnd = true;
//        }
//        else
//        {
//            // первый привязанный по времени блок - внутри последовательности данных,
//            // и возможно, далее имеются тоже блоки с привязкой

//            FELB_inside = true;
//        }
//    }

//    // просчитать вперёд: от начала или из "середины"
//    if (FELB_atBegin || FELB_inside)
//    {
//        //auto ib_tmp_recovery = idx_FELB + 1; // второй и далее ...
//        auto it = it_FirstEncounteredLinkedBlock;
//        for (; it != srcDat.blocks.end(); it++)
//        {
//            if (it->linkedByTime)
//            {
//                //ib_tmp_recovery++;
//                continue;
//            }
//            auto itPrevios = it;
//            itPrevios--;
//            auto timeElement_first = it->firstTimeElement;
//            auto timeElement_last = it->lastTimeElement;
//            auto timeElement_last_LinkedBlock = itPrevios->lastTimeElement;
//            auto diff_ts = timeElement_first->timestamp_raw; // условно первая таймстампа (её длительность) после включения
//                                                             // питания станет разницей между временными границами двух блоков

//            auto utc_last_linked_block = timeElement_last_LinkedBlock->UTC_millisecondsSinceEpoch; //auto utc_last_linked_block = timeElement_last_LinkedBlock->UTC_orExactTimeAny;
//            timeElement_first->UTC_millisecondsSinceEpoch = utc_last_linked_block + static_cast<int64_t>(diff_ts);
//            for (auto it_time = timeElement_first; it_time != timeElement_last; ++it_time)
//            {
//                auto it_nextTime = it_time;
//                it_nextTime++;
//                diff_ts = it_nextTime->timestamp_raw - it_time->timestamp_raw;
//                it_nextTime->UTC_millisecondsSinceEpoch = it_time->UTC_millisecondsSinceEpoch + static_cast<int64_t>(diff_ts);
//            }

//            it->linkedByTime = true;
//        }
//    }

//    // просчитать назад: от конца или из "середины"
//    if (FELB_atEnd || FELB_inside)
//    {
//        auto it = it_FirstEncounteredLinkedBlock;
//        while (it != srcDat.blocks.begin())
//        {
//            auto previosBlock_Linked = it;
//            previosBlock_Linked--;
//            auto currentBlock = it;

//            if (currentBlock->linkedByTime)
//            {
//                continue;
//            }

//            auto t_firstOfNextBlock = previosBlock_Linked->firstTimeElement;

//            // первый отсчет ts от начала текущего блока - условная разница с концом предыдущего блока:
//            auto delta0_UTC = static_cast<int64_t>(t_firstOfNextBlock->timestamp_raw);

//            auto t_current_first = currentBlock->firstTimeElement;
//            auto t_current_last = currentBlock->lastTimeElement;

//            t_current_last->UTC_millisecondsSinceEpoch = t_firstOfNextBlock->UTC_millisecondsSinceEpoch - delta0_UTC;

//            for (auto it_time = t_current_last;
//                 it_time != t_current_first; it_time--)
//            {
//                auto it_previosTime = it_time;
//                it_previosTime--;
//                auto diff_ts = static_cast<int64_t>(it_time->timestamp_raw - it_previosTime->timestamp_raw);
//                it_previosTime->UTC_millisecondsSinceEpoch = it_time->UTC_millisecondsSinceEpoch - diff_ts;
//            }

//            currentBlock->linkedByTime = true;
//        }
//    }

//    return;
//}

//void GceDataSource::formallyJoinUnlinkedBlocksToTimeAndDate(QDateTime dt)
//{
//    auto beginTimeUtcDbl = dt.toMSecsSinceEpoch();//static_cast<double>(dt.toSecsSinceEpoch());
//    //srcDat->times.first().UTC_orExactTimeAny = beginTimeUtcDbl;
//    srcDat.times.front().UTC_millisecondsSinceEpoch = beginTimeUtcDbl;

//    int64_t diff_ts;
//    for (auto it = srcDat.times.begin();
//         (it+1) != srcDat.times.end(); it++)
//    {
//        auto nextTime = it+1;
//        if (nextTime->timestamp_raw >= it->timestamp_raw)
//        {
//            diff_ts = static_cast<int64_t>(nextTime->timestamp_raw - it->timestamp_raw);
//        }
//        else
//        {
//            diff_ts = static_cast<int64_t>(nextTime->timestamp_raw);
//        }
//        nextTime->UTC_millisecondsSinceEpoch = diff_ts + it->UTC_millisecondsSinceEpoch;
//    }

//    for (auto itBlock = srcDat.blocks.begin();
//         itBlock != srcDat.blocks.end(); itBlock++)
//    {
//        itBlock->linkedByTime = true;
//    }
//}

//void GceDataSource::updateLogDatas()
//{
////    foreach (LoggingCurve *curveData, srcDat->curves)
////    {
////        QVector<ulong*> timesX;
////        int nextIndex = 0;
////        QVector<LoggingDataTimeElement>::Iterator it_t = srcDat->times.begin();
////        foreach (ElementaryCurveData *e, curveData->values)
////        {
////            timesX.push_back(e->id_element);
////            //ulong timeId = e->id_element;
////        }
////        int step = 0;
////        for (; it_t != srcDat->times.end(); it_t++)
////        {
////            int currIndex = timesX.indexOf(it_t->id_element, nextIndex);
////            if (currIndex < 0)
////            {
////                continue;
////            }
////            nextIndex = currIndex;
////            curveData->values[step]->timeOrAnotherX = it_t->UTC_orExactTimeAny;
////        }
////    }
////}

//void GceDataSource::addBlock(QVector<LoggingDataTimeElement*> timeElements,
//                             QVector<LoggingDataTimeElement*> timeElementsInterpolation)
//{
//    LoggingDatasBlock newBlock;
//    newBlock.isShifted = 0;
//    newBlock.blockID = next_id_block;
//    next_id_block++;
//    newBlock.sourceID = srcDat.sourceID;
//    //newBlock.timeElementsIds = ids_timeElements;
//    newBlock.firstTimeElement = timeElements.first();
//    newBlock.lastTimeElement = timeElements.last();
//    newBlock.t_elements_interpolated_values = timeElementsInterpolation;
//    for (auto it = srcDat->times.begin(); it != srcDat->times.end(); it++)
//    {
//        if (timeElements.contains(it))
//        {
//            newBlock.linkedByTime = it->UTC_millisecondsSinceEpoch == UTC_NONE; //qFuzzyCompare(it->UTC_orExactTimeAny, UTC_NONE);
//            break;
//        }
//    }
//    srcDat->blocks << newBlock;
//}

//bool GceDataSource::getBlock(uint32_t idBlock, BlockIterator outBlock)
//{
//    for (auto it = srcDat.blocks.begin(); it != srcDat.blocks.end(); it++)
//    {
//        if (it->blockID == idBlock)
//        {
//            outBlock = it;
//            return true;
//        }
//    }
//    return false;
//}

//QVector<LoggingDatasBlock*> GceDataSource::getAllBlocks()
//{
//    QVector<LoggingDatasBlock*> retValue;
//    for (auto it = srcDat->blocks.begin(); it != srcDat->blocks.end(); it++)
//    {
//        retValue << it;
//    }
//    return retValue;
//}

std::vector<std::pair<double,double>> GceDataSource::getAllBlocksStartsWithEnds() const
{
    std::vector<std::pair<double,double>> retV2;
    auto srcDatTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());
    if (srcDatTimes && !srcDatTimes->blocks.empty())
    {
        for (auto it = impl->srcDat->blocks.begin(); it != impl->srcDat->blocks.end(); it++)
        {
            auto start = 0.001 *
                    static_cast<double>(srcDatTimes->times[it->first_element_position]),
                    end = 0.001 *
                    static_cast<double>(srcDatTimes->times[it->last_element_position]);
            retV2.push_back(std::pair<double,double>(start, end));
        }
    }

    return retV2;
}

//LoggingDataTimeElement GceDataSource::getTimeElementById(ulong elementID)
//{
//    for (QVector<LoggingDataTimeElement>::iterator it = srcDat->times.begin();
//         it != srcDat->times.end(); ++it)
//    {
//        if (it->id_element == elementID)
//        {
//            return *it;
//            break;
//        }
//    }
//    return LoggingDataTimeElement();
//}

//int GceDataSource::blocksCount()
//{
//    return impl->srcDat->blocks.size();
//}

//void GceDataSource::setBlockAsLinkedByTime(uint32_t blockID, bool isLinked)
//{
//    auto it = srcDat.blocks.begin();
//    for (; it != srcDat.blocks.end(); ++it)
//    {
//        if (it->blockID == blockID)
//        {
//            it->linkedByTime = isLinked;
//            break;
//        }
//    }
//}

uint32_t GceDataSource::addLoggingDataRaw(AbscissParameter val_type, QString curve_name, QString description)
{
    LoggingCurve curve;
    curve.loaded = false;
    curve.curve_id = impl->srcDat->curves.size() + 1;
    curve.name = curve_name.toStdString();
    curve.description = description.toStdString();
    curve.source_id = impl->srcDat->source_id;
    curve.x_values_type = val_type;
    curve.null_value_exist = false;
    curve.null_value = 0.0;
    auto cid = curve.curve_id;
    impl->srcDat->curves.insert(std::make_pair(cid, std::move(curve)));
    return cid;
}

void GceDataSource::addHierarchyMember(HierarchyMember mmb)
{
    auto alreadyExist = false;
    for (size_t ih = 0; ih < impl->srcDat->hierarchy_members.size(); ++ih)
    {
        if (mmb.member_id == impl->srcDat->hierarchy_members.at(ih).member_id)
        {
            alreadyExist = true;
            break;
        }
    }
    if (!alreadyExist)
    {
        impl->srcDat->hierarchy_members.push_back(mmb);
    }
}

HierarchyMember GceDataSource::getHierarchyRootElement() const
{
    HierarchyMember root;
    for (size_t i = 0; i < impl->srcDat->hierarchy_members.size(); ++i)
    {
        if (impl->srcDat->hierarchy_members.at(i).depend_on_id == -1)
        {
            root = impl->srcDat->hierarchy_members.at(i); // take a root element
            break;
        }
    }
    return root;
}

QVector<HierarchyMember> GceDataSource::getSubElementsOf(HierarchyMember elementHierarchy) const
{
    QVector<HierarchyMember> ret;
    for (size_t i = 0; i < impl->srcDat->hierarchy_members.size(); ++i)
    {
        if (impl->srcDat->hierarchy_members.at(i).depend_on_id == elementHierarchy.member_id)
        {
            ret << impl->srcDat->hierarchy_members.at(i);
        }
    }
    return ret;
}

void GceDataSource::getCurvesIDs(QList<uint32_t> &ids)
{
    ids.clear();
    for (auto &n : impl->srcDat->curves)
    {
        ids.append(n.first);
    }
}

uint32_t GceDataSource::getSourceID()
{
    return impl->srcDat->source_id;
}

QVector<double> GceDataSource::getTimesAsXFromCurve(uint32_t curveID, double regularTimeShift)
{
    QVector<double> x_values;
    auto srcDatTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());
    if (srcDatTimes != nullptr)
    {
        auto &loggingCurve = srcDatTimes->curves.at(curveID);
        if (srcDatTimes->excludedIdsAxisX.empty()) // no excluded times
        {
            for (size_t idx_te = 0; idx_te < loggingCurve.ids_x.size(); ++idx_te)
            {
                auto it_found = srcDatTimes->times.find(loggingCurve.ids_x[idx_te]);
                if (it_found == srcDatTimes->times.end())
                {
                    continue;
                }
                auto timeValue = .001 *
                        static_cast<double>(srcDatTimes->times[loggingCurve.ids_x[idx_te]])
                        + regularTimeShift;
                x_values.push_back(timeValue);
            }
        }
        else
        {
            for (size_t idx_te = 0; idx_te < loggingCurve.ids_x.size(); ++idx_te)
            {
                if (std::find(srcDatTimes->excludedIdsAxisX.begin(), srcDatTimes->excludedIdsAxisX.end(),
                             loggingCurve.ids_x[idx_te]) == srcDatTimes->excludedIdsAxisX.end())
                {
                    auto timeValue = .001 *
                            static_cast<double>(srcDatTimes->times[loggingCurve.ids_x[idx_te]])
                            + regularTimeShift;
                    x_values.push_back(timeValue);
                }
            }
        }

//        auto lastTimeElement = srcDatTimes->times.begin();
//        for (size_t i = 0; i < srcDatTimes->excludedIdsAxisX.size(); ++i)
//        {
//            auto excludedId = srcDatTimes->excludedIdsAxisX.at(i);
//            auto it_x = std::find_if(loggingCurve.ids_x.cbegin(), loggingCurve.ids_x.cend(),
//                                     [excludedId](const uint32_t &v) { return v == excludedId; });
//            if (it_x != loggingCurve.ids_x.cend())
//            {
//                loggingCurve.ids_x.erase(it_x);
//            }
//        }
//        auto idLastCurve = loggingCurve.ids_x.begin();
//        for (; idLastCurve != loggingCurve.ids_x.end(); idLastCurve++)
//        {
//            while (*idLastCurve != lastTimeElement->id)
//            {
//                lastTimeElement++;
//                if (lastTimeElement == srcDatTimes->times.end())
//                {
//                    break;
//                }
//            }

//            if (loggingCurve.excluded_time_ids.size())
//            {
//                auto it_x_2 = std::find_if(loggingCurve.excluded_time_ids.cbegin(),
//                                           loggingCurve.excluded_time_ids.cend(),
//                                           [lastTimeElement](const uint32_t &v)
//                { return v == lastTimeElement->id; });
//                if (it_x_2 != loggingCurve.excluded_time_ids.cend())   continue;
//            }

//            if (lastTimeElement->id == *idLastCurve)
//            {
//                auto timeValue = 0.001 *
//                        static_cast<double>(lastTimeElement->time_utc_milliseconds_since_epoch) + regularTimeShift;
//                x_values.push_back(timeValue);
//            }
//        }

    }
    return x_values;
}

void GceDataSource::shiftAllDataSourceBlocks(double xOffset)
{
    auto srcDatTimes = static_cast<SourceDataTimes*>(impl->srcDat.get());
    auto srcDatDepths = static_cast<SourceDataDepths*>(impl->srcDat.get());
    if (srcDatTimes != nullptr)
    {
        for (auto it = srcDatTimes->times.begin();
             it != srcDatTimes->times.end(); it++)
        {
            auto offst = static_cast<int64_t>(qRound64(xOffset*1000.0));
            it->second += offst;
        }
    }
    if (srcDatDepths != nullptr)
    {
        for (auto it = srcDatDepths->depths.begin();
             it != srcDatDepths->depths.end(); it++)
        {
            it->second += xOffset;
        }
    }
    for (auto it_b = impl->srcDat->blocks.begin();
         it_b != impl->srcDat->blocks.end(); it_b++)
    {
        if (it_b->isShifted == false)
        {
            it_b->isShifted = true;
        }
        it_b->shiftings_time.push_back(xOffset);
    }
}

//void GceDataSource::shiftSomeUnlinkedBlock(uint32_t blockID, double xOffset, bool &success)
//{
//    success = false;
//    //QVector<LoggingDataTimeElement *> blocktimesIds4shift;
//    std::vector<TimeElementIterator> blocktimesIds4shift;
//    TimeElementIterator firstTime;
//    TimeElementIterator lastTime;
//    auto enteredIn = false;
//    for (auto it_b = srcDat.blocks.begin();
//         it_b != srcDat.blocks.end(); it_b++)
//    {
//        if (it_b->blockID == blockID)
//        {
//            if (it_b->linkedByTime) // блок-то увязан по времени!
//            {
//                return;
//            }
//            enteredIn = true;
//            firstTime = it_b->firstTimeElement;
//            lastTime = it_b->lastTimeElement;
//            //blocktimesIds4shift.push_back(it_b->t_elements_interpolated_values);
//            for (auto elem : it_b->t_elements_interpolated_values)
//            {
//                blocktimesIds4shift.push_back(elem);
//            }
//            if (xOffset >= 0.0)
//            {
//                // проверка, чтоб не получилось наслоения на следующий блок:
//                it_b++;
//                auto nextBlockFirstTime = it_b->firstTimeElement;
//                double diffTimesWithNextBlock =
//                        0.001 * static_cast<double>(
//                            nextBlockFirstTime->UTC_millisecondsSinceEpoch - lastTime->UTC_millisecondsSinceEpoch
//                            );
//                if (diffTimesWithNextBlock < xOffset)
//                {
//                    return;
//                }
//                it_b--;
//            }
//            if (xOffset < 0.0)
//            {
//                // проверка, чтоб не получилось наслоения на предыдущий блок:
//                it_b--;
//                auto previosBlockLastTime = it_b->lastTimeElement;
//                double diffTimesWithPreviosBlock =
//                        0.001 * static_cast<double>(
//                            firstTime->UTC_millisecondsSinceEpoch - previosBlockLastTime->UTC_millisecondsSinceEpoch
//                            );

//                if (diffTimesWithPreviosBlock < xOffset)
//                {
//                    return;
//                }
//                it_b++;
//            }
//            if (it_b->isShifted == false)
//            {
//                it_b->isShifted = true;
//            }
//            success = true;
//            break;
//        }
//    }
//    //if (lastTime != nullptr && firstTime != nullptr)
//    if (enteredIn)
//    {
//        for (auto it = srcDat.times.begin(); it != srcDat.times.end(); it++)
//        {
//            auto it2 = std::find_if(blocktimesIds4shift.cbegin(), blocktimesIds4shift.cend(),
//                                    [it](const TimeElementIterator &v) { return v == it; }
//                                    );
//            //if (blocktimesIds4shift.contains(it)
//            if (it2 != blocktimesIds4shift.cend()
//                    || (firstTime <= it && lastTime >= it))
//            {
//                it->UTC_millisecondsSinceEpoch += qRound64(xOffset*1000.0);
//            }
//        }
//    }
//}

//uint32_t GceDataSource::getBlockIdByValueX_InCurve(uint32_t curveID, double value)
//{
//    uint32_t idBlock = -1;
//    auto &loggingCurve = srcDat.curves.at(curveID);
//    TimeElementIterator time_;
//    auto fount_time_ = false;

//    int idx = 0;
//    uint32_t id;
//    foreach (id, loggingCurve.times_ids)
//    {
//        if (qFuzzyCompare(loggingCurve.values.at(idx), value))
//        {
//            break;
//        }
//        idx++;
//    }

//    for (auto it_t = srcDat.times.begin();
//         it_t != srcDat.times.end(); ++it_t)
//    {
//        if (id == it_t->id)
//        {
//            time_ = it_t;
//            fount_time_ = true;
//            break;
//        }
//    }

//    if (!fount_time_)
//    {
//        return idBlock;
//    }
//    for (auto it = srcDat.blocks.begin();
//         it != srcDat.blocks.end(); it++)
//    {
//        auto it_interp = std::find_if(it->t_elements_interpolated_values.cbegin(),
//                                      it->t_elements_interpolated_values.cend(),
//                                      [time_](const TimeElementIterator &v) { return v == time_; });
//        if ( (it->firstTimeElement <= time_ && it->lastTimeElement >= time_)
//             || it_interp != it->t_elements_interpolated_values.cend())
//        {
//            idBlock = it->blockID;
//            break;
//        }
//    }
//    return idBlock;
//}

//uint32_t GceDataSource::getBlockIdByValueX(double value)
//{
//    uint32_t idBlock = -1;
//    TimeElementIterator time_;
//    auto found_time_ = false;
//    for (auto it_t = srcDat.times.begin();
//         it_t != srcDat.times.end(); it_t++)
//    {
//        if (qFuzzyCompare(0.001*static_cast<double>(it_t->UTC_millisecondsSinceEpoch), value))
//        {
//            time_ = it_t;
//            found_time_ = true;
//            break;
//        }
//    }
//    if (!found_time_)
//    {
//        return idBlock;
//    }
//    for (auto it = srcDat.blocks.begin();
//         it != srcDat.blocks.end(); it++)
//    {
//        auto it_interp = std::find_if(it->t_elements_interpolated_values.cbegin(),
//                                      it->t_elements_interpolated_values.cend(),
//                                      [time_](const TimeElementIterator &v) { return v == time_; });
//        if ( (it->firstTimeElement <= time_ && it->lastTimeElement >= time_)
//             || it_interp != it->t_elements_interpolated_values.cend())
//        {
//            idBlock = it->blockID;
//            break;
//        }
//    }
//    return idBlock;
//}

uint32_t GceDataSource::getCurveIdByCurveName(QString curveName)
{
    auto curveNameS = curveName.toStdString();
    for (auto &nd : impl->srcDat->curves)
    {
        if (nd.second.name == curveNameS)
        {
            return nd.second.curve_id;
        }
    }
    return -1;
}

std::pair<bool, LoggingCurve*> GceDataSource::getLoggingCurve(uint32_t curveId)
{
    auto it = impl->srcDat->curves.find(curveId);
    if (it == impl->srcDat->curves.end()) return std::pair<bool, LoggingCurve*>(false, nullptr);
    else return std::pair<bool, LoggingCurve*>(true, &it->second);
}

//GceLoggingData *GceDataSource::getLoggingData(uint curveId)
//{
//    GceLoggingData *logData = new GceLoggingData(srcDat->sourceID, srcDat->curves.value(curveId), &srcDat->times);
//    return logData;
//}

//SourceFileKind GceDataSource::getSourceKind()
//{
//    return kindFileImported;
//}

const QDate GceDataSource::getSourceDate() const
{
    return QDate::fromString(impl->DateString_yyyy_mm_dd, QString("yyyy_MM_dd"));
}

const QString &GceDataSource::getSourceFileName() const
{
    return impl->nameFileImported;
}

const QString GceDataSource::getSourceFileType() const
{
    return QString().append(impl->extensionFileImported).toUpper();
}

const QString GceDataSource::getSourceFilePath() const
{
    return QString().append(impl->pathFileImported);
}

const QString &GceDataSource::getAdditionalInfo() const
{
    return impl->information;
}

void GceDataSource::setAdditionalInfoSpec(QString data_dev)
{
    impl->information = data_dev;
}

void GceDataSource::setSourceInfo(QString date_string_yyyy_mm_dd, QString baseFileName, QString extensionFileName, QString pathFile, QString additional)
{
    impl->DateString_yyyy_mm_dd = date_string_yyyy_mm_dd;
    impl->pathFileImported = pathFile;
    impl->nameFileImported = baseFileName;
    impl->extensionFileImported = extensionFileName;
    impl->information = additional;
}

const QString &GceDataSource::getDeviceName() const
{
    return impl->dataSourceName_Device;
}

void GceDataSource::setDeviceName(QString devNameNew)
{
    impl->dataSourceName_Device = devNameNew;
}

void GceDataSource::generateMagicWord()
{
    magicWord_DS = QString::number( QDateTime::currentDateTimeUtc().toMSecsSinceEpoch() );
}

void GceDataSource::addCurveName(QString cname)
{
    impl->namesCurves.append(cname);
}

std::list<std::string> GceDataSource::getNamesOfCurves_correct()
{
    std::list<std::string> result;
    for (auto it = impl->srcDat->curves.cbegin(); it != impl->srcDat->curves.cend(); ++it)
    {
        result.push_back(it->second.name);
    }
    return result;
}

bool GceDataSource::dataEmpty()
{
    auto timesSrc = static_cast<SourceDataTimes*>(impl->srcDat.get());
    auto depthsSrc = static_cast<SourceDataDepths*>(impl->srcDat.get());
    if ((timesSrc && !timesSrc->times.empty()) || (depthsSrc && !depthsSrc->depths.empty())) return false;
    return true;
}

void GceDataSource::setTimesData(std::unordered_map<uint32_t, TimeLogElement> &timesData)
{
    auto timesSrc = static_cast<SourceDataTimes*>(impl->srcDat.get());
    if( timesSrc ) {
        timesSrc->times.swap(timesData);
    }
}

std::vector<uint32_t> &GceDataSource::getExcludedTimeData()
{
    //auto timesSrc = static_cast<SourceDataTimes*>(impl->srcDat.get());
    //if( timesSrc ) return timesSrc->excludedIdsAxisX;
    //std::vector<uint32_t> nothing;
    //return nothing;

    return static_cast<SourceDataTimes*>(impl->srcDat.get())->excludedIdsAxisX;
}

std::unordered_map<uint32_t, LoggingCurve> &GceDataSource::getCurves()
{
    return impl->srcDat->curves;
}

void GceDataSource::getTimeShiftMilliseconds(int64_t &timeShift)
{
    timeShift = impl->timeShiftMilliseconds;
}

void GceDataSource::setTimeShiftMilliseconds(int64_t timeShift)
{
    impl->timeShiftMilliseconds = timeShift;
}

void GceDataSource::setExcludedTimesForNullValues(uint32_t curveID, bool setNulls, double valueForNull)
{
    for (auto it_curve = impl->srcDat->curves.begin(); it_curve != impl->srcDat->curves.end(); ++it_curve)
    {
        if (it_curve->first == curveID)
        {
            it_curve->second.null_value = setNulls;
            it_curve->second.null_value_exist = valueForNull;
        }
    }
}

void GceDataSource::getSettingOfNullValue(uint32_t curveID, bool &settedNulls, double &valueForNull)
{
    for (auto it = impl->srcDat->curves.begin(); it != impl->srcDat->curves.end(); ++it)
    {
        if (it->first == curveID)
        {
            settedNulls = it->second.null_value_exist;
            valueForNull = it->second.null_value;
            break;
        }
    }

}

GceDataSource::~GceDataSource() = default;
