#ifndef GCEDATASOURCE_H
#define GCEDATASOURCE_H

#include "base/types.h"

#include <memory>

#include <QVector>
#include <QDateTime>
#include <QString>

struct SourceData;

class QDate;
class GceDataSource
{
public:
    GceDataSource(uint32_t srcId, AbscissParameter type_for_ranging_data);
    ~GceDataSource();
    AbscissParameter getAbscissAxisType() const;
    // сохранение в папку проекта
    void        saveDataSource(QString dirProjectSaving);
    // открыть из папки проекта
    bool        openDataSource(QString dirProjectSaved);
    // импорт: учет временных петель в миллисекундах:
    void        setTimeLoopMaxPeriodMSecs(int timeLoopMSecs);
    // импорт: формирование информации о блоках после процедуры импорта
    //void        makeBlocksInfo();
    // привязать блоки, не увязанные по времени, к предыдущим увязанным автоматически,
    // оставив пометку о их не увязанности
    //void        formallyJoinUnlinkedBlocksAfterLinkedBlocks(bool &linkedBlocksExist, bool &blocksExist);
    // привязать блоки, не увязанные по времени, к указанному времени
    //void        formallyJoinUnlinkedBlocksToTimeAndDate(QDateTime dt);
    //void        updateLogDatas();
    // добавление блока данных (сомнительно, что пригодится)
    //void        addBlock(QVector<LoggingDataTimeElement *> timeElements,
    //                     QVector<LoggingDataTimeElement *> timeElementsInterpolation = QVector<LoggingDataTimeElement *>());
    // получить указатель на структуру блока по его идентификатору
    //bool getBlock(uint32_t idBlock, BlockIterator outBlock);
    //QVector<LoggingDatasBlock *> getAllBlocks();
    std::vector<std::pair<double, double>> getAllBlocksStartsWithEnds() const;
    // получить копию элемента времени по его идентификатору
//    LoggingDataTimeElement getTimeElementById(ulong elementID);
    // число блоков данных
    //int         blocksCount();
    // пометить блок, как привязанный по времени (isLinked = true),
    // либо не привязанный по времени (... false) по идентификатору блока
    //void        setBlockAsLinkedByTime(uint32_t blockID, bool isLinked);
    // импорт: добавить данные значений для кусочно-непрерывной кривой и присвоение ей имени + возврат её ID
    uint32_t    addLoggingDataRaw(AbscissParameter val_type, QString curve_name, QString description);
    // импорт: добавить данные по компоненту иерархии
    void        addHierarchyMember(HierarchyMember mmb);
    // импорт: обработка данных по иерархии
    HierarchyMember        getHierarchyRootElement() const;
    QVector<HierarchyMember> getSubElementsOf(HierarchyMember elementHierarchy) const;
    // получить список идентификаторов кривых
    void        getCurvesIDs(QList<uint32_t> &ids);
    // получить идентифиактор источника данных (прибора)
    uint32_t    getSourceID();
    // получить элементы времени по OX для кривой с ID
    QVector<double> getTimesAsXFromCurve(uint32_t curveID, double regularTimeShift);
    // сдвиг всех данных источника данных (прибора)
    void        shiftAllDataSourceBlocks(double xOffset);
    // сдвиг одного блока данных, если у него отсутствует привязка по времени
    //void        shiftSomeUnlinkedBlock(uint32_t blockID, double xOffset, bool &success);
    // получить идентификатор блока данных по значению X точки кривой, для которой известен идентификатор
    //uint32_t    getBlockIdByValueX_InCurve(uint32_t curveID, double value);
    // получить идентификатор блока данных по значению времени
    //uint32_t    getBlockIdByValueX(double value);
    //GceLoggingData *getLoggingData(uint curveId);

    uint32_t    getCurveIdByCurveName(QString curveName);
    std::pair<bool, LoggingCurve*> getLoggingCurve(uint32_t curveId);

    //SourceFileKind  getSourceKind();
    const QDate getSourceDate() const;
    const QString &   getSourceFileName() const;
    const QString getSourceFileType() const;
    const QString getSourceFilePath() const;
    const QString &   getAdditionalInfo() const;
    void        setAdditionalInfoSpec(QString data_dev);

    void        setSourceInfo(QString date_string_yyyy_mm_dd, QString baseFileName, QString extensionFileName,
                              QString pathFile,
                              QString additional = QString());

    const QString &    getDeviceName() const;
    void        setDeviceName(QString devNameNew);
    QString     magicWord_DS;
    uint32_t    index_forImportedDataIndexer;
    void        generateMagicWord();
    void        addCurveName(QString cname);
    std::list<std::string> getNamesOfCurves_correct();

    bool        dataEmpty();
    void        setTimesData(std::unordered_map<uint32_t, TimeLogElement> &timesData);
    std::vector<uint32_t> &getExcludedTimeData();
    std::unordered_map<uint32_t, LoggingCurve> &getCurves();

    void        getTimeShiftMilliseconds(int64_t &timeShift);
    void        setTimeShiftMilliseconds(int64_t timeShift);

    void        setExcludedTimesForNullValues(uint32_t curveID, bool setNulls, double valueForNull);
    void        getSettingOfNullValue(uint32_t curveID, bool &settedNulls, double &valueForNull);

private:
    struct Impl;
    std::unique_ptr<Impl> impl;
};

#endif // GCEDATASOURCE_H
