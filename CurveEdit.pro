TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    gceImportModule \
    gceLogicLayer \
    gce \
    stdcsv
#    upd
TRANSLATIONS += gce/gce_RU.ts
