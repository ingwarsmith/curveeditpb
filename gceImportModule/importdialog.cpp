#include "importdialog.h"

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>

#include <QFileDialog>

ImportDialog::ImportDialog(QWidget *parent) :
    QDialog(parent)
{
    auto ltSuper = new QVBoxLayout;
    this->setLayout(ltSuper);

    auto lt = new QVBoxLayout;
    ltSuper->addLayout(lt, 1);
    setWindowTitle(tr("Import"));
}

ImportDialog::~ImportDialog()
{}

void ImportDialog::setFormatNames(QStringList formats)
{
    QStringList dynFormats = formats;
    if (dataTypes.isEmpty())
    {
        dataTypes = dynFormats;
    }
    else
    {
        for (QString &fmt : dataTypes)
        {
            if (dynFormats.contains(fmt))
            {
                dynFormats.removeOne(fmt);
            }
        }
        dataTypes.append(dynFormats);
    }
    for (QString &fmt : dynFormats)
    {
        QPushButton *btnAny = new QPushButton;
        auto fn = fmt.split("|").at(0);
        btnAny->setText(fn);
        connect(btnAny, SIGNAL(clicked(bool)), this,  SLOT(slotSelectedFormat()));

        this->layout()->addWidget(btnAny);
    }
}

void ImportDialog::slotSelectedFormat()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    importedDataType = btn->text();

    if (importedDataType == tr("Load all compatible files from folder"))
    {
        absoluteImportedObjectPath = QFileDialog::getExistingDirectory(this, tr("Select folder with data files"));
    }
    else
    {
        for (QString &fmt : dataTypes)
        {
            auto fn = fmt.split("|").at(0);
            if (fn == importedDataType)
            {
                auto formats = fmt.split("|").at(1);
                absoluteImportedObjectPath = QFileDialog::getOpenFileName(this, fn, QString(), formats);
                break;
            }
        }
    }
    if (!absoluteImportedObjectPath.isEmpty())
    {
        this->accept();
    }
    else
    {
        this->reject();
    }
}
