#ifndef GCEIMPORTMODULE_H
#define GCEIMPORTMODULE_H

#include "gceimportmodule_global.h"
#include <QObject>
#include <QDateTime>
#include <QTextStream>

class GCEIMPORTMODULESHARED_EXPORT GceImportModule : public QObject
{
    Q_OBJECT

public:
    explicit GceImportModule( QObject *parent = nullptr);
    ~GceImportModule();
    QString showDialog(QStringList formatNames);
    QDateTime showDialogForDateTime();
    void    showErrorsLoadingData(QString msg);
    bool    isSuccessfull();
    QString selectedFileFormat();

private:
    QString         absPathToImport;
    struct          privateDialog;
    privateDialog   *privDialog;
};

#endif // GCEIMPORTMODULE_H
