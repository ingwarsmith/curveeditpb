#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

#include <QDialog>
#include <QMap>

class ImportDialog : public QDialog
{
    Q_OBJECT

    enum class DialogImportWidgetType
    {
        WidgetMain = 1,
        WidgetLoadFormat,
        WidgetLoadFromFolder
    };

public:    
    explicit ImportDialog(QWidget *parent = 0);
    ~ImportDialog();
    QString         getOpenedObjectAbsPath()
    {
        return absoluteImportedObjectPath;
    }
    void            setFormatNames(QStringList formats);
    QString         getDataType() const { return importedDataType; }

private:
    DialogImportWidgetType currentWgtType;
    QString             absoluteImportedObjectPath;
    QString             importedDataType;
    QStringList         dataTypes;

private slots:
    void    slotSelectedFormat();
};

#endif // IMPORTDIALOG_H
