#-------------------------------------------------
#
# Project created by QtCreator 2019-02-27T18:01:39
#
#-------------------------------------------------

QT       += core gui widgets xml

CONFIG += c++17

TARGET = gceImportModule
TEMPLATE = lib

DEFINES += GCEIMPORTMODULE_LIBRARY

CONFIG += skip_target_version_ext

NAME = CurveEditor Import Module

QMAKE_TARGET_DESCRIPTION = CurveEditor Import Module

#VERSION = 0.5.1.1 #
VERSION = 1.0.5.1
DEFINES += VERSION=\\\"$$VERSION\\\"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += gceimportmodule.cpp \
    importdialog.cpp 

HEADERS += gceimportmodule.h\
        gceimportmodule_global.h \
    importdialog.h \

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

#FORMS +=
#   importdialog.ui

unix {
    TARGET_EXT = .so
#    CONFIG(release, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/release/libgceImportModule.so $$OUT_PWD/../sys/libgceImportModule.so
#    else: CONFIG(debug, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/debug/libgceImportModule.so $$OUT_PWD/../sys/libgceImportModule.so
    QMAKE_POST_LINK = \
        #if ! [ -d /path/directory/ ]; then && \
        #mkdir $$OUT_PWD/../sys; && \
        #fi && \
        mkdir -p $$OUT_PWD/../sys && \
        cp $$OUT_PWD/libgceImportModule.so $$OUT_PWD/../sys/libgceImportModule.so.1.0.5 && \
        if ! [ -f $$OUT_PWD/../sys/libgceImportModule.so.1 ]; then \
        ln -s $$OUT_PWD/../sys/libgceImportModule.so.1.0.5 $$OUT_PWD/../sys/libgceImportModule.so.1.0; \
        ln -s $$OUT_PWD/../sys/libgceImportModule.so.1.0.5 $$OUT_PWD/../sys/libgceImportModule.so.1; fi
}
win32 {
    DLLDESTDIR = $$OUT_PWD/../sys
}

