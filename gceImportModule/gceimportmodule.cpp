#include "gceimportmodule.h"
#include "importdialog.h"
#include <QMessageBox>
#include <QDateTimeEdit>
#include <QVBoxLayout>

#include <QDialogButtonBox>

#include <QFile>
#include <QTextStream>

struct GceImportModule::privateDialog {
    ImportDialog dlg;
};

GceImportModule::GceImportModule(QObject *parent)
    : QObject(parent)
{
    privDialog = new GceImportModule::privateDialog;
}

GceImportModule::~GceImportModule()
{
    delete privDialog;
}

QString GceImportModule::showDialog(QStringList formatNames)
{
    absPathToImport.clear();
    auto dlg = &privDialog->dlg;
    dlg->setFormatNames(formatNames);
    if (dlg->exec() == QDialog::Rejected)
    {
        return QString();
    }
    absPathToImport = dlg->getOpenedObjectAbsPath();
    return absPathToImport;
}

QDateTime GceImportModule::showDialogForDateTime()
{
    QDialog wgtd;
    wgtd.setWindowTitle(tr("Select date for data"));
    auto ltw = new QVBoxLayout;
    wgtd.setLayout(ltw);
    auto dte = new QDateTimeEdit(nullptr);
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    dte->setCalendarPopup(true);
    auto dt = QDateTime::currentDateTimeUtc();
    dt.setTime(QTime::fromString("00:00:00", "hh:mm:ss"));
    dte->setDateTime(dt);
    dte->setDisplayFormat("dd.MM.yyyy hh:mm:ss");
    ltw->addWidget(dte);
    ltw->addWidget(btns);
    wgtd.setMinimumWidth(300);
    connect(btns, SIGNAL(accepted()), &wgtd, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), &wgtd, SLOT(reject()));
    if (wgtd.exec() == QDialog::Accepted)
    {
        dt = dte->dateTime();
    }
    btns->disconnect();
    return dt;
}

void GceImportModule::showErrorsLoadingData(QString msg)
{
    QMessageBox::critical(nullptr, tr("Error"), msg);
}

bool GceImportModule::isSuccessfull()
{
    return !absPathToImport.isEmpty();
}

QString GceImportModule::selectedFileFormat()
{
    return privDialog->dlg.getDataType();
}

