#ifndef GCEIMPORTMODULE_GLOBAL_H
#define GCEIMPORTMODULE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(GCEIMPORTMODULE_LIBRARY)
#  define GCEIMPORTMODULESHARED_EXPORT Q_DECL_EXPORT
#else
#  define GCEIMPORTMODULESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // GCEIMPORTMODULE_GLOBAL_H
