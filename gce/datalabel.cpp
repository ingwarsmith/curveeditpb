#include "datalabel.h"
#include "track.h"
#include "elements/label.h"
#include "project.h"

struct DataLabelPrivate
{
    Label    *elementLabel;

    QBrush   colorBrush;
    QPen     colorPen;
    QPen     colorPen2;
    QCPItemTracer *tracerPositionLabelUpper;
    QCPItemTracer *tracerPositionLabelLower;
    QCPItemLine *arrowPositionLabel;
    QCPItemTracer *tracerLabelTextBlockPos_TopLeft;
    QCPItemLine *arrowToTextBlock;
    QCPItemText *textBlock;
    QCPItemText *idBlock;
    double  xCoord;
    Track   *track_;
    bool    shown;

    DataLabelPrivate(Track *plot)
    {
        tracerPositionLabelLower = new QCPItemTracer(plot);
        tracerPositionLabelUpper = new QCPItemTracer(plot);
        arrowPositionLabel = new QCPItemLine(plot);
        tracerLabelTextBlockPos_TopLeft = new QCPItemTracer(plot);
        arrowToTextBlock = new QCPItemLine(plot);
        textBlock = new QCPItemText(plot);
        idBlock = new QCPItemText(plot);
        track_ = plot;

        colorBrush = Qt::red;
        colorPen.setColor(colorBrush.color());
        colorPen2.setColor(colorBrush.color());
        colorPen2.setStyle(Qt::DashLine);

        idBlock->setColor(colorBrush.color());
        textBlock->setBrush(/*colorBrush*/Qt::yellow);
        auto font = textBlock->font();
        font.setPixelSize(12);
        textBlock->setFont(font);

        tracerPositionLabelUpper->setBrush(colorBrush);
        arrowPositionLabel->setPen(colorPen);
        arrowToTextBlock->setPen(colorPen);
        arrowPositionLabel->setPen(colorPen2);

        tracerLabelTextBlockPos_TopLeft->setVisible(false);
        tracerPositionLabelLower->setVisible(false);
        tracerPositionLabelUpper->setStyle(QCPItemTracer::TracerStyle::tsCircle);
        tracerLabelTextBlockPos_TopLeft->position->setTypeY(QCPItemPosition::ptAbsolute);
        tracerLabelTextBlockPos_TopLeft->position->setTypeX(QCPItemPosition::ptPlotCoords);
        tracerPositionLabelLower->position->setTypeY(QCPItemPosition::ptAbsolute);
        tracerPositionLabelLower->position->setTypeX(QCPItemPosition::ptPlotCoords);
        tracerPositionLabelUpper->position->setTypeY(QCPItemPosition::ptAbsolute);
        tracerPositionLabelUpper->position->setTypeX(QCPItemPosition::ptPlotCoords);
        textBlock->position->setTypeY(QCPItemPosition::ptAbsolute);
        textBlock->position->setTypeX(QCPItemPosition::ptPlotCoords);
    }

    static DataLabelPrivate *makeDataLabel(Track *plot, QPoint pos, QString textLabel)
    {
        auto dl = new DataLabelPrivate(plot);
        dl->xCoord = plot->xAxis->pixelToCoord(pos.x());
        dl->arrowPositionLabel->start->setParentAnchor(dl->tracerPositionLabelLower->position);
        dl->arrowPositionLabel->end->setParentAnchor(dl->tracerPositionLabelUpper->position);
        auto yPosStart = plot->axisRect()->top() + 4;
        auto yPosEnd = plot->axisRect()->bottom();
        dl->tracerPositionLabelLower->position->setCoords(dl->xCoord, yPosEnd);
        dl->tracerPositionLabelUpper->position->setCoords(dl->xCoord, yPosStart);
        dl->textBlock->setText(textLabel);
        dl->textBlock->position->setParentAnchor(dl->tracerPositionLabelUpper->position);
        dl->textBlock->setPositionAlignment(Qt::AlignLeft | Qt::AlignTop);
        dl->idBlock->position->setParentAnchor(dl->tracerPositionLabelUpper->position);
        dl->idBlock->setPositionAlignment(Qt::AlignRight | Qt::AlignTop);

        return dl;
    }

    static DataLabelPrivate *makeDataLabelWithCoordX(Track *plot, double xCoord, QString textLabel)
    {
        auto dl = new DataLabelPrivate(plot);
        dl->xCoord = xCoord;
        dl->arrowPositionLabel->start->setParentAnchor(dl->tracerPositionLabelLower->position);
        dl->arrowPositionLabel->end->setParentAnchor(dl->tracerPositionLabelUpper->position);
        auto yPosStart = plot->axisRect()->top() + 4;
        auto yPosEnd = yPosStart + plot->height();
        dl->tracerPositionLabelLower->position->setCoords(dl->xCoord, yPosEnd);
        dl->tracerPositionLabelUpper->position->setCoords(dl->xCoord, yPosStart);
        dl->textBlock->setText(textLabel);
        dl->textBlock->position->setParentAnchor(dl->tracerPositionLabelUpper->position);
        dl->textBlock->setPositionAlignment(Qt::AlignLeft | Qt::AlignTop);
        dl->idBlock->position->setParentAnchor(dl->tracerPositionLabelUpper->position);
        dl->idBlock->setPositionAlignment(Qt::AlignRight | Qt::AlignTop);

        return dl;
    }
};

DataLabel::DataLabel(Track *plot, QPoint pos, QString textLabel,  double xCoord, QObject *parent) : QObject(parent)
{
    if (pos.x() == 0 && pos.y() == 777)
    {
        data = DataLabelPrivate::makeDataLabelWithCoordX(plot, xCoord, textLabel);
    }
    else
    {
        data = DataLabelPrivate::makeDataLabel(plot, pos, textLabel);
    }
    plot->replot();
    hidden = false;
    hidden_text = false;
    auto project = qobject_cast<Project*>(plot->getItem()->getProjectPointer());
    currentTimeOffset = project->getFullOffsetFromUTC_msecs();
    connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), this, SLOT(slotShiftTime()));
}

DataLabel::DataLabel(Track *plot, QString textLabel, double xCoord, QObject *parent) : QObject(parent)
{
    data = DataLabelPrivate::makeDataLabelWithCoordX(plot, xCoord, textLabel);
    plot->replot();
    hidden = false;
    hidden_text = false;
    auto project = qobject_cast<Project*>(plot->getItem()->getProjectPointer());
    currentTimeOffset = project->getFullOffsetFromUTC_msecs();
    connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), this, SLOT(slotShiftTime()));
}

DataLabel *DataLabel::makeCopy()
{
    DataLabel *copyNew = new DataLabel(this->data->track_, this->getText(), this->data->xCoord);
    auto project = qobject_cast<Project*>(this->data->elementLabel->getItem()->getProjectPointer());
    auto labelNew = project->createLabel();
    labelNew->setText(this->getText());
    labelNew->setTrackPointer(this->data->track_);
    copyNew->setLabelElement(labelNew);

    return copyNew;
}

DataLabel *DataLabel::makeCopyInPlot(Track *plot)
{
    DataLabel *copyNew = new DataLabel(plot, this->getText(), this->data->xCoord);
    auto project = qobject_cast<Project*>(this->data->elementLabel->getItem()->getProjectPointer());
    auto labelNew = project->createLabel();
    labelNew->setText(this->getText());
    labelNew->setTrackPointer(plot);
    copyNew->setLabelElement(labelNew);

    return copyNew;
}

void DataLabel::setLabelElement(Label *label, bool afterCreate)
{
    data->elementLabel = label;
    data->idBlock->setText(QString::number(label->getID()).append(" "));
    connect(label, SIGNAL(sigShowText()), this, SLOT(slotShowText()));
    connect(label, SIGNAL(sigHideText()), this, SLOT(slotHideText()));
    connect(label, SIGNAL(sigHide()), this, SLOT(slotHide()));
    connect(label, SIGNAL(sigShow()), this, SLOT(slotShow()));

    connect(label, SIGNAL(sigChangeText()), this, SLOT(slotChangeText()));
    connect(label, SIGNAL(sigGoToLabel()), this, SLOT(slotGotoLabel()));

    connect(label, &Label::sigRemoveLabel, this, &DataLabel::sigRemoveMe);
    connect(label, &Label::sigCopyLabel, this, &DataLabel::sigCopyMe);
    connect(label, &Label::sigCopyLabelToAnotherTrack, this, &DataLabel::sigCopyMeToAnotherTrack);
    connect(label, &Label::sigMoveLabel, this, &DataLabel::sigMoveMe);
    connect(label, &Label::sigEditProperties, this, &DataLabel::slotChangeProperties);
    connect(label, &Label::sigSetTextFontPixelSize,
            this, &DataLabel::slotSetTextFontPixelSize);

    auto fnt = data->textBlock->font();
    fnt.setPixelSize(label->fontPixelSize);
    data->textBlock->setFont(fnt);

    if (afterCreate)
    {
        slotChangeText();
    }
}

void DataLabel::setLabelID(uint32_t newID)
{
    if (data->elementLabel == nullptr) return;
    data->elementLabel->setID(newID);
    data->elementLabel->updateText();
    data->idBlock->setText(QString::number(newID).append(" "));
}

uint32_t DataLabel::getLabelID()
{
    if (data->elementLabel == nullptr) return 0;
    return data->elementLabel->getID();
}

double DataLabel::getLabel_Xcoord()
{
    return data->xCoord;
}

void DataLabel::setLabel_Xcoord(double x)
{
    data->xCoord = x;
    auto lowY = data->tracerPositionLabelLower->position->coords().y();
    auto highY = data->tracerPositionLabelUpper->position->coords().y();
    data->tracerPositionLabelLower->position->setCoords(data->xCoord, lowY);
    data->tracerPositionLabelUpper->position->setCoords(data->xCoord, highY);
}

QString DataLabel::getText()
{
    return data->textBlock->text();
}

void DataLabel::setText(QString txt)
{
    data->textBlock->setText(txt);
    data->elementLabel->setText(txt);
}

bool DataLabel::isVisible()
{
    return hidden;
}

bool DataLabel::isTextVisible()
{
    return data->textBlock->visible();
}

double DataLabel::getTextBlockLastCoordX()
{
    return data->track_->xAxis->pixelToCoord(
                data->textBlock->bottomRight->pixelPosition().x()
                );
}

double DataLabel::getTextBlockBottomCoordY()
{
    return data->track_->yAxis->pixelToCoord(
                data->textBlock->bottomRight->pixelPosition().y()
                );
}

double DataLabel::getTextBlockHeightInCoordUnits()
{
    auto pxSz = static_cast<double>(data->textBlock->font().pixelSize());
    QStringList tmp = data->textBlock->text().split("\n");
    auto mul = static_cast<double>(tmp.size()) + 0.7;
    return data->track_->yAxis->pixelToCoord(
                pxSz*mul*2.0
                );
}

void DataLabel::setMargineFromAnotherTextAndFromBottom(double yDelta)
{
    if (data->textBlock->position->coords().y() == 0.0)
    {
        data->textBlock->position->setCoords(0.0, yDelta);
    }
}

void DataLabel::setCoordY_forTextBlock(double coordY)
{
    if (data->textBlock->position->coords().y() == 0.0)
    {
        data->textBlock->position->setCoords(0.0, coordY);
    }
}

double DataLabel::getPixelCoordY_forTextBlock()
{
    return data->textBlock->position->pixelPosition().y();
}

void DataLabel::setPixelCoordY_forTextBlock(double pixcrdY)
{
    auto posf = data->textBlock->position->pixelPosition();
    posf.setY(pixcrdY);
    data->textBlock->position->setPixelPosition(posf);
    posf = data->idBlock->position->pixelPosition();
    posf.setY(pixcrdY);
    data->idBlock->position->setPixelPosition(posf);
    data->track_->replot();
}

void DataLabel::resetMargineFromAnotherTextAndFromBottom()
{
    data->textBlock->position->setCoords(0.0, 0.0);
}

void DataLabel::removeMe()
{
    auto treeElem = data->elementLabel->getItem();
    auto parentTreeElem_LabelsList = data->elementLabel->getParentItem();
    parentTreeElem_LabelsList->removeSubElement(treeElem);

    auto project = qobject_cast<Project*>(this->data->elementLabel->getItem()->getProjectPointer());
    delete treeElem;

    project->removeLabelById(data->elementLabel->getID());
    data->elementLabel->prepareToDeleting();
    delete data->elementLabel;

    data->track_->removeItem(data->arrowToTextBlock);
    data->track_->removeItem(data->idBlock);
    data->track_->removeItem(data->textBlock);
    data->track_->removeItem(data->arrowPositionLabel);
    data->track_->removeItem(data->tracerPositionLabelLower);
    data->track_->removeItem(data->tracerLabelTextBlockPos_TopLeft);
    data->track_->removeItem(data->tracerPositionLabelUpper);

    delete data;
}

void DataLabel::hide(bool yes)
{
    data->shown = !yes;
    if (yes)
    {
        slotHide();
    }
    else
    {
        slotShow();
    }
}

void DataLabel::slotShiftTime()
{
    auto project = qobject_cast<Project*>(sender());
    auto fullOffsetNew = project->getFullOffsetFromUTC_msecs();
    auto timeDifference = fullOffsetNew - currentTimeOffset;
    currentTimeOffset = fullOffsetNew;
    auto timeDiffDouble_seconds = static_cast<double>(timeDifference) / 1000.0;
    auto newX = data->xCoord + timeDiffDouble_seconds;
    setLabel_Xcoord(newX);
}

void DataLabel::slotShowText()
{
    hidden_text = false;
    if (hidden)
    {
        return;
    }
    data->textBlock->setVisible(true);
    data->textBlock->parentPlot()->replot();
}

void DataLabel::slotHideText()
{
    hidden_text = true;
    if (hidden)
    {
        return;
    }
    data->textBlock->setVisible(false);
    data->textBlock->parentPlot()->replot();
}

void DataLabel::slotShow()
{
    if (!hidden_text)
    {
        data->textBlock->setVisible(true);
    }
    data->arrowPositionLabel->setVisible(true);
    data->tracerPositionLabelUpper->setVisible(true);
    data->idBlock->setVisible(true);
    data->textBlock->parentPlot()->replot();
    hidden = false;
}

void DataLabel::slotHide()
{
    if (!hidden_text)
    {
        data->textBlock->setVisible(false);
    }
    data->arrowPositionLabel->setVisible(false);
    data->tracerPositionLabelUpper->setVisible(false);
    data->idBlock->setVisible(false);
    data->textBlock->parentPlot()->replot();
    hidden = true;
}

void DataLabel::slotChangeText()
{
    QDialog dlg;
    auto l = new QVBoxLayout;
    auto te = new QTextEdit;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    te->setPlainText(data->textBlock->text());
    l->addWidget(te);
    l->addWidget(btns);
    dlg.setLayout(l);
    connect(btns, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), &dlg, SLOT(reject()));

    dlg.setWindowTitle(tr("Change label text"));

    if (dlg.exec() == QDialog::Accepted)
    {
        data->textBlock->setText(te->toPlainText());
        data->textBlock->parentPlot()->replot();
        data->elementLabel->setText(te->toPlainText());
    }
}

void DataLabel::slotGotoLabel()
{
    emit sigGotoLabel(data->xCoord);
}

void DataLabel::slotChangeProperties()
{
    QDialog dlg;
    dlg.setMinimumWidth(400);
    auto l = new QVBoxLayout;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    auto lbl = new QLabel(tr("Offset from top, pixels"));
    auto lned = new QLineEdit;
    auto ivld = new QIntValidator(0, data->track_->height());
    lned->setValidator(ivld);
    auto valInt = qRound(getPixelCoordY_forTextBlock());
    lned->setText(QString::number(valInt));

    l->addWidget(lbl);
    l->addWidget(lned);
    l->addWidget(btns);

    dlg.setLayout(l);
    connect(btns, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), &dlg, SLOT(reject()));

    dlg.setWindowTitle(tr("Change label properties"));

    if (dlg.exec() == QDialog::Accepted)
    {
        auto value__ = static_cast<double>(lned->text().toInt());
        setPixelCoordY_forTextBlock(value__);
    }
}

void DataLabel::slotSetTextFontPixelSize(int sz)
{
    data->elementLabel->fontPixelSize = sz;
    auto fnt = data->textBlock->font();
    fnt.setPixelSize(sz);
    data->textBlock->setFont(fnt);
    data->track_->replot();
}
