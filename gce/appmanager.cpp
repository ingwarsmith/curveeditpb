#include "appmanager.h"
#include "mainwindow.h"

#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QHostInfo>
#include <QMessageBox>
#include <QThread>

#include <gcelogiclayer.h>
#include "filesearchernodb.h"

AppManager::AppManager(QObject *parent) : QObject(parent)
{
    QDir templDir(qApp->applicationDirPath().append("/templates"));
    QDir userTmplDir(qApp->applicationDirPath().append("/user.templates"));
    if (!templDir.exists())
    {
        QDir(qApp->applicationDirPath()).mkdir("templates");
    }
    if (!userTmplDir.exists())
    {
        QDir(qApp->applicationDirPath()).mkdir("user.templates");
    }
}

void AppManager::updateChecking(bool &needToUpdate)
{
    needToUpdate = false;
    auto domain = QProcessEnvironment::systemEnvironment().value("USERDOMAIN");

    if (domain != "GEOMASH")
    {
        return;
    }

    auto cyrillicSymbolsExistInPath = false;
    auto apath = qApp->applicationDirPath();
    for (QChar ch : apath)
    {
        if (ch.isLetter())
        {
            if (ch.toLatin1() == (char)0)
            {
                cyrillicSymbolsExistInPath = true;
                break;
            }
        }
    }

    // чистка папки update после обновления
    QFileInfo oldDataAfterUpdate(QString(apath).append("/upd"));
    if (oldDataAfterUpdate.exists())
    {
        QThread::sleep(1);
        FileSearcherNoDb fsr;
        QFileInfo fi4Remove(QString(apath).append("/upd"));
        for (QFileInfo &fi : fsr.retFilesInFolderAndSubFolders(fi4Remove))
        {
            QFile fl(fi.absoluteFilePath());
            fl.remove();
            //programUpdated = true;
        }
    }

    auto updateCfgPath = QString(apath).append("/upd.cfg");
    QFileInfo updateCfgFI(updateCfgPath);
    if (updateCfgFI.isDir())
    {
        QDir appDir(apath);
        appDir.rmdir("upd.cfg");        
    }
    if (!updateCfgFI.exists())
    {
        QFile makeCfgFl(updateCfgPath);
        makeCfgFl.open(QFile::WriteOnly|QFile::Text);
        QTextStream stream(&makeCfgFl);
        stream << QString("dist=//ADDS2/Public/Сервисные утилиты/GM-CurveEdit/dist") << "\n";
        stream << QString("comp=//ADDS2/Public/Сервисные утилиты/GM-CurveEdit/components") << "\n";
        makeCfgFl.close();
    }

    QFile cfgFl(updateCfgPath);
    if (!cfgFl.open(QFile::ReadOnly|QFile::Text))
    {
        errorUpdateInfo = tr("Configuration file is unaccessible.");
        return;
    }
    QTextStream stream(&cfgFl);
    QString componentsPath, distributivePath;
    while (!stream.atEnd())
    {
        auto line = stream.readLine();
        if (line.contains("="))
        {
            if (line.split("=").at(0) == "dist")
                distributivePath = line.split("=").at(1);
            if (line.split("=").at(0) == "comp")
                componentsPath = line.split("=").at(1);
        }
    }
    cfgFl.close();
    if (componentsPath.isEmpty() || distributivePath.isEmpty())
    {
        errorUpdateInfo = tr("Update links in configuration file are empty. It may be damaged.");
        return;
    }
    updateServerName = componentsPath;
    updateServerName.remove("//");
    auto tmp_sl = updateServerName.split("/");
    updateServerName = tmp_sl.at(0);

    auto connectedToUpdateServer = isConnectedToNetwork();

    if (!connectedToUpdateServer)
    {
        errorUpdateInfo = tr("Not connected to update server.");
        return;
    }

    QDir dirDist(distributivePath);
    if (!dirDist.isReadable())
    {
        errorUpdateInfo = tr("Update data on server is unreadable.");
        return;
    }

    // обновление программных модулей

    auto programUpdateNeeded = false;
    auto _update_info_file = QString(distributivePath).append("/upd/upd_instructions.txt");
    QFile fl_upd_info(_update_info_file);
    QStringList names__;
    if (!fl_upd_info.exists())
    {
        errorUpdateInfo = tr("Update instructions file not exist!");
    }
    else
    {
        auto headerUpdList = QString("[updateList_normal]");
        auto inUpdList = false;
        if (fl_upd_info.open(QIODevice::ReadOnly|QIODevice::Text))
        {
            QTextStream istr(&fl_upd_info);

            while (!istr.atEnd())
            {
                auto line = istr.readLine();
                if (line.isEmpty() || line.at(0) == QChar('#'))
                    continue;
                if (inUpdList && line.at(0) == QChar('['))
                    break;
                if (inUpdList)
                    names__.append(line);
                if (line.left(headerUpdList.size()) == headerUpdList)
                    inUpdList = true;
            }

            fl_upd_info.close();
        }

        if ( !names__.isEmpty() )
        {
            for (QString &key_name : names__)
            {
                auto nameContainLetters = false;
                for (QChar ch : key_name)
                {
                    if (ch.isLetter())
                    {
                        nameContainLetters = true;
                        break;
                    }
                }
                if (!nameContainLetters) continue;
                QString localPath = apath;
                QString sourceNetPath = distributivePath;
                QPair<QString, QString> src2dst;
                src2dst.first = key_name.split(";").first().prepend("/").prepend(sourceNetPath);
                src2dst.second = key_name.split(";").last().prepend("/").prepend(localPath);

                QString elemPath = src2dst.second;
                QString elemOnServerPath = src2dst.first;
                QFileInfo elemFile(elemPath);
                QFileInfo elemOnServerFile(elemOnServerPath);
                bool exists = true;
                exists = elemFile.exists();
                bool theSame = true;

                if (elemFile.lastModified().date() < elemOnServerFile.lastModified().date())
                {
                    theSame = false;
                }
                else if (elemFile.lastModified().date() == elemOnServerFile.lastModified().date())
                {
                    if (elemFile.lastModified().time() < elemOnServerFile.lastModified().time())
                        theSame = false;
                }

                if (!exists || !theSame)
                {
                    programUpdateNeeded = true;
                    break;
                }
            }
        }

        if (programUpdateNeeded)
        {
            if (cyrillicSymbolsExistInPath)
            {
                QMessageBox::warning(NULL, tr("Warning: non-latin symbols exist in path"),
                                     tr("There are cyrillic symbols in program path, so only components will update."
                                        "\nIf path contains only latin symbols, then program can update too."));
            }
            else
            {
                if (QMessageBox::question(NULL, tr("Update"), tr("Program may be updated. Do you want to update now?"))
                        == QMessageBox::Yes)
                {
                    QDir appFolder(apath);
                    appFolder.mkdir("upd");
                    QString updLocalPath = QString("/upd").prepend(apath);

                    QString updaterPathNet = distributivePath;
                    updaterPathNet.append("/upd");

                    FileSearcherNoDb fsr;
                    QFileInfo netSrcUpdaterDirFi(updaterPathNet);
                    for (QFileInfo &fi : fsr.retFilesInFolderAndSubFolders(netSrcUpdaterDirFi))
                    {
                        QFile fl(fi.absoluteFilePath());
                        if (fi.absolutePath() == updaterPathNet)
                        {
                            fl.copy(QString(updLocalPath).append("/").append(fi.fileName()));
                        }
                        else
                        {
                            QString fullPathWithSubDirs = fi.absolutePath();
                            QString pathWithSubDirs = fullPathWithSubDirs;
                            pathWithSubDirs.remove(updaterPathNet);
                            QString readyPath = QString(updLocalPath).append(pathWithSubDirs);
                            QDir subDir(readyPath);
                            if (!subDir.exists())
                            {
                                QDir d2(updLocalPath);
                                QString pathWOslash = pathWithSubDirs;
                                pathWOslash.remove(0, 1);
                                d2.mkpath(pathWOslash);
                            }
                            fl.copy(QString(updLocalPath).append(pathWithSubDirs).append("/").append(fi.fileName()));
                        }
                    }

                    needToUpdate = true;

                    updaterExecutablePath = QString("/upd.exe").prepend(updLocalPath);

                    return;
                }
            }
        }
    }


    // обновление компонентов

    auto appLocalPath_UserDirs = apath;
    auto componentsNetPath = componentsPath;

    QDir componentsDir(componentsNetPath);

    if (componentsDir.exists())
    {
        auto componentsSubDirs = componentsDir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot);
        for (QFileInfo &dirfi : componentsSubDirs)
        {
            FileSearcherNoDb searcherDesc;

            auto fi_list = searcherDesc.retFilesInFolderAndSubFolders(dirfi);

            for (QFileInfo &fi_ : fi_list)
            {
                auto pathcp = fi_.absoluteFilePath();
                pathcp.remove(componentsNetPath);
                pathcp.prepend(appLocalPath_UserDirs);
                QFileInfo localCopy;
                localCopy.setFile(pathcp);
                auto mustToBeReplaced = false;
                if ( !localCopy.exists() ) mustToBeReplaced = true;
                else
                {
                    auto localDate = localCopy.lastModified().date();
                    auto remoteDate = fi_.lastModified().date();

                    if (remoteDate > localDate)
                    {
                        mustToBeReplaced = true;
                    }
                    else if (localDate == remoteDate)
                    {
                        if (localCopy.lastModified().time() < fi_.lastModified().time())
                            mustToBeReplaced = true;
                    }
                }
                if (mustToBeReplaced)
                {
                    if (localCopy.exists())
                    {
                        QFile lf_(localCopy.absoluteFilePath());
                        lf_.remove();
                    }

                    QFile onSrv(fi_.absoluteFilePath());
                    QDir local;
                    local.mkpath(localCopy.absolutePath());
                    onSrv.copy(localCopy.absoluteFilePath());
                }
            }
        }
    }
}

void AppManager::viewUpdateErrorIfExist()
{
    if (errorUpdateInfo.size())
    {
        QMessageBox::warning(NULL, tr("Update"), errorUpdateInfo);
    }
}

void AppManager::updateStart()
{
    QProcess::startDetached(updaterExecutablePath);
}

bool AppManager::checkConfiguration()
{
    configuration.checkCfg();
    return true;
}

void AppManager::loadConfiguration()
{
    //
}

void AppManager::saveConfiguration()
{
    configuration.saveSettingsToCfg();
}

void AppManager::init()
{
    GceLogicLayer::getInstance().setObjectsContainerPointer(&objectsContainer);

    configuration.setObjectName(ObjectsNames::objNameConfiguration);
    objectsContainer << qobject_cast<QObject*>(&configuration);

    auto projectMgr = new ProjectManager;
    projectMgr->setObjectName(ObjectsNames::objNameProjectManager);
    objectsContainer << qobject_cast<QObject*>(projectMgr);

    auto wnd = new MainWindow(projectMgr);
    wnd->setObjectName(ObjectsNames::objNameMainWindow);
    objectsContainer << qobject_cast<QObject*>(wnd);
}

void AppManager::run()
{
    for (QObject *o : objectsContainer)
    {
        if (o->objectName() == ObjectsNames::objNameMainWindow)
        {
            qobject_cast<MainWindow*>(o)->show();
            break;
        }
    }
}

QString AppManager::getLanguageFromCgf() const
{
    return configuration.dataMembers.language;
}

bool AppManager::isConnectedToNetwork()
{
    QWidget wgtWait;
    wgtWait.setWindowTitle(tr("Please wait: searching of the update server..."));
    wgtWait.setFixedWidth(400);
    wgtWait.setFixedHeight(20);

    auto updateServerOk = false;

    if (!updateServerName.isEmpty())
    {
        wgtWait.show();
        QHostInfo hostinfo_ = QHostInfo::fromName(updateServerName);
        bool noError = hostinfo_.error() == QHostInfo::NoError;
        bool hasAddresses = hostinfo_.addresses().size() > 0;
                                                                //bool correctStatIP = (hasAddresses ? hostinfo_.addresses().at(0).toString() == srvStatIp : false);
        updateServerOk = (noError && hasAddresses);             //(srvStatIp.size()>0 ? hasAddresses && correctStatIP : hasAddresses));
                                                                //(hostinfo_.error() == QHostInfo::NoError && hostinfo_.addresses().size()>0 && hostinfo_.addresses().at(0).toString()=="192.168.0.3");
        wgtWait.hide();
    }

    return updateServerOk;
}
