#include "curve.h"

Curve::Curve(uint32_t projectIdx, uint32_t indexInDevice, QObject *prjPtr, QObject *parent)
    : QObject(parent)
{
    idxProject = projectIdx;
    indexCurveInDevice = indexInDevice;
    sourceID = 0;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementNotLoadedCurve);
}

TreeElement *Curve::getItem() const
{
    return rootItem;
}

void Curve::setParentForItem(TreeElement *elementParent)
{
    parentItem = elementParent;
}

void Curve::setCurveType_RAW()
{
    rootItem->setType(TreeElement::ElementType::elementRawCurve);
}

void Curve::setCurveType_CALCULATED()
{
    rootItem->setType(TreeElement::ElementType::elementCalcCurve);
}

void Curve::setCurveType_CHANGED()
{
    rootItem->setType(TreeElement::ElementType::elementChangedCurve);
}

void Curve::setCurveType_CurveLinkInTrack()
{
    rootItem->setType(TreeElement::ElementType::elementCurveLinkInTrack);
}

void Curve::setCurveName(QString nm)
{
    rootItem->setNewNameElement(nm);
}

QString Curve::getCurveName() const
{
    auto nm = rootItem->getNameElement();
    if (nm.contains(" / "))
    {
        QStringList tmp = nm.split(" / ");
        nm = tmp.at(0);
    }
    return nm;
}

void Curve::setLinkingToDeviceName(bool yes)
{
    if (yes)
    {
        connect(parentItem, SIGNAL(sigElementNameChanged(QString)), this, SLOT(slotChangeAffixDeviceName(QString)));
    }
    else
    {
        disconnect(parentItem, SIGNAL(sigElementNameChanged(QString)), this, SLOT(slotChangeAffixDeviceName(QString)));
    }
}

void Curve::slotChangeAffixDeviceName(QString newDevName)
{
    auto nmOld = rootItem->getNameElement();
    QStringList tmp = nmOld.split(" / ");
    nmOld = tmp.at(1);

    auto nm = baseCurveNameInTrackList;
    nm.append(" / ");
    nm.append(newDevName);
    rootItem->setNewNameElement(nm);
    emit sigDeviceNameChanged(nmOld, newDevName);
}
