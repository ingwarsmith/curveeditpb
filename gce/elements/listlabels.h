#ifndef LISTLABELS_H
#define LISTLABELS_H

#include <QObject>
#include "treeelement.h"
#include "label.h"

class ListLabels : public QObject
{
    Q_OBJECT
public:
    explicit ListLabels(uint32_t projectIdx, QObject *prjPtr, QObject *parent = nullptr);
    Label       *createLabel();
    QList<QAction *> getActionsList();
    TreeElement     *getItem() { return rootItem; }
    void        removeLabelById(uint32_t id);
    void        clearAll();

private:
    uint32_t        idxProject;
    TreeElement     *rootItem;
    TreeElement     *parentItem;
    QAction         *hideLabelsAction;
    QAction         *showLabelsAction;
    QAction         *hideLabelsTextAction;
    QAction         *showLabelsTextAction;
    QAction         *changeLabelsFontSize;
    QMap<uint32_t, Label*> labels;

    bool            labelsHided;
    bool            labelsTextsHided;

    uint32_t        maxKey;
    int             fontPixelSizeLabels;

signals:
    void    sigHideTextLabels();
    void    sigShowTextLabels();
    void    sigHideLabels();
    void    sigShowLabels();
    void    sigSetTextFontPixelSize(int sz);

private slots:
    void    slotSetUnsetChecked();
    void    slotChangeLabelsFontPixelSize();
};

#endif // LISTLABELS_H
