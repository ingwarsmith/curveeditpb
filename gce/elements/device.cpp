#include "device.h"
#include "curve.h"
#include "project.h"
#include "gcedataview.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QDateTimeEdit>
#include <QComboBox>

#include "gcelogiclayer.h"

Device::Device(uint32_t projectIdx, uint32_t indexInDevicesBlock, QObject *prjPtr, QObject *parent)
    : QObject(parent)
{
    idxProject = projectIdx;
    sourceID = indexInDevicesBlock;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementDevice);

    connect(this, SIGNAL(sigDeviceRenamedForEmptyPlotDeviceElements(QString,QString,uint32_t)),
            qobject_cast<Project*>(prjPtr), SLOT(slotDeviceRenamed_EmptyPlotDevElements(QString,QString,uint32_t)));
    connect(this, SIGNAL(sigRemove_Device(QString,uint32_t)),
            qobject_cast<Project*>(prjPtr), SLOT(slotRemoveDevice(QString,uint32_t)));

    auto num = GceLogicLayer::getInstance().getSourcesIds().size() + 1;
    rootItem->setNewNameElement(tr("Data_source").append("_").append(QString::number(num)));

    deviceInformation = new QAction(tr("Data source info"));
    connect(deviceInformation, SIGNAL(triggered(bool)), this, SLOT(slotShowDialogWithSourceProperties()));

    renameDevice = new QAction(tr("Rename data source"));
    connect(renameDevice, SIGNAL(triggered(bool)), this, SLOT(slotShowRenameDialog()));

    deleteDevice = new QAction(tr("Delete data source"));
    connect(deleteDevice, SIGNAL(triggered(bool)), this, SLOT(slotDeleteDevice()));

    deviceTimeShiftSet = new QAction(tr("Set data source time shift"));
    connect(deviceTimeShiftSet, SIGNAL(triggered(bool)), this, SLOT(slotShowSetTimeShiftDialog()));

    openPlotWithDevice = new QAction(tr("Add plot"));
    connect(openPlotWithDevice, SIGNAL(triggered(bool)), this, SLOT(slotAddPlotForDevice()));

    rootItem->setContextMenuActions(getActionsList());
}

TreeElement *Device::getItem()
{
    return rootItem;
}

void Device::setParentForItem(TreeElement *elementParent)
{
    parentItem = elementParent;
}

Curve *Device::createCurve(uint curveID, TreeElement *base)
{
    nextCurveIdx = curvesMap.keys().size();
    auto c_ = new Curve(idxProject, nextCurveIdx, getItem()->getProjectPointer(), this);
    curvesMap.insert(curveID, c_);
    c_->setCurveID(curveID);
    c_->setSourceID(sourceID);
    if (base == nullptr)
    {
        rootItem->addSubElement(c_->getItem());
    }
    else
    {
        base->addSubElement(c_->getItem());
    }
    c_->setParentForItem(rootItem);
    connect(this, SIGNAL(sigDeviceRenamed(QString)), rootItem, SIGNAL(sigElementNameChanged(QString)));
    auto okDesc = false;
    auto loggingCurveDescr = GceLogicLayer::getInstance().getCurveDescriptionStringByCurveId(
                sourceID, curveID, okDesc);
    if (okDesc)
    {
        if (loggingCurveDescr.size())
        {
            c_->getItem()->setToolTip(loggingCurveDescr);
        }
    }
    return c_;
}

QList<QAction *> Device::getActionsList()
{
    QList<QAction*> ret;
    ret << deviceInformation << renameDevice << deviceTimeShiftSet
        << openPlotWithDevice << deleteDevice;
    return ret;
}

void Device::slotShowDialogWithSourceProperties()
{
    auto dlgDeviceInfo = new QDialog;
    auto lt = new QVBoxLayout;
    auto lblFName = new QLabel(tr("Data source file name:"));
    auto lblFExt = new QLabel(tr("Data source file type:"));
    auto lblFPath = new QLabel(tr("Data source file path:"));
    auto lblFDateImport = new QLabel(tr("Date and time of data source's import:"));
    auto lblFAdditionalInfo = new QLabel(tr("Additional info:"));
    auto lnedFn = new QLineEdit;
    lnedFn->setReadOnly(true);
    lnedFn->setMinimumWidth(380);
    auto lnedFe = new QLineEdit;
    lnedFe->setReadOnly(true);
    auto teFilePath = new QTextEdit,
            teAddnlInfo = new QTextEdit;
    teFilePath->setReadOnly(true);
    teFilePath->setMinimumHeight(20);
    teFilePath->setMaximumHeight(60);
    teAddnlInfo->setReadOnly(true);
    teAddnlInfo->setMinimumHeight(320);
    auto dteImport = new QDateTimeEdit;
    dteImport->setReadOnly(true);
    dlgDeviceInfo->setLayout(lt);
    lt->addWidget(lblFName);
    lt->addWidget(lnedFn);
    lt->addWidget(lblFExt);
    lt->addWidget(lnedFe);
    lt->addWidget(lblFPath);
    lt->addWidget(teFilePath);
    lt->addWidget(lblFDateImport);
    lt->addWidget(dteImport);
    lt->addWidget(lblFAdditionalInfo);
    lt->addWidget(teAddnlInfo);

    lnedFn->setText(GceLogicLayer::getInstance().getSourceFileName(sourceID));
    lnedFe->setText(GceLogicLayer::getInstance().getSourceFileType(sourceID));
    teFilePath->setPlainText(GceLogicLayer::getInstance().getSourceFilePath(sourceID));
    teAddnlInfo->setPlainText(GceLogicLayer::getInstance().getAdditionalInfo(sourceID));
    dteImport->setDateTime(GceLogicLayer::getInstance().getSourceDateTime(sourceID));
    dteImport->setDisplayFormat("dd.MM.yyyy hh:mm:ss");
    dlgDeviceInfo->setWindowTitle(tr("Display data source properties"));

    dlgDeviceInfo->exec();
    dlgDeviceInfo->deleteLater();
}

void Device::slotShowSetTimeShiftDialog()
{
    auto days_LineEdit = new QLineEdit;
    auto validateLnedInt = new QIntValidator;
    validateLnedInt->setBottom(0);
    validateLnedInt->setTop(10000);

    days_LineEdit->setText("0");
    days_LineEdit->setValidator(validateLnedInt);

    auto hours_timeEdit = new QTimeEdit;
    hours_timeEdit->setDisplayFormat("hh");

    auto minutes_timeEdit = new QTimeEdit;
    minutes_timeEdit->setDisplayFormat("mm");

    auto seconds_timeEdit = new QTimeEdit;
    seconds_timeEdit->setDisplayFormat("ss");

    auto milliseconds_timeEdit = new QTimeEdit;
    milliseconds_timeEdit->setDisplayFormat("zzz");

    auto isForward_chk = new QCheckBox(tr("Forward time shift?"));

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);

    auto gridLt = new QGridLayout;
    gridLt->addWidget(new QLabel(tr("Days:")), 0, 0);
    gridLt->addWidget(days_LineEdit, 0, 1);
    gridLt->addWidget(new QLabel(tr("Hours:")), 1, 0);
    gridLt->addWidget(hours_timeEdit, 1, 1);
    gridLt->addWidget(new QLabel(tr("Minutes:")), 2, 0);
    gridLt->addWidget(minutes_timeEdit, 2, 1);
    gridLt->addWidget(new QLabel(tr("Seconds:")), 3, 0);
    gridLt->addWidget(seconds_timeEdit, 3, 1);
    gridLt->addWidget(new QLabel(tr("Milliseconds:")), 4, 0);
    gridLt->addWidget(milliseconds_timeEdit, 4, 1);

    auto lt = new QVBoxLayout;

    lt->addLayout(gridLt);
    lt->addWidget(isForward_chk);
    lt->addWidget(btns);

    auto dlg = new QDialog;
    dlg->setWindowTitle(tr("Data source data time set"));
    connect(btns, SIGNAL(accepted()), dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlg, SLOT(reject()));
    dlg->setLayout(lt);

    auto msecs_shift = GceLogicLayer::getInstance().getTimeShiftOfDataSource(sourceID);
    if (msecs_shift < 0)
    {
        isForward_chk->setChecked(false);
        msecs_shift *= -1LL;
    }
    else
    {
        isForward_chk->setChecked(true);
    }
    QDateTime tmpdt;
    tmpdt.setOffsetFromUtc(0);
    tmpdt.setTimeSpec(Qt::UTC);
    QDateTime epochStart(tmpdt);
    tmpdt = QDateTime::fromMSecsSinceEpoch(msecs_shift, Qt::UTC);
    epochStart = QDateTime::fromMSecsSinceEpoch(static_cast<int64_t>(0), Qt::UTC);
    auto daysCount = epochStart.daysTo(tmpdt);
    QTime tmpt = tmpdt.time();
    QTime dayStart = QTime::fromMSecsSinceStartOfDay(static_cast<int64_t>(0));
    auto secondsCountFull = dayStart.secsTo(tmpt);
    auto minutesCountFull = secondsCountFull / 60;
    auto hoursCount = minutesCountFull / 60;
    auto minutesResudie = minutesCountFull - hoursCount*60;
    auto secondsResudie = secondsCountFull - (hoursCount*60 - minutesResudie)*60;
    auto millisecondsCountFull = dayStart.msecsTo(tmpt);
    auto millisecondsResudie = millisecondsCountFull - ((hoursCount*60 - minutesResudie)*60 - secondsResudie)*1000;

    days_LineEdit->setText(QString::number(daysCount));
    auto hoursCountString = QString::number(hoursCount);
    if (hoursCountString.size() == 1)
    {
        hoursCountString.prepend("0");
    }
    hours_timeEdit->setTime(QTime::fromString(hoursCountString, QString("hh")));
    auto minutesResudieString = QString::number(minutesResudie);
    if (minutesResudieString.size() == 1)
    {
        minutesResudieString.prepend("0");
    }
    minutes_timeEdit->setTime(QTime::fromString(minutesResudieString, QString("mm")));
    auto secondsResudieString = QString::number(secondsResudie);
    if (secondsResudieString.size() == 1)
    {
        secondsResudieString.prepend("0");
    }
    seconds_timeEdit->setTime(QTime::fromString(secondsResudieString, QString("ss")));
    milliseconds_timeEdit->setTime(QTime::fromMSecsSinceStartOfDay(millisecondsResudie));

    auto result = dlg->exec();
    if (result == QDialog::Accepted)
    {
        QDateTime summ(epochStart);
        summ = summ.addMSecs(milliseconds_timeEdit->text().toLongLong());
        int64_t seconds = seconds_timeEdit->text().toLongLong()
                + 60LL * (minutes_timeEdit->text().toLongLong()
                + 60LL *(hours_timeEdit->text().toLongLong() /*+ 5LL*/
                + 24LL*days_LineEdit->text().toLongLong()));
        summ = summ.addSecs(seconds);
        auto millisecondsShift_res = summ.toMSecsSinceEpoch();
        if (!isForward_chk->isChecked())
        {
            millisecondsShift_res *= -1LL;
        }
        GceLogicLayer::getInstance().setTimeShiftOfDataSource(sourceID, millisecondsShift_res);
    }

    dlg->deleteLater();
}

void Device::slotAddPlotForDevice()
{
    auto project = qobject_cast<Project*>(rootItem->getProjectPointer());
    project->slotForAddPlotFromTemplateByDevNameInTemplateAtDeviceSubmenu(
                rootItem->getNameElement()
                );
}

void Device::slotShowRenameDialog()
{
    auto dlgRename = new QDialog;
    auto lt = new QVBoxLayout;
    auto lnedRenameFromDSfileName = new QLineEdit;
    auto cmbRenameFromDevNamesOfPlots = new QComboBox;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel, dlgRename);
    dlgRename->setLayout(lt);
    lt->addWidget(new QLabel(tr("You may choose data source name from list:")));
    lt->addWidget(cmbRenameFromDevNamesOfPlots);
    lt->addWidget(new QLabel(tr("or you may change it manually:")));
    lt->addWidget(lnedRenameFromDSfileName);
    lnedRenameFromDSfileName->setMinimumWidth(50);
    QString oldName = getItem()->getNameElement();
    lnedRenameFromDSfileName->setText(getItem()->getNameElement());
    lt->addWidget(btns);
    connect(btns, SIGNAL(accepted()), dlgRename, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlgRename, SLOT(reject()));

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
    auto dv = qobject_cast<GceDataView*>(o);
    QStringList devNamesFromPlots;
    cmbRenameFromDevNamesOfPlots->addItem(QString(""));
    for (SpecialWidgetForPlot *swp : dv->getPlotWidgetsList())
    {
        for (QString &devName : swp->getDevNames())
        {
            if (devNamesFromPlots.contains(devName)) continue;
            devNamesFromPlots << devName;
        }
    }
    cmbRenameFromDevNamesOfPlots->addItems(devNamesFromPlots);
    cmbRenameFromDevNamesOfPlots->setEditable(false);

    dlgRename->setWindowTitle(tr("Renaming data source ..."));

    auto project = qobject_cast<Project*>(getItem()->getProjectPointer());

    auto result = dlgRename->exec();
    if (result == QDialog::Accepted)
    {
        auto nameNew = QString();
        auto okChangeName = true;
        if (cmbRenameFromDevNamesOfPlots->currentIndex() == 0)
        {
            if (getItem()->getNameElement() == lnedRenameFromDSfileName->text())
            {
                okChangeName = false;
            }
            nameNew = lnedRenameFromDSfileName->text();
        }
        else
        {
            if (getItem()->getNameElement() == cmbRenameFromDevNamesOfPlots->currentText())
            {
                okChangeName = false;
            }
            nameNew = cmbRenameFromDevNamesOfPlots->currentText();
        }

        if (okChangeName)
        {
            auto sameNameExist = false;
            for (Device *dev : project->getDevicesMapPtrInProject()->values())
            {
                if (dev == this)
                {
                    continue;
                }
                if (dev->getItem()->getNameElement() == nameNew)
                {
                    sameNameExist = true;
                }
            }
            if (sameNameExist)
            {
                QMessageBox::critical(nullptr, tr("Error"),
                                      tr("Data source with the same name is present in data sources list. Please, run this dialog again and input different name.")
                                      .append(tr("\n(Context menu \"Rename data source\" of item in devices list)")));
            }
            else
            {
                if (nameNew.isEmpty())
                {
                    QMessageBox::critical(nullptr, tr("Error"),
                                          tr("Data source name can't be empty"));
                }
                else
                {
                    getItem()->setNewNameElement(nameNew);
                    GceLogicLayer::getInstance().setDeviceName(sourceID, nameNew);
                    if (devNamesFromPlots.contains(oldName))
                    {
                        emit sigDeviceRenamed(nameNew);
                    }
                    if (devNamesFromPlots.contains(nameNew))
                    {
                        emit sigDeviceRenamedForEmptyPlotDeviceElements(nameNew, oldName, sourceID);
                    }
                }
            }
        }
        else
        {
            if (devNamesFromPlots.contains(nameNew))
            {
                emit sigDeviceRenamedForEmptyPlotDeviceElements(nameNew, oldName, sourceID);
            }
        }
    }

    dlgRename->deleteLater();
}

void Device::slotDeleteDevice()
{
    QString msg = tr("Do you really want to delete data source ")
            .append(getItem()->getNameElement())
            .append(tr(" from project?"))
            .append("\n").append(tr("This action is irreversible!"));

    auto result = QMessageBox::question(nullptr, tr("Data source delete"), msg);
    if (result != QMessageBox::Yes)
    {
        return;
    }

    emit sigRemove_Device(getItem()->getNameElement(), getSourceID());
    disconnect();

    parentItem->takeChild(parentItem->indexOfChild(getItem()));
    deleteLater();
}
