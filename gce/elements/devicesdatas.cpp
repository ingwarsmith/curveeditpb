#include "project.h"
#include "devicesdatas.h"
#include "device.h"
#include "curve.h"

#include "gcelogiclayer.h"

#include <QApplication>

DevicesDatas::DevicesDatas(uint32_t projectIdx, QObject *prjPtr, QObject *parent) : QObject(parent)
{
    next_device_idx = 0;
    idxProject = projectIdx;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementDevicesData);
    rootItem->setNewNameElement(tr("Data sources data"));
    addDeviceAction = new QAction(tr("Add data source (import data)"));
    connect(addDeviceAction, SIGNAL(triggered(bool)), this, SLOT(slotStartImportDialog()));

    rootItem->setContextMenuActions(getActionsList());

    projectHdrPath = rootItem->getProjectHeaderPath();
}

TreeElement *DevicesDatas::getItem()
{
    return rootItem;
}

void DevicesDatas::setParentForItem(TreeElement *elementParent)
{
    parentItem = elementParent;
}

void DevicesDatas::addDevice(Device *devNew)
{
    devices.insert(devNew->getSourceID(), devNew);
    rootItem->addSubElement(devNew->getItem());
}

Device *DevicesDatas::createNewDeviceObject(uint32_t srcId)
{
    auto newDev = new Device(idxProject, srcId, this->getItem()->getProjectPointer(), this->parent());
    newDev->setParentForItem(rootItem);
    addDevice(newDev);
    return newDev;
}

void DevicesDatas::removeDeviceByItem(TreeElement *devItem)
{
    Q_UNUSED(devItem);
}

QList<QAction *> DevicesDatas::getActionsList()
{
    QList<QAction*> ret;
    ret << addDeviceAction;
    return ret;
}

void DevicesDatas::clearAll()
{
    auto cnt = rootItem->childCount();
    for (decltype (cnt) dc = 0; dc < cnt; ++dc)
    {
        rootItem->removeChild(rootItem->child(0));
    }
    devices.clear();
}

void DevicesDatas::getChildsOfHierarchyElement(HierarchyMember *elementHierarchy, Device *dev, TreeElement *base,
                                                                     QList< QPair<uint32_t, uint32_t> > crvuids_srcs,
                                                                        int32_t indexForInsert)
{
    auto sourceID = dev->getSourceID();

    TreeElement *list = new TreeElement(dev->getThisProjectIndex(), nullptr, dev->getItem()->getProjectPointer());
    list->setNewNameElement(QString::fromStdString(elementHierarchy->name));
    if (indexForInsert == -1)
    {
        base->addSubElement(list);
    }
    else
    {
        if (elementHierarchy->is_flag)
        {
            auto childrenNum = base->childCount();
            for (decltype (childrenNum) idxChild = 0; idxChild < childrenNum; ++idxChild)
            {
                if (base->child(idxChild)->text(0) == QString::fromStdString(elementHierarchy->name))
                {
                    auto treeElement4remove = static_cast<TreeElement*>(base->child(idxChild));
                    base->removeSubElement(treeElement4remove);
                    break;
                }
            }
        }
        base->insertSubElement(list, indexForInsert);
    }
    if (elementHierarchy->can_drag)
    {
        list->setType(TreeElement::ElementType::elementArrayType);
    }
    else
    {
        list->setType(TreeElement::ElementType::elementHierarchy);
    }
    if (elementHierarchy->curves_names.size())
    {
        for (auto &elem : crvuids_srcs)
        {
            if (elem.second != sourceID)
            {
                continue;
            }
            auto curveID = elem.first;
            auto success = false;
            QString nameCurve = GceLogicLayer::getInstance().getCurveStringIdByCurveId(sourceID, curveID, success);
            auto it_eh = std::find_if(elementHierarchy->curves_names.cbegin(),
                                      elementHierarchy->curves_names.cend(),
                                      [nameCurve](const std::string &v) { return v == nameCurve.toStdString(); });
            if (it_eh == elementHierarchy->curves_names.cend())
            {
                continue;
            }
            auto curveNew = dev->createCurve(curveID, list);
            curveNew->setSourceID(sourceID);
            auto loggingCurveLoaded = GceLogicLayer::getInstance()
                    .isLoggingCurveLoaded(sourceID, curveID, success);
            if (success && loggingCurveLoaded)
            {
                curveNew->setCurveType_RAW();
            }
            curveNew->setCurveName(nameCurve);
        }
    }
    QVector<HierarchyMember> subElemsHierarchy = GceLogicLayer::getInstance().getHierarchySubElementsOf(sourceID, *elementHierarchy);
    for (HierarchyMember &elemHierarchy : subElemsHierarchy)
    {
        if (!elementHierarchy->curves_names.size()) getChildsOfHierarchyElement(&elemHierarchy, dev, list, crvuids_srcs);
        else
        {
            auto it_he = std::find_if(elementHierarchy->curves_names.cbegin(),
                                      elementHierarchy->curves_names.cend(),
                                      [elemHierarchy](const std::string & v) { return v == elemHierarchy.name; });
            if (it_he == elementHierarchy->curves_names.cend())
            {
                getChildsOfHierarchyElement(&elemHierarchy, dev, list, crvuids_srcs);
            }
            else
            {
                auto idx_Inserting = std::distance(elementHierarchy->curves_names.cbegin(), it_he);
                getChildsOfHierarchyElement(&elemHierarchy, dev, list, crvuids_srcs, idx_Inserting);
            }
        }
    }
}

void DevicesDatas::extractDataFromSource(bool afterImport)
{
    auto curvesIdsWithSrcIds = GceLogicLayer::getInstance().getCurvesIdsWithSourcesIds();
    if (curvesIdsWithSrcIds.isEmpty())
    {
        return;
    }

    auto project = qobject_cast<Project*>(getItem()->getProjectPointer());

    QList<uint> sourcesIds = GceLogicLayer::getInstance().getSourcesIds();

    for (uint32_t sourceID : sourcesIds)
    {
        auto existAlready = false;
        for (Device *dv : devices.values())
        {
            if (dv->getSourceID() == sourceID)
            {
                existAlready = true;
                break;
            }
        }
        if (existAlready)
        {
            continue;
        }

        auto devNew = createNewDeviceObject(sourceID);
        auto devName = GceLogicLayer::getInstance().getDeviceName(sourceID);
        if (!devName.isEmpty())
        {
            devNew->getItem()->setNewNameElement(devName);
        }
        auto indexedFileDataSourceExist =
                GceLogicLayer::getInstance().checkExistingImportedFileForDataSource(sourceID);

        if (!indexedFileDataSourceExist)
        {
            auto txtColorGray = QColor(Qt::gray);
            devNew->getItem()->setForeground(0, QBrush(txtColorGray));
        }

        HierarchyMember rootOfDevice = GceLogicLayer::getInstance().getHierarchyRootElement(sourceID);
        if (rootOfDevice.curves_names.size())
        {
            for (auto &elem : curvesIdsWithSrcIds)
            {
                if (elem.second != sourceID)
                {
                    continue;
                }
                auto curveID = elem.first;
                auto success = false;
                QString nameCurve = GceLogicLayer::getInstance().getCurveStringIdByCurveId(sourceID, curveID, success);
                auto it_rd = std::find(rootOfDevice.curves_names.cbegin(), rootOfDevice.curves_names.cend(), nameCurve.toStdString());
                if (it_rd == rootOfDevice.curves_names.cend())
                {
                    continue;
                }
                auto curveNew = devNew->createCurve(curveID);
                curveNew->setSourceID(sourceID);
                auto loggingCurveLoaded =
                        GceLogicLayer::getInstance().isLoggingCurveLoaded(sourceID, curveID, success);
                if (success && loggingCurveLoaded)
                {
                    curveNew->setCurveType_RAW();
                }
                curveNew->setCurveName(nameCurve);
            }
        }

        QVector<HierarchyMember> subElemsHierarchy = GceLogicLayer::getInstance().getHierarchySubElementsOf(sourceID, rootOfDevice);
        for (HierarchyMember &elemHierarchy : subElemsHierarchy)
        {
            getChildsOfHierarchyElement(&elemHierarchy, devNew, devNew->getItem(), curvesIdsWithSrcIds);
        }

        if (afterImport)
        {
            devNew->slotShowRenameDialog();
            getItem()->setExpanded(true);

            project->slotForAddPlotFromTemplateByDevNameInTemplate(devNew->getItem()->getNameElement());
        }
    }

    if (afterImport)
    {
        project->slotSaveProject();
    }
}

void DevicesDatas::slotStartImportDialog()
{
    projectHdrPath = rootItem->getProjectHeaderPath();
    auto success = false;
    uint32_t resultIndex = GceLogicLayer::getInstance().callImportDialog(success, projectHdrPath);
    if (!success)
    {
        return;
    }
    Q_UNUSED(resultIndex)
    extractDataFromSource(true);
}
