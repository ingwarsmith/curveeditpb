#include "plot.h"

#include <QHBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QMessageBox>

Plot::Plot(uint32_t projectIdx, uint32_t indexInListOfPlots, QObject *prjPtr, QObject *parent) : QObject(parent)
{
    idxProject = projectIdx;
    sourceID = 0;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementView);

    auto num = indexInListOfPlots + 1;
    idxPlot = num;
    rootItem->setNewNameElement(tr("Plot").append("_").append(QString::number(num)));

    addTrackAction = new QAction(tr("Add track"));
    swapAxesAction = new QAction(tr("Swap X and Y axis of tracks"));
    selectCurvesForPlotAction = new QAction(tr("Select curves for plot"));
    loadDataOfSelectedCurvesAction = new QAction(tr("Load data of selected curves from source"));
    renamePlot = new QAction(tr("Rename plot"));
    saveAsPdf = new QAction(tr("Save plot as PDF file"));
    saveAsBmp = new QAction(tr("Save plot as BMP file"));
    removePlot = new QAction(tr("Delete plot"));
    saveStandalonePlotFile = new QAction(tr("Save plot as plot template"));
    changeLinkToDataOfDeviceInPlot = new QAction(tr("Change linked data for device in plot"));
    editPlotHeaderString = new QAction(tr("Edit plot header string"));
    rootItem->setContextMenuActions(getActionsList());

    connect(swapAxesAction, SIGNAL(triggered(bool)), this, SIGNAL(sigSwapAxesOfPlot()));
    connect(addTrackAction, SIGNAL(triggered(bool)), this, SIGNAL(sigAddNewTrack()));
    connect(selectCurvesForPlotAction, SIGNAL(triggered(bool)), this, SIGNAL(sigSelectCurvesForPlot()));
    connect(loadDataOfSelectedCurvesAction, SIGNAL(triggered(bool)), this, SIGNAL(sigLoadDataOfSelectedCurves()));
    connect(renamePlot, SIGNAL(triggered(bool)), this, SLOT(slotRenamePlot()));
    connect(saveAsPdf, SIGNAL(triggered(bool)), this, SIGNAL(sigSaveAsPDF()));
    connect(saveAsBmp, SIGNAL(triggered(bool)), this, SIGNAL(sigSaveAsBMP()));
    connect(removePlot, SIGNAL(triggered(bool)), this, SIGNAL(sigRemovePlot()));
    connect(saveStandalonePlotFile, SIGNAL(triggered(bool)), this,  SIGNAL(sigSaveStandalonePlotTemplate()));
    connect(changeLinkToDataOfDeviceInPlot, SIGNAL(triggered(bool)), this, SIGNAL(sigChangeLinkToDataOfDeviceInPlot()));
    connect(editPlotHeaderString,SIGNAL(triggered(bool)), this, SIGNAL(sigEditPlotHeaderString()));
}

TreeElement *Plot::getItem()
{
    return rootItem;
}

void Plot::setParentForItem(TreeElement *elementParent)
{
    parentItem = elementParent;
}

QList<QAction *> Plot::getActionsList()
{
    QList<QAction*> ret;
    ret << addTrackAction //<< selectCurvesForPlotAction
        << renamePlot
        << editPlotHeaderString
        << loadDataOfSelectedCurvesAction // << swapAxesAction;
        << saveAsPdf
        << saveAsBmp
        << saveStandalonePlotFile
        << changeLinkToDataOfDeviceInPlot
        << removePlot;
    return ret;
}

void Plot::slotRenamePlot()
{
    auto dlgRename = new QDialog;
    auto lt = new QHBoxLayout;
    auto lnedRename = new QLineEdit;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel, dlgRename);
    dlgRename->setLayout(lt);
    lt->addWidget(lnedRename);
    lnedRename->setMinimumWidth(50);
    lnedRename->setText(getItem()->getNameElement());
    lt->addWidget(btns);
    connect(btns, SIGNAL(accepted()), dlgRename, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlgRename, SLOT(reject()));
    dlgRename->setWindowTitle(tr("Renaming plot ..."));
    auto result = dlgRename->exec();
    if (result == QDialog::Accepted && getItem()->getNameElement() != lnedRename->text())
    {
        auto nameNew = lnedRename->text();
        if (nameNew.isEmpty())
        {
            QMessageBox::critical(nullptr, tr("Error"),
                                  tr("Data source name can't be empty"));
        }
        else
        {
            auto plotsCount = parentItem->childCount();
            QStringList tmpSl;
            for (decltype (plotsCount) p = 0; p < plotsCount; ++p)
            {
                tmpSl.append(parentItem->child(p)->text(0));
            }

            if (tmpSl.contains(nameNew))
            {
                QMessageBox::critical(nullptr, tr("Error"),
                                      tr("There is plot with the same name!"));
            }
            else
            {
                getItem()->setNewNameElement(nameNew);
                emit sigRenamedPlot(nameNew);
            }
        }
    }

    dlgRename->deleteLater();
}
