#ifndef CURVE_H
#define CURVE_H

#include <QObject>
#include "treeelement.h"

class Curve : public QObject
{
    Q_OBJECT
public:
    explicit Curve(uint32_t projectIdx, uint32_t indexInDevice, QObject *prjPtr, QObject *parent = nullptr);
    void     setCurveID(uint32_t id_curve) { curveID = id_curve; }
    uint32_t     getThisProjectIndex() { return idxProject; }

    TreeElement *getItem() const;
    TreeElement *getParentForItem() { return parentItem; }
    void        setParentForItem(TreeElement *elementParent);
    void        setSourceID(uint32_t srcId) { sourceID = srcId; }
    void        setCurveType_RAW();
    void        setCurveType_CALCULATED();
    void        setCurveType_CHANGED();
    void        setCurveType_CurveLinkInTrack();
    void        setCurveName(QString nm);

    uint32_t        getCurveID()  { return curveID; }
    uint32_t        getSourceID()  { return sourceID; }
    QString     getCurveName() const;

    void        setBaseCurveName_withinTrack(QString bsnm)
    {
        baseCurveNameInTrackList = bsnm;
    }
    QString     getBaseCurveName_withinTrack()
    {
        return baseCurveNameInTrackList;
    }
    void        setLinkingToDeviceName(bool yes);

private:
    uint32_t     idxProject;
    uint32_t     indexCurveInDevice;
    uint32_t    curveID;
    uint32_t    sourceID;
    TreeElement *deviceItem;
    TreeElement *rootItem;
    TreeElement *parentItem;
    QString     baseCurveNameInTrackList;

signals:
    void        sigDeviceNameChanged(QString oldDevName, QString newDevName);
    void        sigRescaleValuesAxis();
    void        sigDeleteFromTrack();
    void        sigChangeCurveColor();
    void        sigForMoveToAnotherTrack();

private slots:
    void        slotChangeAffixDeviceName(QString newDevName);

public slots:
};

#endif // CURVE_H
