#ifndef PLOTS_H
#define PLOTS_H

#include <QObject>
#include "treeelement.h"
#include "plot.h"

class Plots : public QObject
{
    Q_OBJECT
public:
    explicit    Plots(uint32_t projectIdx, QObject *prjPtr, QObject *parent = nullptr);

    void        setProjectIndex(uint32_t idx) { idxProject = idx; }
    uint32_t        getThisProjectIndex() { return idxProject; }

    TreeElement *getItem();
    void        setParentForItem(TreeElement *elementParent);
    void        addPlot(Plot *plotNew);
    uint32_t    getNextNewPlotIndex();

    bool        existPlotWithName(QString namePlot);

    QList<QAction *> getActionsList();

    void        clearAll();

private:
    uint32_t    idxProject;
    TreeElement *rootItem;
    TreeElement *parentItem;
    QAction     *addPlotAction;
    QAction     *openPlotTemplate;
    QAction     *joinPlotsByTime;
    QMap<uint32_t, TreeElement*> plots;
    uint32_t    next_plot_idx;

signals:
    void        sigAddPlot();
    void        sigOpenPlotTemplate();
    void        sigForJoinPlotsByTime();

//private slots:
//    void        slotMovePlotItemToIndexInList(int newIndex, int oldIdx);

public slots:
};

#endif // PLOTS_H
