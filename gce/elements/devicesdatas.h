#ifndef DEVICESDATAS_H
#define DEVICESDATAS_H

#include <QObject>
#include "treeelement.h"

struct HierarchyMember;

class Device;

class DevicesDatas : public QObject
{
    Q_OBJECT
public:
    explicit    DevicesDatas(uint32_t projectIdx, QObject *prjPtr, QObject *parent = nullptr);

    void        setProjectIndex(uint32_t idx) { idxProject = idx; }
    uint32_t        getThisProjectIndex() { return idxProject; }

    TreeElement *getItem();
    void        setParentForItem(TreeElement *elementParent);
    void        addDevice(Device *devNew);
    Device      *createNewDeviceObject(uint32_t srcId = 0);
    void        removeDeviceByItem(TreeElement *devItem);
    void        extractDataFromSource(bool afterImport = false);

    QMap<uint32_t, Device*>        *getDeviceMapPtr() { return &devices; }

    QList<QAction*> getActionsList();

    void        clearAll();

private:
    uint32_t        idxProject;
    TreeElement     *rootItem;
    TreeElement     *parentItem;
    QAction         *addDeviceAction;
    QMap<uint32_t, Device*>  devices;
    uint32_t        next_device_idx;
    QString         projectHdrPath;

    void /*QVector<HierarchyMember *>*/ getChildsOfHierarchyElement(HierarchyMember *elementHierarchy, Device *dev, TreeElement *base,
                                                           QList<QPair<uint32_t, uint32_t> > crvuids_srcs, int32_t indexForInsert = -1);

private slots:

signals:


public slots:
    void        slotStartImportDialog();
};

#endif // DEVICESDATAS_H
