#ifndef LABEL_H
#define LABEL_H

#include <QObject>
#include "treeelement.h"

class Track;

class Label : public QObject
{
    Q_OBJECT
public:
    explicit Label(uint32_t projectIdx, QObject *prjPtr, QObject *parent = nullptr);
    void        prepareToDeleting();
    void        setID(uint32_t id_label) { label_ID = id_label; }
    uint32_t    getThisProjectIndex() { return idxProject; }
    uint32_t    getID() { return label_ID; }
    void        setTrackID(uint32_t id_track) { trackID = id_track; }
    uint32_t    getTrackID() { return trackID; }
    void        setTrackPointer(Track *pointer);
    QString     text() const { return labelText; }
    void        setText(QString text);
    TreeElement *getItem() { return rootItem; }
    TreeElement *getParentItem() { return parentItem; }
    void        setParentForItem(TreeElement *elementParent)
    {
        parentItem = elementParent;
    }
    QList<QAction*> getActionsList();
    void        updateText();
    int         fontPixelSize;

private:
    uint32_t    label_ID;
    uint32_t    trackID;
    uint32_t    idxProject;
    TreeElement *rootItem;
    TreeElement *parentItem;

    Track       *trackPtr;

    QString     labelText;

    int         X_fromLabelPosition;
    int         Y_fromTrackTop;

    QAction     *infoAction;
    QAction     *changeTextAction;
    QAction     *gotoLabelAction;
    QAction     *removeLabelAction;
    QAction     *moveLabelAction;
    QAction     *copyLabelAction;
    QAction     *copyLabelToAnotherTrackAction;
    QAction     *editLabelProperties;

signals:
    void        sigGoToLabel();
    void        sigChangeText();
    void        sigHideText();
    void        sigShowText();
    void        sigHide();
    void        sigShow();
    void        sigRemoveLabel();
    void        sigMoveLabel();
    void        sigCopyLabel();
    void        sigCopyLabelToAnotherTrack();
    void        sigEditProperties();
    void        sigSetTextFontPixelSize(int sz);

private slots:
    void        slotInfo();
};

#endif // LABEL_H
