#ifndef PLOT_H
#define PLOT_H

#include <QObject>
#include "treeelement.h"

class Plot : public QObject
{
    Q_OBJECT
public:
    explicit Plot(uint32_t projectIdx, uint32_t indexInListOfPlots, QObject *prjPtr, QObject *parent = nullptr);

    void        setProjectIndex(uint32_t idx) { idxProject = idx; }
    uint32_t    getThisProjectIndex() { return idxProject; }

    TreeElement *getItem();
    void        setParentForItem(TreeElement *elementParent);
    TreeElement *getParentItem()
    {
        return parentItem;
    }
    void        setSourceID(uint32_t srcId) { sourceID = srcId; }
    uint32_t        getSourceID() { return sourceID; }
    uint32_t        getPlotIndex() { return idxPlot; }

    QList<QAction *> getActionsList();

private:
    uint32_t    idxProject;
    uint32_t    idxPlot;
    uint32_t    sourceID;
    TreeElement *rootItem;
    TreeElement *parentItem;
    QAction     *addTrackAction;
    QAction     *swapAxesAction;
    QAction     *selectCurvesForPlotAction;
    QAction     *loadDataOfSelectedCurvesAction;
    QAction     *saveStandalonePlotFile;
    QAction     *changeLinkToDataOfDeviceInPlot;
    QAction     *renamePlot;
    QAction     *saveAsPdf;
    QAction     *saveAsBmp;
    QAction     *removePlot;
    QAction     *editPlotHeaderString;

signals:
    void        sigSwapAxesOfPlot();
    void        sigAddNewTrack();
    void        sigSelectCurvesForPlot();
    void        sigLoadDataOfSelectedCurves();
    void        sigSavePlotInProject(QString projectDataDirPath);
    void        sigExportPlotToFile(QString fileNameWithPath);
    void        sigRenamedPlot(QString newName);
    void        sigSaveAsPDF();
    void        sigSaveAsBMP();
    void        sigRemovePlot();
    void        sigSaveStandalonePlotTemplate();
    void        sigChangeLinkToDataOfDeviceInPlot();
    void        sigMovePlotItemToIndexInList(int newIndex, int oldIdx);
    void        sigEditPlotHeaderString();

public slots:

private slots:
    void        slotRenamePlot();
};

#endif // PLOT_H
