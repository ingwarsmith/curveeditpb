#include "label.h"
#include "track.h"
#include "gcedataview.h"

Label::Label(uint32_t projectIdx, QObject *prjPtr, QObject *parent) : QObject(parent)
{
    idxProject = projectIdx;
    label_ID = 0;
    trackID = 0;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementLabel);

    changeTextAction = new QAction(tr("Change label text"));
    gotoLabelAction = new QAction(tr("Go to label"));

    connect(changeTextAction, SIGNAL(triggered(bool)), this, SIGNAL(sigChangeText()));
    connect(gotoLabelAction, SIGNAL(triggered(bool)), this, SIGNAL(sigGoToLabel()));

    removeLabelAction = new QAction(tr("Delete label"));
    moveLabelAction = new QAction(tr("Move label"));
    copyLabelAction = new QAction(tr("Copy label"));
    copyLabelToAnotherTrackAction = new QAction(tr("Copy label to another track"));
    editLabelProperties= new QAction(tr("Edit label properties"));

    connect(removeLabelAction, &QAction::triggered, this, &Label::sigRemoveLabel);
    connect(moveLabelAction, &QAction::triggered, this, &Label::sigMoveLabel);
    connect(copyLabelAction, &QAction::triggered, this, &Label::sigCopyLabel);
    connect(copyLabelToAnotherTrackAction, &QAction::triggered, this, &Label::sigCopyLabelToAnotherTrack);
    connect(editLabelProperties, &QAction::triggered, this, &Label::sigEditProperties);

    infoAction = new QAction(tr("Information"));
    connect(infoAction, &QAction::triggered, this, &Label::slotInfo);

    rootItem->setContextMenuActions(getActionsList());
}

void Label::prepareToDeleting()
{
    auto actionsList = getActionsList();
    while (actionsList.size())
    {
        auto action = actionsList.takeFirst();
        delete action;
    }
}

void Label::setTrackPointer(Track *pointer)
{
    trackPtr = pointer;
}

void Label::setText(QString text)
{
    labelText = text;
    QString elementText = QString::number(label_ID).append(": ");

    if (text.contains("\n"))
    {
        QStringList lines = text.split("\n");
        bool first = true;
        QString p_;
        for (QString &line : lines)
        {
            if (first)
            {
                first = false;
                auto szl = elementText.length();
                for (decltype (szl) i = 0; i < szl; ++i)
                {
                    p_.append(" ");
                }
                elementText.append(line).append("\n");
            }
            else
            {
                elementText.append(p_).append(line).append("\n");
            }
        }
        elementText.remove(elementText.size()-1, 1); // last '\n'
    }
    else
    {
        elementText.append(text);
    }
    rootItem->setNewNameElement(elementText);
}

QList<QAction *> Label::getActionsList()
{
    QList<QAction *> lst;
    lst << infoAction << changeTextAction << gotoLabelAction
        << moveLabelAction
        << copyLabelAction << copyLabelToAnotherTrackAction
        << editLabelProperties
           << removeLabelAction
              ;
    return lst;
}

void Label::updateText()
{
    setText(labelText);
}

void Label::slotInfo()
{
    QString info = trackPtr->getItem()->getNameElement().prepend(tr("Track: ")).append(", ")
            .append("Plot: ").append(trackPtr->getPLotPtr()->getPlot()->getItem()->getNameElement());
    QMessageBox::information(nullptr, tr("Information"), info);
}
