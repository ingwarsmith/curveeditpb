#include "listlabels.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QLabel>

ListLabels::ListLabels(uint32_t projectIdx, QObject *prjPtr, QObject *parent) : QObject(parent)
{
    maxKey = 0UL;
    idxProject = projectIdx;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementListLabels);
    rootItem->setNewNameElement(tr("Labels"));
    hideLabelsAction = new QAction(tr("Hide labels"));
    showLabelsAction = new QAction(tr("Show labels"));
    hideLabelsTextAction = new QAction(tr("Hide labels texts"));
    showLabelsTextAction = new QAction(tr("Show labels texts"));

    changeLabelsFontSize = new QAction(tr("Change font size"));

    hideLabelsAction->setCheckable(true);
    showLabelsAction->setCheckable(true);
    hideLabelsTextAction->setCheckable(true);
    showLabelsTextAction->setCheckable(true);

    showLabelsAction->setChecked(true);
    showLabelsTextAction->setChecked(true);
    labelsHided = false;
    labelsTextsHided = false;

    connect(hideLabelsTextAction, SIGNAL(triggered(bool)), this, SIGNAL(sigHideTextLabels()));
    connect(showLabelsTextAction, SIGNAL(triggered(bool)), this, SIGNAL(sigShowTextLabels()));
    connect(hideLabelsAction, SIGNAL(triggered(bool)), this, SIGNAL(sigHideLabels()));
    connect(showLabelsAction, SIGNAL(triggered(bool)), this, SIGNAL(sigShowLabels()));

    connect(hideLabelsTextAction, SIGNAL(triggered(bool)), this, SLOT(slotSetUnsetChecked()));
    connect(showLabelsTextAction, SIGNAL(triggered(bool)), this, SLOT(slotSetUnsetChecked()));
    connect(hideLabelsAction, SIGNAL(triggered(bool)), this, SLOT(slotSetUnsetChecked()));
    connect(showLabelsAction, SIGNAL(triggered(bool)), this, SLOT(slotSetUnsetChecked()));

    fontPixelSizeLabels = 12;

    connect(changeLabelsFontSize, SIGNAL(triggered(bool)), this, SLOT(slotChangeLabelsFontPixelSize()));

    rootItem->setContextMenuActions(getActionsList());
}

Label *ListLabels::createLabel()
{
    Label *lblNew = new Label(idxProject, rootItem->getProjectPointer());
    auto l_ks = labels.keys();

    if (l_ks.isEmpty())
        maxKey = 1UL;
    else
    {
        for (Label *l : labels.values())
        {
            if (maxKey < l->getID())
            {
                maxKey = l->getID();
            }
        }
        maxKey++;
    }
    labels.insert(maxKey, lblNew);
    lblNew->setID(maxKey);
    lblNew->setText(QString());
    rootItem->addSubElement(lblNew->getItem());
    lblNew->setParentForItem(rootItem);
    connect(this, SIGNAL(sigHideTextLabels()), lblNew, SIGNAL(sigHideText()));
    connect(this, SIGNAL(sigShowTextLabels()), lblNew, SIGNAL(sigShowText()));
    connect(this, SIGNAL(sigHideLabels()), lblNew, SIGNAL(sigHide()));
    connect(this, SIGNAL(sigShowLabels()), lblNew, SIGNAL(sigShow()));
    connect(this, SIGNAL(sigSetTextFontPixelSize(int)), lblNew,SIGNAL(sigSetTextFontPixelSize(int)));
    lblNew->fontPixelSize = fontPixelSizeLabels;
    return lblNew;
}

QList<QAction *> ListLabels::getActionsList()
{
    QList<QAction *> l;
    l << hideLabelsAction << showLabelsAction << hideLabelsTextAction << showLabelsTextAction
      << changeLabelsFontSize;
    return l;
}

void ListLabels::removeLabelById(uint32_t id)
{
    labels.remove(id);
}

void ListLabels::clearAll()
{
    auto cnt = rootItem->childCount();
    for (decltype (cnt) dc = 0; dc < cnt; ++dc)
    {
        rootItem->removeChild(rootItem->child(0));
    }
    labels.clear();
}

void ListLabels::slotSetUnsetChecked()
{
    auto action = qobject_cast<QAction*>(sender());

    if (action == showLabelsAction)
    {
        if (hideLabelsAction->isChecked())
            hideLabelsAction->setChecked(false);

        if (!labelsHided)
            showLabelsAction->setChecked(true);

        labelsHided = false;
    }
    else if (action == hideLabelsAction)
    {
        if (showLabelsAction->isChecked())
            showLabelsAction->setChecked(false);

        if (labelsHided)
            hideLabelsAction->setChecked(true);

        labelsHided = true;
    }
    else if (action == showLabelsTextAction)
    {
        if (hideLabelsTextAction->isChecked())
            hideLabelsTextAction->setChecked(false);

        if (!labelsTextsHided)
            showLabelsTextAction->setChecked(true);

        labelsTextsHided = false;
    }
    else if (action == hideLabelsTextAction)
    {
        if (showLabelsTextAction->isChecked())
            showLabelsTextAction->setChecked(false);

        if (labelsTextsHided)
            hideLabelsTextAction->setChecked(true);

        labelsTextsHided = true;
    }
}

void ListLabels::slotChangeLabelsFontPixelSize()
{
    QDialog dlg;
    dlg.setWindowTitle(tr("Change font size for labels"));
    dlg.setMinimumWidth(200);

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    connect(btns, &QDialogButtonBox::accepted, &dlg, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, &dlg, &QDialog::reject);

    auto lned= new QLineEdit;
    auto ivld = new QIntValidator(5, 100);
    lned->setValidator(ivld);
    lned->setText(QString::number(fontPixelSizeLabels));

    auto lt = new QVBoxLayout;
    lt->addWidget(lned);
    lt->addWidget(btns);

    dlg.setLayout(lt);

    if (dlg.exec() == QDialog::Accepted)
    {
        fontPixelSizeLabels = lned->text().toInt();
        emit sigSetTextFontPixelSize(fontPixelSizeLabels);
    }
}
