#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include "treeelement.h"

class Curve;

class Device : public QObject
{
    Q_OBJECT
public:
    explicit Device(uint32_t projectIdx, uint32_t indexInDevicesBlock, QObject *prjPtr, QObject *parent = nullptr);

    void        setProjectIndex(uint32_t idx) { idxProject = idx; }
    uint32_t    getThisProjectIndex() { return idxProject; }

    TreeElement *getItem();
    void        setParentForItem(TreeElement *elementParent);
    void        setSourceID(uint srcId) { sourceID = srcId; }
    uint32_t    getSourceID() { return sourceID; }
    Curve       *createCurve(uint curveID, TreeElement *base = nullptr/*, TreeElement *addedElem = nullptr*/);

    QMap<uint32_t, Curve*>        *getCurvesMapPtr() { return &curvesMap; }

    QList<QAction *> getActionsList();

private:
    uint32_t        idxProject;
    uint32_t        sourceID;
    TreeElement     *rootItem;
    TreeElement     *parentItem;
    QAction         *deviceInformation,
                    *renameDevice,
                    *deviceTimeShiftSet,
                    *openPlotWithDevice,
                    *deleteDevice;

    uint32_t        nextCurveIdx;
    QMap<uint32_t, Curve*> curvesMap;

signals:
    void sigDeviceRenamed(QString nameNew);
    void sigDeviceRenamedForEmptyPlotDeviceElements(QString nameNew, QString nameOld, uint32_t sourceID);
    void sigRemove_Device(QString devNm, uint32_t srcID);

private slots:
    void slotShowDialogWithSourceProperties();
    void slotShowSetTimeShiftDialog();
    void slotAddPlotForDevice();

public slots:
    void slotShowRenameDialog();
    void slotDeleteDevice();
};

#endif // DEVICE_H
