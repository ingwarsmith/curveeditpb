#include "plots.h"

Plots::Plots(uint32_t projectIdx, QObject *prjPtr, QObject *parent) : QObject(parent)
{
    idxProject = projectIdx;
    rootItem = new TreeElement(idxProject, nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementViews);
    rootItem->setNewNameElement(tr("Plots"));
    next_plot_idx = 0;

    addPlotAction = new QAction(tr("Add empty plot"));
    connect(addPlotAction, SIGNAL(triggered(bool)),this, SIGNAL(sigAddPlot()));

    openPlotTemplate = new QAction(tr("Add plot"));
    connect(openPlotTemplate, SIGNAL(triggered(bool)), this, SIGNAL(sigOpenPlotTemplate()));

    joinPlotsByTime = new QAction(tr("Join plots by time"));
    connect(joinPlotsByTime, SIGNAL(triggered(bool)), this, SIGNAL(sigForJoinPlotsByTime()));

    rootItem->setContextMenuActions(getActionsList());
}

TreeElement *Plots::getItem()
{
    return rootItem;
}

void Plots::setParentForItem(TreeElement *elementParent)
{
    parentItem = elementParent;
}

void Plots::addPlot(Plot *plotNew)
{
    if (plots.keys().isEmpty())
        next_plot_idx = 0;
    else
    {
        QList<uint32_t> ks = plots.keys();
        std::sort(ks.begin(), ks.end());
        next_plot_idx = ks.last();
    }
    next_plot_idx++;
    plots.insert(next_plot_idx, plotNew->getItem());
    //next_plot_idx++;
    rootItem->addSubElement(plotNew->getItem());
    plotNew->setParentForItem(rootItem);
}

uint32_t Plots::getNextNewPlotIndex()
{
    return next_plot_idx;
}

bool Plots::existPlotWithName(QString namePlot)
{
    for (auto it = plots.values().cbegin(); it != plots.values().cend(); ++it)
    {
        if ((*it)->getNameElement() == namePlot)
        {
            return true;
        }
    }
    return false;
}

QList<QAction *> Plots::getActionsList()
{
    QList<QAction *> lsret;
    lsret << openPlotTemplate
          << joinPlotsByTime;
    return lsret;
}

void Plots::clearAll()
{
    auto cnt = rootItem->childCount();
    for (decltype (cnt) dc = 0; dc < cnt; ++dc)
    {
        rootItem->removeChild(rootItem->child(0));
    }
    plots.clear();
}
