#ifndef PROJECTTREEWIDGET_H
#define PROJECTTREEWIDGET_H

#include <QTreeWidget>
#include <QMap>
#include <QMouseEvent>
#include <QPoint>

class TreeElement;
class Curve;

class ProjectTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    ProjectTreeWidget(QWidget *parent = nullptr);
    QMap<uint32_t, QTreeWidgetItem*> itemsProject;

protected:
    virtual void mousePressEvent(QMouseEvent *mse);
    virtual void mouseMoveEvent(QMouseEvent *msme);

private:
    TreeElement *lastSelectedItem;
    QPoint      lastPosDragDrop;
    QPoint      dragStartPosition;

signals:
    void        sigShowCurveOnTrack(Curve *crv, TreeElement *trkItem);
    void        sigAddingCurveToTrackModeEnable();
    void        sigCollapsed();
    void        sigNotCollapsed();

private slots:
    void        slotItemClicked(QTreeWidgetItem *item);
};

#endif // PROJECTTREEWIDGET_H
