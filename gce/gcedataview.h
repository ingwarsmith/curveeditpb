#ifndef GCEDATAVIEW_H
#define GCEDATAVIEW_H

#include <QTabWidget>
#include <QStackedWidget>
#include "elements/plot.h"
#include "elements/curve.h"
#include "elements/device.h"

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QSplitter>

#include <QMouseEvent>
#include <QResizeEvent>
#include <QDragEnterEvent>
#include <QDropEvent>

#include "track.h"

class SpecLabel : public QLabel
{
    Q_OBJECT

public:
    SpecLabel(QString name);
    void setTimeZoneText(QString tz);
    void setDescription(QString desc);
    QString getDesciptionText() { return descrText; }

protected:
    virtual void mouseDoubleClickEvent(QMouseEvent *evt) override;

private:
    QString tzText;
    QString descrText;
    void updateSpecLabelText();

signals:
    void sigMouseDoubleClicked(QPoint pos);
};

class SpecialWidgetForPlot : public QWidget
{
    Q_OBJECT

public:
    explicit SpecialWidgetForPlot(Plot *basePlot, QWidget *parent = nullptr);
    void                        setPlot(Plot *plt) { plot4wgt = plt; }
    Plot                        *getPlot() { return plot4wgt; }
    Track                       *addEmptyTrack_public();
    QVector<Track*>             *getTracksVectorPtr()
    {
        return  &tracksArray;
    }
    Track                       *getTimeTrack()
    {
        return trackTimeOnly;
    }
    void                        setDevNames(QVector<QString> devicesNamesOfPlot)
    {
        plot_devicesNames = devicesNamesOfPlot;
    }
    QVector<QString>            getDevNames()
    {
        return plot_devicesNames;
    }
    void                        setDescriptionInHeader(QString text)
    {
        lbl_TimeZone->setDescription(text);
    }
    void                        updatePlots(bool scaledValues = false);
    void                        refreshHeadersOfTracks();
    void                        showWholeTimesRange();
    void                        shiftValuesAxesToShowLastValuesOnGraphics();
    void                        resetScroll();

    void                        recalcWgt2HeightForScrollBar();

    bool                        viewGraphicsValuesMode;
    bool                        viewGraphicsValuesMode_Fixation;
    bool                        viewGraphicsValuesModeInAnotherPlot;

private:
    SpecLabel                   *lbl_TimeZone;
    QStackedWidget              *centerWgtstack;
    QSplitter                   *vSplitter;
    QScrollBar                  *scrollBar;
    int                         lastScrollBarPositionValue;
    QVBoxLayout                 *v_Layout;
    QHBoxLayout                 *h_Layout;
    QWidget                     *wgt2;

    Track                       *trackTimeOnly;
    QVector<Track*>             tracksArray;
    QVector<QString>            plot_devicesNames;
    Track                       *baseTrack;
    bool                        rightMouseButtonPressed;
    bool                        leftMouseButtonPressed;
    QVector<QCPItemLine*>       linesVectorPtrForViewGraphicsValuesMode;
    QVector<QCPItemTracer*>     positionsVectorPtrForViewGraphicsValuesMode;
    QPoint                      positionLast, positionFirst;
    Qt::Orientation             orientationMode;

    void                        addPrototypeDatas();
    Track                       *addEmptyTrack();
    Plot                        *plot4wgt;
    int32_t                     maxTrackHeaderWidth;

    QString                     plotName;

    QMap<uint8_t, QPair<QColor, bool>> selectedColors;

    uint8_t                     firstIndexSelectedColorWhichAreUsedInPlot();
    uint8_t                     firstIndexSelectedColorWhichAreUsedInTrack(Track *track);
    void                        savingPreparedFileWithPlot(QString fullFileName, bool makeTemplate = false);
    void                        refreshTrackHeader(int32_t idxTrack);
    void                        refreshDevicesLinkedToPlotAfterDeleteAnyTrackOrGraphic();

    void                        setPosViewGraphicsValuesMode(Track *pltTrack, QMouseEvent* msevt);

    void                        addGraphToTrack(uint32_t curvId, uint32_t srcId, int32_t idxTrack, bool loaded);

    bool                        copyLabelMode;
    bool                        copyLabelToAnotherTrackMode;
    bool                        moveLabelMode;

    bool                        dimensionMode;
    bool                        dimensionProcess;
    QCPItemRect                 *dimensionRect;
    QCPItemTracer               *dimensionStartPoint, *dimensionEndPoint;
    Track                       *dimensionTrack;

    DataLabel                   *dl4copy;
    DataLabel                   *dlCopiedToAnotherTrack;
    DataLabel                   *dl4move;
    Track                       *track_senderSignalsOfMoveOrCopy;
    bool                        mousePressedOnceAtCopyOrMove;
    QVector<int>                trackidsInWhichCopyed4copyInAnotherTracks;
    int                         dialogAV_lastHeight;

    bool                        scalingByRectangleMode;
    QCPItemRect                 *scalingRect;
    Track                       *scaledTrack;

    bool                        valuesRescaledByMouseWheel;

protected:
    virtual void                resizeEvent(QResizeEvent *rsze);

signals:
    void    sigResetAddingCurveMode();
    void    sigPlotRenamed(QString nm);
    void    sigPlotMustBeRemoved(Plot *plt);
    void    sigShowLastValuesForX(double coordX);
    void    sigActivatedViewValues(bool yes);
    void    sigActivateMe();
    void    sigChangeTimeRangeForJoinedByTimePlots(double newMinTime, double newMaxTime);

public slots:
    void    slotSwapAxes();
    void    slotAddNewTrack();
    void    slotSavePlotToProject(QString projectDataDir);
    void    slotExportPlotToLocalFile(QString localFileNameWithPath);
    void    slotMouseWheel(QWheelEvent *mwe);
    void    slotMousePress(QMouseEvent *mpe);
    void    slotMouseRelease(QMouseEvent *mre);
    void    slotMouseMove(QMouseEvent *mme);
    void    slotMouseDoubleClick(QMouseEvent *mdce);
    void    slotRescaleXaxis(double min, double max);
    void    slotLoadDataForSelectedCurves();
    void    slotSaveAsPDF();
    void    slotSaveAsBMP();
    void    slotAfterRemovingCurve();
    void    slotGoToLabel(double value);
    void    slotChangeTimeZone(QTimeZone tZone);

private slots:
    void    slotAddGraphToTrack(Curve *curve);
    void    slotResizedTrack();
    void    slotDeviceNameChanged(QString oldName, QString newName);
    void    slotPlotRenamed(QString nm);
    void    slotRemovingTrack();
    void    slotRemovePlot();
    void    slotSaveAsStandalonePlotTemplateFile();
    void    slotChangeLinkToDataOfDeviceInPlot();
    void    slotScrolledVerticalScrollBar(int newValue);
    void    slotStopViewValuesMode_CloseDialog();
    void    slotCopyLabel_(DataLabel *dlCopy);
    void    slotMoveLabel_(DataLabel *dl4Move);
    void    slotCopyLabelToAnotherTrack_(DataLabel *dl4copy);
    void    slotTrackHeightChanged(int newHeight);
    void    slotMovingTrackWithinPlot();
    void    slotForMoveCurveFromTrackToTrack(Curve *curveForMove);
    void    slotChangeFontUtcHeaderSize(int fontSzNew);
    void    slotChangeFontGraphicsHeaderNamesSize(int fontSzNew);
    void    slotChangeFontGraphicsHeaderMinMaxSize(int fontSzNew);
};

class GceDataView : public QTabWidget
{
    Q_OBJECT
public:
    explicit GceDataView(QWidget *parent = nullptr);
    void        addPlot(Plot *plotAdded, bool withEmptyTrack = false);
    QList<SpecialWidgetForPlot*>
                getPlotWidgetsList()
    {
        return widgetsPlots.values();
    }
    void        removeDeviceFromPlots(QString devName, uint32_t srcID);
    void        removeAllPlotsOnClose();
    void        openPlotFromTemplateFile(QString templateFileName);

private:
    QMap<Plot*, SpecialWidgetForPlot*> widgetsPlots;
    QList<SpecialWidgetForPlot*>       widgetsJoinedByTimeScale_List;
    int                                lastPlotNum;
    QString                            nameProject;
    bool                               projectOpeningComplete;
    void                               plotAddingInternal(Plot *plotElement, SpecialWidgetForPlot *plotWidget);

protected:
    //virtual void    DragEnterEvent(QDragEnterEvent *dragEntEvt);
    //virtual void    DropEvent(QDropEvent *dropEvt);

signals:
    //void    sigResetAddingCurveMode();
    void    sigForAddEmptyPlot();

public slots:
    void    slotSavePlots(QString projectHeaderPath);
    void    slotOpenPlots(QString projectHeaderPath);
    void    slotOpenPlotTemplateFromFile();
    void    slotLoadDataAndRefreshCurrentPlot();
    void    slotShowWholeTimeRangeOnCurrentPlot();
    void    slotAutoRescaleValuesAxesOfCurrentPlot();
    void    slotCurrentPlotToPDF();
    void    slotCurrentPlotToBMP();
    void    slotAutoShiftValuesOnCurrentPlot();
    void    slotShowHideOnGraphicsAllDots();
    void    slotChangeTimeRangeForJoinedByTimePlots(double newMinTime, double newMaxTime);

private slots:
    void    slotShowSelectedPlot(QTreeWidgetItem* now,QTreeWidgetItem* old);
    void    slotPlotRenamed(QString nm);
    void    slotPlotRemove(Plot *plt);
    void    slotDeviceRenamedForEmptyPlotDeviceElements(QString devNameNew, QString nameOld, uint32_t sourceID, Device *deviceSender);
    void    slotActivatedValuesView(bool yes);
    void    slotDrawOrRedrawGraphic(uint32_t sourceID, uint32_t curveID);
    void    slotPlotQuery4activate();
    void    slotResetScrollOnSelected();
    void    slotJoinPlotsByTimeDialogStart();
};

#endif // GCEDATAVIEW_H
