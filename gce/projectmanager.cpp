#include "projectmanager.h"
#include "mainwindow.h"
#include <gcelogiclayer.h>
#include <QMessageBox>
#include <QFileDialog>

#include <QApplication>
#include <QThread>
#include "filesearchernodb.h"

#include "gcedataview.h"

#include "cfg.h"

ProjectManager::ProjectManager(QObject *parent) : QObject(parent)
{
    projectsArray = QMap<uint32_t, Project*>();
    projectTreeWidget = new ProjectTreeWidget;

    auto fnt =  projectTreeWidget->font();
    fnt.setPointSize(fnt.pointSize()+4);
    projectTreeWidget->setFont(fnt);

    actionAddDataSource = new QAction(tr("Add device (import data)"));
    actionAddDataSource->setObjectName(SPRJ_AddDataSource);
    actionAddDataSource->setIcon(QIcon(":/ico1/add_datasource"));

    actionAddPlot = new QAction(tr("Add plot"));
    actionAddPlot->setObjectName(SPRJ_AddPlot);
    actionAddPlot->setIcon(QIcon(":/ico1/add_plot"));

    actionCreateProject = new QAction(tr("Create project"));
    actionCreateProject->setIcon(qApp->style()->standardIcon(QStyle::SP_FileIcon));
    actionOpenProject = new QAction(tr("Open project"));
    actionOpenProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogOpenButton));
    actionSaveCurrentProject = new QAction(tr("Save project"));
    actionSaveCurrentProject->setObjectName(SPRJ_SaveProject);
    actionSaveCurrentProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton));
    actionSaveCurrentProjectAs = new QAction(tr("Save project copy to"));
    actionSaveCurrentProjectAs->setObjectName(SPRJ_SaveProjectAs);
    actionSetProjectTimeZone = new QAction(tr("Set time of project"));
    actionSetProjectTimeZone->setIcon(QIcon(":/ico1/times"));
    actionSetProjectTimeZone->setObjectName(SPRJ_TimeZone);
    actionCloseCurrentProject = new QAction(tr("Close project"));
    actionCloseCurrentProject->setObjectName(SPRJ_CloseProject);
    actionCloseCurrentProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogCloseButton));
    actionRenameProject = new QAction(tr("Rename project"));
    actionRenameProject->setObjectName(SPRJ_RenameProject);
    actionRenameProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogApplyButton));
    actionViewEditProjectProperties = new QAction(tr("View/edit project description"));
    actionViewEditProjectProperties->setObjectName(SPRJ_ProjectInfo);
    actionViewEditProjectProperties->setIcon(qApp->style()->standardIcon(QStyle::SP_FileDialogInfoView));
    //actionCloseAllProjects = new QAction(tr("Close all"));
    actionRestoreProjectFromBackup = new QAction(tr("Restore project from backup"));
    actionRestoreProjectFromBackup->setObjectName(SPRJ_RestoreFromBackup);

    actionRecoveryDataSourcesOfProject = new QAction(tr("Recovery data sources"));
    actionRecoveryDataSourcesOfProject->setObjectName("RECOVERY");

    actionCleanProjectFromDataSources = new QAction(tr("Delete data sources from project"));
    actionCleanProjectFromDataSources->setObjectName("CLEAN_PROJECT_FROM_DATA_SOURCES");

    actionDesignOfProject = new QAction(tr("Design of project"));
    actionDesignOfProject->setObjectName(SPRJ_Design);

    connect(actionCreateProject, SIGNAL(triggered(bool)), this, SIGNAL(sigProjectCreate()));
    connect(actionOpenProject, SIGNAL(triggered(bool)), this, SLOT(slotOpenProject()));

    connect(this, SIGNAL(sigProjectCreate()), this, SLOT(slotCreateProject()));

    connect(actionSaveCurrentProject, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionSaveCurrentProjectAs, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionSetProjectTimeZone, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionViewEditProjectProperties, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionRenameProject, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionRestoreProjectFromBackup, SIGNAL(triggered(bool)),
            this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionRecoveryDataSourcesOfProject, SIGNAL(triggered(bool)),
            this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionCleanProjectFromDataSources, SIGNAL(triggered(bool)),
            this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionDesignOfProject, SIGNAL(triggered(bool)),
            this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionCloseCurrentProject, SIGNAL(triggered(bool)), this, SLOT(slotCloseCurrentProject()));

    connect(actionAddDataSource, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
    connect(actionAddPlot, SIGNAL(triggered(bool)), this, SLOT(slotCurrentProjectActionProcessing()));
}

QString ProjectManager::getLast_Current_ProjectFolderPath()
{
    QStringList tmp = projectsArray.last()->getProjectHeaderPath().split("/");
    tmp.removeLast();
    return tmp.join("/");
}

Project *ProjectManager::createNewProject(bool forOpen)
{
    auto newKey = (projectsArray.size() ? projectsArray.keys().last()+1 : 1);
    auto nPrj = new Project(newKey, this);
    connect(nPrj, SIGNAL(sigCloseProject()), this, SLOT(slotCloseCurrentProject()));
    projectsArray.insert(newKey, nPrj);
    nPrj->setProjectName(tr("New project").append(newKey > 1 ? QString::number(newKey).prepend(" ") : QString()));
    nPrj->fillElementsBase(forOpen);
    nPrj->setSaved(false);
    projectTreeWidget->itemsProject.insert(newKey, nPrj->getItemProject());
    auto dataView = qobject_cast<GceDataView*>(
                GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView)
                );
    connect(nPrj, SIGNAL(sigSaveProjectPlots(QString)), dataView, SLOT(slotSavePlots(QString)));
    connect(nPrj, SIGNAL(sigDeviceRenamedForEmptyPlotDeviceElements(QString,QString,uint32_t,Device*)),
            dataView, SLOT(slotDeviceRenamedForEmptyPlotDeviceElements(QString,QString,uint32_t,Device*)));
    return nPrj;
}

QList<QAction *> ProjectManager::getCommonActions()
{
    return QList<QAction*>() << actionCreateProject << actionOpenProject << actionCloseCurrentProject;
}

QList<QAction *> ProjectManager::getProjectActions()
{
    return QList<QAction*>() << actionAddDataSource
                             << actionAddPlot
                             << actionSaveCurrentProject
                             << actionSaveCurrentProjectAs
                             << actionViewEditProjectProperties
                             << actionSetProjectTimeZone
                             << actionRenameProject
                             << actionRestoreProjectFromBackup
                             << actionRecoveryDataSourcesOfProject
                             << actionCleanProjectFromDataSources
                             << actionDesignOfProject;
}

bool ProjectManager::workWithProjectAlready()
{
    return !projectsArray.isEmpty();
}

void ProjectManager::openExistingProject(QString projectFolderPath, bool withBackup)
{
    auto opProjectHeaderFileName = projectFolderPath;
    opProjectHeaderFileName.append("/project.ceproj");

    auto nP = createNewProject(true);
    nP->setProjectHeaderPath(opProjectHeaderFileName);
    int projectsCount = projectsArray.keys().size();
    projectTreeWidget->insertTopLevelItem(projectsCount-1, nP->getItemProject());
    auto successfullOpenAtFirst = true;
    while (!nP->openProject())
    {
        successfullOpenAtFirst = false;
        if (nP->getOpeningProjectError() != Project::OpeningProjectError::ProjectHeaderCantOpenForRead
                &&
                nP->getOpeningProjectError()
                != Project::OpeningProjectError::ProjectInternalDataBroken_UnRecoverable)
        {
            QMessageBox mboxRestoreFromBackup;
            mboxRestoreFromBackup.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mboxRestoreFromBackup.setWindowTitle(tr("Problem opening project"));
            mboxRestoreFromBackup.setText(tr("At first project may be restored from backup. Restore from backup copy? "));
            if (mboxRestoreFromBackup.exec() == QMessageBox::Yes)
            {
                auto restoredFromBackup = nP->restoreFromBackup();

                if (restoredFromBackup) continue;
            }
        }

//        mboxRestoreFromBackup

        QString messageIfCorrupt;
        if (nP->getOpeningProjectError() == Project::OpeningProjectError::None)
        {
            projectsArray.remove(projectsArray.key(nP));
            projectTreeWidget->clear();
            nP->deleteLater();
            break;
        }
        QMessageBox mbox;
        auto mayRecovery = false;
        switch (nP->getOpeningProjectError())
        {
        case Project::OpeningProjectError::ProjectHeaderNotExists:
        {
            messageIfCorrupt.append(tr("Project's header file not found."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("This file may be recreated again with default content."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("After that recovery process will started:"));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("data files will be searched in the project's internal folder."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Do you want to start recovery process?"));
            mbox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mayRecovery = true;
            break;
        }
        case Project::OpeningProjectError::ProjectHeaderCantOpenForRead:
        {
            messageIfCorrupt.append(tr("Project's header file can't open even in read-only mode."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("There are two possible reasons:"));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("1. Security settings not allow to open this file for you"));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("2. Problems with file system integrity"));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Please, contact with your system administrator."));
            messageIfCorrupt.append("\n");
            mbox.setStandardButtons(QMessageBox::Ok);
            break;
        }
        case Project::OpeningProjectError::ProjectHeaderCorrupt:
        {
            messageIfCorrupt.append(tr("Project's header file corrupt."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("It's possible to recovery data files and to rebuild project again"));
            messageIfCorrupt.append(",\n");
            messageIfCorrupt.append(tr("data files will be searched in the project's internal folder."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Do you want to start recovery process?"));
            mbox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mayRecovery = true;
            break;
        }
        case Project::OpeningProjectError::ProjectInternalDataBroken_UnRecoverable:
        {
            messageIfCorrupt.append(tr("Project's internal data lost and can't be recovered."));
            mbox.setStandardButtons(QMessageBox::Ok);
            break;
        }
        case Project::OpeningProjectError::ProjectImportedFilesIndexerFail:
        {
            messageIfCorrupt.append(tr("Project's imported files with data not indexed correctly."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("It's possible to recovery data files and to rebuild project again"));
            messageIfCorrupt.append(":\n");
            messageIfCorrupt.append(tr("data files will be searched in the project's internal folder."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Do you want to start recovery process?"));
            mbox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mayRecovery = true;
            break;
        }
        case Project::OpeningProjectError::ProjectDataSourceIndexCorrupt:
        {
            messageIfCorrupt.append(tr("Project's data source index corrupt."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("It's possible to recovery data files and to rebuild project again"));
            messageIfCorrupt.append(":\n");
            messageIfCorrupt.append(tr("data files will be searched in the project's internal folder."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Do you want to start recovery process?"));
            mbox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mayRecovery = true;
            break;
        }
        case Project::OpeningProjectError::ProjectSpecialFilesBroken:
        {
            messageIfCorrupt.append(tr("Project's special files (curves data, plots settings, e.t.c.) corrupt."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("It's possible to recovery data files and to rebuild project again"));
            messageIfCorrupt.append(":\n");
            messageIfCorrupt.append(tr("data files will be searched in the project's internal folder."));
            messageIfCorrupt.append("\n");
            messageIfCorrupt.append(tr("Do you want to start recovery process?"));
            mbox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            mayRecovery = true;
            break;
        }
        }

        mbox.setText(messageIfCorrupt);
        mbox.setWindowTitle(tr("Problem opening project"));

        if (!mayRecovery)
        {
            mbox.exec();
            projectsArray.remove(projectsArray.key(nP));
            projectTreeWidget->clear();
            nP->deleteLater();
            break;
        }
        else
        {
            auto res = mbox.exec();
            auto recovered = false;
            if (res == QMessageBox::Yes)
            {
                recovered = nP->tryForRecoveryProject();
            }

            if (!recovered)
            {
                projectsArray.remove(projectsArray.key(nP));
                projectTreeWidget->clear();
                nP->deleteLater();
                break;
            }
        }
    }
    if (successfullOpenAtFirst && withBackup)
    {
        nP->saveCopyToBackupForRecovery();
    }
    qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                 .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration))
            ->addProjectPathToListOfLastProjectPaths(projectFolderPath);
}

void ProjectManager::creatingProjectInternal()
{
    if (workWithProjectAlready())
    {
        QMessageBox::critical(nullptr, tr("Opened/created project exist"),
                              tr("You must close current project to create another"));
        return;
    }

    QString appDirPath = qApp->applicationDirPath();
    QString projectsDirPath = QString().append(appDirPath).append("/projects");
    QFileInfo projectsDirFI(projectsDirPath);
    if (!projectsDirFI.exists())
    {
        QDir appDir(appDirPath);
        appDir.mkdir("projects");
    }

    QString newName = QFileDialog::getSaveFileName(
                nullptr, tr("Create project and save it to folder..."),
                projectsDirPath, QString(tr("GM-CurveEdit project folder (*.DIRCE)")), nullptr,
                QFileDialog::DontConfirmOverwrite
                );

    if (newName.isEmpty())
    {
        return;
    }

    if (newName.right(6) != QString(".DIRCE"))
    {
        newName.append(".DIRCE");
    }

    qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                 .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration))
            ->addProjectPathToListOfLastProjectPaths(newName);

    QDir newProjectDir(newName);
    if (newProjectDir.exists())
    {
        if (QMessageBox::question(NULL, tr("Folder already exist"),
                                  tr("Folder already exist, probably with project inside. Do you want remove old folder?"))
                == QMessageBox::Yes)
        {
            FileSearcherNoDb fsrch4remove;
            QFileInfo fiNewName(newName);
            QFileInfoList filesInTree = fsrch4remove.retFilesInFolderAndSubFolders(fiNewName);
            while (filesInTree.size() > 0)
            {
                QFileInfo file_fromTree = filesInTree.takeLast();
                QFile fl_file(file_fromTree.absoluteFilePath());
                fl_file.remove();
            }
            auto removed = false;
            auto counter = 0;
            while (!removed && counter < 10)
            {
                removed = newProjectDir.removeRecursively();
                thread()->msleep(3UL);
                counter++;
            }
        }
        else
        {
            return;
        }
    }

    QStringList tmp = newName.split("/");
    QString toCreate = tmp.takeLast();
    QDir dirSuper(tmp.join("/"));
    dirSuper.mkdir(toCreate);

    newName.append("/project.ceproj");

    auto projectFolder = newName;
    projectFolder.remove(".ceproj");
    QFileInfo fiSv(projectFolder);
    FileSearcherNoDb fsrchr;
    auto FIlist = fsrchr.retFilesInFolderAndSubFolders(fiSv);
    while (FIlist.size())
    {
        for (QFileInfo &fi : FIlist)
        {
            QFile fl(fi.absoluteFilePath());
            fl.remove();
        }
        FIlist = fsrchr.retFilesInFolderAndSubFolders(fiSv);
    }

    auto nP = createNewProject(false);
    nP->setProjectHeaderPath(newName);
    int projectsCount = projectsArray.keys().size();
    projectTreeWidget->insertTopLevelItem(projectsCount-1, nP->getItemProject());

    nP->slotSaveProject();

    nP->getItemProject()->setExpanded(true);

    if (nP->getProjectHeaderPath().size())
    {
        QString prjName = nP->getProjectHeaderPath();
        QStringList tmp1 = prjName.split("/");
        tmp1.removeLast();
        prjName = tmp1.last();

        auto o = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameMainWindow);
        if (o)
        {
            auto mainWnd = qobject_cast<MainWindow*>(o);
            mainWnd->setProjectPathInTitle(prjName);
        }

        tmp1 = prjName.split(".");
        tmp1.removeLast();
        if (tmp1.size() > 1)
        {
            prjName = tmp1.join(".");
        }
        else
        {
            prjName = tmp1.at(0);
        }
        nP->setProjectName(prjName);
    }
}

//QDateTime ProjectManager::getDateTimeFromDialog()
//{
//    MainWindow *w = qobject_cast<MainWindow*>(
//                GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
//                    ObjectsNames::objNameMainWindow
//                    )
//                );
//    QDateTime ret;
//    w->getDateTime(ret);

//    return ret;
//}

void ProjectManager::slotCreateProject()
{
    creatingProjectInternal();

    if (projectsArray.size())
    {
        projectsArray.values().last()->slotForAddPlotFromSelectedTemplate();
    }
}

void ProjectManager::slotOpenProject()
{
    if (workWithProjectAlready())
    {
        QMessageBox::critical(nullptr, tr("Opened/created project exist"),
                              tr("You must close current project to open another"));
        return;
    }

    QString appDirPath = qApp->applicationDirPath();
    QString projectsDirPath = QString().append(appDirPath).append("/projects");
    QFileInfo projectsDirFI(projectsDirPath);
    if (!projectsDirFI.exists())
    {
        QDir appDir(appDirPath);
        appDir.mkdir("projects");
    }

    QString opProjectFolderName;
    QFileDialog openProjectFolderDlg(nullptr, tr("Open project"));
    openProjectFolderDlg.setFileMode(QFileDialog::DirectoryOnly);
    openProjectFolderDlg.setAcceptMode(QFileDialog::AcceptOpen);
    openProjectFolderDlg.setDirectory(projectsDirPath);

#ifdef Q_OS_WIN
    openProjectFolderDlg.setNameFilter("CurveEdit project folder (*.DIRCE)");
#endif

    if (openProjectFolderDlg.exec() != QDialog::Accepted)
    {
        return;
    }

    for (QString &selection : openProjectFolderDlg.selectedFiles())
    {
        QString elem = selection.split("/").last();
        QString extension = elem.split(".").last();
        if (extension == "DIRCE")
        {
            opProjectFolderName = selection;
        }
    }

    if (opProjectFolderName.isEmpty())
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("It's not GM-CurveEdit project folder!"));
        return;
    }

    openExistingProject(opProjectFolderName);
}

void ProjectManager::slotCurrentProjectSave()
{
    if (projectsArray.isEmpty()) return;
    auto project2save = projectsArray.value(projectsArray.lastKey());
    project2save->slotSaveProject();
}

void ProjectManager::slotCloseCurrentProject()
{
    if (projectsArray.isEmpty()) return;

    auto result = QMessageBox::question(nullptr, tr("Save project on close"), tr("Do you want to save changes?"),
                                        QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel);
    if (result == QMessageBox::Yes)
    {
        projectsArray.values().last()->slotSaveProject();
    }

    if (result != QMessageBox::Cancel)
    {
        auto o = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameMainWindow);
        if (o)
        {
            auto mainWnd = qobject_cast<MainWindow*>(o);
            mainWnd->setProjectPathInTitle(QString());
        }

        projectsArray.values().last()->slotCloseProject();
        auto project2close = projectsArray.take(projectsArray.lastKey());
        delete project2close;
    }
}

void ProjectManager::slotCurrentProjectActionProcessing()
{
    auto ObNmAction = sender()->objectName();

    if (projectsArray.isEmpty())
    {
        if (ObNmAction == SPRJ_AddDataSource)
        {
            auto res = QMessageBox::question(nullptr,
                                  tr("Create object for add data"),
                                  tr("It's neccessary to create project before add data")
                                  .append("\n")
                                  .append(tr("Do you want to create project?")));

            if (res == QMessageBox::Yes)
            {
                creatingProjectInternal();
                if (projectsArray.size())
                {
                    projectsArray.values().last()->addDataSource();
                    auto o = GceLogicLayer::getInstance()
                            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
                    if (o)
                    {
                        auto gdv = qobject_cast<GceDataView*>(o);
                        gdv->slotShowWholeTimeRangeOnCurrentPlot();
                    }
                }
            }
        }

        return;
    }
    auto project2process = projectsArray.value(projectsArray.lastKey());

    if (ObNmAction == SPRJ_SaveProject)
    {
        project2process->slotSaveProject();
    }
    else if (ObNmAction == SPRJ_SaveProjectAs)
    {
        project2process->slotSaveProjectAs();
    }
    else if (ObNmAction == SPRJ_ProjectInfo)
    {
        project2process->slotShowDescr();
    }
    else if (ObNmAction == SPRJ_TimeZone)
    {
        project2process->slotShowProjectTimeSet();
    }
    else if (ObNmAction == SPRJ_RenameProject)
    {
        project2process->slotRenameProject();
    }
    else if (ObNmAction == SPRJ_AddDataSource)
    {
        project2process->addDataSource();
    }
    else if (ObNmAction == SPRJ_AddPlot)
    {
        project2process->slotForAddPlotFromSelectedTemplate();
    }
    else if (ObNmAction == SPRJ_RestoreFromBackup)
    {
        auto project2restore = projectsArray.value(projectsArray.lastKey());
        project2restore->slotRestoreProjectFromBackup();
    }
    else if (ObNmAction == "RECOVERY")
    {
        auto projRecDS = projectsArray.value(projectsArray.lastKey());
        projRecDS->slotRecoveryDataSourcesOfProject();
    }
    else if (ObNmAction == "CLEAN_PROJECT_FROM_DATA_SOURCES")
    {
        auto prjCurr = projectsArray.value(projectsArray.lastKey());
        prjCurr->cleanDataSourceFiles();

    }
    else if (ObNmAction == SPRJ_Design)
    {
        project2process->slotDesignSettings();
    }
}
