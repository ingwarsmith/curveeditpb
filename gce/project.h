#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <QHash>
#include "treeelement.h"

#include "elements/devicesdatas.h"
#include "elements/plots.h"
#include "elements/listlabels.h"

#include <QTimeZone>

class Project : public QObject
{
    Q_OBJECT
    const int CURRENT_PROJECT_VERSION_MAJOR = 2;
    const int CURRENT_PROJECT_VERSION_MINOR = 0;

public:

    enum class OpeningProjectError
    {
        None,
        ProjectHeaderNotExists,
        ProjectHeaderCantOpenForRead,
        ProjectHeaderCorrupt,
        ProjectInternalDataBroken_UnRecoverable,
        ProjectImportedFilesIndexerFail,
        ProjectDataSourceIndexCorrupt,
        ProjectSpecialFilesBroken
    };

    explicit    Project(uint32_t idx, QObject *parent = nullptr);

    void        addDataSource();

    void        setProjectIndex(uint32_t idx);
    void        fillElementsBase(bool forOpen = false);
    uint32_t    getThisProjectIndex();

    QString     getDescription() const;
    void        setDescription(QString newDescription);

    QString     getProjectName() const;
    void        setProjectName(QString newProjectName);

    QString     getStringFileNameOfProject() const;

    bool        unsavedChangesExists() const;
    void        setUnsavedChangesExists();
    void        setSaved(bool saved);

    void        removeOnePlot(Plot *plt);
    bool        existPlotWithName(QString name)
    {
        return plotsList->existPlotWithName(name);
    }
    Plots       *getPlotsList()
    {
        return plotsList;
    }

    bool        openProject();
    void        saveCopyToBackupForRecovery();
    bool        restoreFromBackup();
    bool        tryForRecoveryProject();
    int32_t     getPlotsCount();

    Device      *getDeviceElemBySourceID(uint32_t srcID);

    TreeElement *getItemProject();
    QVector<TreeElement*> getAllElements();
    QMap<uint32_t, TreeElement*> *getElementsHashPtr()
    {
        return &elements;
    }

    void        sendElementToMap(TreeElement* addedElem);
    void        addPlotToPlotList(Plot *addedPlot);

    QList<QAction*> getProjectActionsList();

    QMap<uint32_t, Device*> *getDevicesMapPtrInProject()
    {
        return devicesList->getDeviceMapPtr();
    }

    void        setProjectHeaderPath(QString path) { projectHeaderPath = path; }
    QString     getProjectHeaderPath() const { return projectHeaderPath; }

    QTimeZone   timeZone;
    int64_t     timeOffsetMilliseconds;

    OpeningProjectError getOpeningProjectError()
    {
        return _openingProjectError;
    }

    int64_t     getFullOffsetFromUTC_msecs();

    Label       *createLabel();
    void        removeLabelById(uint32_t id);

    void        cleanDataSourceFiles();

    // Fonts settings:
    int         fontPixelSize_UtcHeader; //default: 12
    int         fontPointSize_TimeScale; //default: 8
    int         fontPointSize_GraphicsNamesInHeader; //default: 8
    int         fontPointSize_GraphicsMinMaxValuesInHeader; //default: 8
    bool        showDotsOnAllGraphics; //default: false

private:
    OpeningProjectError _openingProjectError;
    QMap<uint32_t, TreeElement*> elements;
    uint32_t         lastElementsKey;
    uint32_t         currentIndexProject;
    QString     description;
    bool        unsaved;
    QAction     *showDescription;
    QAction     *saveProject;
    QAction     *saveProjectAs;
    QAction     *closeProject;
    QAction     *renameProject;
    QAction     *setProjectTime;
    QAction     *designOfProject;
    QAction     *projectPath;
    TreeElement *rootElem;
    DevicesDatas *devicesList;
    Plots        *plotsList;
    ListLabels  *labelsList;
    QString     projectHeaderPath;

    QVector<uint32_t> _srcIds4removeAtSaving;

    void        closeProjectInternal();
    void        forAddPlotFromTemplateByDevNameInTemplateInternal(QString deviceName,
                                                                  bool question = false);

signals:
    void        sigSaveProjectPlots(QString projectHeaderPath);
    void        sigOpenProjectPlots(QString projectHeaderPath);
    void        sigCloseProject();
    void        sigDeviceRenamedForEmptyPlotDeviceElements(QString nameNew, QString nameOld, uint32_t sourceID, Device *deviceSender);
    void        sigChangeTimeZone(QTimeZone tzn);
    void        sigChangeFontUtcHeaderSize(int fontSzNew);
    void        sigChangeFontGraphicsHeaderNamesSize(int fontSzNew);
    void        sigChangeFontGraphicsHeaderMinMaxSize(int fontSzNew);

public slots:
    void        slotOpenProject();
    void        slotSaveProject();
    void        slotSaveProjectAs();
    void        slotCloseProject();
    void        slotRecoveryDataSourcesOfProject();
    void        slotRestoreProjectFromBackup();
    void        slotActionsList();
    void        slotForAddPlot();
    void        slotForAddPlotFromSelectedTemplate();
    void        slotForAddPlotFromTemplateByDevNameInTemplate(QString deviceName);
    void        slotForAddPlotFromTemplateByDevNameInTemplateAtDeviceSubmenu(QString deviceName);
    void        slotShowDescr();
    void        slotShowProjectTimeSet();
    void        slotRenameProject();
    void        slotDeviceRenamed_EmptyPlotDevElements(QString nameNew, QString nameOld, uint32_t sourceID);
    void        slotRemoveDevice(QString devNm, uint32_t srcID);
    void        slotDesignSettings();
    void        slotProjectPath();
};

#endif // PROJECT_H
