#include "helpbaseform.h"
#include "ui_helpbaseform.h"

HelpBaseForm::HelpBaseForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HelpBaseForm)
{
    ui->setupUi(this);
}

HelpBaseForm::~HelpBaseForm()
{
    delete ui;
}
