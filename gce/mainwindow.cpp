#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSplitter>
#include <QDateTimeEdit>
#include <QDialog>
#include <QStyle>

#include <QDesktopServices>

#include <gcelogiclayer.h>

#include "cfg.h"
#include "gcedataview.h"

MainWindow::MainWindow(ProjectManager *prjMgrInput, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    prjMgr(prjMgrInput)
{
    addingCurveToTrackModeEnabled = false;

    ui->setupUi(this);
    ui->mainToolBar->setWindowTitle(tr("Show toolbar"));
    QList<QAction*> lst;
    lst << prjMgr->actionCreateProject
        << prjMgr->actionOpenProject << prjMgr->actionSaveCurrentProject
        << prjMgr->actionCloseCurrentProject;
    ui->mainToolBar->addAction(prjMgr->actionAddDataSource);
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addActions(lst);
    ui->mainToolBar->addSeparator();
    auto wgt = new QWidget;
    wgt->setMinimumHeight(ui->mainToolBar->height());
    auto szPolicy = wgt->sizePolicy();
    szPolicy.setHorizontalPolicy(QSizePolicy::Expanding);
    wgt->setSizePolicy(szPolicy);
    ui->mainToolBar->addWidget(wgt);
    auto actionRefresh = ui->mainToolBar->addAction(
                style()->standardIcon(QStyle::SP_BrowserReload),
                tr("Load/refresh data")
                );
    actionRefresh->setObjectName("actionRefresh");
    if (qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                     .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration))
            ->dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack)
    {
        actionRefresh->setDisabled(true);
    }
    QIcon icoFl1;
    icoFl1.addFile(":/ico1/autorescale_hor");
    auto actionShowDotsOnAllGraphics = ui->mainToolBar->addAction(
                QIcon(":/ico1/show_dots_on_all_graphics"),
                tr("Show/hide dots in all graphics")
                );

    ui->mainToolBar->addSeparator();
    auto actionRescaleAxisX = ui->mainToolBar->addAction(
                icoFl1,
                tr("Show whole time range")
                );
    auto actionRescaleValuesAxes = ui->mainToolBar->addAction(
                QIcon(":/ico1/autorescale_vert"),
                tr("Autorescale values axes")
                );
    auto actionShiftValues = ui->mainToolBar->addAction(
                QIcon(":/ico1/autoshift_vert"),
                tr("Autoshift values axes")
                );
    auto actionScalingByRectangleMode = ui->mainToolBar->addAction(
                QIcon(":/ico1/magnifier_4"),
                tr("Scaling by rectangle")
                );
    ui->mainToolBar->addSeparator();

    auto actionSaveToPdf = ui->mainToolBar->addAction(
                QIcon(":/ico1/save2pdf"),
                tr("Save current plot to PDF")
                );

    auto actionSaveToBmp = ui->mainToolBar->addAction(
                QIcon(":/ico1/save2bmp"),
                tr("Save current plot to BMP")
                );
    ui->mainToolBar->addSeparator();

    auto actionAddLabel = ui->mainToolBar->addAction(
                QIcon(":/ico1/label_color_red_simple"),
                tr("Add label")
                );

    auto actionDimensionMode = ui->mainToolBar->addAction(
                QIcon(":/ico1/dimensions"),
                tr("Dimension mode")
                );

    ui->mainToolBar->setMovable(false);

    ui->centralWidget->setLayout(new QHBoxLayout);

    _lt_main = qobject_cast<QHBoxLayout*>(ui->centralWidget->layout());

    prjMgr->projectTreeWidget->setParent(this);

    menuSession = new QMenu(tr("Session"));
    menuSession->addActions(prjMgr->getCommonActions());

    prjMgr->projectTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(prjMgr->projectTreeWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotPrepareContextMenu(QPoint)));

    ui->menuBar->addMenu(menuSession);

    auto menuRecentProjects = menuSession->addMenu(tr("Recent projects"));
    connect(menuRecentProjects, SIGNAL(aboutToShow()), this, SLOT(slotRecentProjectsMenu()));

    menuProject = new QMenu(tr("Project"));
    menuProject->addActions(prjMgr->getProjectActions());

    ui->menuBar->addMenu(menuProject);

    menuActions = new QMenu(tr("Actions"));
    menuActions->addAction(actionRefresh);
    menuActions->addAction(actionShowDotsOnAllGraphics);

    menuActions->addAction(actionRescaleAxisX);
    menuActions->addAction(actionRescaleValuesAxes);
    menuActions->addAction(actionScalingByRectangleMode);
    menuActions->addAction(actionSaveToPdf);
    menuActions->addAction(actionSaveToBmp);
    menuActions->addAction(actionAddLabel);
    menuActions->addAction(actionDimensionMode);

    ui->menuBar->addMenu(menuActions);

    menuSettings = new QMenu(tr("Settings"));

    auto actionSettings = menuSettings->addAction(QIcon(":/ico1/settings_program_2"), tr("Program parameters"));
    connect(actionSettings, SIGNAL(triggered(bool)), this, SLOT(slotSettings()));

    ui->menuBar->addMenu(menuSettings);

    menuHelp = new QMenu(tr("Help"));
    auto actionAbout = menuHelp->addAction(tr("About program"));
    connect(actionAbout, SIGNAL(triggered(bool)), this, SLOT(slotAbout()));
    auto actionAboutQt = menuHelp->addAction(tr("About development environment"));
    connect(actionAboutQt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
    makeHelpMenu(menuHelp);

    menuView = new QMenu(tr("View"));
    ui->menuBar->addMenu(menuView);
    actionShowProjectTree = menuView->addAction(tr("Show project tree"));
    actionShowProjectTree->setCheckable(true);
    actionShowProjectTree->setChecked(true);

    ui->menuBar->addMenu(menuHelp);

    dview = new GceDataView(this);
    dview->setObjectName(ObjectsNames::objNameGceDataView);

    connect(actionShowDotsOnAllGraphics, &QAction::triggered, this, [&]()
    {
        if (prjMgr->getProjectsArrayPtr()->size())
        {
            auto project = prjMgr->getProjectsArrayPtr()->last();
            project->showDotsOnAllGraphics = !project->showDotsOnAllGraphics;

            emit sigShowHideOnGraphicsAllDots();
        }
    });
    connect(this, SIGNAL(sigShowHideOnGraphicsAllDots()),
            dview, SLOT(slotShowHideOnGraphicsAllDots()));

    connect(actionRefresh, SIGNAL(triggered(bool)), dview, SLOT(slotLoadDataAndRefreshCurrentPlot()));
    connect(actionRescaleAxisX, SIGNAL(triggered(bool)), dview, SLOT(slotShowWholeTimeRangeOnCurrentPlot()));
    connect(actionRescaleValuesAxes, SIGNAL(triggered(bool)), dview, SLOT(slotAutoRescaleValuesAxesOfCurrentPlot()));
    connect(actionSaveToPdf, SIGNAL(triggered(bool)), dview, SLOT(slotCurrentPlotToPDF()));
    connect(actionSaveToBmp, SIGNAL(triggered(bool)), dview, SLOT(slotCurrentPlotToBMP()));
    connect(actionShiftValues, SIGNAL(triggered(bool)), dview, SLOT(slotAutoShiftValuesOnCurrentPlot()));
    connect(actionAddLabel, SIGNAL(triggered(bool)), this, SLOT(slotAddingLabelMode()));
    connect(actionDimensionMode, SIGNAL(triggered(bool)), this, SLOT(slotDimensionMode()));
    connect(actionScalingByRectangleMode, SIGNAL(triggered(bool)),
            this, SLOT(slotScalingByRectangleMode()));

    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(qobject_cast<QObject*>(dview));

    splt = new QSplitter(Qt::Horizontal);

    splt->addWidget(prjMgr->projectTreeWidget);

    splt->addWidget(dview);
    connect(actionShowProjectTree, &QAction::triggered,
            this, &MainWindow::slotActionPTW);

    connect(splt, &QSplitter::splitterMoved,
            this, [&](int pos, int index)
    {
        if (index == 1)
        {
            if (pos == 0)
            {
                actionShowProjectTree->blockSignals(true);
                actionShowProjectTree->setChecked(false);
                actionShowProjectTree->blockSignals(false);
            }
            else
            {
                actionShowProjectTree->blockSignals(true);
                actionShowProjectTree->setChecked(true);
                actionShowProjectTree->blockSignals(false);

                auto w_ = prjMgr->projectTreeWidget->width();
                w_ = w_ < 350 ? 350 : w_;
                widthSavingForActionShowProjectTree = w_;
            }
        }
    });

    _lt_main->addWidget(splt, 9);

    prjMgr->projectTreeWidget->setMinimumWidth(350);
    prjMgr->projectTreeWidget->setMaximumWidth(550);
    dview->setMinimumWidth(500);
    dview->setMinimumHeight(500);

    connect(&GceLogicLayer::getInstance(), SIGNAL(sigOpenStartProgressBarWindow(QString)),
            &pBarWnd, SLOT(slotOpenAndStart(QString)));
    connect(&GceLogicLayer::getInstance(), SIGNAL(sigStopCloseProgressBarWindow()),
            &pBarWnd, SLOT(slotStopAndClose()));

    setAcceptDrops(true);
    baseTitle = QString("CurveEditor").append(" v").append(QString(VERSION));
    setWindowTitle(baseTitle);

    QTimer::singleShot(200, this, [&]()
    {
        szs = splt->sizes();
    });
}

MainWindow::~MainWindow()
{
    auto o = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameTreeElementAddedCurve);
    if (o != nullptr)
    {
        GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(o);
    }
    delete ui;
}

//void MainWindow::mouseMoveEvent(QMouseEvent *mme)
//{
//    //
//    Q_UNUSED(mme)
//}

void MainWindow::closeEvent(QCloseEvent *event)
{
    auto obj = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameTextEditForViewGraphicsValuesMode);
    if (obj)
    {
        auto textEditValues = qobject_cast<QTextEdit*>(obj);
        textEditValues->parentWidget()->close();
    }
    obj = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameDialogDimensionMode);
    if (obj)
    {
        auto dlgDim = qobject_cast<QDialog*>(obj);
        dlgDim->accept();
    }
    if (prjMgr->getProjectsArrayPtr()->size())
    {
        auto result = QMessageBox::question(this, tr("Save project on close"), tr("Do you want to save changes?"),
                                            QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel);
        if (result == QMessageBox::Yes)
        {
            auto project = prjMgr->getProjectsArrayPtr()->last();
            project->slotSaveProject();
        }
        else if (result == QMessageBox::Cancel)
        {
            event->ignore();
        }
    }
}

void MainWindow::resizeEvent(QResizeEvent *revt)
{
    if (szs.size())
    {
        auto growingKoeff = static_cast<double>(revt->size().width())
                / static_cast<double>(revt->oldSize().width());
        auto szFirstOldDbl = static_cast<double>(szs[0]);
        auto szFirstNewDbl = szFirstOldDbl * growingKoeff;
        auto szSecondOldDbl = static_cast<double>(szs[1]);
        auto szSecondNewDbl = szSecondOldDbl * growingKoeff;
        auto szFirstNew = qRound(szFirstNewDbl);
        auto szSecondNew = qRound(szSecondNewDbl);
        szs[0] = szFirstNew;
        szs[1] = szSecondNew;
    }
    else
    {
        //szs = splt->sizes();
    }

    QMainWindow::resizeEvent(revt);
}

void MainWindow::keyPressEvent(QKeyEvent *kevt)
{
    if (kevt->key() == Qt::Key_Escape)
    {
        if (qApp->property("ScalingByRectangleMode").isValid()
                && qApp->property("ScalingByRectangleMode").toBool())
        {
            qApp->setProperty("ScalingByRectangleMode", QVariant(false));
            qApp->restoreOverrideCursor();
        }
        else if (qApp->property("LabelCreatingMode").isValid()
                && qApp->property("LabelCreatingMode").toBool())
        {
            qApp->setProperty("LabelCreatingMode", QVariant(false));
            qApp->restoreOverrideCursor();
        }
    }
    QMainWindow::keyPressEvent(kevt);
}

void MainWindow::makeHelpMenu(QMenu *mnu)
{
    QDir helpDir(qApp->applicationDirPath().append("/help"));
    if (helpDir.exists())
    {
        mnu->addSeparator();
    }
    else
    {
        return;
    }
    for (QFileInfo &fiHelp : helpDir.entryInfoList(QDir::Files))
    {
        auto action = mnu->addAction(fiHelp.completeBaseName());
        mapHelpActionsByFileNames.insert(fiHelp.absoluteFilePath(), action);
        connect(action, SIGNAL(triggered(bool)), this, SLOT(slotHelpMenu()));
    }
}

void MainWindow::slotPrepareContextMenu(QPoint pos)
{
    auto tree = static_cast<QTreeWidget*>(prjMgr->projectTreeWidget);
    auto itm = tree->itemAt(pos);

    if (!itm)
    {
        return;
    }

    auto custom = static_cast<TreeElement*>(itm)->getCustomMenu();
    if (custom->actions().count() == 0)
    {
        return;
    }
    custom->exec(tree->mapToGlobal(pos));
}

void MainWindow::slotRecentProjectsMenu()
{
    auto menuRecentProjects = qobject_cast<QMenu*>(sender());

    while (menuRecentProjects->actions().size() > 0)
    {
        auto oldAction = menuRecentProjects->actions().takeFirst();
        delete oldAction;
    }

    auto obj = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration);
    Configuration *config = qobject_cast<Configuration*>(obj);

    if (config->dataMembers.lastProjectsPaths.size() > 0)
    {
        for (QString &recentPrjPath : config->dataMembers.lastProjectsPaths)
        {
            auto actionProjectPath = menuRecentProjects->addAction(recentPrjPath);
            connect(actionProjectPath, SIGNAL(triggered(bool)), this, SLOT(slotRecentProjectOpen()));
        }
    }
    else
    {
        auto actionEmpty = menuRecentProjects->addAction(tr("(empty)"));
        Q_UNUSED(actionEmpty);
    }
}

void MainWindow::slotRecentProjectOpen()
{
    auto action_ = qobject_cast<QAction*>(sender());
    QDir projectDir(action_->text());
    if (projectDir.exists())
    {
        if (prjMgr->openedProjectExist())
        {
            QMessageBox::critical(nullptr, tr("Opened/created project exist"),
                                  tr("You must close current project to open another"));
            return;
        }
        prjMgr->openExistingProject_public(action_->text());
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), tr("Project not exist!"));
    }
}

void MainWindow::slotSettings()
{
    auto config = qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration));

    auto settingsDialog = new QDialog;
    settingsDialog->setWindowTitle(tr("Program parameters"));
    auto lt = new QVBoxLayout;

    auto lt_lang = new QHBoxLayout;
    lt->addLayout(lt_lang);
    auto lbl_language = new QLabel(tr("Language of program: "));
    auto cmb_languages = new QComboBox;
    lt_lang->addWidget(lbl_language);
    lt_lang->addWidget(cmb_languages);
    cmb_languages->addItems(config->getLanguages()); // русский первый, английский второй ////!!!!
    if (config->dataMembers.language == "en")
    {
        cmb_languages->setCurrentIndex(1); // номер два
    }

    auto chk_deleteNonmonotonicIntervals = new QCheckBox(tr("Delete nonmonotonic time intervals")); ////!!!!
    lt->addWidget(chk_deleteNonmonotonicIntervals);
    chk_deleteNonmonotonicIntervals->setChecked(config->dataMembers.removeTimeLoops);
    auto lt_nonmonotonicIntervals = new QHBoxLayout;
    lt->addLayout(lt_nonmonotonicIntervals);
    auto lbl_nonmomotonicIntervalValueMaximum = new QLabel(tr(" - maximum period for nonmonotonic interval, seconds: "));
    auto lned_nonmomotonicIntervalValueMaximum = new QLineEdit; ////!!!!
    auto nmivm_ms = config->dataMembers.timeLoopPeriodMilliseconds;
    auto nmivm_ms_dbl = static_cast<double>(nmivm_ms);
    auto nmivm_s_dbl = nmivm_ms_dbl * 0.001;
    QString dvalue;
    dvalue.setNum(nmivm_s_dbl);
    lned_nonmomotonicIntervalValueMaximum->setText(dvalue);
    auto dvalid_nonmomotonicIntervalValueMaximum = new QDoubleValidator;
    lt_nonmonotonicIntervals->addWidget(lbl_nonmomotonicIntervalValueMaximum);
    lt_nonmonotonicIntervals->addWidget(lned_nonmomotonicIntervalValueMaximum);
    dvalid_nonmomotonicIntervalValueMaximum->setBottom(0.0);
    dvalid_nonmomotonicIntervalValueMaximum->setTop(1000.0);
    dvalid_nonmomotonicIntervalValueMaximum->setDecimals(3);
    lned_nonmomotonicIntervalValueMaximum->setValidator(dvalid_nonmomotonicIntervalValueMaximum);

    auto lt_recentProjectsCount = new QHBoxLayout;
    lt->addLayout(lt_recentProjectsCount);
    auto lbl_recentProjectsCount = new QLabel(tr("Count of recent projects: "));
    auto lned_recentProjectsCount = new QLineEdit; ////!!!!
    lned_recentProjectsCount->setText(QString::number(config->dataMembers.lastProjectsCount));
    auto ivalid_recentProjectsCount = new QIntValidator;
    lt_recentProjectsCount->addWidget(lbl_recentProjectsCount);
    lt_recentProjectsCount->addWidget(lned_recentProjectsCount);
    ivalid_recentProjectsCount->setBottom(1);
    ivalid_recentProjectsCount->setTop(33);
    lned_recentProjectsCount->setValidator(ivalid_recentProjectsCount);

    auto lt_defaultTrackHeight = new QHBoxLayout;
    lt->addLayout(lt_defaultTrackHeight);
    auto lbl_defTrackHt = new QLabel(tr("Default track height, pixels: "));
    auto lned_defTrackHt = new QLineEdit;
    lned_defTrackHt->setText(QString::number(config->dataMembers.tracksDefaultHeight));
    auto ivalid_defTrackHt= new QIntValidator;
    ivalid_defTrackHt->setBottom(20);
    ivalid_defTrackHt->setTop(1000);
    lned_defTrackHt->setValidator(ivalid_defTrackHt);
    lt_defaultTrackHeight->addWidget(lbl_defTrackHt);
    lt_defaultTrackHeight->addWidget(lned_defTrackHt);

    auto chk_autoLoadData = new QCheckBox(tr("Load data automatically then add graphics to track"));
    chk_autoLoadData->setChecked(config->dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack);
    lt->addWidget(chk_autoLoadData);

    auto btn_SetDefaults = new QPushButton(tr("Set default values"));
    lt->addWidget(btn_SetDefaults);
    connect(btn_SetDefaults, SIGNAL(clicked(bool)), this, SLOT(slotDefaultSettings()));

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    lt->addWidget(btns);
    connect(btns, SIGNAL(accepted()), settingsDialog, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), settingsDialog, SLOT(reject()));

    settingsDialog->setLayout(lt);

    cmb_languages->setObjectName(ObjectsNames::objNameConfigurationDefaultLanguage);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(cmb_languages);
    chk_deleteNonmonotonicIntervals->setObjectName(ObjectsNames::objNameConfigurationDefaultDeleteNonmonotonicInterval);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(chk_deleteNonmonotonicIntervals);
    lned_nonmomotonicIntervalValueMaximum->setObjectName(ObjectsNames::objNameConfigurationDefaultNonmonotonicIntervalMaximum);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(lned_nonmomotonicIntervalValueMaximum);
    lned_recentProjectsCount->setObjectName(ObjectsNames::objNameConfigurationDefaultRecentProjectsCount);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(lned_recentProjectsCount);
    chk_autoLoadData->setObjectName(ObjectsNames::objNameConfigurationDefaultAutoLoadData);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(chk_autoLoadData);

    auto result = settingsDialog->exec();

    if (result == QDialog::Accepted)
    {
        auto needRestart = false;
        // языки:
        switch (cmb_languages->currentIndex())
        {
        case 0:
        {
            if (config->dataMembers.language == "en")
            {
                config->dataMembers.language = "ru";
                needRestart = true;
            }
            break;
        }
        case 1:
        {
            if (config->dataMembers.language == "ru")
            {
                config->dataMembers.language = "en";
                needRestart = true;
            }
            break;
        }
        }

        // немонотонные интервалы:
        config->dataMembers.removeTimeLoops = chk_deleteNonmonotonicIntervals->isChecked();
        auto sec_dbl = lned_nonmomotonicIntervalValueMaximum->text().toDouble();
        auto msec_dbl = sec_dbl * 1000.0;
        auto msec = static_cast<int>(msec_dbl);
        config->dataMembers.timeLoopPeriodMilliseconds = msec;

        // число последних проектов:
        config->dataMembers.lastProjectsCount = lned_recentProjectsCount->text().toInt();

        // признак автозагрузки данных при добавении графика на трек
        config->dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack = chk_autoLoadData->isChecked();

        // высота трека по умолчанию
        config->dataMembers.tracksDefaultHeight = lned_defTrackHt->text().toInt();

        for (QAction *a : ui->mainToolBar->actions())
        {
            if (a->objectName() == "actionRefresh")
            {
                a->setDisabled(config->dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack);
                break;
            }
        }

        config->saveSettingsToCfg();

        if (needRestart)
        {
            QMessageBox::information(this, tr("Need to restart application"), tr("Please restart to changes take effect"));
        }
    }

    GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(lned_recentProjectsCount);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(lned_nonmomotonicIntervalValueMaximum);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(chk_deleteNonmonotonicIntervals);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(cmb_languages);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(chk_autoLoadData);
    settingsDialog->deleteLater();
}

void MainWindow::slotDefaultSettings()
{
    QString dvalue;
    auto config = qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration));
    qobject_cast<QComboBox*>(GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfigurationDefaultLanguage))
            ->setCurrentIndex(0); // русский по умолчанию
    qobject_cast<QCheckBox*>(GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfigurationDefaultDeleteNonmonotonicInterval))
            ->setChecked(config->dataMembers.defaultValues.removeTimeLoopsDEFAULT);
    dvalue.setNum(0.001 * static_cast<double>(config->dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT));
    qobject_cast<QLineEdit*>(GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfigurationDefaultNonmonotonicIntervalMaximum))
            ->setText(dvalue);
    qobject_cast<QLineEdit*>(GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfigurationDefaultRecentProjectsCount))
            ->setText(QString::number(config->dataMembers.defaultValues.lastProjectsCountDEFAULT));
    qobject_cast<QCheckBox*>(GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfigurationDefaultAutoLoadData))
            ->setChecked(config->dataMembers.defaultValues.loadDataAutomaticallyThenAddGraphicsToTrackDEFAULT);
}

void MainWindow::slotAbout()
{
    QMessageBox::about(this, tr("About ").append(QString(GMCE_APP_NAME)),
                       QString(GMCE_APP_NAME)//.append(tr(" (Universal Management and Control Application), version "))
                       .append(QString(" ver."))
                       .append(QString(VERSION))
                       .append("\n")
                       .append(tr("Build date: ")).append(QString(BUILDING_DATE)).append(QString(". "))
                       .append(tr("Build number: ")).append(QString(VERSION).split(".").last())
                       //.append("\n")
                       //.append(tr("Developed by OOO NPO \"GeoMash\""))
                       );
}

void MainWindow::slotAddingLabelMode()
{
    if (prjMgr->getProjectsArrayPtr()->size() == 0) return;
    if (qApp->overrideCursor() && qApp->overrideCursor()->shape() == (Qt::PointingHandCursor)) return;
    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        return;
    }
    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        return;
    }
    qApp->setOverrideCursor(Qt::PointingHandCursor);
    qApp->setProperty("LabelCreatingMode", QVariant(true));
}

void MainWindow::slotDimensionMode()
{
    if (prjMgr->getProjectsArrayPtr()->size() == 0) return;
    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        return;
    }
    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        return;
    }
    if (qApp->property("LabelCreatingMode").isValid()
            && qApp->property("LabelCreatingMode").toBool())
    {
        return;
    }
    qApp->setOverrideCursor(Qt::CrossCursor);
    qApp->setProperty("DimensionMode", QVariant(true));

    auto dialogDimensionMode = new QDialog;
    auto teDimensions = new QTextEdit;
    teDimensions->setObjectName(ObjectsNames::objNameDialogDimensionModeTextEdit);
    GceLogicLayer::getInstance().getObjectsContainerPointer()
            ->append(teDimensions);
    auto lt = new QVBoxLayout;
    lt->addWidget(teDimensions);
    dialogDimensionMode->setLayout(lt);
    dialogDimensionMode->setWindowTitle(tr("Dimension mode"));
    connect(dialogDimensionMode, &QDialog::finished,
            this, [&](int res)
    {
        Q_UNUSED(res)
        qApp->restoreOverrideCursor();
        qApp->setProperty("DimensionMode", QVariant(false));
        auto o = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameDialogDimensionMode);
        if (o)
        {
            GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(o);
            o->deleteLater();
            o = GceLogicLayer::getInstance()
                            .getObjectFromObjectContainerByObjName(
                        ObjectsNames::objNameDialogDimensionModeTextEdit);
            GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(o);
            o->deleteLater();
        }
    });

    auto flgs = dialogDimensionMode->windowFlags();
    flgs.setFlag(Qt::WindowStaysOnTopHint);
    dialogDimensionMode->setWindowFlags(flgs);

    dialogDimensionMode->setGeometry(this->pos().x()+20, this->pos().y()+50,
                                     450, dialogDimensionMode->height());
    dialogDimensionMode->setObjectName(ObjectsNames::objNameDialogDimensionMode);
    dialogDimensionMode->show();
    GceLogicLayer::getInstance().getObjectsContainerPointer()
            ->append(dialogDimensionMode);
}

void MainWindow::slotScalingByRectangleMode()
{
    if (prjMgr->getProjectsArrayPtr()->size() == 0) return;
    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        return;
    }
    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        return;
    }
    if (qApp->property("LabelCreatingMode").isValid()
            && qApp->property("LabelCreatingMode").toBool())
    {
        return;
    }
    qApp->setOverrideCursor(Qt::CrossCursor);
    qApp->setProperty("ScalingByRectangleMode", QVariant(true));
}

void MainWindow::slotHelpMenu()
{
    auto actionTriggered = qobject_cast<QAction*>(sender());
    for (QString &path : mapHelpActionsByFileNames.keys())
    {
        if (mapHelpActionsByFileNames.value(path) == actionTriggered)
        {
            QDesktopServices::openUrl(QUrl::fromLocalFile(path));
            break;
        }
    }
}

void MainWindow::slotActionPTW(bool isChecked)
{
    if (isChecked)
    {
        splt->setSizes(szs);
    }
    else
    {
        szs = splt->sizes();
        int summ = szs[0] + szs[1];
        QList<int> tszs;
        tszs << 0 << summ;
        splt->setSizes(tszs);
    }
}

ProgressBarWindow::ProgressBarWindow(QWidget *parent) : QWidget(parent)
{
    setWindowModality(Qt::WindowModal);
    setFixedSize(300, 60);
}

void ProgressBarWindow::slotOpenAndStart(QString title)
{
    setWindowTitle(title);
    show();
}

void ProgressBarWindow::slotStopAndClose()
{
    close();
}
