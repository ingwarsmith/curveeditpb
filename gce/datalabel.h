#ifndef DATALABEL_H
#define DATALABEL_H

#include <QObject>

struct DataLabelPrivate;
class Track;
class Label;

class DataLabel : public QObject
{
    Q_OBJECT
public:
    explicit    DataLabel(Track *plot, QPoint pos, QString textLabel, double xCoord = 0.0, QObject *parent = nullptr);
    explicit    DataLabel(Track *plot, QString textLabel, double xCoord = 0.0, QObject *parent = nullptr);
    DataLabel   *makeCopy();
    DataLabel   *makeCopyInPlot(Track *plot);
    void        setLabelElement(Label *label, bool afterCreate = false);
    void        setLabelID(uint32_t newID);
    uint32_t    getLabelID();
    double      getLabel_Xcoord();
    void        setLabel_Xcoord(double x);
    QString     getText();
    void        setText(QString txt);
    bool        isVisible();
    bool        isTextVisible();
    double      getTextBlockLastCoordX();
    double      getTextBlockBottomCoordY();
    double      getTextBlockHeightInCoordUnits();
    void        setMargineFromAnotherTextAndFromBottom(double yDelta);
    void        setCoordY_forTextBlock(double coordY);
    double      getPixelCoordY_forTextBlock();
    void        setPixelCoordY_forTextBlock(double pixcrdY);
    void        resetMargineFromAnotherTextAndFromBottom();
    void        removeMe();

private:
    DataLabelPrivate *data;
    bool             hidden;
    bool             hidden_text;
    int64_t          currentTimeOffset;

signals:
    void        sigGotoLabel(double x_coord_find);
    void        sigRemoveMe();
    void        sigCopyMe();
    void        sigCopyMeToAnotherTrack();
    void        sigMoveMe();

public slots:
    void        hide(bool yes);
    void        slotShiftTime(/*uint64_t fullOffset*/);

private slots:
    void        slotShowText();
    void        slotHideText();
    void        slotShow();
    void        slotHide();
    void        slotChangeText();
    void        slotGotoLabel();
    void        slotChangeProperties();
    void        slotSetTextFontPixelSize(int sz);
};



#endif // DATALABEL_H
