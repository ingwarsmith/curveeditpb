#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T15:54:56
#
#-------------------------------------------------

QT       += core gui widgets printsupport network

CONFIG += c++17
#DEFINES += QCUSTOMPLOT_USE_OPENGL

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CurveEdit
TEMPLATE = app

NAME = CurveEditor
QMAKE_TARGET_DESCRIPTION = CurveEditor

#VERSION = 0.7.0.51
VERSION = 1.2.3.56
BUILDING_DATE_Q = 14.01.2023
DEFINES += BUILDING_DATE=\\\"$$BUILDING_DATE_Q\\\"
DEFINES += VERSION=\\\"$$VERSION\\\"
DEFINES += GMCE_APP_NAME=\\\"$$NAME\\\"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    appmanager.cpp \
    gcedataview.cpp \
    projectmanager.cpp \
    project.cpp \
    treeelement.cpp \
    projecttreewidget.cpp \
    qcustomplot.cpp \
    elements/devicesdatas.cpp \
    elements/device.cpp \
    elements/curve.cpp \
    track.cpp \
    elements/plots.cpp \
    elements/plot.cpp \
    filesearchernodb.cpp \
    cfg.cpp \
    elements/label.cpp \
    elements/listlabels.cpp \
    datalabel.cpp \
    helpbaseform.cpp

HEADERS += \
        mainwindow.h \
    appmanager.h \
    gcedataview.h \
    projectmanager.h \
    project.h \
    treeelement.h \
    projecttreewidget.h \
    qcustomplot.h \
    elements/devicesdatas.h \
    elements/device.h \
    elements/curve.h \
    track.h \
    elements/plots.h \
    elements/plot.h \
    filesearchernodb.h \
    cfg.h \
    elements/label.h \
    elements/listlabels.h \
    datalabel.h \
    helpbaseform.h

FORMS += \
        mainwindow.ui \
    helpbaseform.ui

INCLUDEPATH     += $$PWD/../gceLogicLayer
DEPENDPATH      += $$PWD/../gceLogicLayer

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../gceLogicLayer/release/ -lgceLogicLayer
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../gceLogicLayer/debug/ -lgceLogicLayer

unix: LIBS += -L$$OUT_PWD/../gceLogicLayer -lgceLogicLayer -L$$OUT_PWD/../gceImportModule -lgceImportModule
#unix: LIBS += -L$$OUT_PWD/../gceImportModule -lgceImportModule

#win32:DESTDIR = $$OUT_PWD/../sys
DESTDIR = $$OUT_PWD/../sys
unix {
#    CONFIG(release, debug|release): QMAKE_POST_LINK = cp $$OUT_PWD/../gce/release/gce $$OUT_PWD/../sys/gce
#    else: CONFIG(debug, debug|release): QMAKE_POST_LINK = cp $$OUT_PWD/../gce/debug/gce $$OUT_PWD/../sys/gce

    #QMAKE_POST_LINK = \
        #cp $$OUT_PWD/gce $$OUT_PWD/../sys/gce
    QMAKE_LFLAGS += -no-pie
    DEFINES += LNX
}

#DISTFILES +=

RESOURCES += \
    rsc_1.qrc

#TRANSLATIONS += gce_RU.ts

