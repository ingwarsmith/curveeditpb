#ifndef FILESEARCHER_H
#define FILESEARCHER_H

#include <QList>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>

class FileSearcherNoDb
{
public:
    FileSearcherNoDb();
    QFileInfoList       retFilesInFolderAndSubFolders(QFileInfo folder, QStringList masks = QStringList(), bool searchInSubFolders = true);
    QFileInfoList       retFilesInFolder(QFileInfo folder, QStringList masks = QStringList());
    QFileInfoList       retSubFoldersInFolder(QFileInfo folder);
    QFileInfoList       retAllSubFoldersTreeAsList(QFileInfo folder);
private:
    //QString
};

#endif // FILESEARCHER_H
