#include "appmanager.h"
#include <QApplication>
#include <QIcon>
#include <QFileInfo>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);    
    app.setWindowIcon(QIcon(":/ico1/appIcon"));
    AppManager mgr;    
    if (mgr.checkConfiguration())
    {
        mgr.loadConfiguration();
    }
    if (mgr.getLanguageFromCgf() == QString("ru"))
    {
        auto myTranslator = new QTranslator;
        myTranslator->setParent(&app);
        myTranslator->load(":/lang/ru_translate");
        qApp->installTranslator(myTranslator);
    }
    auto updateProgramNeeded = false;
    mgr.updateChecking(updateProgramNeeded);
    mgr.viewUpdateErrorIfExist();
    if (updateProgramNeeded)
    {
        mgr.updateStart();
        return 0;
    }
#ifdef Q_OS_UNIX
    QStringList libPaths = app.libraryPaths();
    libPaths.append( QString(app.applicationDirPath()).append("/import.modules"));
    app.setLibraryPaths(libPaths);
#endif
    int result = 0;
    mgr.init();
    mgr.run();
    result = app.exec();

    mgr.saveConfiguration();

    return result;
}
