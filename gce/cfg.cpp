#include "cfg.h"
#include <QProcessEnvironment>
#include <QDir>
#include <QFile>
#include <QTextStream>

#include <gcelogiclayer.h>

Configuration::Configuration()
{
    dataMembers.defaultValues.languageDEFAULT = LANGUAGE_RUSSIAN;
    dataMembers.defaultValues.lastProjectsCountDEFAULT = 10;
    dataMembers.defaultValues.removeTimeLoopsDEFAULT = true;
    dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT = 1000;
    dataMembers.defaultValues.loadDataAutomaticallyThenAddGraphicsToTrackDEFAULT = true;
    dataMembers.defaultValues.tracksDefaultHeightDEFAULT = 250;

    dataMembers.language = dataMembers.defaultValues.languageDEFAULT;
    dataMembers.lastProjectsCount = dataMembers.defaultValues.lastProjectsCountDEFAULT;
    dataMembers.removeTimeLoops = dataMembers.defaultValues.removeTimeLoopsDEFAULT;
    dataMembers.timeLoopPeriodMilliseconds = dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT;
    dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack = dataMembers.defaultValues.loadDataAutomaticallyThenAddGraphicsToTrackDEFAULT;
    dataMembers.tracksDefaultHeight = dataMembers.defaultValues.tracksDefaultHeightDEFAULT;
}

void Configuration::checkCfg()
{
#ifdef Q_OS_WIN
    auto userAppDataPath = QProcessEnvironment::systemEnvironment().value("APPDATA");
    userAppDataPath.replace("\\", "/");
    auto configFileDirPath = QString(userAppDataPath).append("/GCE");
#else
    auto userAppDataPath = QProcessEnvironment::systemEnvironment().value("HOME");
    auto configFileDirPath = QString(userAppDataPath).append("/.GCE");
#endif
    auto configFilePath = QString(configFileDirPath).append("/configuration.f");
    QDir configFileDir(configFileDirPath);
    if (!configFileDir.exists())
    {
        QDir userAppDataDir(userAppDataPath);
#ifdef Q_OS_WIN
        userAppDataDir.mkdir("GCE");
#else
        userAppDataDir.mkdir(".GCE");
#endif
    }
    dataMembers.configFileFullPath = configFilePath;
    QFile configFile(configFilePath);
    if (!configFile.exists())
    {
        // заполнение файла
        if (configFile.open(QFile::WriteOnly|QFile::Text))
        {
            QTextStream stream(&configFile);
            stream << LAST_PROJECTS_MARKER << DELIMETER
                   << QString::number(dataMembers.defaultValues.lastProjectsCountDEFAULT)
                   << DELIMETER << "\n";
            stream << LANGUAGE_MARKER << DELIMETER
                   << dataMembers.defaultValues.languageDEFAULT << "\n";
            stream << TIME_LOOPS_MARKER << DELIMETER << DELETE
                   << DELIMETER
                   << QString::number(dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT) << "\n";
            stream << AUTO_LOAD_GRAPH_DATA_MARKER << DELIMETER << AUTOLOAD << "\n";
            stream << TRACK_HEIGHT_DATA_MARKER << DELIMETER
                   << dataMembers.tracksDefaultHeight << "\n";

            configFile.close();
        }

        // настройки по умолчанию
        setDefaultSettings();
    }
    else
    {
        if (configFile.open(QFile::ReadOnly|QFile::Text))
        {
            QTextStream stream(&configFile);
            while (!stream.atEnd())
            {
                auto line = stream.readLine();
                if (line.isEmpty() && !line.contains(DELIMETER))
                {
                    continue;
                }
                auto tmp_sl = line.split(DELIMETER);
                if (tmp_sl.size() == 1)
                {
                    continue;
                }
                if (tmp_sl.at(0) == LAST_PROJECTS_MARKER)
                {
                    auto okBool = false;
                    auto value = tmp_sl.at(1);
                    value.toInt(&okBool);
                    if (okBool)
                    {
                        dataMembers.lastProjectsCount = value.toInt();
                    }
                    else
                    {
                        dataMembers.lastProjectsCount = dataMembers.defaultValues.lastProjectsCountDEFAULT;
                    }
                    if (tmp_sl.at(2) != "")
                    {
                        auto prjList = tmp_sl.at(2);
                        QStringList sl_prjLst = prjList.split(";");
                        dataMembers.lastProjectsPaths = sl_prjLst;
                    }
                }
                if (tmp_sl.at(0) == LANGUAGE_MARKER)
                {
                    dataMembers.language = tmp_sl.at(1);
                }
                if (tmp_sl.at(0) == TIME_LOOPS_MARKER)
                {
                    dataMembers.removeTimeLoops = tmp_sl.at(1) == DELETE;
                    auto isInt = false;
                    tmp_sl.at(2).toInt(&isInt);
                    if (isInt)
                    {
                        dataMembers.timeLoopPeriodMilliseconds = tmp_sl.at(2).toInt();
                    }
                    else
                    {
                        dataMembers.timeLoopPeriodMilliseconds = dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT;
                    }
                    GceLogicLayer::getInstance().setTimeLoopPeriodMilliseconds(dataMembers.timeLoopPeriodMilliseconds);
                }
                if (tmp_sl.at(0) == AUTO_LOAD_GRAPH_DATA_MARKER)
                {
                    dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack = tmp_sl.at(1) == AUTOLOAD;
                }
                if (tmp_sl.at(0) == TRACK_HEIGHT_DATA_MARKER)
                {
                    dataMembers.tracksDefaultHeight = QString(tmp_sl.at(1)).toInt();
                }
            }
        }
    }
}

void Configuration::setDefaultSettings()
{
    dataMembers.language = dataMembers.defaultValues.languageDEFAULT;
    dataMembers.lastProjectsCount = dataMembers.defaultValues.lastProjectsCountDEFAULT;
    dataMembers.removeTimeLoops = dataMembers.defaultValues.removeTimeLoopsDEFAULT;
    dataMembers.timeLoopPeriodMilliseconds = dataMembers.defaultValues.timeLoopPeriodMillisecondsDEFAULT;
    dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack = dataMembers.defaultValues.loadDataAutomaticallyThenAddGraphicsToTrackDEFAULT;
    GceLogicLayer::getInstance().setTimeLoopPeriodMilliseconds(dataMembers.timeLoopPeriodMilliseconds);
    dataMembers.tracksDefaultHeight = dataMembers.defaultValues.tracksDefaultHeightDEFAULT;
}

void Configuration::addProjectPathToListOfLastProjectPaths(QString projectPathNew)
{
    if (dataMembers.lastProjectsPaths.isEmpty())
    {
        dataMembers.lastProjectsPaths.append(projectPathNew);
        return;
    }
    if (dataMembers.lastProjectsPaths.contains(projectPathNew))
    {
        dataMembers.lastProjectsPaths.removeOne(projectPathNew);
    }
    else
    {
        while (dataMembers.lastProjectsPaths.size() >= dataMembers.lastProjectsCount)
        {
            dataMembers.lastProjectsPaths.removeLast();
        }
    }
    dataMembers.lastProjectsPaths.prepend(projectPathNew);
}

void Configuration::setLastProjectsCount(int count)
{
    dataMembers.lastProjectsCount = count;
    while (dataMembers.lastProjectsPaths.size() > count)
    {
        dataMembers.lastProjectsPaths.removeLast();
    }
}

void Configuration::saveSettingsToCfg()
{
    QFile configFile(dataMembers.configFileFullPath);
    if (configFile.exists())
    {
        // заполнение файла
        if (configFile.open(QFile::WriteOnly|QFile::Text))
        {
            QTextStream stream(&configFile);
            stream << LAST_PROJECTS_MARKER << DELIMETER
                   << QString::number(dataMembers.lastProjectsCount)
                   << DELIMETER << dataMembers.lastProjectsPaths.join(";") << "\n";
            stream << LANGUAGE_MARKER << DELIMETER
                   << dataMembers.language << "\n";
            stream << TIME_LOOPS_MARKER << DELIMETER << (dataMembers.removeTimeLoops ? DELETE : SAVE)
                   << DELIMETER
                   << QString::number(dataMembers.timeLoopPeriodMilliseconds) << "\n";
            stream << AUTO_LOAD_GRAPH_DATA_MARKER << DELIMETER
                   << (dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack ? AUTOLOAD : MANUALLOAD)
                   << "\n";
            stream << TRACK_HEIGHT_DATA_MARKER << DELIMETER
                   << dataMembers.tracksDefaultHeight << "\n";

            configFile.close();
        }
    }
}

QStringList Configuration::getLanguages()
{
    QStringList ret;
    ret << tr("Russian") << tr("English");
    return ret;
}
