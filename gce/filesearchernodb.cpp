#include "filesearchernodb.h"

FileSearcherNoDb::FileSearcherNoDb()
{ }

QFileInfoList FileSearcherNoDb::retFilesInFolderAndSubFolders(QFileInfo folder, QStringList masks, bool searchInSubFolders)
{
    QFileInfoList result = retFilesInFolder(folder, masks);

    if (searchInSubFolders)
    {
        QFileInfoList subdirs = retSubFoldersInFolder(folder);

        for (QFileInfo &folder__ : subdirs)
        {
            result.append(
                        retFilesInFolderAndSubFolders(folder__, masks, true)
                        );
        }
    }

    return result;
}

QFileInfoList FileSearcherNoDb::retFilesInFolder(QFileInfo folder, QStringList masks)
{
    if (masks == QStringList()) masks = QStringList(QString("*"));

    QFileInfoList result;

    QDir fldr(folder.absoluteFilePath());
    fldr.setNameFilters(masks);

    for (QFileInfo &fl : fldr.entryInfoList(QDir::Files | QDir::Hidden | QDir::System ))
    {
        if (fl.suffix().toUpper() != QString("DB"))
            result.append(fl);
    }

    return result;
}

QFileInfoList FileSearcherNoDb::retSubFoldersInFolder(QFileInfo folder)
{
    QFileInfoList result;

    QDir fldr(folder.absoluteFilePath());

    QFileInfoList folderLst = fldr.entryInfoList(QDir::Dirs | QDir::Hidden);

    for (QFileInfo &fl : folderLst)
    {
        if (fl.fileName() == QString(".") ||
                fl.fileName() == QString(".."))
            continue;
        result.append(fl);
    }

    return result;
}

QFileInfoList FileSearcherNoDb::retAllSubFoldersTreeAsList(QFileInfo folder)
{
    QFileInfoList result = retSubFoldersInFolder(folder);
    QFileInfoList deep;
    for (QFileInfo &folder__ : result)
    {
        deep.append(
                    retAllSubFoldersTreeAsList(folder__)
                    );
    }
    result.append(deep);
    return result;
}
