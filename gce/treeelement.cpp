#include "treeelement.h"
#include "project.h"

TreeElement::TreeElement(uint32_t indexProject_, QWidget *parent, QObject *projectPtr,
                         bool isProjectElement) //: QWidget(parent)
{
    menuCustom = new QMenu(parent);
    type = ElementType::elementNone;

    ptrProject = projectPtr;

    indexProject = indexProject_;

    isGroup = false;

    if (isProjectElement)
    {
        return;
    }

    auto project = qobject_cast<Project*>(projectPtr);
    project->sendElementToMap(this);
}

TreeElement::TreeElement(TreeElement *another)
{
    menuCustom = new QMenu(another);

    this->type = another->type;
    setNewNameElement(another->getNameElement());
    keyElem = another->keyElem + 1;
    auto chCnt = another->childCount();
    for (decltype (chCnt) childIdx = 0; childIdx < chCnt; ++childIdx)
    {
        auto chld = static_cast<TreeElement*>(another->child(childIdx));
        insertChild(childIdx, chld);
    }
    ptrProject = another->getProjectPointer();
    indexProject = another->getIndexProject();

    auto project = qobject_cast<Project*>(ptrProject);
    project->sendElementToMap(this);
}

QString TreeElement::getProjectHeaderPath()
{
    auto project = qobject_cast<Project*>(ptrProject);
    return project->getProjectHeaderPath();
}

void TreeElement::setType(ElementType tp)
{
    auto fnt = QTreeWidgetItem::font(0);
    fnt.setPointSize(12);
    if (tp == ElementType::elementProject)
    {
        fnt.setBold(true);
        setTextColor(0, Qt::red);
        setExpanded(true);
    }
    if (tp == ElementType::elementDevicesData ||
            tp == ElementType::elementCalculatedData ||
            tp == ElementType::elementViews ||
            tp == ElementType::elementListLabels)
    {
        fnt.setItalic(true);
        fnt.setUnderline(true);
        setExpanded(true);
    }
    if (tp == ElementType::elementView)
    {
        fnt.setItalic(true);
    }
    if (tp == ElementType::elementDevice || tp == ElementType::elementView
            || tp == ElementType::elementCalcCurve || tp == ElementType::elementChangedCurve
            || tp == ElementType::elementCurveLinkInTrack || tp == ElementType::elementTrack)
    {
        setExpanded(true);
    }
    QTreeWidgetItem::setFont(0, fnt);
    type = tp;

    if (tp == ElementType::elementCalcCurve || tp == ElementType::elementChangedCurve ||
            tp == ElementType::elementRawCurve || tp == ElementType::elementCurveLinkInTrack ||
            tp == ElementType::elementSavedRawCurve || tp == ElementType::elementNotLoadedCurve)
    {
        fnt.setPointSize(fnt.pointSize()-3);

        if (tp != ElementType::elementNotLoadedCurve)
        {
            fnt.setBold(true);
        }

        QTreeWidgetItem::setFont(0,fnt);
    }

//    if (tp == elementTrack || tp == elementCalculatedData || tp == elementView)
//    {
//        setAcceptDrops(true);
//    }
//    else
//    {
//        setAcceptDrops(false);
//    }
}

void TreeElement::setMenuParent(QWidget *parent)
{
    menuCustom->setParent(parent);
}

QWidget *TreeElement::getMenuParent()
{
    return qobject_cast<QWidget*>(menuCustom->parent());
}

void TreeElement::setNewNameElement(QString nm)
{
    setText(0, nm);
    txtNm = nm;
}

QString TreeElement::getNameElement()
{
    return text(0);
}

void TreeElement::insertSubElement(TreeElement *subElem, int32_t index)
{
    if (index == -1)
        index = childCount();
    insertChild(index, static_cast<QTreeWidgetItem*>(subElem));
}

void TreeElement::addSubElement(TreeElement *subElem)
{
    insertChild(childCount(), static_cast<QTreeWidgetItem*>(subElem));
}

void TreeElement::removeSubElement(TreeElement *subElemDel)
{
    removeChild(static_cast<QTreeWidgetItem*>(subElemDel));
}

void TreeElement::setContextMenuActions(QList<QAction *> actions)
{
    menuCustom->clear();
    menuCustom->addActions(actions);
    return;
}

void TreeElement::setToolTip(const QString &toolTip)
{
    QTreeWidgetItem::setToolTip(0, toolTip);
}

void TreeElement::slotShowCustomContextMenu(QPoint pos)
{
    Q_UNUSED(pos)
    if (menuCustom->children().size() == 0)
    {
        return;
    }
    menuCustom->show();
}


