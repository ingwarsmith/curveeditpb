#include "projecttreewidget.h"
#include <QMessageBox>
#include <QApplication>

#include <QDrag>
#include <QMimeData>

#include "treeelement.h"
#include "projectmanager.h"

#include "elements/curve.h"
#include "elements/device.h"

#include <gcelogiclayer.h>

ProjectTreeWidget::ProjectTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
    lastSelectedItem = nullptr;
    setHeaderHidden(true);
    connect(this, SIGNAL(itemEntered(QTreeWidgetItem*,int)), this, SLOT(slotItemClicked(QTreeWidgetItem*)));
    connect(this, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(slotItemClicked(QTreeWidgetItem*)));
    setSelectionMode(QTreeWidget::SingleSelection);
    setDragEnabled(true);
    setDropIndicatorShown(true);

    viewport()->setAcceptDrops(false);
}

void ProjectTreeWidget::mousePressEvent(QMouseEvent *mse)
{
    if (itemsProject.isEmpty())
    {
        return;
    }
    ::QTreeWidget::mousePressEvent(mse);
    auto item = this->itemAt(mse->pos());
    if (item)
    {
        slotItemClicked(item);
        mse->accept();
        auto elementItem = static_cast<TreeElement*>(item);
        auto item_type = elementItem->type;
        if ((item_type == TreeElement::ElementType::elementRawCurve ||
                item_type == TreeElement::ElementType::elementCalcCurve ||
                item_type == TreeElement::ElementType::elementChangedCurve ||
                item_type == TreeElement::ElementType::elementSavedRawCurve ||
                item_type == TreeElement::ElementType::elementNotLoadedCurve ||
                item_type == TreeElement::ElementType::elementArrayType)
                && mse->button() == Qt::LeftButton   )
        {
            dragStartPosition = mse->pos();
            lastSelectedItem = elementItem;
        }
    }
}

void ProjectTreeWidget::mouseMoveEvent(QMouseEvent *msme)
{
    if (itemsProject.isEmpty())
    {
        return;
    }

    if (!(msme->buttons() & Qt::LeftButton))
    {
        return;
    }

    if ((msme->pos() - dragStartPosition).manhattanLength() < qApp->startDragDistance())
    {
        return;
    }

    auto project = qobject_cast<Project*>(lastSelectedItem->getProjectPointer());
    auto found = false;
    QList<Curve*> curvesList;
    for (Device *dev : project->getDevicesMapPtrInProject()->values())
    {
        for (Curve *crInSrc : dev->getCurvesMapPtr()->values())
        {
            curvesList << crInSrc;
            if (crInSrc->getItem() == lastSelectedItem)
            {
                found = true;
                crInSrc->setObjectName(ObjectsNames::objNameAddedCurve);
                if (GceLogicLayer::getInstance()
                        .existObjectInObjectContainerByObjName(ObjectsNames::objNameAddedCurve))
                {
                    auto o = GceLogicLayer::getInstance()
                            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameAddedCurve);
                    auto idx = GceLogicLayer::getInstance().getObjectsContainerPointer()->indexOf(o);
                    GceLogicLayer::getInstance().getObjectsContainerPointer()->takeAt(idx);
                }
                GceLogicLayer::getInstance().getObjectsContainerPointer()
                        ->append(qobject_cast<QObject*>(crInSrc));
                break;
            }
        }
        if (found)
        {
            break;
        }
    }

    bool arrayCurves = false;
    if (!found && lastSelectedItem->getType() == TreeElement::ElementType::elementArrayType)
    {
        QList<Curve*> curvesList4drag;
        auto childCount = lastSelectedItem->childCount();
        if (childCount > 0)
        {
            for (decltype (childCount) i_child = 0; i_child < childCount; ++i_child)
            {
                auto treeWgtItem = lastSelectedItem->child(i_child);
                auto treeElem = static_cast<TreeElement*>(treeWgtItem);

                for (Curve *crInSrc : curvesList)
                {
                    if (crInSrc->getItem() == treeElem)
                    {
                        curvesList4drag << crInSrc;
                    }
                }
            }
        }
        if (curvesList4drag.size())
        {
            found = true;
            arrayCurves = true;
            if (GceLogicLayer::getInstance()
                    .existObjectInObjectContainerByObjName(ObjectsNames::objNameAddedCurves_))
            {
                auto o = GceLogicLayer::getInstance()
                        .getObjectFromObjectContainerByObjName(ObjectsNames::objNameAddedCurves_);
                auto idx = GceLogicLayer::getInstance().getObjectsContainerPointer()->indexOf(o);
                GceLogicLayer::getInstance().getObjectsContainerPointer()->takeAt(idx);
            }
            ObjectsPack *pack = new ObjectsPack;
            pack->setObjectName(ObjectsNames::objNameAddedCurves_);
            auto sz = curvesList4drag.size();
            for (decltype (sz) c = 0; c < sz; ++c)
            {
                pack->pack.insert(c, qobject_cast<QObject*>(curvesList4drag.at(c)));
            }
            GceLogicLayer::getInstance().getObjectsContainerPointer()
                    ->append(qobject_cast<QObject*>(pack));
        }
    }

    if (!found)
        return;

    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    if (!arrayCurves)
    {
        mimeData->setData(QString("specialcopy/curve"), QByteArray());
    }
    else
    {
        mimeData->setData(QString("specialcopy/curves_array"), QByteArray());
    }
    drag->setMimeData(mimeData);
    drag->exec(Qt::CopyAction);
}

void ProjectTreeWidget::slotItemClicked(QTreeWidgetItem *item)
{
    auto projectMgr =
            qobject_cast<ProjectManager*>(
                GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                    ObjectsNames::objNameProjectManager)
            );
    auto found = false;
    auto szPrj = projectMgr->getProjectsArrayPtr()->keys().size();
    for (decltype (szPrj) p = 0; p < szPrj; ++p)
    {
        auto project_ = projectMgr->getProjectsArrayPtr()->value(projectMgr->getProjectsArrayPtr()->keys().at(p));
        for (TreeElement *el : project_->getAllElements())
        {
            //emit el->sigClicked();
            if (el->baseItem() == item)
            {
                lastSelectedItem = el;
                found = true;
                projectMgr->setCurrentProjectByID(el->getIndexProject());
                break;
            }
        }
        if (found)
        {
            break;
        }
    }
}


