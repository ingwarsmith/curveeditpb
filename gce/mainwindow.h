#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QTimer>
#include <QDateTime>
#include <QMouseEvent>
#include <QCloseEvent>
#include <QProgressBar>
#include "projectmanager.h"

class GceDataView;

namespace Ui {
class MainWindow;
}

class ProgressBarWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ProgressBarWindow(QWidget *parent = nullptr);

private:
    QTimer timer;
    QProgressBar pBar;

public slots:
    void    slotOpenAndStart(QString title);
    void    slotStopAndClose();
};

class QSplitter;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(ProjectManager *prjMgrInput, QWidget *parent = nullptr);
    ~MainWindow();
    void        setProjectPathInTitle(QString prjPath)
    {
        auto title = baseTitle;
        if (!prjPath.isEmpty())
        {
            title.append(" - [");
            title.append(prjPath);
            title.append("]");
        }
        setWindowTitle(title);
    }

protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void resizeEvent(QResizeEvent *revt);
    virtual void keyPressEvent(QKeyEvent *kevt);

private:
    Ui::MainWindow *ui;
    ProjectManager *prjMgr;
    QHBoxLayout    *_lt_main;
    QMenu          *menuSession,
                   *menuProject,
                   *menuActions,
                   *menuSettings,
                   *menuView,
                   *menuHelp;
    GceDataView    *dview;

    QString        baseTitle;

    ProgressBarWindow pBarWnd;

    bool           addingCurveToTrackModeEnabled;
    QCursor        usualCursor;
    QMap<QString, QAction*> mapHelpActionsByFileNames;
    QAction        *actionShowProjectTree;
    int            widthSavingForActionShowProjectTree;
    QList<int>     szs;
    QSplitter      *splt;

    void           makeHelpMenu(QMenu *mnu);

signals:
    void    sigShowHideOnGraphicsAllDots();

public slots:
    void    slotPrepareContextMenu(QPoint pos);

private slots:
    void    slotRecentProjectsMenu();
    void    slotRecentProjectOpen();
    void    slotSettings();
    void    slotDefaultSettings();
    void    slotAbout();
    void    slotAddingLabelMode();
    void    slotDimensionMode();
    void    slotScalingByRectangleMode();
    void    slotHelpMenu();
    void    slotActionPTW(bool isChecked);
};

#endif // MAINWINDOW_H
