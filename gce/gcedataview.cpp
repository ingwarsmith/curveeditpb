#include "gcedataview.h"
#include <gcelogiclayer.h>
#include "gcedatasource.h"
//#include <gceloggingdata.h>
#include <base/variablecontainer.h>
#include "projectmanager.h"
#include "projecttreewidget.h"
#include "track.h"
#include "elements/device.h"
#include "elements/curve.h"

#include "mainwindow.h"

#include "datalabel.h"

#include <base/variablecontainer.h>

#include "cfg.h"

#include <QSplitter>
#include <QPair>
#include <QBrush>

#include <QStringList>

#include <cstdlib>
#include <iostream>

SpecLabel::SpecLabel(QString name) : QLabel(name) {}

void SpecLabel::setTimeZoneText(QString tz)
{
    tzText = tz;
    updateSpecLabelText();
}

void SpecLabel::setDescription(QString desc)
{
    descrText = desc;
    updateSpecLabelText();
}
void SpecLabel::mouseDoubleClickEvent(QMouseEvent *evt)
{
    emit sigMouseDoubleClicked(mapToGlobal(evt->pos()));
}

void SpecLabel::updateSpecLabelText()
{
    QString txt = tzText;
    if (!descrText.isEmpty())
    {
        txt.append("\t").append(descrText);
    }
    setText(txt);
}

GceDataView::GceDataView(QWidget *parent) : QTabWidget(parent)
{
    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameProjectManager
                );
    auto pmgr = qobject_cast<ProjectManager*>(o);
    connect(pmgr->projectTreeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
                this, SLOT(slotShowSelectedPlot(QTreeWidgetItem*,QTreeWidgetItem*)));

    connect(&GceLogicLayer::getInstance(), SIGNAL(sigDrawOrRedrawGraphic(uint32_t,uint32_t)),
            this, SLOT(slotDrawOrRedrawGraphic(uint32_t,uint32_t)));

    connect(this, SIGNAL(currentChanged(int)), this, SLOT(slotResetScrollOnSelected()));

    setAcceptDrops(true);

    projectOpeningComplete = false;
}

void GceDataView::addPlot(Plot *plotAdded, bool withEmptyTrack)
{    
    auto swpNew = new SpecialWidgetForPlot(plotAdded, this);
    auto project = qobject_cast<Project*>( plotAdded->getItem()->getProjectPointer() );
    swpNew->slotChangeTimeZone( project->timeZone );
    connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), swpNew, SLOT(slotChangeTimeZone(QTimeZone)));
    plotAddingInternal(plotAdded, swpNew);
    connect(swpNew, SIGNAL(sigPlotMustBeRemoved(Plot*)), this, SLOT(slotPlotRemove(Plot*)));
    connect(swpNew, SIGNAL(sigActivatedViewValues(bool)), this, SLOT(slotActivatedValuesView(bool)));
    connect(swpNew, &SpecialWidgetForPlot::sigActivateMe, this, &GceDataView::slotPlotQuery4activate);
    this->setCurrentWidget(swpNew);
    if (withEmptyTrack)
        swpNew->addEmptyTrack_public();
}

void GceDataView::removeDeviceFromPlots(QString devName, uint32_t srcID)
{
    Q_UNUSED(srcID)
    auto project = qobject_cast<ProjectManager*>(GceLogicLayer::getInstance()
                                                  .getObjectFromObjectContainerByObjName(ObjectsNames::objNameProjectManager))
            ->getProjectsArrayPtr()->values().last();
    for (decltype (widgetsPlots.keys().size()) iwPlot = 0; iwPlot < widgetsPlots.keys().size(); ++iwPlot)
    {
        auto plotWidget = widgetsPlots.value(widgetsPlots.keys().at(iwPlot));
        for (decltype (plotWidget->getTracksVectorPtr()->size()) itrack_k = 0;
             itrack_k < plotWidget->getTracksVectorPtr()->size(); itrack_k++)
        {
            auto track = plotWidget->getTracksVectorPtr()->value(itrack_k);
            QVector<int> indexesCurveItemsForDeletingFromLast;
            QStringList namesOfDeletingCurves;
            for (decltype (track->getItem()->childCount()) i_twi = 0; i_twi < track->getItem()->childCount(); ++i_twi)
            {
                TreeElement *element = static_cast<TreeElement*>(track->getItem()->child(i_twi));
                if (element->getNameElement().split(" / ").at(1) == devName)
                {
                    indexesCurveItemsForDeletingFromLast.prepend(i_twi);
                    namesOfDeletingCurves.append(element->getNameElement().split(" / ").at(0));
                }
            }
            while (indexesCurveItemsForDeletingFromLast.size())
            {
                QTreeWidgetItem *tw_item = track->getItem()->takeChild(indexesCurveItemsForDeletingFromLast.takeFirst());
                TreeElement *element = static_cast<TreeElement*>(tw_item);
                auto elementsMap = project->getElementsHashPtr();
                auto keyElem = elementsMap->key(element);
                elementsMap->take(keyElem);
                delete element;
            }

            for (decltype (track->graphicsArray.size()) t_graph = 0; t_graph < track->graphicsArray.size(); ++t_graph)
            {
                GraphWithSettings *gws = track->graphicsArray.value(t_graph);
                if (gws->deviceName == devName && namesOfDeletingCurves.contains(gws->nameGraph))
                {
                    gws->loaded = false;
                    gws->sourceID = NULL_ID;
                    gws->curveID = NULL_ID;
                    gws->graphic->setData(QVector<double>(), QVector<double>());

                    track->makeCurveToTrackWithoutDevice(gws->nameGraph, gws->deviceName);
                }
            }
        }
        plotWidget->slotAfterRemovingCurve();
        plotWidget->updatePlots(true);
    }
}

void GceDataView::removeAllPlotsOnClose()
{
    projectOpeningComplete = false;

    widgetsJoinedByTimeScale_List.clear();
    while (widgetsPlots.size())
    {
        auto pElem = widgetsPlots.firstKey();
        auto pWgt = widgetsPlots.take(pElem);
        removeTab(indexOf(pWgt));
        delete pWgt;
        // pElem будет удален при чистке всех элементов дерева проекта в деструкторе класса Project
    }
}

void GceDataView::openPlotFromTemplateFile(QString templateFileName)
{
    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameProjectManager
                );
    auto pmgr = qobject_cast<ProjectManager*>(o);
    auto project = pmgr->getProjectsArrayPtr()->last();

    auto devicesList = project->getDevicesMapPtrInProject()->values(); // QList<Device*>

    QStringList devNamesInProject, devNamesOnStandalonePlot;
    for (Device *dev : devicesList)
    {
        devNamesInProject << dev->getItem()->getNameElement();
    }

    auto fName = templateFileName;

    QFile fl(fName);
    auto ok = fl.open(QFile::ReadOnly);
    if (!ok)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("File can't be read"));
        return;
    }

    QDataStream odatastr(&fl);
    QString namePlotElement, namePlotInternal;
    odatastr >> namePlotElement;

    if (namePlotElement.isEmpty())
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("Wrong file format"));
        return;
    }

    odatastr >> namePlotInternal;

    if (namePlotElement != namePlotInternal)
        namePlotInternal = namePlotElement;

    auto idxPlot = project->getPlotsCount();
    auto newPlotElement = new Plot(project->getThisProjectIndex(), idxPlot, project, project);
    newPlotElement->getItem()->setNewNameElement(namePlotElement);
    project->addPlotToPlotList(newPlotElement);

    auto newPlotWidget = new SpecialWidgetForPlot(newPlotElement, this);
    newPlotWidget->slotChangeTimeZone( project->timeZone );
    connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), newPlotWidget, SLOT(slotChangeTimeZone(QTimeZone)));
    connect(newPlotWidget, SIGNAL(sigPlotMustBeRemoved(Plot*)), this, SLOT(slotPlotRemove(Plot*)));
    connect(newPlotWidget, SIGNAL(sigActivatedViewValues(bool)), this, SLOT(slotActivatedValuesView(bool)));
    connect(newPlotWidget, &SpecialWidgetForPlot::sigActivateMe, this, &GceDataView::slotPlotQuery4activate);

    plotAddingInternal(newPlotElement, newPlotWidget);

    int32_t devCountInStandalonePlot;
    odatastr >> devCountInStandalonePlot;

    for (int32_t iDevName = 0; iDevName < devCountInStandalonePlot; ++iDevName)
    {
        QString devName;
        odatastr >> devName;
        devNamesOnStandalonePlot << devName;
    }

    newPlotWidget->setDevNames(devNamesOnStandalonePlot.toVector());

    double minX, maxX;

    odatastr >> minX;
    odatastr >> maxX;

    int32_t trackCount;
    odatastr >> trackCount;

    QHash<uint32_t, QStringList> graphicsSetsBySourceId;
    QList<GraphWithSettings*> eachGwsToLoadAndDrawRedraw;

    while (trackCount > 0)
    {
        auto track = newPlotWidget->addEmptyTrack_public();

        if (minX == maxX) maxX++;

        int32_t graphsCount;
        odatastr >> graphsCount;

        while (graphsCount > 0)
        {
            QString deviceName, nameGraph;
            uint32_t sourceID, curveID;
            int thicknessLine, arrowOffsetFromPlot;
            QColor currentPenColor, savedStandardPenColor;
            QPen colorPen;
            QBrush colorBrush;
            double scaleMinimumValue, scaleMaximumValue;

            odatastr >> deviceName;
            odatastr >> sourceID;
            odatastr >> curveID;
            odatastr >> thicknessLine;
            odatastr >> currentPenColor;
            odatastr >> savedStandardPenColor;
            odatastr >> colorBrush;
            odatastr >> colorPen;
            odatastr >> arrowOffsetFromPlot;
            odatastr >> scaleMinimumValue;
            odatastr >> scaleMaximumValue;
            odatastr >> nameGraph;

            VariableContainer vcontainer;

            auto data_ = vcontainer.read_container_hash();

            odatastr >> data_;

            vcontainer.write_container_hash(data_);

            // прочитать из контейнера... о графике
            auto useNullValues_ = false;
            useNullValues_ = vcontainer.get_value<bool>("gws_use_null_values");
            auto nv_val = vcontainer.get_value<double>("gws_null_value_indic");
            auto frozenScaleValues_ = vcontainer.get_value<bool>("gws_frozen_scale_values", false);
            auto showDots = false;
            if (vcontainer.keyExist(QString("ShowDots_bool")))
            {
                showDots = vcontainer.get_value<bool>(QString("ShowDots_bool"));
            }
            auto pseudoNameGraph = vcontainer.get_value<QString>(QString("PseudoNameGraph"), QString());

            auto fixedDivisionValue = vcontainer.get_value<bool>(QString("gws_fix_division_value"), false);
            auto divisionValue = vcontainer.get_value<double>(QString("gws_division_value"), 1.0);

            graphsCount--;

            auto graphWS = track->addGraphicWithSettings();
            graphWS->useNullValues = useNullValues_;
            graphWS->frozenValuesScaleForAutoScale = frozenScaleValues_;
            graphWS->nullValueIfUsed = nv_val;
            graphWS->nameGraph = nameGraph;
            graphWS->pseudoNameGraph= pseudoNameGraph;
            graphWS->scaleMaximumValue = scaleMaximumValue;
            graphWS->scaleMinimumValue = scaleMinimumValue;
            graphWS->tracerForArrowUpper = new QCPItemTracer(track);
            graphWS->tracerForArrowMiddle = new QCPItemTracer(track);
            graphWS->tracerForArrowLower = new QCPItemTracer(track);
            graphWS->tracerForArrowUpper->setVisible(false);
            graphWS->tracerForArrowLower->setVisible(false);
            graphWS->tracerForArrowMiddle->setVisible(false);
            graphWS->tracerForArrowUpper->position->setType(QCPItemPosition::ptAbsolute);
            graphWS->tracerForArrowLower->position->setType(QCPItemPosition::ptAbsolute);
            graphWS->tracerForArrowMiddle->position->setType(QCPItemPosition::ptAbsolute);
            QPoint posTopLeft = track->axisRect()->topLeft(),
                    posBottomLeft = track->axisRect()->bottomLeft();
            posTopLeft.setX(posTopLeft.x() - track->graphicsArray.count()*TRACK_HEADER_WIDTH);
            posBottomLeft.setX(posTopLeft.x());
            graphWS->tracerForArrowUpper->position->setCoords(posTopLeft.x(), posTopLeft.y());
            graphWS->tracerForArrowLower->position->setCoords(posBottomLeft.x(), posBottomLeft.y());
            graphWS->tracerForArrowMiddle->position->setCoords(posTopLeft.x(), (posTopLeft.y()+posBottomLeft.y())/2);

            graphWS->arrow = new QCPItemLine(track);
            graphWS->arrow->setClipToAxisRect(false);
            graphWS->arrow->start->setParentAnchor(graphWS->tracerForArrowLower->position);
            graphWS->arrow->end->setParentAnchor(graphWS->tracerForArrowUpper->position);
            graphWS->arrow->start->setCoords(0, 0);
            graphWS->arrow->end->setCoords(0, 0);
            graphWS->lMaximum = new QCPItemText(track);
            graphWS->lMaximum->setClipToAxisRect(false);
            graphWS->lMaximum->position->setParentAnchor(graphWS->tracerForArrowUpper->position);
            graphWS->lMaximum->position->setCoords(/*-6*/6, -1);
            QString bnd;
            bnd.setNum(5000.);
            graphWS->lMaximum->setText(bnd.prepend(" ").append(" "));
            graphWS->lMaximum->setTextAlignment(Qt::AlignRight);
            graphWS->lMaximum->setPositionAlignment(Qt::AlignRight | Qt::AlignBottom);
            graphWS->lMaximum->setRotation(-90.0);
            graphWS->lMinimum = new QCPItemText(track);
            graphWS->lMinimum->setClipToAxisRect(false);
            graphWS->lMinimum->position->setParentAnchor(graphWS->tracerForArrowLower->position);
            graphWS->lMinimum->position->setCoords(/*-6*/6, 1);
            bnd.setNum(0.);
            graphWS->lMinimum->setText(bnd.prepend(" ").append(" "));
            graphWS->lMinimum->setTextAlignment(Qt::AlignLeft);
            graphWS->lMinimum->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
            graphWS->lMinimum->setRotation(-90.0);
            graphWS->labelNameGraphic = new QCPItemText(track);
            graphWS->labelNameGraphic->setClipToAxisRect(false);
            graphWS->updateLabelGraphicName();
            graphWS->labelNameGraphic->position->setParentAnchor(graphWS->tracerForArrowMiddle->position);
            graphWS->labelNameGraphic->position->setCoords(-12, 0);
            graphWS->labelNameGraphic->setRotation(-90.0);

            auto fnt = graphWS->labelNameGraphic->font();
            fnt.setPointSize(project->fontPointSize_GraphicsNamesInHeader);
            graphWS->labelNameGraphic->setFont(fnt);
            fnt = graphWS->lMaximum->font();
            fnt.setPointSize(project->fontPointSize_GraphicsMinMaxValuesInHeader);
            graphWS->lMaximum->setFont(fnt);
            graphWS->lMinimum->setFont(fnt);

            graphWS->loaded = false;
            graphWS->curveID = curveID;
            graphWS->sourceID = sourceID;
            graphWS->deviceName = deviceName;

            graphWS->currentPenColor = currentPenColor;
            graphWS->colorBrush = colorBrush;
            graphWS->colorPen = colorPen;
            graphWS->graphic->setPen(graphWS->colorPen);

            auto pen = graphWS->colorPen;
            pen.setWidth(2.0);
            graphWS->arrow->setPen(pen);
            graphWS->lMaximum->setColor(graphWS->currentPenColor);
            graphWS->lMinimum->setColor(graphWS->currentPenColor);
            graphWS->lMaximum->setBrush(QBrush(Qt::white));
            graphWS->lMinimum->setBrush(QBrush(Qt::white));

            graphWS->divisionValueForAxis = divisionValue;
            graphWS->divisionValueIsFixed = fixedDivisionValue;

            track->graphicsArray.append(graphWS);

            auto devicesExist = !devNamesInProject.isEmpty();
            auto neededDevicePresent = false;

            if (devNamesInProject.contains(deviceName))
            {
                neededDevicePresent = true;
                auto success = false;
                auto srcIdInData = GceLogicLayer::getInstance().getSourceIDbyDevNameOfSource(deviceName, success);
                if (success)
                {
                    auto crvIdInData = GceLogicLayer::getInstance().getCurveIDbySourceIDandCurveName(srcIdInData, nameGraph, success);
                    if (success)
                    {
                        graphWS->curveID = crvIdInData;
                        graphWS->sourceID = srcIdInData;

                        auto oklc = false;
                        auto loggingCurveLoaded = GceLogicLayer::getInstance()
                                .isLoggingCurveLoaded(graphWS->sourceID, graphWS->curveID, oklc);

                        if (oklc && !loggingCurveLoaded)
                        {
                            if (graphicsSetsBySourceId.keys().size()
                                    && graphicsSetsBySourceId.keys().contains(graphWS->sourceID))
                            {
                                QStringList slst = graphicsSetsBySourceId.value(graphWS->sourceID);
                                if (!slst.contains(graphWS->nameGraph))
                                {
                                    slst.append(graphWS->nameGraph);
                                    graphicsSetsBySourceId[graphWS->sourceID] = slst;
                                }
                            }
                            else
                            {
                                QStringList slst;
                                slst.append(graphWS->nameGraph);
                                graphicsSetsBySourceId[graphWS->sourceID] = slst;
                            }
                        }
                        eachGwsToLoadAndDrawRedraw.append(graphWS);
                    }
                }
            }

            Curve *curveInTrack;
            if (devicesExist && neededDevicePresent)
            {
                auto deviceElem = project->getDeviceElemBySourceID(graphWS->sourceID);
                auto curveElem = deviceElem->getCurvesMapPtr()->value(graphWS->curveID);
                curveElem->setCurveType_RAW();
                if (showDots)
                {
                    QCPScatterStyle square_style(QCPScatterStyle::ssSquare, 4);
                    graphWS->graphic->setScatterStyle(square_style);
                    graphWS->graphicScatterStyleIsSquare = true;
                }
                curveInTrack = track->copyCurveFromDeviceToTrack(curveElem);
            }
            else
            {
                curveInTrack = track->makeCurveToTrackWithoutDevice(nameGraph, deviceName);
            }

            connect(curveInTrack, SIGNAL(sigDeviceNameChanged(QString,QString)),
                    newPlotWidget, SLOT(slotDeviceNameChanged(QString,QString)));
        }

        VariableContainer vcontainer;

        auto data_ = vcontainer.read_container_hash();

        odatastr >> data_;

        vcontainer.write_container_hash(data_);

        // прочитать из контейнера... о треке

        track->setFixedHeight(vcontainer.get_value<int>(QString("TrackHeader_int")));
        if (vcontainer.keyExist(QString("HorizontalSectionsCount_int")))
        {
            auto hsc = vcontainer.get_value<int>(QString("HorizontalSectionsCount_int"));
            track->setHorizontalSectionsCount(hsc);
        }

        trackCount--;
    }

    for (uint32_t srcId : graphicsSetsBySourceId.keys())
    {
        QStringList tmp;
        GceLogicLayer::getInstance()
                .loadGraphicsDataFromImportedFiles(srcId,
                                                   graphicsSetsBySourceId.value(srcId),
                                                   tmp);
    }

    for (GraphWithSettings *gws : eachGwsToLoadAndDrawRedraw)
    {
        gws->loaded = true;
        GceLogicLayer::getInstance()
                .setExcludedTimesForNullValues(gws->sourceID,
                                               gws->curveID,
                                               gws->useNullValues,
                                               gws->nullValueIfUsed);

        emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(gws->sourceID, gws->curveID);
    }

    VariableContainer vcontainer;

    auto data_ = vcontainer.read_container_hash();

    odatastr >> data_;

    vcontainer.write_container_hash(data_);

    // прочитать из контейнера... о планшете

    if (vcontainer.keyExist(QString("PlotHeaderText")))
    {
        QString headerPlotTxt = vcontainer.get_value<QString>(QString("PlotHeaderText"));
        newPlotWidget->setDescriptionInHeader(headerPlotTxt);
    }

    newPlotWidget->updatePlots(true);
    newPlotWidget->recalcWgt2HeightForScrollBar();

    fl.close();
}

void GceDataView::plotAddingInternal(Plot *plotElement, SpecialWidgetForPlot *plotWidget)
{
    connect(plotElement, SIGNAL(sigSwapAxesOfPlot()), plotWidget, SLOT(slotSwapAxes()));
    connect(plotElement, SIGNAL(sigAddNewTrack()), plotWidget, SLOT(slotAddNewTrack()));
    connect(plotElement, SIGNAL(sigSavePlotInProject(QString)), plotWidget, SLOT(slotSavePlotToProject(QString)));
    connect(plotElement, SIGNAL(sigExportPlotToFile(QString)), plotWidget, SLOT(slotExportPlotToLocalFile(QString)));
    connect(plotWidget, SIGNAL(sigPlotRenamed(QString)), this, SLOT(slotPlotRenamed(QString)));
    connect(plotWidget, &SpecialWidgetForPlot::sigChangeTimeRangeForJoinedByTimePlots,
            this, &GceDataView::slotChangeTimeRangeForJoinedByTimePlots);

    QStringList namesPlots;
    for (Plot* plotElem : widgetsPlots.keys())
    {
        namesPlots << plotElem->getItem()->getNameElement();
    }
    QString name_ = plotElement->getItem()->getNameElement();
    auto existPlotWithSameName = (namesPlots.size() && namesPlots.contains(name_));

    if (existPlotWithSameName)
    {
        while (existPlotWithSameName)
        {
            QStringList tmp_sl = name_.split("_");
            auto isInt = false;
            QString lastInSl = tmp_sl.last();
            auto intValue = lastInSl.toInt(&isInt);
            if (!intValue)
            {
                intValue = 1;
                tmp_sl.append(QString::number(intValue));
            }
            else
            {
                intValue++;
                tmp_sl.last() = QString::number(intValue);
            }
            name_ = tmp_sl.join("_");

            existPlotWithSameName = (namesPlots.size() && namesPlots.contains(name_));
        }

        plotElement->getItem()->setNewNameElement(name_);
    }

    widgetsPlots.insert(plotElement, plotWidget);
    widgetsJoinedByTimeScale_List.append(plotWidget);
    addTab(plotWidget, plotElement->getItem()->getNameElement());
}

void GceDataView::slotSavePlots(QString projectHeaderPath)
{
    auto prjName = projectHeaderPath;
    auto tmp1 = prjName.split("/");
    prjName = tmp1.last();
    tmp1.removeLast();
    auto tmp2 = prjName.split(".");
    tmp2.removeLast();
    if (tmp2.size() > 1)
    {
        prjName = tmp2.join(".");
    }
    else
    {
        prjName = tmp2.at(0);
    }
    tmp1.append(prjName);
    prjName = tmp1.join("/");

    QDir projectDir(prjName);
    if (!projectDir.exists())
    {
        auto tmp3 = prjName.split("/");
        auto nm = tmp3.takeLast();
        QDir superDir(tmp3.join("/"));
        superDir.mkdir(nm);
    }

    QDir forRemove(QString().append(prjName).append("/p"));
    for (QFileInfo &plotFile : forRemove.entryInfoList(QDir::Files))
    {
        QFile pf(plotFile.absoluteFilePath());
        pf.remove();
    }

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameProjectManager
                );
    auto pmgr = qobject_cast<ProjectManager*>(o);
    auto project = pmgr->getProjectsArrayPtr()->last();

    auto chCnt = project->getPlotsList()->getItem()->childCount();
    for (decltype (chCnt) ch = 0; ch < chCnt; ++ch)
    {
        QString namePlot = project->getPlotsList()->getItem()->child(ch)->text(0);
        for (Plot* plt : widgetsPlots.keys())
        {
            if (plt->getItem()->getNameElement() == namePlot)
            {
                widgetsPlots.value(plt)->slotSavePlotToProject(prjName);
                break;
            }
        }
    }
}

void GceDataView::slotOpenPlots(QString projectHeaderPath)
{
    auto prjName = projectHeaderPath;
    auto tmp1 = prjName.split("/");
    prjName = tmp1.last();
    tmp1.removeLast();
    auto tmp2 = prjName.split(".");
    tmp2.removeLast();
    if (tmp2.size() > 1)
    {
        prjName = tmp2.join(".");
    }
    else
    {
        prjName = tmp2.at(0);
    }
    tmp1.append(prjName);
    prjName = tmp1.join("/");

    auto plotsDataDirPath = QString().append(prjName).append("/p");
    QDir dirPlotsData(plotsDataDirPath);
    if (!dirPlotsData.exists())
    {
        QStringList tmp = plotsDataDirPath.split("/");
        QString plotsDir = tmp.takeLast();
        QDir dirCommon(tmp.join("/"));
        dirCommon.mkdir(plotsDir);
        emit sigForAddEmptyPlot();
        return;
    }

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameProjectManager
                );
    auto pmgr = qobject_cast<ProjectManager*>(o);
    auto project = pmgr->getProjectsArrayPtr()->last();

    connect(project, SIGNAL(sigSaveProjectPlots(QString)), this, SLOT(slotSavePlots(QString)));

    for (QFileInfo &entry : dirPlotsData.entryInfoList(QDir::Files))
    {
        QString fName = entry.fileName();
        fName.remove(".data");
        auto idxPlot = fName.toInt();

        QFile fl(entry.absoluteFilePath());
        auto ok = fl.open(QFile::ReadOnly);
        if (!ok)
        {
            return;
        }
        QDataStream odatastr(&fl);
        QString namePlotElement, namePlotInternal;
        odatastr >> namePlotElement;
        odatastr >> namePlotInternal;

        if (namePlotElement != namePlotInternal)
            namePlotInternal = namePlotElement;

        auto newPlotElement = new Plot(project->getThisProjectIndex(), idxPlot, project, project);
        newPlotElement->getItem()->setNewNameElement(namePlotElement);
        project->addPlotToPlotList(newPlotElement);

        auto newPlotWidget = new SpecialWidgetForPlot(newPlotElement, this);
        newPlotWidget->slotChangeTimeZone( project->timeZone );
        connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), newPlotWidget, SLOT(slotChangeTimeZone(QTimeZone)));
        connect(newPlotWidget, SIGNAL(sigPlotMustBeRemoved(Plot*)), this, SLOT(slotPlotRemove(Plot*)));
        connect(newPlotWidget, SIGNAL(sigActivatedViewValues(bool)), this, SLOT(slotActivatedValuesView(bool)));
        connect(newPlotWidget, &SpecialWidgetForPlot::sigActivateMe, this, &GceDataView::slotPlotQuery4activate);

        plotAddingInternal(newPlotElement, newPlotWidget);

        int32_t devCount;
        odatastr >> devCount;
        QVector<QString> devicesNamesOfPlot;
        while (devCount > 0)
        {
            QString devNm;
            odatastr >> devNm;
            devicesNamesOfPlot << devNm;
            devCount--;
        }

        newPlotWidget->setDevNames(devicesNamesOfPlot);

        double minX, maxX;

        odatastr >> minX;
        odatastr >> maxX;

        int32_t trackCount;
        odatastr >> trackCount;

        while (trackCount > 0)
        {
            auto track = newPlotWidget->addEmptyTrack_public();

            if (minX == maxX) maxX++;

            int32_t graphsCount;
            odatastr >> graphsCount;

            while (graphsCount > 0)
            {
                QString deviceName, nameGraph;
                uint32_t sourceID, curveID;
                int thicknessLine, arrowOffsetFromPlot;
                QColor currentPenColor, savedStandardPenColor;
                QPen colorPen;
                QBrush colorBrush;
                double scaleMinimumValue, scaleMaximumValue;

                odatastr >> deviceName;
                odatastr >> sourceID;
                odatastr >> curveID;
                odatastr >> thicknessLine;
                odatastr >> currentPenColor;
                odatastr >> savedStandardPenColor;
                odatastr >> colorBrush;
                odatastr >> colorPen;
                odatastr >> arrowOffsetFromPlot;
                odatastr >> scaleMinimumValue;
                odatastr >> scaleMaximumValue;
                odatastr >> nameGraph;

                VariableContainer vcontainer;

                auto data_ = vcontainer.read_container_hash();

                odatastr >> data_;

                vcontainer.write_container_hash(data_);

                // прочитать из контейнера... о графике

                auto useNullValues_ = false;
                useNullValues_ = vcontainer.get_value<bool>("gws_use_null_values");
                auto nv_val = vcontainer.get_value<double>("gws_null_value_indic");
                auto frozenScaleValues_ = vcontainer.get_value<bool>("gws_frozen_scale_values", false);
                auto showDots = false;
                if (vcontainer.keyExist(QString("ShowDots_bool")))
                {
                    showDots = vcontainer.get_value<bool>(QString("ShowDots_bool"));
                }

                auto pseudoNameGraph =
                        vcontainer.get_value<QString>(QString("PseudoNameGraph"), QString());

                auto fixedDivisionValue = vcontainer.get_value<bool>(QString("gws_fix_division_value"), false);
                auto divisionValue = vcontainer.get_value<double>(QString("gws_division_value"), 1.0);

                graphsCount--;

                auto graphWS = track->addGraphicWithSettings();

                graphWS->useNullValues = useNullValues_;
                graphWS->nullValueIfUsed = nv_val;
                graphWS->frozenValuesScaleForAutoScale = frozenScaleValues_;

                graphWS->nameGraph = nameGraph;
                graphWS->pseudoNameGraph = pseudoNameGraph;
                graphWS->scaleMaximumValue = scaleMaximumValue;
                graphWS->scaleMinimumValue = scaleMinimumValue;
                graphWS->tracerForArrowUpper = new QCPItemTracer(track);
                graphWS->tracerForArrowMiddle = new QCPItemTracer(track);
                graphWS->tracerForArrowLower = new QCPItemTracer(track);
                graphWS->tracerForArrowUpper->setVisible(false);
                graphWS->tracerForArrowLower->setVisible(false);
                graphWS->tracerForArrowMiddle->setVisible(false);
                graphWS->tracerForArrowUpper->position->setType(QCPItemPosition::ptAbsolute);
                graphWS->tracerForArrowLower->position->setType(QCPItemPosition::ptAbsolute);
                graphWS->tracerForArrowMiddle->position->setType(QCPItemPosition::ptAbsolute);
                QPoint posTopLeft = track->axisRect()->topLeft(),
                        posBottomLeft = track->axisRect()->bottomLeft();
                posTopLeft.setX(posTopLeft.x() - track->graphicsArray.count()*TRACK_HEADER_WIDTH);
                posBottomLeft.setX(posTopLeft.x());
                graphWS->tracerForArrowUpper->position->setCoords(posTopLeft.x(), posTopLeft.y());
                graphWS->tracerForArrowLower->position->setCoords(posBottomLeft.x(), posBottomLeft.y());
                graphWS->tracerForArrowMiddle->position->setCoords(posTopLeft.x(), (posTopLeft.y()+posBottomLeft.y())/2);

                graphWS->arrow = new QCPItemLine(track);
                graphWS->arrow->setClipToAxisRect(false);
                graphWS->arrow->start->setParentAnchor(graphWS->tracerForArrowLower->position);
                graphWS->arrow->end->setParentAnchor(graphWS->tracerForArrowUpper->position);
                graphWS->arrow->start->setCoords(0, 0);
                graphWS->arrow->end->setCoords(0, 0);
                graphWS->lMaximum = new QCPItemText(track);
                graphWS->lMaximum->setClipToAxisRect(false);
                graphWS->lMaximum->position->setParentAnchor(graphWS->tracerForArrowUpper->position);
                graphWS->lMaximum->position->setCoords(/*-6*/6, -1);
                QString bnd;
                bnd.setNum(5000.);
                graphWS->lMaximum->setText(bnd.prepend(" ").append(" "));
                graphWS->lMaximum->setTextAlignment(Qt::AlignRight);
                graphWS->lMaximum->setPositionAlignment(Qt::AlignRight | Qt::AlignBottom);
                graphWS->lMaximum->setRotation(-90.0);
                graphWS->lMinimum = new QCPItemText(track);
                graphWS->lMinimum->setClipToAxisRect(false);
                graphWS->lMinimum->position->setParentAnchor(graphWS->tracerForArrowLower->position);
                graphWS->lMinimum->position->setCoords(/*-6*/6, 1);
                bnd.setNum(0.);
                graphWS->lMinimum->setText(bnd.prepend(" ").append(" "));
                graphWS->lMinimum->setTextAlignment(Qt::AlignLeft);
                graphWS->lMinimum->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
                graphWS->lMinimum->setRotation(-90.0);
                graphWS->labelNameGraphic = new QCPItemText(track);
                graphWS->labelNameGraphic->setClipToAxisRect(false);
                graphWS->updateLabelGraphicName();
                graphWS->labelNameGraphic->position->setParentAnchor(graphWS->tracerForArrowMiddle->position);
                graphWS->labelNameGraphic->position->setCoords(-12, 0);
                graphWS->labelNameGraphic->setRotation(-90.0);

                auto fnt = graphWS->labelNameGraphic->font();
                fnt.setPointSize(project->fontPointSize_GraphicsNamesInHeader);
                graphWS->labelNameGraphic->setFont(fnt);
                fnt = graphWS->lMaximum->font();
                fnt.setPointSize(project->fontPointSize_GraphicsMinMaxValuesInHeader);
                graphWS->lMaximum->setFont(fnt);
                graphWS->lMinimum->setFont(fnt);

                graphWS->currentPenColor = currentPenColor;
                graphWS->colorBrush = colorBrush;
                graphWS->colorPen = colorPen;
                graphWS->graphic->setPen(graphWS->colorPen);

                auto pen = graphWS->colorPen;
                pen.setWidth(2.0);
                graphWS->arrow->setPen(pen);
                graphWS->lMaximum->setColor(graphWS->currentPenColor);
                graphWS->lMinimum->setColor(graphWS->currentPenColor);
                graphWS->lMaximum->setBrush(QBrush(Qt::white));
                graphWS->lMinimum->setBrush(QBrush(Qt::white));

                track->graphicsArray.append(graphWS);

                graphWS->curveID = curveID;
                graphWS->sourceID = sourceID;
                graphWS->deviceName = deviceName;

                graphWS->divisionValueForAxis = divisionValue;
                graphWS->divisionValueIsFixed = fixedDivisionValue;

                if (sourceID == NULL_ID && curveID == NULL_ID)
                {
                    graphWS->loaded = false;
                }
                else
                {
                    auto deviceElem = project->getDeviceElemBySourceID(sourceID);
                    if (deviceElem)
                    {
                        auto curveElem = deviceElem->getCurvesMapPtr()->value(curveID);

                        if (curveElem == nullptr)
                        {
                            graphWS->loaded = false;
                        }
                        else
                        {
                            graphWS->loaded = true;
                            emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(sourceID, curveID);
                            if (showDots)
                            {
                                QCPScatterStyle square_style(QCPScatterStyle::ssSquare, 4);
                                graphWS->graphic->setScatterStyle(square_style);
                                graphWS->graphicScatterStyleIsSquare = true;
                            }
                            auto curveInTrack = track->copyCurveFromDeviceToTrack(curveElem);
                            connect(curveInTrack, SIGNAL(sigDeviceNameChanged(QString,QString)),
                                    newPlotWidget, SLOT(slotDeviceNameChanged(QString,QString)));
                        }
                    }
                }
                if ( !graphWS->loaded )
                {
                    track->makeCurveToTrackWithoutDevice(graphWS->nameGraph, graphWS->deviceName);
                }
            }

            VariableContainer vcontainer;

            auto data_ = vcontainer.read_container_hash();

            odatastr >> data_;

            vcontainer.write_container_hash(data_);

            // прочитать из контейнера... о треке

            if (vcontainer.keyExist(QString("TrackHeader_int")))
            {
                auto heightTrack = vcontainer.get_value<int>(QString("TrackHeader_int"));
                track->setFixedHeight(heightTrack);
            }
            if (vcontainer.keyExist(QString("HorizontalSectionsCount_int")))
            {
                auto hsc = vcontainer.get_value<int>(QString("HorizontalSectionsCount_int"));
                track->setHorizontalSectionsCount(hsc);
            }

            if (vcontainer.keyExist(QString("Labels_count")))
            {
                int labelsCountOnTrack = vcontainer.get_value<int>(QString("Labels_count"));

                for (int iL = 0; iL < labelsCountOnTrack; ++iL)
                {
                    int ID_label = vcontainer.get_value<int>(QString("Label_ID").append(QString::number(iL)));
                    double xValue = vcontainer.get_value<double>(QString("Label_coord_X")
                                                                 .append(QString::number(iL)));
                    QString textLabel = vcontainer.get_value<QString>(QString("Label_text")
                                                                      .append(QString::number(iL)));
                    double yOffsetFromTop = vcontainer.get_value<double>(QString("Label_offset_y")
                                                                         .append(QString::number(iL)), 0.0);

                    auto label = project->createLabel();
                    label->setID(ID_label);
                    label->setText(textLabel);
                    QPoint pSpec;
                    pSpec.setX(0);
                    pSpec.setY(777);
                    auto dlabel = new DataLabel(track, pSpec, textLabel, xValue);
                    connect(dlabel, SIGNAL(sigGotoLabel(double)), track, SIGNAL(sigGoToLabel(double)));
                    connect(dlabel, &DataLabel::sigRemoveMe, track, &Track::slotRemoveLabel);
                    connect(dlabel, &DataLabel::sigCopyMe, track, &Track::slotCopyLabel);
                    connect(dlabel, &DataLabel::sigCopyMeToAnotherTrack, track, &Track::slotCopyLabelToAnotherTrack);
                    connect(dlabel, &DataLabel::sigMoveMe, track, &Track::slotMoveLabel);
                    dlabel->setLabelElement(label);
                    label->setTrackPointer(track);
                    dlabel->setPixelCoordY_forTextBlock(yOffsetFromTop);
                    // !!!! track->replot();
                    track->labelsArray.append(dlabel);
                }
            }

            trackCount--;
        }

        newPlotWidget->slotRescaleXaxis(minX, maxX);

        VariableContainer vcontainer;

        auto data_ = vcontainer.read_container_hash();

        odatastr >> data_;

        vcontainer.write_container_hash(data_);

        // прочитать из контейнера... о планшете

        if (vcontainer.keyExist(QString("PlotHeaderText")))
        {
            QString headerPlotTxt = vcontainer.get_value<QString>(QString("PlotHeaderText"));
            newPlotWidget->setDescriptionInHeader(headerPlotTxt);
        }

        newPlotWidget->updatePlots(true);
        newPlotWidget->recalcWgt2HeightForScrollBar();

        fl.close();
    }

    projectOpeningComplete = true;
}

void GceDataView::slotOpenPlotTemplateFromFile()
{
    QString fName = QFileDialog::getOpenFileName(this, tr("Open plot template from file"), QString(qApp->applicationDirPath()).append("/templates"), QString(tr("Plot template (*.cetplt)")) );
    if (fName.isEmpty())
        return;

    openPlotFromTemplateFile(fName);
}

void GceDataView::slotLoadDataAndRefreshCurrentPlot()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->slotLoadDataForSelectedCurves();
}

void GceDataView::slotShowWholeTimeRangeOnCurrentPlot()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->showWholeTimesRange();
    QCPRange rng = plotWidget->getTimeTrack()->xAxis2->range();
    emit plotWidget->sigChangeTimeRangeForJoinedByTimePlots(rng.lower, rng.upper);
}

void GceDataView::slotAutoRescaleValuesAxesOfCurrentPlot()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->updatePlots(false);
}

void GceDataView::slotCurrentPlotToPDF()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->slotSaveAsPDF();
}

void GceDataView::slotCurrentPlotToBMP()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->slotSaveAsBMP();
}

void GceDataView::slotAutoShiftValuesOnCurrentPlot()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    plotWidget->shiftValuesAxesToShowLastValuesOnGraphics();
}

void GceDataView::slotShowHideOnGraphicsAllDots()
{
    if (widgetsPlots.isEmpty()) return;
    auto plotWidget = qobject_cast<SpecialWidgetForPlot*>( currentWidget() );
    auto project = qobject_cast<Project*>(plotWidget->getPlot()->getItem()->getProjectPointer());

    for (SpecialWidgetForPlot *plotWgt : widgetsPlots.values())
    {
        for (Track *track : *plotWgt->getTracksVectorPtr())
        {
            for (GraphWithSettings *gws : track->graphicsArray)
            {
                QCPScatterStyle square(QCPScatterStyle::ssSquare, 4);
                QCPScatterStyle newStyle =
                        (( gws->graphicScatterStyleIsSquare || project->showDotsOnAllGraphics) ?
                             square :
                             QCPScatterStyle(QCPScatterStyle::ssNone));
                gws->graphic->setScatterStyle(newStyle);
            }
            track->replot();
        }
    }
}

void GceDataView::slotChangeTimeRangeForJoinedByTimePlots(double newMinTime, double newMaxTime)
{
    if (!projectOpeningComplete) return;

    SpecialWidgetForPlot *senderPlotWgt = qobject_cast<SpecialWidgetForPlot*>(sender());
    if (widgetsJoinedByTimeScale_List.size()
            && widgetsJoinedByTimeScale_List.contains(senderPlotWgt))
    {
        for (SpecialWidgetForPlot *plotWgt : widgetsJoinedByTimeScale_List)
        {
            if (plotWgt == senderPlotWgt)
            {
                continue;
            }
            plotWgt->slotRescaleXaxis(newMinTime, newMaxTime);
        }
    }
}

void GceDataView::slotShowSelectedPlot(QTreeWidgetItem *now, QTreeWidgetItem *old)
{
    if (now == nullptr /*|| old == nullptr*/)
    {
        return;
    }
    auto sz_ = widgetsPlots.keys().size();
    for (decltype (sz_) k = 0; k < sz_; ++k)
    {
        if ((widgetsPlots.keys().at(k)->getItem() == static_cast<TreeElement*>(now))
                &&
                (static_cast<TreeElement*>(now)->getType() == TreeElement::ElementType::elementView))
        {
            auto plot = widgetsPlots.value(widgetsPlots.keys().at(k));
            plot->resetScroll();
            setCurrentWidget( plot );
        }

    }
}

void GceDataView::slotPlotRenamed(QString nm)
{
    setTabText(indexOf(qobject_cast<QWidget*>(sender())), nm);
}

void GceDataView::slotPlotRemove(Plot *plt)
{
    auto plotWidget = widgetsPlots.take(plt);
    widgetsJoinedByTimeScale_List.removeOne(plotWidget);
    removeTab(indexOf(plotWidget));
    plotWidget->deleteLater();
    plt->deleteLater();
}

void GceDataView::slotDeviceRenamedForEmptyPlotDeviceElements(QString devNameNew, QString nameOld, uint32_t sourceID, Device *deviceSender)
{
    Q_UNUSED(nameOld)

    QList<GraphWithSettings*> graphsForDraw;
    for (decltype (widgetsPlots.keys().size()) ik_plot = 0; ik_plot < widgetsPlots.keys().size(); ++ik_plot)
    {
        auto plotWidget = widgetsPlots.value(widgetsPlots.keys().at(ik_plot));
        if (plotWidget->getDevNames().contains(devNameNew))
        {
            for (decltype (plotWidget->getTracksVectorPtr()->size()) itr = 0; itr < plotWidget->getTracksVectorPtr()->size(); ++itr)
            {
                auto track = plotWidget->getTracksVectorPtr()->value(itr);

                QVector<int> indexesCurveItemsForDeletingFromLast;

                for (decltype (track->getItem()->childCount()) i_twi = 0; i_twi < track->getItem()->childCount(); ++i_twi)
                {
                    TreeElement *element = static_cast<TreeElement*>(track->getItem()->child(i_twi));
                    if (element->getNameElement().split(" / ").at(1) == devNameNew)
                    {
                        indexesCurveItemsForDeletingFromLast.prepend(i_twi);
                    }
                }

                while (indexesCurveItemsForDeletingFromLast.size())
                {
                    QTreeWidgetItem *tw_item = track->getItem()->takeChild(indexesCurveItemsForDeletingFromLast.takeFirst());
                    TreeElement *element = static_cast<TreeElement*>(tw_item);
                    delete element;
                }

                for (decltype (track->graphicsArray.size()) i_graph = 0; i_graph < track->graphicsArray.size(); ++i_graph)
                {
                    auto graphWS = track->graphicsArray[i_graph];
                    if (graphWS->sourceID == NULL_ID && graphWS->curveID == NULL_ID)
                    {
                        for (Curve *curve : deviceSender->getCurvesMapPtr()->values())
                        {
                            if (curve->getCurveName() == graphWS->nameGraph)
                            {
                                graphWS->sourceID = curve->getSourceID();
                                graphWS->curveID = curve->getCurveID();
                                graphsForDraw.append(graphWS);

                                curve->setCurveType_RAW();

                                auto newCurveInTrack4replace = track->copyCurveFromDeviceToTrack(curve);
                                connect(newCurveInTrack4replace, SIGNAL(sigDeviceNameChanged(QString,QString)),
                                        plotWidget, SLOT(slotDeviceNameChanged(QString,QString)));

                                break;
                            }
                        }
                    }
                }
            }
        }
        plotWidget->slotAfterRemovingCurve();
    }

    QStringList grNames;
    QStringList ignoredCurveNames;

    // проверка наличия незагруженных кривых:
    for (GraphWithSettings *graphWS : graphsForDraw)
    {
        auto oklc = false;
        auto loadedCurve =
                GceLogicLayer::getInstance().isLoggingCurveLoaded(
                    graphWS->sourceID, graphWS->curveID, oklc);
        if (oklc && !loadedCurve)
        {
            grNames.append(GceLogicLayer::getInstance().getCurveStringIdByCurveId(
                               graphWS->sourceID, graphWS->curveID, oklc
                               ));
        }
    }

    if (!grNames.isEmpty())
    {
        GceLogicLayer::getInstance().loadGraphicsDataFromImportedFiles(sourceID, grNames, ignoredCurveNames);
    }

    for (GraphWithSettings *graphWS : graphsForDraw)
    {
        graphWS->loaded = true;
        GceLogicLayer::getInstance().setExcludedTimesForNullValues(graphWS->sourceID, graphWS->curveID,
                                                                   graphWS->useNullValues, graphWS->nullValueIfUsed);
    }

    for (decltype (widgetsPlots.keys().size()) ik_plot = 0; ik_plot < widgetsPlots.keys().size(); ++ik_plot)
    {
        auto plotWidget = widgetsPlots.value(widgetsPlots.keys().at(ik_plot));
        plotWidget->updatePlots(true);
        plotWidget->showWholeTimesRange();
    }
}

void GceDataView::slotActivatedValuesView(bool yes)
{
    if (yes)
    {
        for (SpecialWidgetForPlot *pltWgt : getPlotWidgetsList())
        {
            if (pltWgt->viewGraphicsValuesMode)
            {
                continue;
            }
            pltWgt->viewGraphicsValuesModeInAnotherPlot = true;
        }
    }
    else
    {
        for (SpecialWidgetForPlot *pltWgt : getPlotWidgetsList())
        {
            pltWgt->viewGraphicsValuesModeInAnotherPlot = false;
        }
    }
}

void GceDataView::slotDrawOrRedrawGraphic(uint32_t sourceID, uint32_t curveID)
{
    auto dlgWait = new QDialog(nullptr, Qt::CustomizeWindowHint|Qt::WindowTitleHint);
    dlgWait->setWindowTitle(tr("Drawing/redrawing graphics..."));
    dlgWait->setFixedSize(300, 100);
    dlgWait->setModal(true);
    dlgWait->show();

    auto prjMgr = qobject_cast<ProjectManager*>(GceLogicLayer::getInstance()
                                                 .getObjectFromObjectContainerByObjName(ObjectsNames::objNameProjectManager));
    auto project = prjMgr->getProjectsArrayPtr()->values().last();

    QVector<Track*> changedTracks;

    auto sz_ = widgetsPlots.values().size();
    for (decltype (sz_) iPlot = 0; iPlot < sz_; ++iPlot)
    {
        auto changesOnPLotExist = false;

        auto szTracks = widgetsPlots.values().value(iPlot)->getTracksVectorPtr()->size();
        for (decltype (szTracks) iVector = 0; iVector < szTracks; ++iVector)
        {
            auto track = widgetsPlots.values().value(iPlot)->getTracksVectorPtr()->value(iVector);
            auto existChanges = false;

            auto szGraphs = track->graphicsArray.size();
            for (decltype (szGraphs) iGraph = 0; iGraph < szGraphs; ++iGraph)
            {
                GraphWithSettings *gws = track->graphicsArray.value(iGraph);
                if (sourceID == gws->sourceID)
                {
                    if (curveID == -1 || curveID == gws->curveID)
                    {
                        existChanges = true;
                        gws->graphic->setData(QVector<double>(), QVector<double>());
                        auto ok = false;
                        auto x_n = GceLogicLayer::getInstance().getValuesX(gws->sourceID, gws->curveID,
                                                                           project->getFullOffsetFromUTC_msecs(),
                                                                           GceLogicLayer::getInstance().getTimeShiftOfDataSource(gws->sourceID),
                                                                           ok);
                        auto y_n = GceLogicLayer::getInstance().getValuesY(gws->sourceID, gws->curveID, ok);
                        gws->graphic->setData(x_n, y_n);
                        gws->loaded = true;
                    }
                }
            }

            if (existChanges)
            {
                changesOnPLotExist = true;
                while (track->noDataAreas.size())
                {
                    auto rect = track->noDataAreas.takeFirst();
                    track->removeItem(rect);
                }
                track->datasBlocks = GceLogicLayer::getInstance().getNoDataBlocksAtrtsWithEndsForSource(sourceID);
                auto previosEnd = 0.0;
                auto first = true;
                for (auto it_tdb = track->datasBlocks.cbegin(); it_tdb != track->datasBlocks.cend(); ++it_tdb)
                {
                    auto startBlockMsecs = it_tdb->first;
                    auto endBlockMsecs = it_tdb->second;
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        auto rectNoData = new QCPItemRect(track);
                        rectNoData->setBrush(QBrush(QColor(220, 220, 220)));
                        rectNoData->setPen(QPen(Qt::NoPen));
                        rectNoData->setLayer(track->axisRect()->layer());
                        rectNoData->topLeft->setCoords(previosEnd, track->yAxis->range().upper);
                        rectNoData->bottomRight->setCoords(startBlockMsecs, track->yAxis->range().lower);
                        track->noDataAreas.append(rectNoData);
                    }
                    previosEnd = endBlockMsecs;
                }
                changedTracks << track;
            }
        }

        if (changesOnPLotExist)
        {
            widgetsPlots.values().value(iPlot)->showWholeTimesRange();
        }
    }

    for (Track *track : changedTracks)
    {
        track->replot();
    }

    dlgWait->close();
    dlgWait->deleteLater();
}

void GceDataView::slotPlotQuery4activate()
{
    auto swp = qobject_cast<SpecialWidgetForPlot*>(sender());
    if (currentWidget() != swp)
    {
        setCurrentWidget(swp);
    }
}

void GceDataView::slotResetScrollOnSelected()
{
    auto swp = qobject_cast<SpecialWidgetForPlot*>(currentWidget());
    if (!swp) return;
    swp->resetScroll();
}

void GceDataView::slotJoinPlotsByTimeDialogStart()
{
    if (widgetsPlots.size() <= 1)
    {
        QMessageBox::information(nullptr, tr("Information"),
                                 tr("It's neccessary two or more plots for this action"));
        return;
    }

    QMap<Plot*, QCheckBox*> chkboxes;
    auto btnSelectAll = new QPushButton(tr("Select all"));
    auto btnDeselectAll = new QPushButton(tr("Deselect all"));
    QHash<Plot*, QString> selected;
    auto sz_ = widgetsPlots.values().size();
    for (decltype (sz_) i = 0; i < sz_; ++i)
    {
        SpecialWidgetForPlot *wgtPlot = widgetsPlots.values().at(i);
        auto cb = new QCheckBox(wgtPlot->getPlot()->getItem()->getNameElement());
        chkboxes[wgtPlot->getPlot()] = cb;
        connect(cb, &QCheckBox::stateChanged,
                this, [&](int state)
        {
            auto cbb = qobject_cast<QCheckBox*>(sender());
            if (state == Qt::Checked)
            {
                selected[chkboxes.key(cbb)] = cbb->text();
            }
            else
            {
                selected.remove(chkboxes.key(cbb));
            }
        });
        if (widgetsJoinedByTimeScale_List.size()
                && widgetsJoinedByTimeScale_List.contains(wgtPlot))
        {
            cb->setChecked(true);
        }
        else
        {
            cb->setChecked(false);
        }
    }
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    auto dlg = new QDialog;
    dlg->setWindowTitle(tr("Join plots by time"));
    connect(btns, &QDialogButtonBox::accepted, dlg, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, dlg, &QDialog::reject);

    connect(btnSelectAll, &QPushButton::clicked,
            this, [&]()
    {
        for (QCheckBox *cb : chkboxes.values())
        {
            cb->setChecked(true);
        }
    });
    connect(btnDeselectAll, &QPushButton::clicked,
            this, [&]()
    {
        for (QCheckBox *cb : chkboxes.values())
        {
            cb->setChecked(false);
        }
    });

    auto lt = new QVBoxLayout;
    lt->addWidget(btnSelectAll);
    lt->addWidget(btnDeselectAll);
    for (QCheckBox *cb : chkboxes.values())
    {
        lt->addWidget(cb);
    }
    lt->addWidget(btns);
    dlg->setLayout(lt);
    if (dlg->exec() == QDialog::Accepted)
    {
        if (!selected.isEmpty())
        {
            widgetsJoinedByTimeScale_List.clear();
            for (SpecialWidgetForPlot *wgtPlot : widgetsPlots.values())
            {
                if (selected.keys().contains(
                            wgtPlot->getPlot()
                            ))
                {
                    widgetsJoinedByTimeScale_List << wgtPlot;
                }
            }

            auto firstPlotKey = selected.keys().first();
            auto firstPlotWgt = widgetsPlots.value(firstPlotKey);
            if (firstPlotWgt->getTracksVectorPtr()->size())
            {
                if (firstPlotWgt->getTimeTrack())
                {
                    auto rng = firstPlotWgt->getTimeTrack()->xAxis2->range();
                    emit firstPlotWgt->sigChangeTimeRangeForJoinedByTimePlots(rng.lower, rng.upper);
                }
            }

        }
        else
        {
            widgetsJoinedByTimeScale_List.clear();
        }
    }

    dlg->deleteLater();
}

void SpecialWidgetForPlot::addPrototypeDatas()
{
    QMap<double, double> data_map;
    data_map.insert(1.2, 1.4);
    data_map.insert(1.1, 1.8);
    data_map.insert(1.7, 1.9);
    data_map.insert(0.7, 2.9);
    data_map.insert(4.5, 0.9);
    data_map.insert(2.7, 3.9);

    for (int i = 0; i < 1; ++i)
    {
        baseTrack = tracksArray.at(0);

        QBrush brs;

        switch (i) {
        case 0:
            brs = QBrush(Qt::red);
            break;
        case 1:
            brs = QBrush(Qt::blue);
            break;
        case 2:
            brs = QBrush(Qt::green);
            break;
        default:
            break;
        }

        baseTrack->graph(baseTrack->graphCount()-1)->addData(data_map.keys().toVector(), data_map.values().toVector());
    }
}

Track *SpecialWidgetForPlot::addEmptyTrack()
{
    auto key = tracksArray.size();
    for (Track *track___ : tracksArray)
    {
        auto id = track___->getTrackID();
        if (id >= static_cast<uint32_t>(key))
        {
            key = id + 1;
        }
    }
    baseTrack = new Track(plot4wgt, key, plot4wgt->getItem()->getProjectPointer(), this);
    connect(baseTrack, SIGNAL(sigGoToLabel(double)), this, SLOT(slotGoToLabel(double)));
    connect(baseTrack, &Track::sigPrepareCopyLabel, this, &SpecialWidgetForPlot::slotCopyLabel_);
    connect(baseTrack, &Track::sigPrepareCopyToAnotherTrack, this, &SpecialWidgetForPlot::slotCopyLabelToAnotherTrack_);
    connect(baseTrack, &Track::sigPrepareMoveLabel, this, &SpecialWidgetForPlot::slotMoveLabel_);

    baseTrack->yAxis->setLabelPadding(6);

    baseTrack->setPlotPtr(this);
    baseTrack->setParentForItem(plot4wgt->getItem());
    connect(baseTrack->getItem(), SIGNAL(sigClicked()), this, SIGNAL(sigActivateMe()));
    plot4wgt->getItem()->addSubElement(baseTrack->getItem());
    baseTrack->setTrackID((uint) key);

    baseTrack->setInteraction(QCP::iRangeDrag, false);
    baseTrack->setInteraction(QCP::iRangeZoom, false);
    orientationMode = Qt::Horizontal;
    baseTrack->axisRect()->setRangeDrag(orientationMode);
    baseTrack->axisRect()->setRangeZoom(orientationMode);
    connect(baseTrack, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(slotMouseWheel(QWheelEvent*)));
    connect(baseTrack, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(slotMousePress(QMouseEvent*)));
    connect(baseTrack, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(slotMouseRelease(QMouseEvent*)));
    connect(baseTrack, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(slotMouseMove(QMouseEvent*)));
    connect(baseTrack, SIGNAL(mouseDoubleClick(QMouseEvent*)), this, SLOT(slotMouseDoubleClick(QMouseEvent*)));
    connect(baseTrack, SIGNAL(sigAddGraphToTrack(Curve*)), this, SLOT(slotAddGraphToTrack(Curve*)));
    connect(baseTrack, SIGNAL(sigTrackResized()), this, SLOT(slotResizedTrack()));
    connect(baseTrack, SIGNAL(sigRescaleAbcissAxis(double,double)), this, SLOT(slotRescaleXaxis(double,double)));
    connect(baseTrack, SIGNAL(sigRemoveTrack()), this, SLOT(slotRemovingTrack()));
    connect(baseTrack, SIGNAL(sigCurveRemoved()), this, SLOT(slotAfterRemovingCurve()));
    connect(baseTrack, SIGNAL(sigTrackHeightChanged(int)), this, SLOT(slotTrackHeightChanged(int)));
    connect(baseTrack, SIGNAL(sigMoveTrack()), this, SLOT(slotMovingTrackWithinPlot()));
    connect(baseTrack, SIGNAL(sigForMoveCurve(Curve*)), this, SLOT(slotForMoveCurveFromTrackToTrack(Curve*)));

    tracksArray.append(baseTrack);

    return baseTrack;
}

uint8_t SpecialWidgetForPlot::firstIndexSelectedColorWhichAreUsedInPlot()
{
    uint8_t ret = 0;
    QList<QColor> usedColors;
    for (Track *track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto color = gws->currentPenColor;
            if (!usedColors.contains(color))
            {
                usedColors.append(color);
            }
        }
    }
    auto sz = selectedColors.keys().size();
    for (decltype (sz) c2 = 0; c2 < sz; ++c2)
    {
        auto ret2 = selectedColors.keys().at(c2);
        if (!usedColors.contains( selectedColors.value(ret2).second))
        {
            ret = ret2;
            break;
        }
    }
    return ret;
}

uint8_t SpecialWidgetForPlot::firstIndexSelectedColorWhichAreUsedInTrack(Track *track)
{
    uint8_t ret = 0;
    QList<QColor> usedColors;
    for (GraphWithSettings *gws : track->graphicsArray)
    {
        auto color = gws->colorPen.color();
        if (!usedColors.contains(color))
        {
            usedColors.append(color);
        }
    }
    auto sz = selectedColors.keys().size();
    for (decltype (sz) c2 = 0; c2 < sz; ++c2)
    {
        auto ret2 = selectedColors.keys().at(c2);
        if (!usedColors.contains( selectedColors.value(ret2).first))
        {
            ret = ret2;
            break;
        }
    }
    return ret;
}

void SpecialWidgetForPlot::savingPreparedFileWithPlot(QString fullFileName, bool makeTemplate)
{
    QFile fl(fullFileName);
    bool ok = fl.open(QFile::WriteOnly);
    if (!ok)
    {
        return;
    }
    QDataStream odatastr(&fl);
    odatastr << plot4wgt->getItem()->getNameElement();
    odatastr << plotName;

    int32_t devCount = plot_devicesNames.size();
    odatastr << devCount;

    for (QString &devName : plot_devicesNames)
    {
        odatastr << devName;
    }

    auto minX = (tracksArray.isEmpty() ? 0.0 : tracksArray.first()->xAxis->range().lower);
    auto maxX = (tracksArray.isEmpty() ? 0.0 : tracksArray.first()->xAxis->range().upper);

    odatastr << minX;
    odatastr << maxX;

    int32_t trackCount = tracksArray.size();
    odatastr << trackCount;
    for (Track *track : tracksArray)
    {
        int32_t graphsCount = track->graphicsArray.size();
        odatastr << graphsCount;
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            odatastr << gws->deviceName;
            if (makeTemplate)
            {
                uint32_t nullIdValue = NULL_ID;
                odatastr << nullIdValue; // вместо sourceID
                odatastr << nullIdValue; // вместо curveID
            }
            else
            {
                odatastr << gws->sourceID;
                odatastr << gws->curveID;
            }
            odatastr << gws->thicknessLine;
            odatastr << gws->currentPenColor;
            odatastr << gws->savedStandardPenColor;
            odatastr << gws->colorBrush;
            odatastr << gws->colorPen;
            odatastr << gws->arrowOffsetFromPlot;
            odatastr << gws->scaleMinimumValue;
            odatastr << gws->scaleMaximumValue;
            odatastr << gws->nameGraph;

            VariableContainer vcontainer;

            // что-то записать в контейнер... о графике

            vcontainer.set_value<bool>(QString("ShowDots_bool"), !gws->graphic->scatterStyle().isNone());
            vcontainer.set_value<bool>(QString("gws_use_null_values"), gws->useNullValues);
            vcontainer.set_value<bool>(QString("gws_frozen_scale_values"), gws->frozenValuesScaleForAutoScale);
            vcontainer.set_value<double>(QString("gws_null_value_indic"), gws->nullValueIfUsed);

            vcontainer.set_value<QString>(QString("PseudoNameGraph"), gws->pseudoNameGraph);

            vcontainer.set_value<bool>(QString("gws_fix_division_value"), gws->divisionValueIsFixed);
            vcontainer.set_value<double>(QString("gws_division_value"), gws->divisionValueForAxis);

            odatastr << vcontainer.read_container_hash();
        }

        VariableContainer vcontainer;

        // что-то записать в контейнер... о треке

        vcontainer.set_value<int>(QString("TrackHeader_int"), track->height());
        vcontainer.set_value<int>(QString("HorizontalSectionsCount_int"),
                                  track->getHorizontalSectionsCount());

        int szLabels = track->labelsArray.size();
        vcontainer.set_value<int>(QString("Labels_count"), szLabels);

        int counter = 0;
        for (DataLabel *lbl : track->labelsArray)
        {
            int ID_label = lbl->getLabelID();
            double xValue = lbl->getLabel_Xcoord();
            QString textLabel = lbl->getText();
            double yOffsetFromTop = lbl->getPixelCoordY_forTextBlock();

            vcontainer.set_value<int>(QString("Label_ID").append(QString::number(counter)), ID_label);
            vcontainer.set_value<double>(QString("Label_coord_X").append(QString::number(counter)), xValue);
            vcontainer.set_value<QString>(QString("Label_text").append(QString::number(counter)), textLabel);
            vcontainer.set_value<double>(QString("Label_offset_y").append(QString::number(counter)),
                                         yOffsetFromTop);
            counter++;
        }

        odatastr << vcontainer.read_container_hash();
    }

    VariableContainer vcontainer;

    // что-то записать в контейнер... о планшете

    vcontainer.set_value<QString>(QString("PlotHeaderText"), lbl_TimeZone->getDesciptionText());

    odatastr << vcontainer.read_container_hash();

    fl.close();
}

SpecialWidgetForPlot::SpecialWidgetForPlot(Plot *basePlot, QWidget *parent) : QWidget(parent)
{
    dialogAV_lastHeight = -1;
    plot4wgt = basePlot;
    plotName = plot4wgt->getItem()->getNameElement();
    rightMouseButtonPressed = leftMouseButtonPressed = viewGraphicsValuesMode =
            viewGraphicsValuesModeInAnotherPlot = viewGraphicsValuesMode_Fixation =
            copyLabelMode = copyLabelToAnotherTrackMode = moveLabelMode =
            mousePressedOnceAtCopyOrMove = dimensionMode = dimensionProcess =
            scalingByRectangleMode = valuesRescaledByMouseWheel = false;

    dimensionEndPoint = dimensionStartPoint = nullptr;
    dimensionRect = nullptr;

    lastScrollBarPositionValue = 0;

    connect(plot4wgt, SIGNAL(sigLoadDataOfSelectedCurves()), this, SLOT(slotLoadDataForSelectedCurves()));
    connect(plot4wgt, SIGNAL(sigRenamedPlot(QString)), this, SLOT(slotPlotRenamed(QString)));
    connect(plot4wgt, SIGNAL(sigSaveAsPDF()), this, SLOT(slotSaveAsPDF()));
    connect(plot4wgt, SIGNAL(sigSaveAsBMP()), this, SLOT(slotSaveAsBMP()));
    connect(plot4wgt, SIGNAL(sigRemovePlot()), this, SLOT(slotRemovePlot()));
    connect(plot4wgt, SIGNAL(sigSaveStandalonePlotTemplate()), this, SLOT(slotSaveAsStandalonePlotTemplateFile()));
    connect(plot4wgt, SIGNAL(sigChangeLinkToDataOfDeviceInPlot()), this, SLOT(slotChangeLinkToDataOfDeviceInPlot()));

    connect(plot4wgt->getItem(), SIGNAL(sigClicked()), this, SIGNAL(sigActivateMe()));

    connect(plot4wgt, &Plot::sigEditPlotHeaderString,
            this, [&]()
    {
        QDialog dlg;
        dlg.setWindowTitle(tr("Edit plot header"));
        auto lned = new QLineEdit;
        lned->setText(lbl_TimeZone->getDesciptionText());
        auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
        connect(btns, &QDialogButtonBox::accepted, &dlg, &QDialog::accept);
        connect(btns, &QDialogButtonBox::rejected, &dlg, &QDialog::reject);
        auto lt = new QVBoxLayout;
        lt->addWidget(lned);
        lt->addWidget(btns);
        dlg.setLayout(lt);
        if (dlg.exec() == QDialog::Accepted)
        {
            lbl_TimeZone->setDescription(lned->text());
        }
    });

    auto project = qobject_cast<Project*>(plot4wgt->getItem()->getProjectPointer());
    connect(project, &Project::sigChangeFontUtcHeaderSize,
            this, &SpecialWidgetForPlot::slotChangeFontUtcHeaderSize);
    connect(project, &Project::sigChangeFontGraphicsHeaderNamesSize,
            this, &SpecialWidgetForPlot::slotChangeFontGraphicsHeaderNamesSize);
    connect(project, &Project::sigChangeFontGraphicsHeaderMinMaxSize,
            this, &SpecialWidgetForPlot::slotChangeFontGraphicsHeaderMinMaxSize);

    centerWgtstack = new QStackedWidget;

    lbl_TimeZone = new SpecLabel(QString("TimeZone"));
    lbl_TimeZone->setStyleSheet("QLabel { background-color : yellow; }");
    auto font = lbl_TimeZone->font();
    font.setBold(true);
    font.setPixelSize(project->fontPixelSize_UtcHeader);
    lbl_TimeZone->setFont(font);
    lbl_TimeZone->setFixedHeight(project->fontPixelSize_UtcHeader);

    connect(lbl_TimeZone, &SpecLabel::sigMouseDoubleClicked,
            this, [&](QPoint pos)
    {
        auto mnu = new QMenu(this);
        mnu->addActions(plot4wgt->getActionsList());
        mnu->popup(pos);
    });

    auto idxSpecTrackTime = -1;
    trackTimeOnly = new Track(plot4wgt, idxSpecTrackTime, plot4wgt->getItem()->getProjectPointer(), this);
    trackTimeOnly->setPlotPtr(this);
    trackTimeOnly->setTrackID(idxSpecTrackTime);

    trackTimeOnly->setFixedHeight(35);

    trackTimeOnly->yAxis->setTickLabels(false);
    trackTimeOnly->yAxis->setTicks(false);

    trackTimeOnly->xAxis2->setSubTicks(false);
    auto fnt = trackTimeOnly->xAxis2->labelFont();
    fnt.setPointSize(project->fontPointSize_TimeScale);
    trackTimeOnly->xAxis2->setLabelFont(fnt);

    auto mainLt = new QHBoxLayout;
    mainLt->addWidget(centerWgtstack);
    scrollBar = new QScrollBar(Qt::Vertical);
    mainLt->addWidget(scrollBar);
    this->setLayout(mainLt);

    connect(scrollBar, SIGNAL(valueChanged(int)), this, SLOT(slotScrolledVerticalScrollBar(int)));

    vSplitter = new QSplitter(Qt::Vertical, nullptr);
    vSplitter->setChildrenCollapsible(false);
    vSplitter->setOpaqueResize(false);

    wgt2 = new QWidget;
    v_Layout = new QVBoxLayout;

    wgt2->setLayout(v_Layout);
    centerWgtstack->addWidget(wgt2);

    v_Layout->addWidget(lbl_TimeZone);
    v_Layout->addWidget(trackTimeOnly);
    v_Layout->addWidget(vSplitter);

    v_Layout->addWidget(new QWidget, 13);

    auto heightSum = 0;
    if (tracksArray.size())
    {
        vSplitter->addWidget(trackTimeOnly);
    }
    auto szTA = tracksArray.size();
    for (decltype (szTA) i = 0; i < szTA; ++i)
    {
        vSplitter->addWidget(tracksArray.at(i));
        tracksArray[i]->setAcceptDrops(true);
        heightSum += tracksArray[i]->height();
    }

    scrollBar->setValue(0);
    scrollBar->setMinimum(0);

    recalcWgt2HeightForScrollBar();

    lastScrollBarPositionValue = 0;

    QColor clr = Qt::red;
    QPair<QColor, bool> pair(clr, false);
    selectedColors.insert(1, pair);
    clr = Qt::blue;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(2, pair);
    clr = Qt::darkGreen;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(3, pair);
    //коричневый
    clr = QColor("#aa5500");
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(4, pair);
    clr = QColor("#eac701");
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(5, pair);
    clr = QColor("#00aaff");
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(6, pair); //голубой
    clr = Qt::darkCyan;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(7, pair);
    clr = Qt::darkMagenta;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(8, pair);
    clr = Qt::darkBlue;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(9, pair);
    clr = Qt::darkGray;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(10, pair);
    clr = Qt::cyan;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(11, pair);
    clr = Qt::green;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(12, pair);
    clr = Qt::darkRed;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(13, pair);
    clr = Qt::magenta;
    pair = QPair<QColor, bool>(clr, false);
    selectedColors.insert(14, pair);
}

Track *SpecialWidgetForPlot::addEmptyTrack_public()
{
    auto trackNew = addEmptyTrack();
    if (vSplitter/*v_Layout*/)
    {
        vSplitter/*v_Layout*/->addWidget(trackNew);
    }
    recalcWgt2HeightForScrollBar();
    return trackNew;
}

void SpecialWidgetForPlot::addGraphToTrack(uint32_t curvId, uint32_t srcId, int32_t idxTrack, bool loaded)
{
    auto ok = false;
    auto wasEmptyGraphs = tracksArray[idxTrack]->graphCount() == 0;
    auto graphWS = tracksArray[idxTrack]->addGraphicWithSettings();
    graphWS->nameGraph = GceLogicLayer::getInstance().getCurveStringIdByCurveId(srcId, curvId, ok);
    graphWS->tracerForArrowUpper = new QCPItemTracer(tracksArray[idxTrack]);
    graphWS->tracerForArrowLower = new QCPItemTracer(tracksArray[idxTrack]);
    graphWS->tracerForArrowMiddle = new QCPItemTracer(tracksArray[idxTrack]);
    graphWS->tracerForArrowUpper->setVisible(false);
    graphWS->tracerForArrowLower->setVisible(false);
    graphWS->tracerForArrowMiddle->setVisible(false);
    graphWS->tracerForArrowUpper->position->setType(QCPItemPosition::ptAbsolute);
    graphWS->tracerForArrowLower->position->setType(QCPItemPosition::ptAbsolute);
    graphWS->tracerForArrowMiddle->position->setType(QCPItemPosition::ptAbsolute);
    QPoint posTopLeft = tracksArray[idxTrack]->axisRect()->topLeft(),
            posBottomLeft = tracksArray[idxTrack]->axisRect()->bottomLeft();
    posTopLeft.setX(posTopLeft.x() - tracksArray[idxTrack]->graphicsArray.count()*TRACK_HEADER_WIDTH);
    posBottomLeft.setX(posTopLeft.x());
    graphWS->tracerForArrowUpper->position->setCoords(posTopLeft.x(), posTopLeft.y());
    graphWS->tracerForArrowLower->position->setCoords(posBottomLeft.x(), posBottomLeft.y());
    graphWS->tracerForArrowMiddle->position->setCoords(posTopLeft.x(), (posTopLeft.y()+posBottomLeft.y())/2);

    graphWS->arrow = new QCPItemLine(tracksArray[idxTrack]);
    graphWS->arrow->setClipToAxisRect(false);
    graphWS->arrow->start->setParentAnchor(graphWS->tracerForArrowLower->position);
    graphWS->arrow->end->setParentAnchor(graphWS->tracerForArrowUpper->position);
    graphWS->arrow->start->setCoords(0, 0);
    graphWS->arrow->end->setCoords(0, 0);
    graphWS->lMaximum = new QCPItemText(tracksArray[idxTrack]);
    graphWS->lMaximum->setClipToAxisRect(false);
    graphWS->lMaximum->position->setParentAnchor(graphWS->tracerForArrowUpper->position);
    graphWS->lMaximum->position->setCoords(6, -1);
    graphWS->lMaximum->setTextAlignment(Qt::AlignRight);
    graphWS->lMaximum->setPositionAlignment(Qt::AlignRight | Qt::AlignBottom);
    graphWS->lMaximum->setRotation(-90.0);
    graphWS->lMinimum = new QCPItemText(tracksArray[idxTrack]);
    graphWS->lMinimum->setClipToAxisRect(false);
    graphWS->lMinimum->position->setParentAnchor(graphWS->tracerForArrowLower->position);
    graphWS->lMinimum->position->setCoords(6, 1);
    graphWS->lMinimum->setTextAlignment(Qt::AlignLeft);
    graphWS->lMinimum->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
    graphWS->lMinimum->setRotation(-90.0);
    graphWS->labelNameGraphic = new QCPItemText(tracksArray[idxTrack]);
    graphWS->labelNameGraphic->setClipToAxisRect(false);
    graphWS->updateLabelGraphicName();
    graphWS->labelNameGraphic->position->setParentAnchor(graphWS->tracerForArrowMiddle->position);
    graphWS->labelNameGraphic->position->setCoords(-15, 0);
    graphWS->labelNameGraphic->setRotation(-90.0);

    auto project = qobject_cast<Project*>( getPlot()->getItem()->getProjectPointer() );

    auto fnt = graphWS->labelNameGraphic->font();
    fnt.setPointSize(project->fontPointSize_GraphicsNamesInHeader);
    graphWS->labelNameGraphic->setFont(fnt);
    fnt = graphWS->lMaximum->font();
    fnt.setPointSize(project->fontPointSize_GraphicsMinMaxValuesInHeader);
    graphWS->lMaximum->setFont(fnt);
    graphWS->lMinimum->setFont(fnt);

    graphWS->setScaleMaxValue(5000.0);
    graphWS->setScaleMinValue(0.0);

    graphWS->loaded = loaded;
    graphWS->curveID = curvId;
    graphWS->sourceID = srcId;
    graphWS->deviceName = GceLogicLayer::getInstance().getDeviceName(srcId);

    if ( !plot_devicesNames.contains(graphWS->deviceName) )
    {
        plot_devicesNames << graphWS->deviceName;
    }

    tracksArray[idxTrack]->graphicsArray.append(graphWS);

    if (loaded)
    {
        auto y_n = GceLogicLayer::getInstance().getValuesY(srcId, curvId, ok);

        emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(srcId, curvId);

        if (wasEmptyGraphs)
        {
            showWholeTimesRange();
        }
        graphWS->axisSpec->rescale(true); // rescale values only for this graphic
        graphWS->setScaleMaxValue(graphWS->axisSpec->range().upper);
        graphWS->setScaleMinValue(graphWS->axisSpec->range().lower);
        if (y_n.size() == 1)
        {
            graphWS->setScaleMaxValue(graphWS->axisSpec->range().upper + 1);
            graphWS->setScaleMinValue(graphWS->axisSpec->range().lower - 1);
        }
        Track::autoRoundRangeValues(graphWS);
        tracksArray[idxTrack]->replot();
    }
    else
    {
        auto conf = qobject_cast<Configuration*>(
                    GceLogicLayer::getInstance().
                    getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration)
                    );
        if (conf->dataMembers.loadDataAutomaticallyThenAddGraphicsToTrack)
        {
            slotLoadDataForSelectedCurves();
        }
    }

    graphWS->divisionValueForAxis = //tracksArray[idxTrack]->yAxis->range().size() / graphWS->axisSpec->range().size();
            graphWS->axisSpec->range().size() / tracksArray[idxTrack]->yAxis->range().size();

    uint8_t idxFreeSelectedColor = firstIndexSelectedColorWhichAreUsedInTrack(tracksArray[idxTrack]);
    if (idxFreeSelectedColor == 0)
    {
        QColor clr;
        clr.setAlpha(255);
        int valRandTo255 = rand()%255;
        clr.setBlue(valRandTo255);
        valRandTo255 = rand()%255;
        clr.setGreen(valRandTo255);
        valRandTo255 = rand()%255;
        clr.setRed(valRandTo255);
        graphWS->currentPenColor = QColor(clr);
    }
    else
    {
        graphWS->currentPenColor = QColor(selectedColors.value(idxFreeSelectedColor).first);
    }

    graphWS->colorBrush = QBrush(graphWS->currentPenColor);

    graphWS->colorPen = QPen(graphWS->currentPenColor);
    graphWS->colorPen.setWidth(LINE_DEFAULT_THICKNESS);
    graphWS->graphic->setPen(graphWS->colorPen);

    auto pen = graphWS->colorPen;
    pen.setWidth(2.0);
    graphWS->arrow->setPen(pen);
    graphWS->lMaximum->setColor(graphWS->currentPenColor);
    graphWS->lMinimum->setColor(graphWS->currentPenColor);
    graphWS->lMaximum->setBrush(QBrush(Qt::white));
    graphWS->lMinimum->setBrush(QBrush(Qt::white));
}

void SpecialWidgetForPlot::refreshDevicesLinkedToPlotAfterDeleteAnyTrackOrGraphic()
{
    QStringList slDevsPlot;
    for (Track *track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto devName = gws->deviceName;
            if (!slDevsPlot.contains(devName))
            {
                slDevsPlot << devName;
            }
        }
    }
    plot_devicesNames = slDevsPlot.toVector();
}

void SpecialWidgetForPlot::resetScroll()
{
    slotScrolledVerticalScrollBar(lastScrollBarPositionValue);
    lastScrollBarPositionValue = 0;
    scrollBar->setSliderPosition(0);
}

void SpecialWidgetForPlot::recalcWgt2HeightForScrollBar()
{
    if (tracksArray.size() == 0) return;
    scrollBar->setValue(0);
    scrollBar->setMinimum(0);
    auto heightSum = 0;
    auto szTA = tracksArray.size();
    for (decltype (szTA) i = 0; i < szTA; ++i)
    {
        if (tracksArray[i]->isHidden()) continue;
        heightSum += tracksArray[i]->height();
        heightSum += vSplitter->handleWidth();
    }
    vSplitter->setFixedHeight(heightSum);
    scrollBar->setMaximum(heightSum);
}

void SpecialWidgetForPlot::setPosViewGraphicsValuesMode(Track *pltTrack, QMouseEvent *msevt)
{
    auto positionInPlotGlobal = msevt->globalPos();
    auto positionInTrack = pltTrack->mapFromGlobal(positionInPlotGlobal);
    if (positionInTrack.x() < pltTrack->axisRect()->left())
    {
        msevt->accept();
        positionLast = msevt->pos();
        return;
    }
    double coord = pltTrack->xAxis->pixelToCoord(positionInTrack.x());

    auto obj = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameTextEditForViewGraphicsValuesMode);
    if (obj)
    {
        auto textEditValues = qobject_cast<QTextEdit*>(obj);
        textEditValues->clear();
        textEditValues->setTextColor(Qt::black);
        int64_t utc = qRound(coord);
        QString timeStr = tr("Time = ");
        QString tmpstr;
        tmpstr.setNum(coord);
        timeStr.append(tmpstr);
        timeStr.append(tr(" seconds")).append("\n[~");
        timeStr.append(QDateTime::fromSecsSinceEpoch(utc, QTimeZone::utc()
                                                     ).toString("dd.MM.yyyy hh:mm:ss")).append("]\n");
        textEditValues->insertPlainText(timeStr);

        auto szLV = linesVectorPtrForViewGraphicsValuesMode.size();
        for (decltype (szLV) iline = 0; iline < szLV; ++iline)
        {
            auto lineInTrack = linesVectorPtrForViewGraphicsValuesMode[iline];
            lineInTrack->start->setCoords(positionInTrack.x(), lineInTrack->start->coords().y());
            lineInTrack->end->setCoords(positionInTrack.x(), lineInTrack->end->coords().y());
        }

        while (positionsVectorPtrForViewGraphicsValuesMode.size())
        {
            auto tracer = positionsVectorPtrForViewGraphicsValuesMode.takeFirst();
            auto plot = tracer->parentPlot();
            plot->removeItem(tracer);
        }
        foreach (Track *track, tracksArray)
        {
            textEditValues->textCursor().insertBlock();
            textEditValues->setTextColor(Qt::black);
            textEditValues->insertPlainText(track->getItem()->getNameElement());
            QVector<double> foundXcoords;
            QVector<QString> valuesAsStrings;
            for (GraphWithSettings *gws : track->graphicsArray)
            {
                double foundX = 0.0;
                double foundY = 0.0;
                QString val;
                QVector<double> xcoordsGraph;
                for (auto it = gws->graphic->data().data()->constBegin(); it != gws->graphic->data().data()->constEnd(); it++)
                {
                    if (it->key > coord)
                    {
                        it--;
                        foundY = it->value;
                        foundX = it->key;
                        val.setNum(foundY);
                        break;
                    }
                }
                foundXcoords << foundX;
                valuesAsStrings << val;
            }

            auto szGA = track->graphicsArray.size();
            for (decltype (szGA) igr = 0; igr < szGA; ++igr)
            {
                auto gws = track->graphicsArray.at(igr);
                textEditValues->textCursor().insertBlock();
                QString forHtmlPrepare = gws->nameGraph;
                forHtmlPrepare.append(" = ");
                forHtmlPrepare.append(valuesAsStrings.at(igr));
                QString html = gws->currentPenColor.name(QColor::HexRgb);
                html.prepend("<font color=\"").append("\">").append(forHtmlPrepare).append("</font>");
                textEditValues->insertHtml(html);

                auto okcrv = false;
                auto [nullValueExist, nullValue_value] =
                        GceLogicLayer::getInstance()
                        .nullValueExistAndValue(gws->sourceID, gws->curveID, okcrv);
                if (!okcrv) continue;
                if (foundXcoords.at(igr) != (nullValueExist ? nullValue_value : V_NONE))
                {
                    auto positionTracer = new QCPItemTracer(track);
                    positionTracer->setClipToAxisRect(false);
                    positionTracer->position->setType(QCPItemPosition::ptPlotCoords);
                    positionTracer->setStyle(QCPItemTracer::TracerStyle::tsSquare);
                    positionTracer->setSize(positionTracer->size()*1.5);
                    positionTracer->setBrush(gws->colorBrush);
                    positionTracer->setGraph(gws->graphic);

                    positionTracer->setGraphKey(foundXcoords.at(igr));
                    positionsVectorPtrForViewGraphicsValuesMode << positionTracer;
                }

            }
            track->replot();
        }
    }
}

void SpecialWidgetForPlot::updatePlots(bool scaledValues)
{
    if (tracksArray.size()==0)
    {
        return;
    }

    auto track_1st = tracksArray.at(0);
    auto upper1 = track_1st->xAxis->range().upper;

    auto margins = track_1st->axisRect()->margins();
    margins.setBottom(5);

    auto szTA = tracksArray.size();
    for (decltype (szTA) itr = 0; itr < szTA; ++itr)
    {
        auto track_curr = tracksArray[itr];
        track_curr->axisRect()->setMargins(margins);
        track_curr->xAxis->setTicks(false);
        if (!scaledValues)
        {
            track_curr->specialAutoRescaleValuesAxes();
        }
        else
        {
            GraphWithSettings *gws_0 = nullptr;
            auto existGraphsWithFixedDivisionValue = false;
            for (GraphWithSettings *gws_track : track_curr->graphicsArray)
            {
                if (!existGraphsWithFixedDivisionValue && gws_track->divisionValueIsFixed )
                {
                    gws_0 = gws_track;
                    existGraphsWithFixedDivisionValue = true;
                }
                gws_track->setScaleMaxValue(gws_track->scaleMaximumValue);
                gws_track->setScaleMinValue(gws_track->scaleMinimumValue);
                gws_track->axisSpec->setRange(gws_track->scaleMinimumValue, gws_track->scaleMaximumValue);
            }
            if (existGraphsWithFixedDivisionValue)
            {
                auto rngSz = gws_0->axisSpec->range().size();
                auto cnt_a = rngSz / gws_0->divisionValueForAxis;

                track_curr->yAxis->setRange(0.0, cnt_a);
                track_curr->yAxis->ticker().data()->setTickCount(static_cast<int>(cnt_a));
            }
        }

        if (itr == 0)
        {
            continue;
        }
        track_curr->xAxis->setScaleRatio(track_1st->xAxis);  // ->scaleRange(track_1st->axisRect()->rangeZoomFactor(Qt::Horizontal));
        auto upper_curr_old = track_curr->xAxis->range().upper;
        auto lower_curr_old = track_curr->xAxis->range().lower;
        auto diff = upper1 - upper_curr_old;
        auto upper_curr_new = diff + upper_curr_old,
                lower_curr_new = diff + lower_curr_old;
        track_curr->xAxis->setRange(lower_curr_new, upper_curr_new); //???
    }

//    tracksArray.last()->xAxis->setTicks(true);
//    margins.setBottom(30);
//    tracksArray.last()->axisRect()->setMargins(margins);

//    for (int itr = 0; itr < tracksArray.size(); ++itr)
//    {
//        tracksArray[itr]->replot();
//    }

    refreshHeadersOfTracks();
}

void SpecialWidgetForPlot::refreshHeadersOfTracks()
{    
    maxTrackHeaderWidth = 0;

    auto szTA = tracksArray.size();
    for (decltype (szTA) idxTrack = 0; idxTrack < szTA; ++idxTrack)
    {
        //refreshTrackHeader(itr);

        auto track = tracksArray[idxTrack];
        //track->update();
        //track->axisRect()->update(QCPAxisRect::upPreparation);
        auto margins = track->axisRect()->margins();
        auto leftMargine = 40;
        leftMargine += TRACK_HEADER_WIDTH*track->graphicsArray.count();
        margins.setLeft(leftMargine);
        track->axisRect()->setMargins(margins);
        if (maxTrackHeaderWidth < leftMargine)
        {
            maxTrackHeaderWidth = leftMargine;
        }
        auto szGA = track->graphicsArray.size();
        for (decltype (szGA) igr = 0; igr < szGA; ++igr)
        {
            GraphWithSettings *gws = track->graphicsArray.value(igr);
            gws->arrowOffsetFromPlot = -TRACK_HEADER_WIDTH*(igr+1);
//            if (maxTrackHeaderWidth < gws->arrowOffsetFromPlot)
//            {
//                maxTrackHeaderWidth = gws->arrowOffsetFromPlot;
//            }
        }
        for (decltype (szGA) igr = 0; igr < szGA; ++igr)
        {
            GraphWithSettings *gws = track->graphicsArray.value(igr);
            QPoint posTopLeft = track->axisRect()->topLeft(),
                    posBottomLeft = track->axisRect()->bottomLeft();
            posTopLeft.setX(posTopLeft.x() + gws->arrowOffsetFromPlot);
            posBottomLeft.setX(posTopLeft.x());
            gws->tracerForArrowUpper->position->setCoords(posTopLeft.x(), posTopLeft.y());
            gws->tracerForArrowLower->position->setCoords(posBottomLeft.x(), posBottomLeft.y());
            gws->tracerForArrowMiddle->position->setCoords(posTopLeft.x(), (posTopLeft.y()+posBottomLeft.y())/2);
        }

        //track->replot();
    }

    for (decltype (szTA) idxTrack = 0; idxTrack < szTA; ++idxTrack)
    {
        auto track = tracksArray[idxTrack];
        auto margins = track->axisRect()->margins();
        margins.setLeft(maxTrackHeaderWidth);
        track->axisRect()->setMargins(margins);
        track->replot();
    }

    if (szTA)
    {
        auto margins = trackTimeOnly->axisRect()->margins();
        margins.setLeft(maxTrackHeaderWidth);
        trackTimeOnly->axisRect()->setMargins(margins);
        trackTimeOnly->replot();
    }
}

void SpecialWidgetForPlot::showWholeTimesRange()
{
    auto found = false, firstFounded = false;
    double min = 0.0, max = 0.0;

    for (Track *track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            if (!gws->loaded)
            {
                continue;
            }
            gws->graphic->getKeyRange(found);
            if (found)
            {
                if (!firstFounded)
                {
                    min = gws->graphic->getKeyRange(found).lower;
                    max = gws->graphic->getKeyRange(found).upper;

                    firstFounded = true;
                }
                else
                {
                    auto newMin = gws->graphic->getKeyRange(found).lower;
                    auto newMax = gws->graphic->getKeyRange(found).upper;

                    if (newMin < min)
                    {
                        min = newMin;
                    }
                    if (newMax > max)
                    {
                        max = newMax;
                    }
                }
            }
        }
    }

    auto range_simple = max - min;
    auto margin = range_simple * 0.1;
    min -= margin;
    max += margin;

    slotRescaleXaxis(min, max);

    emit sigChangeTimeRangeForJoinedByTimePlots(min, max);
}

void SpecialWidgetForPlot::shiftValuesAxesToShowLastValuesOnGraphics()
{
   // auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameProjectManager);
    //auto prjMgr = qobject_cast<ProjectManager*>(o);
    //auto project = prjMgr->getProjectsArrayPtr()->values().last();

    for (Track *track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            if (gws->frozenValuesScaleForAutoScale)
            {
                continue;
            }
            auto lastX = track->xAxis->range().upper;
            auto ok = false;
            auto pairIDs = QPair<uint32_t, uint32_t>(gws->sourceID, gws->curveID);
            auto lstPairIDs = QList<QPair<uint32_t, uint32_t> >();
            lstPairIDs << pairIDs;
            QVector<double> xcoordsGraph;
            double tmp_d = 0.0;
            QString val;
            for (auto it = gws->graphic->data().data()->constBegin(); it != gws->graphic->data().data()->constEnd(); it++)
            {
                if (it->key > lastX)
                {
                    it--;
                    tmp_d = it->value;
                    val.setNum(tmp_d);
                    break;
                }
            }
            auto currentValueString = val;
            auto currentValue = tmp_d;
            if (!ok) continue;
            auto minValGr = gws->scaleMinimumValue;
            auto maxValGr = gws->scaleMaximumValue;
            auto rangeValues = maxValGr - minValGr;

            // все последние точки на середину:
            auto newMaxValGr = currentValue + 0.5*rangeValues;
            auto newMinValGr = newMaxValGr - rangeValues;
            gws->setScaleMinValue(newMinValGr);
            gws->setScaleMaxValue(newMaxValGr);
            gws->axisSpec->setRange(newMinValGr, newMaxValGr);
            Track::autoRoundRangeValues(gws, true);
        }

        track->replot();
    }
}

void SpecialWidgetForPlot::resizeEvent(QResizeEvent *rsze)
{
    refreshHeadersOfTracks();
    recalcWgt2HeightForScrollBar();
}

//void SpecialWidgetForPlot::DragEnterEvent(QDragEnterEvent *dragEntEvt)
//{
//    if (dragEntEvt->mimeData()->hasFormat("specialcopy/curve"))
//    {
//        dragEntEvt->setDropAction(Qt::CopyAction);
//        dragEntEvt->accept();
//    }
//}

//void SpecialWidgetForPlot::DropEvent(QDropEvent *dropEvt)
//{
//    if (!dropEvt->mimeData()->hasFormat("specialcopy/curve"))
//    {
//        return;
//    }
//    //dropEvt->acceptProposedAction();
//    dropEvt->accept();
//    auto o = GceLogicLayer::getInstance()
//            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameAddedCurve);
//    auto idx = GceLogicLayer::getInstance().getObjectsContainerPointer()->indexOf(o);
//    GceLogicLayer::getInstance().getObjectsContainerPointer()->takeAt(idx);
//    auto curve = qobject_cast<Curve*>(o);
//    for (int itrack = 0; itrack < tracksArray.size(); ++itrack)
//    {
//        if ( tracksArray[itrack]->underMouse() )
//        {
//            addGraphToTrack(curve->getCurveID(), curve->getSourceID(),
//                            itrack,
//                            (curve->getItem()->type != TreeElement::ElementType::elementNotLoadedCurve));
//            break;
//        }
//    }
//}

void SpecialWidgetForPlot::slotSwapAxes()
{
//    if (centerWgtstack->currentIndex() == 0)
//    {
//        orientationMode = Qt::Vertical;
//        centerWgtstack->setCurrentIndex(1);
//        for (int i = 0; i < tracksArray.size(); ++i)
//        {
//            h_Layout->insertWidget(0, tracksArray.at(i));
//            tracksArray[i]->axisRect()->setRangeDrag(orientationMode);
//            tracksArray[i]->axisRect()->setRangeZoom(orientationMode);

//            QCPAxis *keyAxis = tracksArray.at(i)->xAxis;
//            QCPAxis *valueAxis = tracksArray.at(i)->yAxis;
//            double minKey = keyAxis->range().lower;
//            double maxKey = keyAxis->range().upper;
//            double minValue = valueAxis->range().lower;
//            double maxValue = valueAxis->range().upper;
//            keyAxis->setRange(minValue, maxValue);
//            valueAxis->setRange(minKey, maxKey);
//            valueAxis->setRangeReversed(true);
//            keyAxis->setRangeReversed(false);

//            if (tracksArray.at(i)->graphCount() == 0)
//            {
//                // ???
//            }
//            else
//            {
//                for (int ig = 0; ig < tracksArray.at(i)->graphCount(); ++ig)
//                {
//                    //
//                    tracksArray[i]->graph(ig)->setKeyAxis(valueAxis);
//                    tracksArray[i]->graph(ig)->setValueAxis(keyAxis);
//                }
//            }
//        }
//    }
//    else
//    {
//        orientationMode = Qt::Horizontal;
//        centerWgtstack->setCurrentIndex(0);
//        for (int i = 0; i < tracksArray.size(); ++i)
//        {
//            v_Layout->addWidget(tracksArray.at(i));
//            tracksArray[i]->axisRect()->setRangeDrag(orientationMode);
//            tracksArray[i]->axisRect()->setRangeZoom(orientationMode);

//            QCPAxis *keyAxis = tracksArray.at(i)->yAxis;
//            QCPAxis *valueAxis = tracksArray.at(i)->xAxis;
//            double minKey = keyAxis->range().lower;
//            double maxKey = keyAxis->range().upper;
//            double minValue = valueAxis->range().lower;
//            double maxValue = valueAxis->range().upper;
//            keyAxis->setRange(minValue, maxValue);
//            valueAxis->setRange(minKey, maxKey);
//            valueAxis->setRangeReversed(false);
//            keyAxis->setRangeReversed(false);

//            if (tracksArray.at(i)->graphCount() == 0)
//            {
//                // ???
//            }
//            else
//            {
//                for (int ig = 0; ig < tracksArray.at(i)->graphCount(); ++ig)
//                {
//                    //
//                    tracksArray[i]->graph(ig)->setKeyAxis(valueAxis);
//                    tracksArray[i]->graph(ig)->setValueAxis(keyAxis);
//                }
//            }
//        }
//    }

////    for (int i = 0; i < tracksArray.size(); ++i)
////    {
////        tracksArray[i]->replot();
////    }
//    updatePlots();
}

void SpecialWidgetForPlot::slotAddNewTrack()
{
    auto track = addEmptyTrack();
    vSplitter/*v_Layout*/->addWidget(track);
//    connect(track->getItem(), SIGNAL(sigClicked()), )
    recalcWgt2HeightForScrollBar();
//    refreshHeadersOfTracks();
//    for (int itr = 0; itr < tracksArray.size(); ++itr)
//    {
//        tracksArray[itr]->replot();
//    }
}

void SpecialWidgetForPlot::slotSavePlotToProject(QString projectDataDir)
{
    auto plotsDataDir = QString().append(projectDataDir).append("/p");
    QDir dirPlotsData(plotsDataDir);
    if (!dirPlotsData.exists())
    {
        QStringList tmp = plotsDataDir.split("/");
        QString plotsDir = tmp.takeLast();
        QDir dirCommon(tmp.join("/"));
        dirCommon.mkdir(plotsDir);
    }
//    QList<int32_t> listEntries;
//    foreach (QFileInfo entry, dirPlotsData.entryInfoList(QDir::Files))
//    {
//        QString entryName = entry.completeBaseName();
//        bool ok;
//        int32_t v = entryName.toInt(&ok);
//        if (ok)
//        {
//            listEntries.append(v);
//        }
//    }
//    int32_t vlast = 1;
//    if (listEntries.size())
//    {
//        std::sort(listEntries.begin(), listEntries.end());
//        vlast = listEntries.last();
//        vlast++;
//    }
//    foreach (QFileInfo entry, dirPlotsData.entryInfoList(QDir::Files))
//    {
//        QFile fl4del(entry.absoluteFilePath());
//        fl4del.remove();
//    }
    QString fName;
    auto cntPF = dirPlotsData.entryList(QDir::Files).count();
    fName = QString::number(cntPF);
    if (fName.length() < 4)
    {
        auto sz = 4 - fName.length();
        while (sz)
        {
            fName.prepend("0");
            sz--;
        }
    }
    fName.append(".data");
    QString fullFileName = QString().append(plotsDataDir)
            .append("/").append(fName);
    savingPreparedFileWithPlot(fullFileName);
}

void SpecialWidgetForPlot::slotExportPlotToLocalFile(QString localFileNameWithPath)
{
    savingPreparedFileWithPlot(localFileNameWithPath);
}

void SpecialWidgetForPlot::slotMouseWheel(QWheelEvent *mwe)
{
    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        return;
    }

    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        return;
    }

    Track *track = nullptr;
    if (rightMouseButtonPressed)
    {
        mwe->accept();
        auto angle = static_cast<double>(mwe->angleDelta().y());
        track = qobject_cast<Track*>(sender());
        auto pos = (orientationMode == Qt::Horizontal ? mwe->pos().y() : mwe->pos().x());
        auto orientation = (orientationMode == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);
        double changing = 0.0, centerPosD = 0.0;

        auto oldRangeSizeCommonAxis = track->yAxis->range().size(), oldRangeSizeForAxis = 0.0, divisionValueForAxis = 0.0;
        QCPAxis *valueAxis_dv = nullptr;
        auto fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated = false;

        for (GraphWithSettings *gws : track->graphicsArray)
        {
            if (gws->divisionValueIsFixed)
            {
                fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated = true;

                valueAxis_dv = gws->axisSpec;
                oldRangeSizeForAxis = valueAxis_dv->range().size();
                divisionValueForAxis = gws->divisionValueForAxis;
                break;
            }
        }

        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto valueAxis = gws->axisSpec;
            if (gws->frozenValuesScaleForAutoScale)
            {
                continue;
            }
            changing = qPow(valueAxis->axisRect()->rangeZoomFactor(orientation), angle/120.0);
            centerPosD = valueAxis->pixelToCoord( pos );
            valueAxis->scaleRange(changing, centerPosD);
            gws->setScaleMaxValue(valueAxis->range().upper);
            gws->setScaleMinValue(valueAxis->range().lower);
            if (gws->graphic->dataCount() == 1)
            {
                gws->setScaleMaxValue(gws->axisSpec->range().upper + 1);
                gws->setScaleMinValue(gws->axisSpec->range().lower - 1);
            }
            Track::autoRoundRangeValues(gws);
        }

        if (fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
        {
            auto newRangeSizeForAxis = valueAxis_dv->range().size();
            auto newRangeCommonAxis = track->yAxis->range().size() * newRangeSizeForAxis / oldRangeSizeForAxis;

            track->yAxis->setRange(0.0, newRangeCommonAxis);
            auto ticker = track->yAxis->ticker();
            auto intCountOfTicks = qRound(newRangeSizeForAxis / divisionValueForAxis);
            ticker.data()->setTickCount(intCountOfTicks);
        }

        for (GraphWithSettings *gws : track->graphicsArray)
        {
            if (!gws->divisionValueIsFixed ) //&& fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
            {
                gws->divisionValueForAxis = gws->axisSpec->range().size() / track->yAxis->range().size();
            }
        }

        track->replot();

        valuesRescaledByMouseWheel = true;
    }
    else if (!rightMouseButtonPressed && !leftMouseButtonPressed && !viewGraphicsValuesMode)
    {
        mwe->accept();

        auto angle = static_cast<double>(mwe->angleDelta().y());
        track = qobject_cast<Track*>(sender());
        auto pos = (orientationMode == Qt::Horizontal ? mwe->pos().x() : mwe->pos().y());

        double changing = 0.0, centerPosD = 0.0;
        auto abscissAxis = (orientationMode == Qt::Horizontal ? track->xAxis : track->yAxis);
        changing = qPow(abscissAxis->axisRect()->rangeZoomFactor(orientationMode), angle/120.0);
        centerPosD = abscissAxis->pixelToCoord( pos );
        auto szTA = tracksArray.size();
        for (decltype (szTA) i = 0; i < szTA; ++i)
        {
            QCPAxis *abscissAxis = (orientationMode == Qt::Horizontal ? tracksArray.at(i)->xAxis : tracksArray.at(i)->yAxis);
            abscissAxis->scaleRange(changing, centerPosD);

            auto track2 = tracksArray[i];
            //track2->relocateLabelsTextsWithoutReplot();

            track2->replot();
        }
        if (tracksArray.size())
        {
            QCPAxis *abscissAxis =
                    (orientationMode == Qt::Horizontal ? trackTimeOnly->xAxis2 : trackTimeOnly->yAxis);
            abscissAxis->scaleRange(changing, centerPosD);

            trackTimeOnly->replot();

            emit sigChangeTimeRangeForJoinedByTimePlots(
                        abscissAxis->range().lower, abscissAxis->range().upper);
        }
    }
//    if (!track)
//    {
//        return;
//    }
//    int leftPosXOnChanged = track->
//    if (tracksArray.size() > 1)
//    {
//        for (int i = 0; i < tracksArray.size(); ++i)
//        {
//            if (tracksArray[i] == track)
//            {
//                continue;
//            }
//            QCPAxisRect *qrct = tracksArray[i]->axisRect();
//            if (qrct->left() == leftPosXOnChanged)
//            {
//                continue;
//            }
//            QRect rct = qrct->outerRect();
//            rct.setLeft(leftPosXOnChanged);
//            tracksArray[i]->axisRect()->setOuterRect(rct);
//            tracksArray[i]->replot();
//        }
//    }
}

void SpecialWidgetForPlot::slotMousePress(QMouseEvent *mpe)
{
    positionLast = mpe->pos();
    positionFirst = positionLast;
    auto pltTrack = qobject_cast<Track*>(sender());

    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        if (mpe->button() == Qt::LeftButton)
        {
            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? pltTrack->xAxis : pltTrack->yAxis);
            double firstDblCoordGraphX = abscissAxis->pixelToCoord(positionFirst.x());
            if (firstDblCoordGraphX > abscissAxis->range().upper)
            {
                firstDblCoordGraphX = abscissAxis->range().upper;
            }
            if (firstDblCoordGraphX < abscissAxis->range().lower)
            {
                firstDblCoordGraphX = abscissAxis->range().lower;
            }

            auto y_axis =
                    (orientationMode == Qt::Vertical ? pltTrack->xAxis : pltTrack->yAxis);
            double firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
            if (firstDblCoordGraphY > y_axis->range().upper)
            {
                firstDblCoordGraphY = y_axis->range().upper;
            }
            if (firstDblCoordGraphY < y_axis->range().lower)
            {
                firstDblCoordGraphY = y_axis->range().lower;
            }

            auto dimensionDialogObj = GceLogicLayer::getInstance()
                    .getObjectFromObjectContainerByObjName(ObjectsNames::objNameDialogDimensionMode);
            auto dimensionDialog = qobject_cast<QDialog*>(dimensionDialogObj);
            if (dimensionMode)
            {
//                dimensionRect->setVisible(false);
//                dimensionStartPoint->setVisible(false);
//                dimensionEndPoint->setVisible(false);
                dimensionTrack->removeItem(dimensionRect);
                dimensionTrack->removeItem(dimensionStartPoint);
                dimensionTrack->removeItem(dimensionEndPoint);
                dimensionTrack->replot();
            }
            else
            {
                dimensionMode = true;
                connect(dimensionDialog, &QDialog::finished,
                        this, [&](int res)
                {
                    Q_UNUSED(res)
                    dimensionMode = false;
//                    dimensionRect->setVisible(false);
//                    dimensionStartPoint->setVisible(false);
//                    dimensionEndPoint->setVisible(false);
                    dimensionTrack->removeItem(dimensionRect);
                    dimensionTrack->removeItem(dimensionStartPoint);
                    dimensionTrack->removeItem(dimensionEndPoint);
                    dimensionTrack->replot();
                });
            }
            dimensionProcess = true;
//            if (dimensionRect == nullptr)
//            {
//            }

            dimensionRect = new QCPItemRect(pltTrack);
            dimensionRect->setBrush(QBrush(Qt::NoBrush));
            auto pen = dimensionRect->pen();
            pen.setBrush(QBrush(Qt::darkBlue));
            pen.setWidth(1);
            pen.setStyle(Qt::DashLine);
            dimensionRect->setPen(pen);
            dimensionRect->topLeft->setType(QCPItemPosition::ptPlotCoords);
            dimensionRect->bottomRight->setType(QCPItemPosition::ptPlotCoords);
            dimensionStartPoint = new QCPItemTracer(pltTrack);
            dimensionEndPoint = new QCPItemTracer(pltTrack);
            dimensionStartPoint->setStyle(QCPItemTracer::tsSquare);
            dimensionEndPoint->setStyle(QCPItemTracer::tsSquare);
            dimensionStartPoint->setBrush(QBrush(QColor("#ff00ff")));
            dimensionEndPoint->setBrush(QBrush(QColor("#ff00ff")));
            dimensionStartPoint->position->setType(QCPItemPosition::ptPlotCoords);
            dimensionEndPoint->position->setType(QCPItemPosition::ptPlotCoords);

//            dimensionRect->setVisible(false);
//            dimensionStartPoint->setVisible(false);
//            dimensionEndPoint->setVisible(false);

            dimensionStartPoint->position->setCoords(firstDblCoordGraphX, firstDblCoordGraphY);
            positionFirst = dimensionStartPoint->position->pixelPosition().toPoint();

            dimensionTrack = pltTrack;

            dimensionTrack->replot();
        }

        mpe->accept();
        return;
    }

    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        if (mpe->button() == Qt::LeftButton)
        {
            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? pltTrack->xAxis : pltTrack->yAxis);
            double firstDblCoordGraphX = abscissAxis->pixelToCoord(positionFirst.x());
            if (firstDblCoordGraphX > abscissAxis->range().upper)
            {
                firstDblCoordGraphX = abscissAxis->range().upper;
            }
            if (firstDblCoordGraphX < abscissAxis->range().lower)
            {
                firstDblCoordGraphX = abscissAxis->range().lower;
            }

            auto y_axis =
                    (orientationMode == Qt::Vertical ? pltTrack->xAxis : pltTrack->yAxis);
            double firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
            if (firstDblCoordGraphY > y_axis->range().upper)
            {
                firstDblCoordGraphY = y_axis->range().upper;
            }
            if (firstDblCoordGraphY < y_axis->range().lower)
            {
                firstDblCoordGraphY = y_axis->range().lower;
            }

            scalingByRectangleMode = true;
            scalingRect = new QCPItemRect(pltTrack);
            auto pen = scalingRect->pen();
            pen.setBrush(QBrush(Qt::black));
            pen.setWidth(2);
            scalingRect->setPen(pen);
            scalingRect->topLeft->setType(QCPItemPosition::ptPlotCoords);
            //scalingRect->topRight->setType(QCPItemPosition::ptPlotCoords);
            //scalingRect->bottomLeft->setType(QCPItemPosition::ptPlotCoords);
            scalingRect->bottomRight->setType(QCPItemPosition::ptPlotCoords);

            scalingRect->topLeft->setCoords(firstDblCoordGraphX, firstDblCoordGraphY);
            positionFirst = scalingRect->topLeft->pixelPosition().toPoint();

            scaledTrack = pltTrack;
        }

        mpe->accept();
        return;
    }

    if (orientationMode == Qt::Horizontal)
    {
        pltTrack->axisRect()->setRangeZoom(Qt::Vertical);
    }
    else
    {
        pltTrack->axisRect()->setRangeZoom(Qt::Horizontal);
    }

    if (mpe->button() == Qt::RightButton)
    {
//        Track *plt = qobject_cast<Track*>(sender());
//        if (plt->axisRect()->rangeZoom() == Qt::Horizontal)
//            plt->axisRect()->setRangeZoom(Qt::Vertical);
//        else
//            plt->axisRect()->setRangeZoom(Qt::Horizontal);


        rightMouseButtonPressed = true;


    }

    if (mpe->button() == Qt::LeftButton)
    {
        leftMouseButtonPressed = true;

        if (viewGraphicsValuesMode)
        {
            if (viewGraphicsValuesMode_Fixation)
            {
                //viewGraphicsValuesMode_Fixation = false;
                setPosViewGraphicsValuesMode(pltTrack, mpe);
            }
            else
            {
                viewGraphicsValuesMode_Fixation = true;
            }
        }
    }

    mpe->accept();
}

void SpecialWidgetForPlot::slotMouseRelease(QMouseEvent *mre)
{
    positionLast = mre->pos();

    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        if (mre->button() == Qt::LeftButton)
        {
            dimensionProcess = false;
        }

        mre->accept();
        return;
    }

    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        if (mre->button() == Qt::LeftButton && scalingByRectangleMode)
        {
            //dimensionProcess = false;
            scalingByRectangleMode = false;
            qApp->setProperty("ScalingByRectangleMode", QVariant(false));
            qApp->restoreOverrideCursor();
            scaledTrack->removeItem(scalingRect);

            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? scaledTrack->xAxis : scaledTrack->yAxis);
            double firstDblCoordGraphX = abscissAxis->pixelToCoord(positionFirst.x());
            double lastDblCoordGraphX = abscissAxis->pixelToCoord(mre->pos().x());
            if (lastDblCoordGraphX > abscissAxis->range().upper)
            {
                lastDblCoordGraphX = abscissAxis->range().upper;
            }
            if (lastDblCoordGraphX < abscissAxis->range().lower)
            {
                lastDblCoordGraphX = abscissAxis->range().lower;
            }

            auto y_axis =
                    (orientationMode == Qt::Vertical ? scaledTrack->xAxis : scaledTrack->yAxis);
            double firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
            double lastDblCoordGraphY = y_axis->pixelToCoord(mre->pos().y());
            if (lastDblCoordGraphY > y_axis->range().upper)
            {
                lastDblCoordGraphY = y_axis->range().upper;
            }
            if (lastDblCoordGraphY < y_axis->range().lower)
            {
                lastDblCoordGraphY = y_axis->range().lower;
            }

            auto firstLowerlastUpperOnY = firstDblCoordGraphY < lastDblCoordGraphY;

            for (GraphWithSettings *gws : scaledTrack->graphicsArray)
            {
                if (gws->frozenValuesScaleForAutoScale)
                {
                    continue;
                }
                auto newLowerY = 0.0, newUpperY = 0.0;
                if (firstLowerlastUpperOnY)
                {
                    newLowerY = gws->axisSpec->pixelToCoord(positionFirst.y());
                    newUpperY = gws->axisSpec->pixelToCoord(mre->pos().y());
                }
                else
                {
                    newLowerY = gws->axisSpec->pixelToCoord(mre->pos().y());
                    newUpperY = gws->axisSpec->pixelToCoord(positionFirst.y());
                }
                gws->axisSpec->setRange(newLowerY, newUpperY);
                gws->setScaleMinValue(newLowerY);
                gws->setScaleMaxValue(newUpperY);
            }

            auto firstLowerLastUpperOnX = firstDblCoordGraphX < lastDblCoordGraphX;
            auto newLowerX = 0.0, newUpperX = 0.0;
            if (firstLowerLastUpperOnX)
            {
                newLowerX = firstDblCoordGraphX;
                newUpperX = lastDblCoordGraphX;
            }
            else
            {
                newLowerX = lastDblCoordGraphX;
                newUpperX = firstDblCoordGraphX;
            }

            slotRescaleXaxis(newLowerX, newUpperX);
            emit sigChangeTimeRangeForJoinedByTimePlots(newLowerX, newUpperX);
        }

        mre->accept();
        return;
    }

    auto pltTrack = qobject_cast<Track*>(sender());

    if (qApp->property("LabelCreatingMode").toBool())
    {
        qApp->setProperty("LabelCreatingMode", QVariant(false));
        qApp->restoreOverrideCursor();
        if (mre->button() == Qt::LeftButton)
        {
            auto project = qobject_cast<Project*>(pltTrack->getItem()->getProjectPointer());
            auto labelNew = project->createLabel();
            //labelNew->setText("OK, change this text ...");
            auto dlabel = new DataLabel(pltTrack, mre->pos(), labelNew->text());
            //connect(project, SIGNAL(sigChangeTimeZone(QTimeZone)), dlabel, SLOT(slotShiftTime()));
            connect(dlabel, SIGNAL(sigGotoLabel(double)), pltTrack, SIGNAL(sigGoToLabel(double)));
            connect(dlabel, &DataLabel::sigRemoveMe, pltTrack, &Track::slotRemoveLabel);
            connect(dlabel, &DataLabel::sigCopyMe, pltTrack, &Track::slotCopyLabel);
            connect(dlabel, &DataLabel::sigCopyMeToAnotherTrack, pltTrack, &Track::slotCopyLabelToAnotherTrack);
            connect(dlabel, &DataLabel::sigMoveMe, pltTrack, &Track::slotMoveLabel);
            dlabel->setLabelElement(labelNew, true);
            labelNew->setTrackPointer(pltTrack);
            pltTrack->replot();
            pltTrack->labelsArray.append(dlabel);
        }
    }

    if (copyLabelMode)
    {        
        if (mre->button() == Qt::LeftButton)
        {
            auto ctrlPressed = qApp->queryKeyboardModifiers().testFlag(Qt::ControlModifier);
            if (!ctrlPressed)
            {
                copyLabelMode = false;
                qApp->restoreOverrideCursor();
            }
//            else
            if (pltTrack != track_senderSignalsOfMoveOrCopy)
            {
                QMessageBox::critical(nullptr, tr("Error"),
                                         tr("You must copy to the same track!"));
            }
            else
            {
                if (!ctrlPressed && mousePressedOnceAtCopyOrMove)
                {
                    mousePressedOnceAtCopyOrMove = false;
                }
                else
                {
                    auto copyDL = dl4copy->makeCopy();
                    connect(copyDL, SIGNAL(sigGotoLabel(double)), pltTrack, SIGNAL(sigGoToLabel(double)));
                    connect(copyDL, &DataLabel::sigRemoveMe, pltTrack, &Track::slotRemoveLabel);
                    connect(copyDL, &DataLabel::sigCopyMe, pltTrack, &Track::slotCopyLabel);
                    connect(copyDL, &DataLabel::sigCopyMeToAnotherTrack, pltTrack, &Track::slotCopyLabelToAnotherTrack);
                    connect(copyDL, &DataLabel::sigMoveMe, pltTrack, &Track::slotMoveLabel);
                    //copyDL->setLabel_Xcoord(pltTrack->xAxis->pixelToCoord(pltTrack->mapFrom(this, mre->pos()).x()));
                    copyDL->setLabel_Xcoord(pltTrack->xAxis->pixelToCoord(mre->pos().x()));
                    pltTrack->labelsArray.append(copyDL);
                    pltTrack->replot();
                }

                if (ctrlPressed) mousePressedOnceAtCopyOrMove = true;
            }
        }
        else
        {
            copyLabelMode = false;
            qApp->restoreOverrideCursor();
        }
    }
    if (moveLabelMode)
    {
        if (mre->button() == Qt::LeftButton)
        {
            auto ctrlPressed = qApp->queryKeyboardModifiers().testFlag(Qt::ControlModifier);
            if (!ctrlPressed)
            {
                moveLabelMode = false;
                qApp->restoreOverrideCursor();
            }
//            else
            if (pltTrack != track_senderSignalsOfMoveOrCopy)
            {
                QMessageBox::critical(nullptr, tr("Error"),
                                         tr("You must move within the same track!"));
            }
            else
            {
                if (!ctrlPressed && mousePressedOnceAtCopyOrMove)
                {
                    mousePressedOnceAtCopyOrMove = false;
                }
                else
                {
                    //dl4move->setLabel_Xcoord(pltTrack->xAxis->pixelToCoord(pltTrack->mapFrom(this, mre->pos()).x()));
                    dl4move->setLabel_Xcoord(pltTrack->xAxis->pixelToCoord(mre->pos().x()));
                    pltTrack->replot();
                }

                if (ctrlPressed) mousePressedOnceAtCopyOrMove = true;
            }
        }
        else
        {
            moveLabelMode = false;
            qApp->restoreOverrideCursor();
        }
    }
    if (copyLabelToAnotherTrackMode)
    {
        if (mre->button() == Qt::LeftButton)
        {
            auto ctrlPressed = qApp->queryKeyboardModifiers().testFlag(Qt::ControlModifier);
            if (!ctrlPressed)
            {
                copyLabelToAnotherTrackMode = false;
                qApp->restoreOverrideCursor();
                trackidsInWhichCopyed4copyInAnotherTracks.clear();
            }
//            else
            if (pltTrack == track_senderSignalsOfMoveOrCopy)
            {
                QMessageBox::critical(nullptr, tr("Error"),
                                         tr("You must copy to another track!"));
            }
            else
            {
                if (!ctrlPressed && mousePressedOnceAtCopyOrMove)
                {
                    mousePressedOnceAtCopyOrMove = false;
                }
                else
                {
                    auto trackID = pltTrack->getTrackID();
                    if (!(trackidsInWhichCopyed4copyInAnotherTracks.size() && trackidsInWhichCopyed4copyInAnotherTracks.contains(trackID)))
                    {
                        auto newDL = dlCopiedToAnotherTrack->makeCopyInPlot(pltTrack);
                        connect(newDL, SIGNAL(sigGotoLabel(double)), pltTrack, SIGNAL(sigGoToLabel(double)));
                        connect(newDL, &DataLabel::sigRemoveMe, pltTrack, &Track::slotRemoveLabel);
                        connect(newDL, &DataLabel::sigCopyMe, pltTrack, &Track::slotCopyLabel);
                        connect(newDL, &DataLabel::sigCopyMeToAnotherTrack, pltTrack, &Track::slotCopyLabelToAnotherTrack);
                        connect(newDL, &DataLabel::sigMoveMe, pltTrack, &Track::slotMoveLabel);
                        pltTrack->labelsArray.append(newDL);
                        pltTrack->replot();
                        trackidsInWhichCopyed4copyInAnotherTracks << trackID;
                    }
                    else
                    {
                        QMessageBox::information(nullptr, tr("Info"),
                                                 tr("Label already copied here..."));
                    }
                }

                if (ctrlPressed) mousePressedOnceAtCopyOrMove = true;
            }
        }
        else
        {
            copyLabelToAnotherTrackMode = false;
            qApp->restoreOverrideCursor();
            trackidsInWhichCopyed4copyInAnotherTracks.clear();
        }
    }

    if (orientationMode == Qt::Horizontal)
    {
        pltTrack->axisRect()->setRangeZoom(Qt::Horizontal);
    }
    else
    {
        pltTrack->axisRect()->setRangeZoom(Qt::Vertical);
    }

    if (rightMouseButtonPressed && !valuesRescaledByMouseWheel)
    {
        QPoint posDiff = positionLast;
        posDiff.rx() -= positionFirst.x();
        posDiff.ry() -= positionFirst.y();

        if (posDiff.manhattanLength() < 2)
        {
            auto mnu = new QMenu(this);
            mnu->addActions(pltTrack->getTrackActionsList());
            mnu->popup(mapToGlobal(pltTrack->mapTo(this, mre->pos())));
        }
    }

    if (mre->button() == Qt::RightButton)
    {
//        Track *plt = qobject_cast<Track*>(sender());
//        if (plt->axisRect()->rangeZoom() == Qt::Horizontal)
//            plt->axisRect()->setRangeZoom(Qt::Vertical);
//        else
//            plt->axisRect()->setRangeZoom(Qt::Horizontal);
        rightMouseButtonPressed = false;

        //
    }

    if (mre->button() == Qt::LeftButton)
        leftMouseButtonPressed = false;

    valuesRescaledByMouseWheel = false;

    mre->accept();
}

void SpecialWidgetForPlot::slotMouseMove(QMouseEvent *mme)
{
    if (dimensionMode)
    {
        //positionLast = mme->pos();
        QPoint diff = mme->pos();
        diff.rx() -= positionFirst.x();
        diff.ry() -= positionFirst.y();
        if (dimensionProcess && diff.manhattanLength() > 3)
        {
            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? dimensionTrack->xAxis : dimensionTrack->yAxis);
            double firstDblCoordGraphX = abscissAxis->pixelToCoord(positionFirst.x());
            double lastDblCoordGraphX = abscissAxis->pixelToCoord(mme->pos().x());
            if (lastDblCoordGraphX > abscissAxis->range().upper)
            {
                lastDblCoordGraphX = abscissAxis->range().upper;
            }
            if (lastDblCoordGraphX < abscissAxis->range().lower)
            {
                lastDblCoordGraphX = abscissAxis->range().lower;
            }

            auto y_axis =
                    (orientationMode == Qt::Vertical ? dimensionTrack->xAxis : dimensionTrack->yAxis);
            double firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
            double lastDblCoordGraphY = y_axis->pixelToCoord(mme->pos().y());
            if (lastDblCoordGraphY > y_axis->range().upper)
            {
                lastDblCoordGraphY = y_axis->range().upper;
            }
            if (lastDblCoordGraphY < y_axis->range().lower)
            {
                lastDblCoordGraphY = y_axis->range().lower;
            }

            dimensionRect->topLeft->setCoords(firstDblCoordGraphX, firstDblCoordGraphY);
            dimensionRect->bottomRight->setCoords(lastDblCoordGraphX, lastDblCoordGraphY);

            dimensionStartPoint->position->setCoords(firstDblCoordGraphX, firstDblCoordGraphY);
            dimensionEndPoint->position->setCoords(lastDblCoordGraphX, lastDblCoordGraphY);

            positionLast = dimensionEndPoint->position->pixelPosition().toPoint();

//            dimensionRect->setVisible(true);
//            dimensionStartPoint->setVisible(true);
//            dimensionEndPoint->setVisible(true);

            dimensionTrack->replot();

            auto teDim = qobject_cast<QTextEdit*>(GceLogicLayer::getInstance()
                                              .getObjectFromObjectContainerByObjName(
                                                  ObjectsNames::objNameDialogDimensionModeTextEdit
                                                  ));
            teDim->clear();
            teDim->setTextColor(Qt::black);

            auto deltaSeconds = lastDblCoordGraphX - firstDblCoordGraphX;
            auto deltaIsNegative = deltaSeconds < 0.0;
            auto deltaSecInteger = std::abs(static_cast<int>(deltaSeconds));
            auto minutesPresent = deltaSecInteger > 60;
            auto hoursPresent = deltaSecInteger > 3600;
            auto daysPresent = deltaSecInteger > 86400;

            auto days = deltaSecInteger / 86400;
            auto hours = deltaSecInteger - days*86400;
            hours /= 3600;
            auto minutes = deltaSecInteger - days*86400 - hours*3600;
            minutes /= 60;

            auto onlySeconds = (deltaIsNegative ? -deltaSeconds : deltaSeconds);
            onlySeconds -= static_cast<double>(days*86400 + hours*3600 + minutes*60);

            QString delta("delta");
            QString deltaTimeStr = QString(delta).append("[")
                    .append(tr("Time")).append("]").append(" = ");
            QString dvalue;
            dvalue.setNum(deltaSeconds);
            deltaTimeStr.append(dvalue).append(tr(" seconds"));
            if (minutesPresent)
            {
                dvalue.setNum(onlySeconds);
                QString timeDHMS = QString(dvalue).append(tr(" seconds"));
                timeDHMS.prepend(QString::number(minutes).append(tr(" minutes, ")));
                if (hoursPresent)
                {
                    timeDHMS.prepend(QString::number(hours).append(tr(" hours, ")));
                    if (daysPresent)
                    {
                        timeDHMS.prepend(QString::number(days).append(tr(" days, ")));
                    }
                }
                if (deltaIsNegative)
                {
                    timeDHMS.prepend("-");
                }
                deltaTimeStr.append(" / ").append(timeDHMS);
            }

            teDim->insertPlainText(deltaTimeStr);

            for (GraphWithSettings *gws : dimensionTrack->graphicsArray)
            {
                teDim->textCursor().insertBlock();
                QString forHtmlPrepare = gws->nameGraph;
                forHtmlPrepare.prepend("[").prepend(delta).append("]").append(" = ");
                y_axis = gws->axisSpec;
                firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
                lastDblCoordGraphY = y_axis->pixelToCoord(positionLast.y());
                QString dblvalue;
                dblvalue.setNum(lastDblCoordGraphY - firstDblCoordGraphY);
                forHtmlPrepare.append(dblvalue);
                QString html = gws->currentPenColor.name(QColor::HexRgb);
                html.prepend("<font color=\"").append("\">").append(forHtmlPrepare).append("</font>");
                teDim->insertHtml(html);
            }
        }

        mme->accept();
        return;
    }

    if (scalingByRectangleMode)
    {
        QPoint diff = mme->pos();
        diff.rx() -= positionFirst.x();
        diff.ry() -= positionFirst.y();

        if (diff.manhattanLength() > 3)
        {
            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? scaledTrack->xAxis : scaledTrack->yAxis);
            double firstDblCoordGraphX = abscissAxis->pixelToCoord(positionFirst.x());
            double lastDblCoordGraphX = abscissAxis->pixelToCoord(mme->pos().x());
            if (lastDblCoordGraphX > abscissAxis->range().upper)
            {
                lastDblCoordGraphX = abscissAxis->range().upper;
            }
            if (lastDblCoordGraphX < abscissAxis->range().lower)
            {
                lastDblCoordGraphX = abscissAxis->range().lower;
            }

            auto y_axis =
                    (orientationMode == Qt::Vertical ? scaledTrack->xAxis : scaledTrack->yAxis);
            double firstDblCoordGraphY = y_axis->pixelToCoord(positionFirst.y());
            double lastDblCoordGraphY = y_axis->pixelToCoord(mme->pos().y());
            if (lastDblCoordGraphY > y_axis->range().upper)
            {
                lastDblCoordGraphY = y_axis->range().upper;
            }
            if (lastDblCoordGraphY < y_axis->range().lower)
            {
                lastDblCoordGraphY = y_axis->range().lower;
            }

            scalingRect->topLeft->setCoords(firstDblCoordGraphX, firstDblCoordGraphY);
            scalingRect->bottomRight->setCoords(lastDblCoordGraphX, lastDblCoordGraphY);

            positionLast = scalingRect->bottomRight->pixelPosition().toPoint();

            scaledTrack->replot();
        }

        mme->accept();
        return;
    }

    if (leftMouseButtonPressed && !viewGraphicsValuesMode)
    {
        int oldCoord, newCoord;
        if (orientationMode == Qt::Horizontal)
        {
            oldCoord = positionLast.x();
            newCoord = mme->pos().x();
        }
        else
        {
            oldCoord = positionLast.y();
            newCoord = mme->pos().y();
        }

//        for (int i = 0; i < tracksArray.size(); ++i)
//        {
//            auto abscissAxis = (orientationMode == Qt::Horizontal ? tracksArray[i]->xAxis : tracksArray[i]->yAxis);
//            double oldDblCoordGraph = abscissAxis->pixelToCoord(oldCoord);
//            double newDblCoordGraph = abscissAxis->pixelToCoord(newCoord);
//            auto diffCoordGraph = newDblCoordGraph - oldDblCoordGraph;
//            auto newMin = abscissAxis->range().lower - diffCoordGraph;
//            auto newMax = abscissAxis->range().upper - diffCoordGraph;
//            abscissAxis->setRange(newMin, newMax);
//            tracksArray[i]->replot();
//        }

        if (tracksArray.size())
        {
            auto abscissAxis =
                    (orientationMode == Qt::Horizontal ? tracksArray[0]->xAxis : tracksArray[0]->yAxis);
            double oldDblCoordGraph = abscissAxis->pixelToCoord(oldCoord);
            double newDblCoordGraph = abscissAxis->pixelToCoord(newCoord);
            auto diffCoordGraph = newDblCoordGraph - oldDblCoordGraph;
            auto newMin = abscissAxis->range().lower - diffCoordGraph;
            auto newMax = abscissAxis->range().upper - diffCoordGraph;

            slotRescaleXaxis(newMin, newMax);
            emit sigChangeTimeRangeForJoinedByTimePlots(newMin, newMax);
        }
    }
    else if (rightMouseButtonPressed)
    {
        int oldCoord, newCoord;
        auto track = qobject_cast<Track*>(sender());
        if (orientationMode == Qt::Vertical)
        {
            oldCoord = positionLast.x();
            newCoord = mme->pos().x();
        }
        else
        {
            oldCoord = positionLast.y();
            newCoord = mme->pos().y();
        }

        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto valueAxisTrack = gws->axisSpec;
            if (gws->frozenValuesScaleForAutoScale)
            {
                continue;
            }
            double oldDblCoordGraph = valueAxisTrack->pixelToCoord(oldCoord);
            double newDblCoordGraph = valueAxisTrack->pixelToCoord(newCoord);
            auto diffCoordGraph = newDblCoordGraph - oldDblCoordGraph;
            auto newMin = valueAxisTrack->range().lower - diffCoordGraph;
            auto newMax = valueAxisTrack->range().upper - diffCoordGraph;
            valueAxisTrack->setRange(newMin, newMax);
            gws->setScaleMaxValue(newMax);
            gws->setScaleMinValue(newMin);
            Track::autoRoundRangeValues(gws/*, true*/);
        }

        track->replot();
    }

    if (viewGraphicsValuesMode && !viewGraphicsValuesMode_Fixation/*&& (mme->pos() - positionLast).manhattanLength() > 2*/)
    {
        auto pltTrack = qobject_cast<Track*>(sender());
        setPosViewGraphicsValuesMode(pltTrack, mme);
    }

    mme->accept();
    positionLast = mme->pos();
}

void SpecialWidgetForPlot::slotMouseDoubleClick(QMouseEvent *mdce)
{
    if (qApp->property("ScalingByRectangleMode").isValid()
            && qApp->property("ScalingByRectangleMode").toBool())
    {
        return;
    }

    if (qApp->property("DimensionMode").isValid()
            && qApp->property("DimensionMode").toBool())
    {
        return;
    }

    if (viewGraphicsValuesModeInAnotherPlot)
    {
        return;
    }

    if (viewGraphicsValuesMode)
    {
        if (viewGraphicsValuesMode_Fixation)
        {
            viewGraphicsValuesMode_Fixation = false;
        }
        return;
    }

    auto pltTrack = qobject_cast<Track*>(sender());
    auto positionInPlotGlobal = mdce->globalPos();    
    auto positionInTrack = pltTrack->mapFromGlobal(positionInPlotGlobal);
    if (positionInTrack.x() < pltTrack->axisRect()->left())
    {
        return;
    }
    double coord = pltTrack->xAxis->pixelToCoord(positionInTrack.x());

    //

    viewGraphicsValuesMode = true;

    auto textEditValues = new QTextEdit;
    textEditValues->setObjectName(ObjectsNames::objNameTextEditForViewGraphicsValuesMode);
    GceLogicLayer::getInstance().getObjectsContainerPointer()->append(textEditValues);
    auto mainWndO = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameMainWindow
                );
    auto mainWnd = qobject_cast<MainWindow*>(mainWndO);
    auto dialogEV = new QDialog(mainWnd);
    auto flgs = dialogEV->windowFlags();
    flgs.setFlag(Qt::WindowStaysOnTopHint);
    dialogEV->setWindowFlags(flgs);
    dialogEV->setMinimumWidth(350);
    //dialogEV->setModal(true);
    connect(dialogEV, SIGNAL(accepted()), this, SLOT(slotStopViewValuesMode_CloseDialog()));
    connect(dialogEV, SIGNAL(rejected()), this, SLOT(slotStopViewValuesMode_CloseDialog()));

    //

    textEditValues->setReadOnly(true);
    //auto palette = textEditValues->palette();
    //palette.setColor(QPalette::Base, Qt::gray);
    //textEditValues->setPalette(palette);
    auto fnt = textEditValues->font();
    fnt.setPointSize(fnt.pointSize()+2);
    textEditValues->setFont(fnt);
    //auto btns = new QDialogButtonBox(QDialogButtonBox::Ok);
    auto lt = new QVBoxLayout;
    dialogEV->setLayout(lt);
    lt->addWidget(textEditValues, 7);
    //lt->addWidget(btns);
    //connect(btns, SIGNAL(accepted()), dialogEV, SLOT(accept()));

    textEditValues->textCursor().insertBlock();
    int64_t utc = qRound(coord);
    QString timeStr = tr("Time = ");
    QString dvl;
    dvl.setNum(coord);
    timeStr.append(dvl).append(tr(" seconds")).append("\n[~");
//    auto timeZone = qobject_cast<Project*>(pltTrack->getItem()->getProjectPointer())->timeZone;
//    timeStr.append(QDateTime::fromSecsSinceEpoch(utc, timeZone
//                                                 ).toString("dd.MM.yyyy hh:mm:ss")).append("]\n");
    timeStr.append(QDateTime::fromSecsSinceEpoch(utc, QTimeZone::utc()
                                                 ).toString("dd.MM.yyyy hh:mm:ss")).append("]\n");
    textEditValues->insertPlainText(timeStr);

    dialogEV->setWindowTitle(tr("Values for plot ").append(plotName));

    // создание/воссоздание линий:
    linesVectorPtrForViewGraphicsValuesMode.clear();
    positionsVectorPtrForViewGraphicsValuesMode.clear();
    for (Track *track : tracksArray)
    {
        auto lineInTrack = new QCPItemLine(track);
        lineInTrack->setClipToAxisRect(false);
        lineInTrack->start->setType(QCPItemPosition::ptAbsolute);
        lineInTrack->end->setType(QCPItemPosition::ptAbsolute);
        lineInTrack->start->setCoords(positionInTrack.x(), track->axisRect()->top());
        lineInTrack->end->setCoords(positionInTrack.x(), track->axisRect()->bottom());
        auto penLine = lineInTrack->pen();
        penLine.setWidth(2);
        penLine.setColor(Qt::black);
        lineInTrack->setPen(penLine);
        linesVectorPtrForViewGraphicsValuesMode << lineInTrack;
//        foreach (GraphWithSettings *gws, track->graphicsArray)
//        {
//            auto positionTracer = new QCPItemTracer(track);
//            positionTracer->setClipToAxisRect(false);
//            positionTracer->position->setType(QCPItemPosition::ptPlotCoords);
//            positionTracer->setStyle(QCPItemTracer::TracerStyle::tsSquare);
//            positionTracer->setBrush(gws->colorBrush);
//            positionTracer->setGraph(gws->graphic);
//            //positionTracer->setGraphKey();
//            positionsVectorPtrForViewGraphicsValuesMode << positionTracer;
//        }
    }

    setPosViewGraphicsValuesMode(pltTrack, mdce);

    emit sigActivatedViewValues(true);
    dialogEV->show();
    if (dialogAV_lastHeight != -1)
    {
        dialogEV->resize(dialogEV->width(), dialogAV_lastHeight);
    }
    QPoint pos;
    pos.setX(50);
    pos.setY(100);
    dialogEV->move(pos);
}

void SpecialWidgetForPlot::slotAddGraphToTrack(Curve *curve)
{
    auto track = qobject_cast<Track*>(sender());
    if (track == nullptr)
    {
        return;
    }
    auto idx = tracksArray.indexOf(track);

    auto mayLoad = true;
    QString devName;
    for (GraphWithSettings *graphWS : track->graphicsArray)
    {
        if (curve->getCurveName() == graphWS->nameGraph
                &&
                curve->getSourceID() == graphWS->sourceID)
        {
            mayLoad = false;
            devName = graphWS->deviceName;
            break;
        }
    }
    if (!mayLoad)
    {
        QMessageBox::critical(nullptr, tr("Error"),
                              tr("Can't add graphic with same name from same device to one track twice or more")
                              .append(tr("Device: ").prepend("\n")).append(devName).append("\n")
                              .append(tr("Graphic curve name: ").append(curve->getCurveName())));
        return;
    }

    connect(curve, SIGNAL(sigDeviceNameChanged(QString,QString)),
            this, SLOT(slotDeviceNameChanged(QString,QString)));

    uint32_t curveId = curve->getCurveID();
    uint32_t srcId = curve->getSourceID();
    auto loaded = false, ok = false;

    loaded = GceLogicLayer::getInstance().isLoggingCurveLoaded(srcId, curveId, ok);
    if (!ok) return;
    addGraphToTrack(curveId,
                    srcId,
                    idx,
                    loaded
                    /*curve->getItem()->type != TreeElement::ElementType::elementNotLoadedCurve*/
                    );
    //refreshHeadersOfTracks(); ////
    //updatePlots();
    auto graphicsPresentAndMayBeScaled = false; //!track->graphicsArray.isEmpty();
    for (Track *track_ : tracksArray)
    {
        if (!track_->graphicsArray.isEmpty())
        {
            graphicsPresentAndMayBeScaled = true;
            break;
        }
    }
    updatePlots(graphicsPresentAndMayBeScaled);
    showWholeTimesRange();
    track->specialAutoRescaleValuesAxes();
    track->replot();

    //emit sigChangeTimeRangeForJoinedByTimePlots();
}

void SpecialWidgetForPlot::slotResizedTrack()
{
    //refreshHeadersOfTracks(); ////
    recalcWgt2HeightForScrollBar();
    auto graphicsPresentAndMayBeScaled = false;
    for (Track *track_ : tracksArray)
    {
        if (!track_->graphicsArray.isEmpty())
        {
            graphicsPresentAndMayBeScaled = true;
            break;
        }
    }
    updatePlots(graphicsPresentAndMayBeScaled);
}

void SpecialWidgetForPlot::slotRescaleXaxis(double min, double max)
{
    if (tracksArray.size() == 0) return;
    trackTimeOnly->xAxis2->setRange(min, max);
    trackTimeOnly->replot();
    for (Track *track : tracksArray)
    {
        track->xAxis->setRange(min, max);
        track->replot();
    }
    //
}

void SpecialWidgetForPlot::slotDeviceNameChanged(QString oldName, QString newName)
{
    //
    for (decltype (plot_devicesNames.size()) idn = 0; idn < plot_devicesNames.size(); ++idn)
    {
        if (plot_devicesNames.at(idn) == oldName)
        {
            plot_devicesNames[idn] = newName;
            break;
        }
    }
    for (decltype (tracksArray.size()) itr = 0; itr < tracksArray.size(); ++itr)
    {
        for (decltype (tracksArray.at(itr)->graphicsArray.size()) igr = 0;
             igr < tracksArray.at(itr)->graphicsArray.size(); ++igr)
        {
            if (tracksArray.at(itr)->graphicsArray.at(igr)->deviceName == oldName)
            {
                tracksArray[itr]->graphicsArray[igr]->deviceName = newName;
            }
        }
    }
}

void SpecialWidgetForPlot::slotLoadDataForSelectedCurves()
{
    auto emptyTracksBeforeLoading = true;
    auto szTA = tracksArray.size();
    for (decltype (szTA) itr = 0; itr < szTA; ++itr)
    {
        if (emptyTracksBeforeLoading)
        {
            auto szGC = tracksArray.at(itr)->graphCount();
            for (decltype (szGC) igr = 0; igr < szGC; ++igr)
            {
                auto found = false;
                tracksArray.at(itr)->graph(igr)->getKeyRange(found);
                if (found)
                {
                    emptyTracksBeforeLoading = false;
                    break;
                }
            }
        }
        tracksArray[itr]->slotLoadDataForSelectedCurves();
        //tracksArray[itr]->specialAutoRescaleValuesAxes();
    }
    if (emptyTracksBeforeLoading)
    {
        showWholeTimesRange();
        updatePlots(true);
    }

    //refreshHeadersOfTracks();
}

void SpecialWidgetForPlot::slotSaveAsPDF()
{
    QStringList tmp = qobject_cast<Project*>(plot4wgt->getItem()->getProjectPointer())->getProjectHeaderPath().split("/");
    tmp.removeLast(); // получаем путь к каталогу проекта
    tmp.removeLast(); // и к надкаталогу

    QString fName = QFileDialog::getSaveFileName(this,
                                                 tr("Save to PDF"),
                                                 tmp.join("/"),
                                                 QString("PDF files (*.pdf)"));
    if (fName.isEmpty())
        return;
    QPrinter printer(QPrinter::ScreenResolution);
    printer.setOutputFileName(fName);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setColorMode(QPrinter::Color);
    printer.printEngine()->setProperty(QPrintEngine::PPK_Creator, QString("GM-CurveEdit"));
    printer.printEngine()->setProperty(QPrintEngine::PPK_DocumentName, plotName);
    QRect vp = wgt2->rect(); //, vp2 = vSplitter->rect();
    auto allHeight = lbl_TimeZone->height() + trackTimeOnly->height() + vSplitter->height();
    auto allHeightDbl = (double)allHeight;
    vp.setHeight(allHeight);
    QPageLayout pageLayout;
    pageLayout.setMode(QPageLayout::FullPageMode);
    pageLayout.setOrientation(QPageLayout::Portrait);
    pageLayout.setMargins(QMarginsF(0, 0, 0, 0));
    pageLayout.setPageSize(QPageSize(vp.size(), QPageSize::Point, QString(), QPageSize::ExactMatch));
    printer.setPageLayout(pageLayout);
    QCPPainter printpainter;

    if (printpainter.begin(&printer))
    {
      printpainter.setMode(QCPPainter::pmVectorized);
      printpainter.setMode(QCPPainter::pmNoCaching);
      printpainter.setMode(QCPPainter::pmNonCosmetic/*, exportPen==QCP::epNoCosmetic*/);
      printpainter.setWindow(vp);

      lbl_TimeZone->render(&printpainter);
      auto rvp = printpainter.viewport();
      double ratio = (double)lbl_TimeZone->height() / allHeightDbl;
      int32_t seg = (int)(ratio * (double) printpainter.viewport().height());
      rvp.moveTop(rvp.top() + seg);
      printpainter.setViewport(rvp);

      trackTimeOnly->drawCustomPlot(&printpainter);
      ratio = (double) trackTimeOnly->height() / allHeightDbl; //(wgt2->height());
      seg = (int)(ratio * (double) printpainter.viewport().height());
      rvp.moveTop(rvp.top() + seg);
      printpainter.setViewport(rvp);

      auto firstStep = true;
      //auto segmentHeight = printpainter.viewport().height() / tracksArray.size();
      QVector<int32_t> segmentsTrackHeights;
      for (Track *track : tracksArray)
      {
          ratio = (double) track->height() / allHeightDbl; //(wgt2->height());
          seg = (int)(ratio * (double) printpainter.viewport().height());
          segmentsTrackHeights << seg;
      }
      for (Track *track : tracksArray)
      {
          if (firstStep)
          {
              firstStep = false;
//              double ratio = (double) (lbl_TimeZone->height() + trackTimeOnly->height()) / allHeightDbl; //(wgt2->height());
//              int32_t seg = (int)(ratio * (double) printpainter.viewport().height());
//              //printpainter.viewport().moveTop(printpainter.viewport().top() + seg);
//              auto rectangleVP = printpainter.viewport();
//              rectangleVP.moveTop(rectangleVP.top() + seg);
//              printpainter.setViewport(rectangleVP);
          }
          else
          {
              auto rectangleVP = printpainter.viewport();
              auto nextSeg = segmentsTrackHeights.takeFirst();
              rectangleVP.moveTop(rectangleVP.top() + nextSeg);
              printpainter.setViewport(rectangleVP);
          }
          track->drawCustomPlot(&printpainter);
      }
      printpainter.end();
    }

    //QMessageBox::information(this, tr("Saved PDF file"), tr("PDF file saved to:\n").append(fName));
}

void SpecialWidgetForPlot::slotSaveAsBMP()
{
    QStringList tmp = qobject_cast<Project*>(plot4wgt->getItem()->getProjectPointer())->getProjectHeaderPath().split("/");
    tmp.removeLast(); // получаем путь к каталогу проекта
    tmp.removeLast(); // и к надкаталогу

    QString fName = QFileDialog::getSaveFileName(this, tr("Save to BMP"), tmp.join("/"),
                                                 QString("Bitmap files (*.bmp)") );
    if (fName.isEmpty())
        return;

    scrollBar->setValue(0);
    resetScroll();

    //QPixmap pmap(vSplitter->size());
    QRect vp = lbl_TimeZone->rect(); //, vp2 = vSplitter->rect();
    //auto deltaBWwidgets12 = trackTimeOnly->geometry().top() - lbl_TimeZone->geometry().bottom();
    //auto deltaBWwidgets23 = vSplitter->geometry().top() - trackTimeOnly->geometry().bottom();
    auto allHeight = lbl_TimeZone->height() +
            trackTimeOnly->height() + vSplitter->height(); // + deltaBWwidgets12 + deltaBWwidgets23;
    vp.setHeight(allHeight);
    QSize sz(vp.width(), vp.height());
    QPixmap pmap(sz);

    //lbl_TimeZone->render(&pmap);
    //vSplitter->render(&pmap);
    //wgt2->render(&pmap);
    lbl_TimeZone->render(&pmap);
    //deltaBWwidgets = trackTimeOnly->rect().top() - lbl_TimeZone->rect().bottom();
    auto dy = lbl_TimeZone->height(); // + deltaBWwidgets12;
    QPoint wpos(0, dy);
    trackTimeOnly->render(&pmap, wpos);
    dy += trackTimeOnly->height(); // + deltaBWwidgets23;
    wpos.setY(dy);
    vSplitter->render(&pmap, wpos);

    pmap.save(fName, "bmp", 100);

    //QMessageBox::information(this, tr("Saved BMP file"), tr("BMP file saved to:\n").append(fName));
}

void SpecialWidgetForPlot::slotAfterRemovingCurve()
{
    refreshDevicesLinkedToPlotAfterDeleteAnyTrackOrGraphic();
}

void SpecialWidgetForPlot::slotGoToLabel(double value)
{
    auto track1 = tracksArray.first();
    auto low = track1->xAxis->range().lower, high = track1->xAxis->range().upper;
    auto middle = 0.5 * (high + low);
    auto diff = value - middle;
    high += diff;
    low += diff;

    slotRescaleXaxis(low, high);
    emit sigChangeTimeRangeForJoinedByTimePlots(low, high);

    emit sigActivateMe();
}

void SpecialWidgetForPlot::slotChangeTimeZone(QTimeZone tZone)
{
    //lbl_TimeZone->setText(QString(tZone.id()));
    lbl_TimeZone->setTimeZoneText(QString(tZone.id()));
}

void SpecialWidgetForPlot::slotPlotRenamed(QString nm)
{
    plotName = nm;
    plot4wgt->getItem()->setNewNameElement(nm);
    emit sigPlotRenamed(nm);
}

void SpecialWidgetForPlot::slotRemovingTrack()
{
    auto track = qobject_cast<Track*>(sender());

    QString msg = tr("Do you really want to delete track ")
            .append(track->getItem()->getNameElement())
            .append(tr(" from plot "))
            .append(plot4wgt->getItem()->getNameElement())
            .append("?");

    auto result = QMessageBox::question(nullptr, tr("Track removing"), msg);
    if (result != QMessageBox::Yes)
    {
        return;
    }

    track->removeAllLabelsOnTrack();

    plot4wgt->getItem()->removeChild(track->getItem());
    tracksArray.removeOne(track);
    track->deleteLater();

    recalcWgt2HeightForScrollBar();
    resetScroll();

    refreshDevicesLinkedToPlotAfterDeleteAnyTrackOrGraphic();
}

void SpecialWidgetForPlot::slotRemovePlot()
{
    auto project = qobject_cast<Project*>(plot4wgt->getItem()->getProjectPointer());
    QString msg = tr("Do you really want to delete plot ")
            .append(plot4wgt->getItem()->getNameElement())
            .append("?");
    auto result = QMessageBox::question(nullptr, tr("Plot removing"), msg);
    if (result != QMessageBox::Yes)
    {
        return;
    }
    project->removeOnePlot(plot4wgt);
    emit sigPlotMustBeRemoved(plot4wgt);
}

void SpecialWidgetForPlot::slotSaveAsStandalonePlotTemplateFile()
{
    QString defaultDirPath = QString(qApp->applicationDirPath()).append("/user.templates");
    QDir defDir(defaultDirPath);
    if (!defDir.exists())
    {
        defDir.cdUp();
        defDir.mkdir("user.templates");
    }
    QString fName = QFileDialog::getSaveFileName(this,
                                                 tr("Save plot template to file"),
                                                 defaultDirPath,
                                                 QString(tr("Plot template (*.cetplt)")) );
    if (fName.isEmpty())
        return;
    if (fName.right(7) != ".cetplt")
    {
        fName.append(".cetplt");
    }
    QString notAllowedPath_FactoryTemplates =
            QString(qApp->applicationDirPath()).append("/templates");
    if (fName.contains(notAllowedPath_FactoryTemplates))
    {
        QMessageBox::critical(this, tr("Error"), tr("You can't save custom templates to folder"
                                                    " with factory templates:").append("\n")
                              .append(notAllowedPath_FactoryTemplates));
        return;
    }
    savingPreparedFileWithPlot(fName, true);
}

void SpecialWidgetForPlot::slotChangeLinkToDataOfDeviceInPlot()
{
    if (plot_devicesNames.isEmpty())
    {
        QMessageBox::information(nullptr, tr("Information"), tr("There is no links to any device in plot"));
        return;
    }

    auto project = qobject_cast<Project*>(plot4wgt->getItem()->getProjectPointer());
    if (project->getDevicesMapPtrInProject()->isEmpty())
    {
        QMessageBox::information(nullptr, tr("Information"), tr("There is no devices in project"));
        return;
    }

    auto dlgChangeLinkToDevice = new QDialog;

    dlgChangeLinkToDevice->setWindowTitle(tr("Change device in plot ").append(plot4wgt->getItem()->getNameElement()));

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    auto cmb_DevicesInPlot = new QComboBox,
            cmb_DevicesDataSources = new QComboBox;
    auto lt = new QVBoxLayout;
    auto ltCmbs = new QHBoxLayout;

    auto lblDevsInPlot = new QLabel(tr("Select device in plot: ")),
            lblVec = new QLabel(tr(" which will replaced by device from source: " ));

    ltCmbs->addWidget(lblDevsInPlot);
    ltCmbs->addWidget(cmb_DevicesInPlot);
    ltCmbs->addWidget(lblVec);
    ltCmbs->addWidget(cmb_DevicesDataSources);

    lt->addLayout(ltCmbs);
    lt->addWidget(btns);

    dlgChangeLinkToDevice->setLayout(lt);

    cmb_DevicesInPlot->addItem(QString());
    cmb_DevicesInPlot->addItems(plot_devicesNames.toList());

    cmb_DevicesDataSources->addItem(QString());
    for (Device *dev : project->getDevicesMapPtrInProject()->values())
    {
        auto devName_ = dev->getItem()->getNameElement();
        if (plot_devicesNames.contains(devName_)) // чтобы не было коллизий
        {
            continue;
        }
        cmb_DevicesDataSources->addItem(devName_);
    }

    connect(btns, SIGNAL(accepted()), dlgChangeLinkToDevice, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlgChangeLinkToDevice, SLOT(reject()));

    dlgChangeLinkToDevice->exec();

    if (dlgChangeLinkToDevice->result() == QDialog::Accepted)
    {
        QString oldDeviceName = cmb_DevicesInPlot->currentText();
        QString newDeviceName = cmb_DevicesDataSources->currentText();

        if (oldDeviceName.isEmpty() || newDeviceName.isEmpty())
        {
            QMessageBox::critical(nullptr, tr("Error"), tr("Both device in plot and other source device must be selected!"));
            return;
        }

        if (oldDeviceName == newDeviceName)
        {
            QMessageBox::critical(nullptr, tr("Error"), tr("You choose the same device. Nothing to do."));
            return;
        }

        Device *newDevice = nullptr;
        for (Device *dev : project->getDevicesMapPtrInProject()->values())
        {
            if (dev->getItem()->getNameElement() == newDeviceName)
            {
                newDevice = dev;
            }
        }

        if (newDevice == nullptr)
        {
            QMessageBox::critical(nullptr, tr("Error"), tr("Fault finding device to link!"));
            return;
        }

        QMap<GraphWithSettings*, Track*> graphsForDraw;
        for (decltype (tracksArray.size()) itr = 0; itr < tracksArray.size(); ++itr)
        {
            auto track = tracksArray.value(itr);
            QVector<int> indexesCurveItemsForDeletingFromLast;
            QStringList namesOfDeletingCurves;
            for (decltype (track->getItem()->childCount()) i_twi = 0; i_twi < track->getItem()->childCount(); ++i_twi)
            {
                TreeElement *element = static_cast<TreeElement*>(track->getItem()->child(i_twi));
                if (element != nullptr && element->getNameElement().split(" / ").at(1) == oldDeviceName)
                {
                    indexesCurveItemsForDeletingFromLast.prepend(i_twi);
                    namesOfDeletingCurves.append(element->getNameElement().split(" / ").at(0));
                }
            }
            while (indexesCurveItemsForDeletingFromLast.size())
            {
                QTreeWidgetItem *tw_item = track->getItem()->takeChild(indexesCurveItemsForDeletingFromLast.takeFirst());
                TreeElement *element = static_cast<TreeElement*>(tw_item);
                delete element;
            }

            for (decltype (track->graphicsArray.size()) i_graph = 0; i_graph < track->graphicsArray.size(); ++i_graph)
            {
                auto graphWS = track->graphicsArray.value(i_graph);
                if (graphWS->deviceName == oldDeviceName
                        && namesOfDeletingCurves.contains(graphWS->nameGraph))
                {
                    graphWS->loaded = false;
                    graphWS->deviceName = newDeviceName;
                    Curve *curve = nullptr;
                    auto found = false;
                    for (Curve *curve_ : newDevice->getCurvesMapPtr()->values())
                    {
                        if (curve_->getCurveName() == graphWS->nameGraph)
                        {
                            curve = curve_;
                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        graphWS->sourceID = curve->getSourceID();
                        graphWS->curveID = curve->getCurveID();
                        graphsForDraw.insert(graphWS, track);

                        curve->setCurveType_RAW();

                        auto newCurveInTrack4replace = track->copyCurveFromDeviceToTrack(curve);
                        connect(newCurveInTrack4replace, SIGNAL(sigDeviceNameChanged(QString,QString)),
                                this, SLOT(slotDeviceNameChanged(QString,QString)));
                    }
                    else
                    {
                        graphWS->sourceID = NULL_ID;
                        graphWS->curveID = NULL_ID;
                        graphWS->graphic->setData(QVector<double>(), QVector<double>());

                        track->makeCurveToTrackWithoutDevice(graphWS->nameGraph, graphWS->deviceName);
                    }
                }
            }
        }
        this->slotAfterRemovingCurve();

        QStringList grNames;
        // проверка наличия незагруженных кривых:
        for (GraphWithSettings *graphWS : graphsForDraw.keys())
        {
            auto oklc = false;
            auto loggingCurveLoaded =
                    GceLogicLayer::getInstance()
                    .isLoggingCurveLoaded(graphWS->sourceID, graphWS->curveID, oklc);
            if (oklc && !loggingCurveLoaded)
            {
                grNames.append(
                            GceLogicLayer::getInstance()
                            .getCurveStringIdByCurveId(graphWS->sourceID, graphWS->curveID, oklc)
                            );
            }
        }
        //emit GceLogicLayer::getInstance().sigOpenStartProgressBarWindow(tr("Loading data..."));

        QStringList ignoredCurveNames;
        if (!grNames.isEmpty())
        {
            GceLogicLayer::getInstance().loadGraphicsDataFromImportedFiles(newDevice->getSourceID(), grNames, ignoredCurveNames);
        }
        QStringList supportedCurves = GceLogicLayer::getInstance().getCurvesNamesForDataSource(newDevice->getSourceID());

        for (GraphWithSettings *graphWS : graphsForDraw.keys())
        {
            if (!supportedCurves.contains(graphWS->nameGraph))
            {
                graphWS->loaded = false;
                //graphsForDraw.value(graphWS)->removeGraph(graphWS->graphic);
                graphWS->graphic->setData(QVector<double>(), QVector<double>());
                continue;
            }
            graphWS->loaded = true;
            GceLogicLayer::getInstance().setExcludedTimesForNullValues(graphWS->sourceID, graphWS->curveID,
                                                                       graphWS->useNullValues, graphWS->nullValueIfUsed);
            //emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(graphWS->sourceID, graphWS->curveID);

//            auto ok = false;
//            auto x_n = GceLogicLayer::getInstance()
//                    .getValuesX(graphWS->sourceID, graphWS->curveID, ok);
//            if (ok)
//            {
//                auto y_n = GceLogicLayer::getInstance()
//                        .getValuesY(graphWS->sourceID, graphWS->curveID, ok);
//                if (ok)
//                {
//                    graphWS->graphic->setData(x_n, y_n);
//                }
//            }

//            if (!ok)
//            {
//                graphWS->loaded = false;
//                //graphsForDraw.value(graphWS)->removeGraph(graphWS->graphic);
//                graphWS->graphic->setData(QVector<double>(), QVector<double>());
//            }
        }

        //emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();
        this->updatePlots(true);
        this->showWholeTimesRange();
    }

    dlgChangeLinkToDevice->deleteLater();
}

void SpecialWidgetForPlot::slotScrolledVerticalScrollBar(int newValue)
{
    //auto scrBarVertical = qobject_cast<QScrollBar*>(sender());
//    auto movedPxls = newValue - lastScrollBarPositionValue;
//    auto mvpDbl = static_cast<double>(movedPxls);
//    auto recalcPixelsDbl = mvpDbl * static_cast<double>(wgt2->height()) / static_cast<double>(centerWgtstack->height());
//    auto recalcPixels = std::floor(recalcPixelsDbl);
//    //auto recalcPixels = movedPxls;
//    //scroll(0, -movedPxls);
    auto deltaRng = newValue - lastScrollBarPositionValue; //scrollBar->minimum();
//    auto allRng = scrollBar->maximum() - scrollBar->minimum() + scrollBar->pageStep();
//    auto recalcPixelsDbl = static_cast<double>(wgt2->height())
//            * static_cast<double>(deltaRng) /static_cast<double>(allRng);
//    auto recalcPixels = std::floor(recalcPixelsDbl);
    //deltaRng /= 2;
    //wgt2->scroll(0, -deltaRng);
    vSplitter->scroll(0, -deltaRng);
    lastScrollBarPositionValue = newValue;
}

void SpecialWidgetForPlot::slotStopViewValuesMode_CloseDialog()
{
    viewGraphicsValuesMode = false;
    viewGraphicsValuesMode_Fixation = false;

    dialogAV_lastHeight = qobject_cast<QDialog*>(sender())->height();

    auto obj = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameTextEditForViewGraphicsValuesMode);
    if (obj)
    {
        GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(obj/*textEditValues*/);
    }

    sender()->deleteLater();
    while (linesVectorPtrForViewGraphicsValuesMode.size())
    {
        auto line__ = linesVectorPtrForViewGraphicsValuesMode.takeFirst();
        auto plotTrack = line__->parentPlot();
        plotTrack->removeItem(line__);
        plotTrack->replot();
    }
    while (positionsVectorPtrForViewGraphicsValuesMode.size())
    {
        auto position__ = positionsVectorPtrForViewGraphicsValuesMode.takeFirst();
        auto plotTrack = position__->parentPlot();
        plotTrack->removeItem(position__);
        plotTrack->replot();
    }

    emit sigActivatedViewValues(false);
}

void SpecialWidgetForPlot::slotCopyLabel_(DataLabel *dlCopy)
{
    if (copyLabelMode || moveLabelMode || copyLabelToAnotherTrackMode)
        return;
    dl4copy = dlCopy;
    track_senderSignalsOfMoveOrCopy = qobject_cast<Track*>(sender());
    copyLabelMode = true;
    qApp->setOverrideCursor(QCursor(Qt::UpArrowCursor));
}

void SpecialWidgetForPlot::slotMoveLabel_(DataLabel *dl4Move)
{
    if (copyLabelMode || moveLabelMode || copyLabelToAnotherTrackMode)
        return;
    dl4move = dl4Move;
    track_senderSignalsOfMoveOrCopy = qobject_cast<Track*>(sender());
    moveLabelMode = true;
    qApp->setOverrideCursor(QCursor(Qt::UpArrowCursor));
}

void SpecialWidgetForPlot::slotCopyLabelToAnotherTrack_(DataLabel *dl4copy)
{
    if (copyLabelMode || moveLabelMode || copyLabelToAnotherTrackMode)
        return;
    dlCopiedToAnotherTrack = dl4copy;
    track_senderSignalsOfMoveOrCopy = qobject_cast<Track*>(sender());
    copyLabelToAnotherTrackMode = true;
    qApp->setOverrideCursor(QCursor(Qt::UpArrowCursor));
}

void SpecialWidgetForPlot::slotTrackHeightChanged(int newHeight)
{
    Q_UNUSED(newHeight)
    recalcWgt2HeightForScrollBar();
}

void SpecialWidgetForPlot::slotMovingTrackWithinPlot()
{
    auto movedTrack = qobject_cast<Track*>(sender());
    auto idxMovedTrack = tracksArray.indexOf(movedTrack);

    auto takedTrack = tracksArray.takeAt(idxMovedTrack);

    QHash<QString, Track* > otherTracks;
    QStringList variantsPlaces;
    for (Track *track : tracksArray)
    {
        QString nm = track->getItem()->getNameElement();
        otherTracks[nm] = track;
        variantsPlaces << QString(nm).prepend(tr("Before "));
    }

    variantsPlaces << tr("Move to end");

    QDialog *dlg = new QDialog;

    dlg->setMinimumWidth(400);

    dlg->setWindowTitle(tr("Select place for track"));
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    connect(btns, &QDialogButtonBox::accepted, dlg, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, dlg, &QDialog::reject);

    auto lt = new QHBoxLayout;
    auto cmbVariants = new QComboBox;
    cmbVariants->addItems(variantsPlaces);

    lt->addWidget(cmbVariants);
    lt->addWidget(btns);

    dlg->setLayout(lt);

    if (dlg->exec() == QDialog::Accepted)
    {
        //
        if (cmbVariants->currentText() == tr("Move to end"))
        {
            tracksArray.append(takedTrack);

            vSplitter->addWidget(takedTrack);

            plot4wgt->getItem()->takeChild(idxMovedTrack);

            plot4wgt->getItem()->addChild(takedTrack->getItem());
        }
        else
        {
            auto text = cmbVariants->currentText();
            text.remove(tr("Before "));

            auto choicedTrackBefore = otherTracks.value(text);
            auto idxBefore = tracksArray.indexOf(choicedTrackBefore);

            tracksArray.insert(idxBefore, takedTrack);

            vSplitter->insertWidget(idxBefore, takedTrack);

            plot4wgt->getItem()->takeChild(idxMovedTrack);

            plot4wgt->getItem()->insertSubElement(takedTrack->getItem(), idxBefore);
        }
    }
    else
    {
        tracksArray.insert(idxMovedTrack, takedTrack);
    }

    dlg->deleteLater();
}

void SpecialWidgetForPlot::slotForMoveCurveFromTrackToTrack(Curve *curveForMove)
{
    auto trackSource = qobject_cast<Track*>(sender());

    QHash<int, QString> probDestTracksNames;
    for (Track *track : tracksArray)
    {
        if (track == trackSource) continue;
        probDestTracksNames.insert(tracksArray.indexOf(track), track->getItem()->getNameElement());
    }

    QStringList tracksNames = probDestTracksNames.values();
    qSort(tracksNames);

    auto dialog = new QDialog;
    dialog->setWindowTitle(tr("Select track for placing moved curve"));
    dialog->setMinimumWidth(350);
    auto lt = new QHBoxLayout;
    auto cmbTracks = new QComboBox;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    dialog->setLayout(lt);
    lt->addWidget(cmbTracks);
    lt->addWidget(btns);
    connect(btns, &QDialogButtonBox::accepted, dialog, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, dialog, &QDialog::reject);

    cmbTracks->addItems(tracksNames);

    if (dialog->exec() == QDialog::Accepted)
    {
        auto trackDestination = tracksArray.at(probDestTracksNames.key(cmbTracks->currentText()));
        auto found = false;
        auto idxInTrackSource = 0;
        for (GraphWithSettings *gws : trackSource->graphicsArray)
        {
            idxInTrackSource = trackSource->graphicsArray.indexOf(gws);
            if (gws->curveID == curveForMove->getCurveID()
                    && gws->sourceID == curveForMove->getSourceID())
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            auto gws = trackSource->graphicsArray.takeAt(idxInTrackSource);

            GraphWithSettings *graphWSNew = trackDestination->addGraphicWithSettings();
            graphWSNew->divisionValueForAxis = gws->axisSpec->range().size() / trackDestination->yAxis->range().size();
            graphWSNew->divisionValueIsFixed = false;
            graphWSNew->useNullValues = gws->useNullValues;
            graphWSNew->frozenValuesScaleForAutoScale = gws->frozenValuesScaleForAutoScale;
            graphWSNew->nullValueIfUsed = gws->nullValueIfUsed;
            graphWSNew->nameGraph = gws->nameGraph;
            graphWSNew->pseudoNameGraph = gws->pseudoNameGraph;
            graphWSNew->scaleMaximumValue = gws->scaleMaximumValue;
            graphWSNew->scaleMinimumValue = gws->scaleMinimumValue;
            graphWSNew->tracerForArrowUpper = new QCPItemTracer(trackDestination);
            graphWSNew->tracerForArrowMiddle = new QCPItemTracer(trackDestination);
            graphWSNew->tracerForArrowLower = new QCPItemTracer(trackDestination);
            graphWSNew->tracerForArrowUpper->setVisible(false);
            graphWSNew->tracerForArrowLower->setVisible(false);
            graphWSNew->tracerForArrowMiddle->setVisible(false);
            graphWSNew->tracerForArrowUpper->position->setType(QCPItemPosition::ptAbsolute);
            graphWSNew->tracerForArrowLower->position->setType(QCPItemPosition::ptAbsolute);
            graphWSNew->tracerForArrowMiddle->position->setType(QCPItemPosition::ptAbsolute);
            QPoint posTopLeft = trackDestination->axisRect()->topLeft(),
                    posBottomLeft = trackDestination->axisRect()->bottomLeft();
            posTopLeft.setX(posTopLeft.x() - trackDestination->graphicsArray.count()*TRACK_HEADER_WIDTH);
            posBottomLeft.setX(posTopLeft.x());
            graphWSNew->tracerForArrowUpper->position->setCoords(posTopLeft.x(), posTopLeft.y());
            graphWSNew->tracerForArrowLower->position->setCoords(posBottomLeft.x(), posBottomLeft.y());
            graphWSNew->tracerForArrowMiddle->position->setCoords(posTopLeft.x(), (posTopLeft.y()+posBottomLeft.y())/2);

            graphWSNew->arrow = new QCPItemLine(trackDestination);
            graphWSNew->arrow->setClipToAxisRect(false);
            graphWSNew->arrow->start->setParentAnchor(graphWSNew->tracerForArrowLower->position);
            graphWSNew->arrow->end->setParentAnchor(graphWSNew->tracerForArrowUpper->position);
            graphWSNew->arrow->start->setCoords(0, 0);
            graphWSNew->arrow->end->setCoords(0, 0);
            graphWSNew->lMaximum = new QCPItemText(trackDestination);
            graphWSNew->lMaximum->setClipToAxisRect(false);
            graphWSNew->lMaximum->position->setParentAnchor(graphWSNew->tracerForArrowUpper->position);
            graphWSNew->lMaximum->position->setCoords(/*-6*/6, -1);
            graphWSNew->lMaximum->setText(gws->lMaximum->text());
            graphWSNew->lMaximum->setTextAlignment(Qt::AlignRight);
            graphWSNew->lMaximum->setPositionAlignment(Qt::AlignRight | Qt::AlignBottom);
            graphWSNew->lMaximum->setRotation(-90.0);
            graphWSNew->lMinimum = new QCPItemText(trackDestination);
            graphWSNew->lMinimum->setClipToAxisRect(false);
            graphWSNew->lMinimum->position->setParentAnchor(graphWSNew->tracerForArrowLower->position);
            graphWSNew->lMinimum->position->setCoords(/*-6*/6, 1);
            graphWSNew->lMinimum->setText(gws->lMinimum->text());
            graphWSNew->lMinimum->setTextAlignment(Qt::AlignLeft);
            graphWSNew->lMinimum->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
            graphWSNew->lMinimum->setRotation(-90.0);
            graphWSNew->labelNameGraphic = new QCPItemText(trackDestination);
            graphWSNew->labelNameGraphic->setClipToAxisRect(false);
            graphWSNew->updateLabelGraphicName();
            graphWSNew->labelNameGraphic->position->setParentAnchor(graphWSNew->tracerForArrowMiddle->position);
            graphWSNew->labelNameGraphic->position->setCoords(-12, 0);
            graphWSNew->labelNameGraphic->setRotation(-90.0);

            auto fnt = graphWSNew->labelNameGraphic->font();
            fnt.setPointSize(gws->labelNameGraphic->font().pointSize());
            graphWSNew->labelNameGraphic->setFont(fnt);
            fnt = graphWSNew->lMaximum->font();
            fnt.setPointSize(gws->lMaximum->font().pointSize());
            graphWSNew->lMaximum->setFont(fnt);
            graphWSNew->lMinimum->setFont(fnt);

            graphWSNew->setScaleMaxValue(gws->scaleMaximumValue);
            graphWSNew->setScaleMinValue(gws->scaleMinimumValue);

            graphWSNew->loaded = gws->loaded;
            graphWSNew->curveID = gws->curveID;
            graphWSNew->sourceID = gws->sourceID;
            graphWSNew->deviceName = gws->deviceName;

            graphWSNew->currentPenColor = gws->currentPenColor;
            graphWSNew->colorBrush = gws->colorBrush;
            graphWSNew->colorPen = gws->colorPen;
            graphWSNew->graphic->setPen(graphWSNew->colorPen);

            auto pen = graphWSNew->colorPen;
            pen.setWidth(2.0);
            graphWSNew->arrow->setPen(pen);

            graphWSNew->graphic->setData(gws->graphic->data());

            graphWSNew->lMaximum->setColor(graphWSNew->currentPenColor);
            graphWSNew->lMinimum->setColor(graphWSNew->currentPenColor);
            graphWSNew->lMaximum->setBrush(QBrush(Qt::white));
            graphWSNew->lMinimum->setBrush(QBrush(Qt::white));

            trackDestination->graphicsArray.append(graphWSNew);

            trackDestination->copyCurveFromDeviceToTrack(curveForMove);

            trackSource->deleteCurveFromTrack(curveForMove, gws);
        }
    }

    dialog->deleteLater();
}

void SpecialWidgetForPlot::slotChangeFontUtcHeaderSize(int fontSzNew)
{
    //
    auto fnt = lbl_TimeZone->font();
    fnt.setPixelSize(fontSzNew);
    lbl_TimeZone->setFont(fnt);
    lbl_TimeZone->setFixedHeight(fontSzNew);
}

void SpecialWidgetForPlot::slotChangeFontGraphicsHeaderNamesSize(int fontSzNew)
{
    for (Track* track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto fnt = gws->labelNameGraphic->font();
            fnt.setPointSize(fontSzNew);
            gws->labelNameGraphic->setFont(fnt);
        }
        track->replot();
    }
}

void SpecialWidgetForPlot::slotChangeFontGraphicsHeaderMinMaxSize(int fontSzNew)
{
    for (Track* track : tracksArray)
    {
        for (GraphWithSettings *gws : track->graphicsArray)
        {
            auto fnt = gws->lMinimum->font();
            fnt.setPointSize(fontSzNew);
            gws->lMinimum->setFont(fnt);
            gws->lMaximum->setFont(fnt);
        }
        track->replot();
    }
}

//void SpecialWidgetForPlot::slotRedrawGraphs()
//{
//    auto track = qobject_cast<Track*>(sender());
//    if (track == nullptr)
//    {
//        return;
//    }
//    for (int igr = 0; igr < track->graphicsArray.size(); ++igr)
//    {
//        GraphWithSettings *gws = track->graphicsArray[igr];
//        auto ok = false;
//        auto curve = GceLogicLayer::getInstance().getLoggingCurve(gws->sourceID, gws->curveID, ok);
//        if (ok && curve->loaded)
//        {
//            gws->loaded = true;
//            gws->graphic->setData(
//                        GceLogicLayer::getInstance().getValuesX(gws->sourceID, gws->curveID, ok),
//                        GceLogicLayer::getInstance().getValuesY(gws->sourceID, gws->curveID, ok)
//                        );
//        }
//    }
//}
