<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AppManager</name>
    <message>
        <location filename="appmanager.cpp" line="93"/>
        <source>Configuration file is unaccessible.</source>
        <translation>Конфигурационный файл недоступен.</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="112"/>
        <source>Update links in configuration file are empty. It may be damaged.</source>
        <oldsource>Update links in configuration file sre empty. It may be damaged.</oldsource>
        <translation>Ссылки обновлений в файле конфигурации пусты. Возможно, этот файл поврежден.</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="124"/>
        <source>Not connected to update server.</source>
        <translation>Нет соединения с сервером обновлений.</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="131"/>
        <source>Update data on server is unreadable.</source>
        <translation>Данные обновлений на сервере не удается прочитать.</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="143"/>
        <source>Update instructions file not exist!</source>
        <translation>Отсутствует файл инструкций для программы обновления!</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="220"/>
        <source>Warning: non-latin symbols exist in path</source>
        <translation>Внимание: нелатинские символы присутствуют в пути</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="221"/>
        <source>There are cyrillic symbols in program path, so only components will update.
If path contains only latin symbols, then program can update too.</source>
        <translation>Символы кириллицы присутсвуют в пути к программе, поэтому будут обновляться только компоненты.
Если путь содержит только латиснкие символы, программные файлы тоже смогут обновиться.</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="226"/>
        <location filename="appmanager.cpp" line="341"/>
        <source>Update</source>
        <translation>Обновление</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="226"/>
        <source>Program may be updated. Do you want to update now?</source>
        <translation>Программа может быть обновлена. Хотите обновить её сейчас?</translation>
    </message>
    <message>
        <location filename="appmanager.cpp" line="415"/>
        <source>Please wait: searching of the update server...</source>
        <translation>Пожалуйста, подождите: идёт поиск сервера обновлений...</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="cfg.cpp" line="232"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="cfg.cpp" line="232"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
</context>
<context>
    <name>DataLabel</name>
    <message>
        <location filename="datalabel.cpp" line="439"/>
        <source>Change label text</source>
        <translation>Изменить текст метки</translation>
    </message>
    <message>
        <location filename="datalabel.cpp" line="460"/>
        <source>Offset from top, pixels</source>
        <translation>Смещение от верха, пиксели</translation>
    </message>
    <message>
        <location filename="datalabel.cpp" line="475"/>
        <source>Change label properties</source>
        <translation>Изменение свойств метки</translation>
    </message>
</context>
<context>
    <name>Device</name>
    <message>
        <location filename="elements/device.cpp" line="42"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="44"/>
        <source>Device info</source>
        <translation>Информация об устройстве</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="47"/>
        <source>Rename device</source>
        <translation>Переименовать устройство</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="50"/>
        <source>Delete device</source>
        <translation>Удалить устройство</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="53"/>
        <source>Set device time shift</source>
        <translation>Настройка смещения времени для устройства</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="56"/>
        <source>Add plot</source>
        <translation>Добавить планшет</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="122"/>
        <source>Data source file name:</source>
        <translation>Имя файла источника данных:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="123"/>
        <source>Data source file type:</source>
        <translation>Тип файла источника данных:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="124"/>
        <source>Data source file path:</source>
        <translation>Путь к источнику данных:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="125"/>
        <source>Date and time of data source&apos;s import:</source>
        <translation>Дата и время и дата импорта источника данных:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="126"/>
        <source>Additional info:</source>
        <translation>Дополнительная информация:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="167"/>
        <source>Display device&apos;s data source properties</source>
        <translation>Показать свойства устройств-источников данных</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="195"/>
        <source>Forward time shift?</source>
        <translation>Смещение времени вперёд?</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="200"/>
        <source>Days:</source>
        <translation>Дни:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="202"/>
        <source>Hours:</source>
        <translation>Часы:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="204"/>
        <source>Minutes:</source>
        <translation>Минуты:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="206"/>
        <source>Seconds:</source>
        <translation>Секунды:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="208"/>
        <source>Milliseconds:</source>
        <translation>Миллисекунды:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="218"/>
        <source>Device data time set</source>
        <translation>Настройка времени данных устройства</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="322"/>
        <source>You may choose device name from list:</source>
        <translation>Вы можете выбрать имя устройства из списка:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="324"/>
        <source>or you may change it manually:</source>
        <translation>или вы можете изменить его вручную:</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="348"/>
        <source>Renaming device ...</source>
        <translation>Переименование устройства ...</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="391"/>
        <location filename="elements/device.cpp" line="399"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="392"/>
        <source>Device with the same name is present in devices list. Please, run this dialog again and input different name.</source>
        <translation>Устройство с тем же именем присутствует в списке устройств. Пожалуйста, перезапустите диалог переименования вновь и введите иное имя.</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="393"/>
        <source>
(Context menu &quot;Rename device&quot; of item in devices list)</source>
        <translation>(Контекстное меню &quot;Переименование устройства&quot; элемента списка устройств)</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="400"/>
        <source>Device name can&apos;t be empty</source>
        <translation>Имя устройства не может быть пустым</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="431"/>
        <source>Do you really want to delete device </source>
        <translation>Вы действительно хотите удалить устройство </translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="433"/>
        <source> from project?</source>
        <translation> из проекта?</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="434"/>
        <source>This action is irreversible!</source>
        <translation>Это действие необратимо!</translation>
    </message>
    <message>
        <location filename="elements/device.cpp" line="436"/>
        <source>Device delete</source>
        <translation>Удаление устройства</translation>
    </message>
</context>
<context>
    <name>DevicesDatas</name>
    <message>
        <location filename="elements/devicesdatas.cpp" line="16"/>
        <source>Devices data</source>
        <translation>Данные устройств</translation>
    </message>
    <message>
        <location filename="elements/devicesdatas.cpp" line="17"/>
        <source>Add device (import data)</source>
        <translation>Добавить устройство (импорт данных)</translation>
    </message>
</context>
<context>
    <name>GceDataView</name>
    <message>
        <location filename="gcedataview.cpp" line="210"/>
        <source>Wrong file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1103"/>
        <source>Open plot template from file</source>
        <translation>Открыть шаблон планшета из файла</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1103"/>
        <source>Plot template (*.gmcetplt)</source>
        <translation>Шаблон планшета (*.gmcetplt)</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1401"/>
        <source>Drawing/redrawing graphics...</source>
        <translation>Отрисовка/перерисовка графиков...</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1523"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1524"/>
        <source>It&apos;s neccessary two or more plots for this action</source>
        <translation>Необходимо два или более планшета для завершения действия</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1530"/>
        <source>Select all</source>
        <translation>Выбрать все</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1531"/>
        <source>Deselect all</source>
        <translation>Отменить выбор всех</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1563"/>
        <source>Join plots by time</source>
        <translation>Связать планшеты по времени</translation>
    </message>
    <message>
        <source>It&apos;s neccessary two or more selected plots for join</source>
        <translation type="vanished">Необходимо выбрать два или более планшета для связывания</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="200"/>
        <location filename="gcedataview.cpp" line="210"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="200"/>
        <source>File can&apos;t be read</source>
        <translation>Файл не может быть прочитан</translation>
    </message>
</context>
<context>
    <name>GceImportModule</name>
    <message>
        <location filename="../gceImportModule/gceimportmodule.cpp" line="51"/>
        <source>Select date for data</source>
        <translation>Выберите дату для данных</translation>
    </message>
    <message>
        <location filename="../gceImportModule/gceimportmodule.cpp" line="76"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>GceLogicLayer</name>
    <message>
        <source>Load all compatible files from folder</source>
        <translation type="vanished">Загрузить все совместимые файлы из каталога</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="434"/>
        <source>Incorrect file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="856"/>
        <source>Loading data ...</source>
        <translation>Загрузка данных ...</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1072"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1101"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1133"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1140"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1149"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1271"/>
        <source>Project data is damaged</source>
        <translation>Данные проекта повреждены</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1124"/>
        <source>Project data may be incorrect: source ID checking fall</source>
        <translation>Данные проекта могут быть некорректны: проверка идентификатора источника данных не прошла успешно</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1279"/>
        <source>Imported files metadata file not found: _HEAD_
</source>
        <translation>Не найден файл метаданных импортированных файлов: _HEAD_
</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1284"/>
        <source>Imported files metadata file can&apos;t read: _HEAD_
</source>
        <translation>Не читается файл метаданных импортированных файлов: _HEAD_
</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1313"/>
        <source>Imported file data file can&apos;t read: </source>
        <translation>Файл данных импортированного файла не читается: </translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1346"/>
        <source>Imported files metadata file corrupt: _HEAD_
</source>
        <translation>Повержден файл метаданныхимпортированных файлов: _HEAD_
</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1365"/>
        <location filename="../gceLogicLayer/gcelogiclayer.cpp" line="1394"/>
        <source>There is not any import module
</source>
        <translation>Отсутствуют модули импорта
</translation>
    </message>
    <message>
        <source>File not found: _HEAD_</source>
        <translation type="vanished">Файл не найден:  _HEAD_</translation>
    </message>
    <message>
        <source>There is not any import module</source>
        <translation type="vanished">Не найдено ни обного модуля импорта</translation>
    </message>
</context>
<context>
    <name>HelpBaseForm</name>
    <message>
        <location filename="helpbaseform.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../gceImportModule/importdialog.cpp" line="26"/>
        <source>Import</source>
        <translation>Импорт</translation>
    </message>
    <message>
        <location filename="../gceImportModule/importdialog.cpp" line="68"/>
        <source>Load all compatible files from folder</source>
        <translation>Загрузить все совместимые файлы из каталога</translation>
    </message>
    <message>
        <location filename="../gceImportModule/importdialog.cpp" line="70"/>
        <source>Select folder with data files</source>
        <translation>Выбор каталога с файлами данных</translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="elements/label.cpp" line="13"/>
        <source>Change label text</source>
        <translation>Изменить текст метки</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="14"/>
        <source>Go to label</source>
        <translation>Перейти к метке</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="19"/>
        <source>Delete label</source>
        <translation>Удалить метку</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="20"/>
        <source>Move label</source>
        <translation>Переместить метку</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="21"/>
        <source>Copy label</source>
        <translation>Копировать метку</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="22"/>
        <source>Copy label to another track</source>
        <translation>Копировать метку на другой трек</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="23"/>
        <source>Edit label properties</source>
        <translation>Изменить свойства метки</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="31"/>
        <location filename="elements/label.cpp" line="121"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="elements/label.cpp" line="119"/>
        <source>Track: </source>
        <translation>Трек: </translation>
    </message>
</context>
<context>
    <name>ListLabels</name>
    <message>
        <location filename="elements/listlabels.cpp" line="15"/>
        <source>Labels</source>
        <translation>Метки</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="16"/>
        <source>Hide labels</source>
        <translation>Скрыть метки</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="17"/>
        <source>Show labels</source>
        <translation>Отобразить метки</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="18"/>
        <source>Hide labels texts</source>
        <translation>Скрыть тексты меток</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="19"/>
        <source>Show labels texts</source>
        <translation>Отобразить тексты меток</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="21"/>
        <source>Change font size</source>
        <translation>Изменить размер шрифта</translation>
    </message>
    <message>
        <location filename="elements/listlabels.cpp" line="154"/>
        <source>Change font size for labels</source>
        <translation>Изменение размера шрифта для меток</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="25"/>
        <source>Show toolbar</source>
        <translation>Показать панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <source>Load/refresh data</source>
        <translation>Загрузить/обновить данные</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>Show/hide dots in all graphics</source>
        <translation>Показать/скрыть точки на всех графиках</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>Show whole time range</source>
        <translation>Показать весь диапазон времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Autorescale values axes</source>
        <translation>Автомасштаб осей значений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="71"/>
        <source>Autoshift values axes</source>
        <translation>Автосдвиг осей значений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="76"/>
        <source>Scaling by rectangle</source>
        <translation>Масштабировать по прямоугольнику</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="82"/>
        <source>Save current plot to PDF</source>
        <translation>Сохранить активный планшет в PDF</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>Save current plot to BMP</source>
        <translation>Сохранить активный планшет в BMP</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="93"/>
        <source>Add label</source>
        <translation>Добавить метку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <location filename="mainwindow.cpp" line="682"/>
        <source>Dimension mode</source>
        <translation>Режим измерения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <source>Session</source>
        <translation>Сессия</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="121"/>
        <source>Recent projects</source>
        <translation>Недавние проекты</translation>
    </message>
    <message>
        <source>Program settings</source>
        <translation type="vanished">Настройки программы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Project</source>
        <translation>Проект</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="132"/>
        <source>Actions</source>
        <translation>Действия</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Program parameters</source>
        <translation>Параметры программы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>About program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>About development environment</source>
        <oldsource>About development</oldsource>
        <translation>О среде разработки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="163"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>Show project tree</source>
        <translation>Показать дерево проекта</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Save project on close</source>
        <translation>Сохранение проекта при выходе</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="303"/>
        <source>Do you want to save changes?</source>
        <translation>Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="418"/>
        <source>(empty)</source>
        <translation>(пусто)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>Opened/created project exist</source>
        <translation>Имеется созданный/открытый проект</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>You must close current project to open another</source>
        <translation>Вы должны сначала закрыть текущий проект, чтоб открыть другой</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>Project not exist!</source>
        <translation>Проект не существует!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="454"/>
        <source>Language of program: </source>
        <translation>Язык программы: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="464"/>
        <source>Delete nonmonotonic time intervals</source>
        <translation>Удалять немонотонные по времени интервалы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="469"/>
        <source> - maximum period for nonmonotonic interval, seconds: </source>
        <oldsource> - maximum period for nonmonotonic interval: </oldsource>
        <translation> - максимальный период немонотонного интервала, секунды: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Count of recent projects: </source>
        <translation>Число недавних проектов: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="497"/>
        <source>Default track height, pixels: </source>
        <translation>Высота трека по умолчанию, пиксели: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Load data automatically then add graphics to track</source>
        <translation>При добавлении графика на трек загружать данные точек автоматически</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Set default values</source>
        <translation>Установить значения по умолчанию</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>Need to restart application</source>
        <oldsource>Need to restart</oldsource>
        <translation>Необходим перезапуск приложения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>Please restart to changes take effect</source>
        <translation>Пожалуйста, перезапустите приложение для применения настроек</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <source>About </source>
        <translation>О программе </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="625"/>
        <source>Build date: </source>
        <translation>Дата сборки: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="626"/>
        <source>Build number: </source>
        <translation>Номер сборки: </translation>
    </message>
    <message>
        <source>Developed by OOO NPO &quot;GeoMash&quot;</source>
        <translation type="vanished">Разработано в ООО НПО &quot;ГеоМаш&quot;</translation>
    </message>
</context>
<context>
    <name>Plot</name>
    <message>
        <location filename="elements/plot.cpp" line="18"/>
        <source>Plot</source>
        <translation>Планшет</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="20"/>
        <source>Add track</source>
        <translation>Добавить трек</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="21"/>
        <source>Swap X and Y axis of tracks</source>
        <translation></translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="22"/>
        <source>Select curves for plot</source>
        <translation>Выбрать кривые для планшета</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="23"/>
        <source>Load data of selected curves from source</source>
        <translation>Загрузить данные выбранных кривых из источника(ов)</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="24"/>
        <source>Rename plot</source>
        <oldsource>Change plot name</oldsource>
        <translation>Переименовать планшет</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="25"/>
        <source>Save plot as PDF file</source>
        <translation>Сохранить планшет как файл PDF</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="26"/>
        <source>Save plot as BMP file</source>
        <oldsource>Save plot as Bitmap file</oldsource>
        <translation>Сохранить планшет как файл BMP</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="27"/>
        <source>Delete plot</source>
        <translation>Удалить планшет</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="28"/>
        <source>Save plot as plot template</source>
        <oldsource>Save plot template as standalone file</oldsource>
        <translation>Сохранить как шаблон планшета</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="29"/>
        <source>Change linked data for device in plot</source>
        <translation>Изменить привязку к данным для устройства в планшете</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="30"/>
        <source>Edit plot header string</source>
        <translation>Ввести строку заголовка планшета</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="84"/>
        <source>Renaming plot ...</source>
        <translation>Переименование планшета ...</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="91"/>
        <location filename="elements/plot.cpp" line="105"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="92"/>
        <source>Device name can&apos;t be empty</source>
        <translation>Имя устройства не может быть пустым</translation>
    </message>
    <message>
        <location filename="elements/plot.cpp" line="106"/>
        <source>There is plot with the same name!</source>
        <translation>Уже существует планшет с тем же именем!</translation>
    </message>
</context>
<context>
    <name>Plots</name>
    <message>
        <location filename="elements/plots.cpp" line="8"/>
        <source>Plots</source>
        <translation>Планшеты</translation>
    </message>
    <message>
        <location filename="elements/plots.cpp" line="11"/>
        <source>Add empty plot</source>
        <oldsource>Add plot to project</oldsource>
        <translation>Добавить пустой планшет</translation>
    </message>
    <message>
        <location filename="elements/plots.cpp" line="14"/>
        <source>Add plot</source>
        <translation>Добавить планшет</translation>
    </message>
    <message>
        <location filename="elements/plots.cpp" line="17"/>
        <source>Join plots by time</source>
        <translation>Связать планшеты по времени</translation>
    </message>
    <message>
        <source>Add plot from template</source>
        <oldsource>Open plot template from standalone file</oldsource>
        <translation type="vanished">Добавить планшет из шаблона</translation>
    </message>
</context>
<context>
    <name>Project</name>
    <message>
        <location filename="project.cpp" line="41"/>
        <location filename="project.cpp" line="1342"/>
        <source>Save project</source>
        <oldsource>Save poject</oldsource>
        <translation>Сохранить проект</translation>
    </message>
    <message>
        <location filename="project.cpp" line="43"/>
        <source>Save project copy to</source>
        <translation>Сохранить копию проекта в</translation>
    </message>
    <message>
        <location filename="project.cpp" line="44"/>
        <source>Close project</source>
        <translation>Закрыть проект</translation>
    </message>
    <message>
        <location filename="project.cpp" line="46"/>
        <source>Rename project</source>
        <translation>Переименовать проект</translation>
    </message>
    <message>
        <location filename="project.cpp" line="48"/>
        <source>View/edit project description</source>
        <translation>Просмотр/правка описания проекта</translation>
    </message>
    <message>
        <location filename="project.cpp" line="50"/>
        <source>Set time of project</source>
        <translation>Настройка времени проекта</translation>
    </message>
    <message>
        <location filename="project.cpp" line="52"/>
        <location filename="project.cpp" line="1934"/>
        <source>Design of project</source>
        <translation>Оформление проекта</translation>
    </message>
    <message>
        <location filename="project.cpp" line="53"/>
        <location filename="project.cpp" line="2021"/>
        <source>Project path</source>
        <translation>Путь к проекту</translation>
    </message>
    <message>
        <location filename="project.cpp" line="198"/>
        <location filename="project.cpp" line="209"/>
        <location filename="project.cpp" line="243"/>
        <location filename="project.cpp" line="300"/>
        <location filename="project.cpp" line="1215"/>
        <location filename="project.cpp" line="1879"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Can&apos;t open project header file (may be it is busy):</source>
        <translation type="vanished">Не открывается файл заголова проекта (возможно, он заблокирован иным процесом):</translation>
    </message>
    <message>
        <location filename="project.cpp" line="199"/>
        <source>Project header file not exist!</source>
        <translation>Файл заголовка проекта не существует!</translation>
    </message>
    <message>
        <location filename="project.cpp" line="209"/>
        <source>Can&apos;t open project header file:</source>
        <translation>Файл заголовка проекта не открывается:</translation>
    </message>
    <message>
        <location filename="project.cpp" line="216"/>
        <source>Opening project...</source>
        <translation>Открытие проекта...</translation>
    </message>
    <message>
        <location filename="project.cpp" line="244"/>
        <source>Project header file has ivalid content</source>
        <translation>Файл заголовка проекта имеет неправильное содержимое</translation>
    </message>
    <message>
        <location filename="project.cpp" line="301"/>
        <source>Project header file has invalid content</source>
        <translation>Файл заголовка проекта имеет неправильное содержимое</translation>
    </message>
    <message>
        <location filename="project.cpp" line="365"/>
        <source>Errors encountered</source>
        <translation>Обнаружены ошибки</translation>
    </message>
    <message>
        <location filename="project.cpp" line="369"/>
        <source>Critical errors!</source>
        <translation>Критические ошибки!</translation>
    </message>
    <message>
        <location filename="project.cpp" line="554"/>
        <source>Restore from backup. Choose backup copy from list</source>
        <translation>Восстановление из бэкапа. Выберите бэкап-копию из списка</translation>
    </message>
    <message>
        <location filename="project.cpp" line="674"/>
        <location filename="project.cpp" line="683"/>
        <source>Recovery error</source>
        <translation>Ошибка восстановления</translation>
    </message>
    <message>
        <location filename="project.cpp" line="675"/>
        <location filename="project.cpp" line="684"/>
        <source>Project&apos;s internal data lost and can&apos;t be recovered.</source>
        <translation>Внутренние данные проекта утрачены и не могут быть восстановлены.</translation>
    </message>
    <message>
        <location filename="project.cpp" line="756"/>
        <source>Errors</source>
        <translation>Ошибки</translation>
    </message>
    <message>
        <location filename="project.cpp" line="762"/>
        <source>Recovered project</source>
        <translation>Восстановленный проект</translation>
    </message>
    <message>
        <location filename="project.cpp" line="862"/>
        <source>Warning</source>
        <translation>Внимание!</translation>
    </message>
    <message>
        <location filename="project.cpp" line="862"/>
        <source>Are you shure?</source>
        <translation>Вы уверены?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1174"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1175"/>
        <source>There are no plots with device </source>
        <translation>Нет шаблонов планшетов с устройством </translation>
    </message>
    <message>
        <location filename="project.cpp" line="1215"/>
        <source>Can&apos;t save project header file (may be it is busy):</source>
        <translation>Файл заголова проекта не доступен для записи (возможно, он заблокирован иным процесом):</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1275"/>
        <source>Saving project...</source>
        <translation>Сохранение проекта...</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1343"/>
        <source>GM-CurveEdit project folder (*.GMCE)</source>
        <translation>Папка проекта GM-CurveEdit (*.GMCE)</translation>
    </message>
    <message>
        <source>Save project on close</source>
        <translation type="vanished">Сохранить проект при выходе</translation>
    </message>
    <message>
        <source>Do you want to save changes?</source>
        <translation type="vanished">Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1489"/>
        <source>Restore project ...</source>
        <translation>Восстановление проекта ...</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1490"/>
        <source>Do you want to restore project from backup?</source>
        <translation>Вы хотите восстановить проект из резервной копии (бэкапа)?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1009"/>
        <location filename="project.cpp" line="1618"/>
        <source>Factory template</source>
        <translation>Заводской шаблон</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1100"/>
        <location filename="project.cpp" line="1635"/>
        <source>User template</source>
        <translation>Шаблон пользователя</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1121"/>
        <location filename="project.cpp" line="1642"/>
        <source>You may choice ready plot template:</source>
        <translation>Вы можете выбрать готовый шаблон планшета:</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1122"/>
        <location filename="project.cpp" line="1643"/>
        <source>(or press No to add empty plot)</source>
        <oldsource>(or press Cancel to add empty plot)</oldsource>
        <translation>(или нажмите No и добавится пустой планшет)</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1124"/>
        <location filename="project.cpp" line="1645"/>
        <source>Select plot template</source>
        <translation>Выбор шаблона планшета</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1696"/>
        <source>Templates not exist</source>
        <translation>Шаблонов нет</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1697"/>
        <source>There are no any plot templates</source>
        <translation>Не обнаружены шаблоны планшетов</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1698"/>
        <source>Do you want to add empty plot?</source>
        <translation>Хотите добавить пустой планшет?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1106"/>
        <source>Found compatible plot templates</source>
        <translation>Найдены подходящие шаблоны плашетов</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1107"/>
        <source>Found plot templates with </source>
        <translation>Найдены шаблоны планшетов с устройством </translation>
    </message>
    <message>
        <location filename="project.cpp" line="1108"/>
        <source> device</source>
        <translation> </translation>
    </message>
    <message>
        <location filename="project.cpp" line="1109"/>
        <source>Do you want to open this plots?</source>
        <translation>Вы хотите открыть такой шаблон?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1437"/>
        <source>Recovery data sources ...</source>
        <translation>Восстановление источников данных ...</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1438"/>
        <source>Do you want to recovery data sources in index of project?</source>
        <translation>Вы хотите восстановить источники данных, индексированные в проекте?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1440"/>
        <source>If you say YES, you may recovery indexed data,</source>
        <translation>Если вы ответите ДА, вы можете восстановить индексированные данные,</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1442"/>
        <source>but all plots, curves, its adjustings, etc, will be lost!</source>
        <translation>но все планшеты, кривые с их настройками и проч. будут утеряны!</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1748"/>
        <source>Description of project</source>
        <translation>Описание проекта</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1795"/>
        <source>UTC is coordinated universal time.</source>
        <translation>UTC - это скоординированное универсальное время.</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1795"/>
        <source>It is the same as Greenvich Mean Time (GMT)</source>
        <translation>Это то же самое, что и Среднее время по Гринвичу (GMT)</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1880"/>
        <source>Project name can&apos;t be empty</source>
        <translation>Имя проекта не может быть пустым</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1964"/>
        <source>Timezone information header font size</source>
        <translation>Размер шрифта заголовка с данными временной зоны</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1965"/>
        <source>Time axis labels font size</source>
        <translation>Размер шрифта подписей оси времени</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1966"/>
        <source>Graphics names in tracks headers font size</source>
        <translation>Размер шрифта названий графиков в заголовках треков</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1967"/>
        <source>Graphics min/max scale values labels font size</source>
        <translation>Размер шрифта мин/макс значений масштабов в заголовках треков</translation>
    </message>
    <message>
        <source>Forward time shift?</source>
        <translation type="vanished">Сдвиг времени вперёд?</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1793"/>
        <source>Select time zone:</source>
        <translation>Выбор временной зоны:</translation>
    </message>
    <message>
        <source>Set time shift if needeed, minutes:</source>
        <oldsource>Set time shift if needeed 
(&lt;hours&gt;:&lt;minutes&gt;):</oldsource>
        <translation type="vanished">Настройка сдвига времени (если необходимо), минуты:</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1816"/>
        <source>Project time set</source>
        <translation>Настройка времени проекта</translation>
    </message>
    <message>
        <location filename="project.cpp" line="1872"/>
        <source>Renaming project ...</source>
        <translation>Переименование проекта ...</translation>
    </message>
</context>
<context>
    <name>ProjectManager</name>
    <message>
        <location filename="projectmanager.cpp" line="36"/>
        <source>Add device (import data)</source>
        <translation>Добавить устройство (импорт данных)</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="40"/>
        <source>Add plot</source>
        <translation>Добавить планшет</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="44"/>
        <source>Create project</source>
        <translation>Создать проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="46"/>
        <location filename="projectmanager.cpp" line="565"/>
        <source>Open project</source>
        <translation>Открыть проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="48"/>
        <source>Save project</source>
        <translation>Сохранить проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="51"/>
        <source>Save project copy to</source>
        <oldsource>Save project as...</oldsource>
        <translation>Сохранить копию проекта</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="53"/>
        <source>Set time of project</source>
        <translation>Настройка времени проекта</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="56"/>
        <source>Close project</source>
        <translation>Закрыть проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="59"/>
        <source>Rename project</source>
        <translation>Переименовать проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="62"/>
        <source>View/edit project description</source>
        <translation>Просмотр/правка описания проекта</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="66"/>
        <source>Restore project from backup</source>
        <translation>Восстановить проект из бэкапа</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="69"/>
        <source>Recovery data sources</source>
        <translation>Восстановление источников данных</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="72"/>
        <source>Delete data sources from project</source>
        <translation>Удалить из проекта источники данных</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="75"/>
        <source>Design of project</source>
        <translation>Оформление проекта</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="127"/>
        <source>New project</source>
        <translation>Новый проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="192"/>
        <location filename="projectmanager.cpp" line="307"/>
        <source>Problem opening project</source>
        <translation>Проблема открытия проекта</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="193"/>
        <source>At first project may be restored from backup. Restore from backup copy? </source>
        <translation>Сначала проект может быть восстановлен из бэкапа (резервной копии). Восстановить из бэкап-копии? </translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="218"/>
        <source>Project&apos;s header file not found.</source>
        <translation>Файл заголовка проекта не найден.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="220"/>
        <source>This file may be recreated again with default content.</source>
        <translation>Этот файл может быть воссоздан с содержимым по умолчанию.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="222"/>
        <source>After that recovery process will started:</source>
        <translation>После этого будет запущен процесс восстановления:</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="224"/>
        <location filename="projectmanager.cpp" line="252"/>
        <location filename="projectmanager.cpp" line="271"/>
        <location filename="projectmanager.cpp" line="284"/>
        <location filename="projectmanager.cpp" line="297"/>
        <source>data files will be searched in the project&apos;s internal folder.</source>
        <translation>во внутренних папках проекта будет произведен поиск файлов данных.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="226"/>
        <location filename="projectmanager.cpp" line="254"/>
        <location filename="projectmanager.cpp" line="273"/>
        <location filename="projectmanager.cpp" line="286"/>
        <location filename="projectmanager.cpp" line="299"/>
        <source>Do you want to start recovery process?</source>
        <translation>Вы хотите запустить процесс восстановления?</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="233"/>
        <source>Project&apos;s header file can&apos;t open even in read-only mode.</source>
        <translation>Файл заголовка проекта не может быть открыт даже в режиме только чтения.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="235"/>
        <source>There are two possible reasons:</source>
        <translation>Возможны две причины:</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="237"/>
        <source>1. Security settings not allow to open this file for you</source>
        <translation>1. Установки безопасности не поволяют вам открыть этот файл</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="239"/>
        <source>2. Problems with file system integrity</source>
        <translation>2. Проблемы целостности файловой системы</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="241"/>
        <source>Please, contact with your system administrator.</source>
        <translation>Пожалуйста, свяжитесь с вашим системным администратором.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="248"/>
        <source>Project&apos;s header file corrupt.</source>
        <translation>Файл заголовка проекта поврежден.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="250"/>
        <location filename="projectmanager.cpp" line="269"/>
        <location filename="projectmanager.cpp" line="282"/>
        <location filename="projectmanager.cpp" line="295"/>
        <source>It&apos;s possible to recovery data files and to rebuild project again</source>
        <translation>Возможно восстановление файлов данных и перестроить проект снова</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="261"/>
        <source>Project&apos;s internal data lost and can&apos;t be recovered.</source>
        <translation>Внутренние данные проекта утеряны и не могут быть восстановлены.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="267"/>
        <source>Project&apos;s imported files with data not indexed correctly.</source>
        <translation>Импортированные файлы проекта с данными индексированы некорректно.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="280"/>
        <source>Project&apos;s data source index corrupt.</source>
        <translation>Источник данных проекта повержден.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="293"/>
        <source>Project&apos;s special files (curves data, plots settings, e.t.c.) corrupt.</source>
        <translation>Специальные файлы проекта (данны кривых, настройки планшетов и т.д.) повреждены.</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="348"/>
        <location filename="projectmanager.cpp" line="545"/>
        <source>Opened/created project exist</source>
        <translation>Имеется созданный/открытый проект</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="349"/>
        <source>You must close current project to create another</source>
        <translation>Вы должны сначала закрыть текущий проект для создания другого</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="372"/>
        <source>Create project and save it to folder...</source>
        <translation>Создать проект и сохранить его в папке...</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="373"/>
        <source>GM-CurveEdit project folder (*.GMCE)</source>
        <translation>Папка проекта GM-CurveEdit (*.GMCE)</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="412"/>
        <source>Folder already exist</source>
        <oldsource>Project already exist</oldsource>
        <translation>Каталог уже существует</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="413"/>
        <source>Folder already exist, probably with project inside. Do you want remove old folder?</source>
        <oldsource>Project in this folder already exist. Do you want remove old project?</oldsource>
        <translation>Каталог уже существует, возможно с проектом внутри. Вы хотите удалить старый каталог?</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="546"/>
        <source>You must close current project to open another</source>
        <translation>Вы должны сначала закрыть текущий проект, чтоб открыть другой</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="591"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="591"/>
        <source>It&apos;s not GM-CurveEdit project folder!</source>
        <translation>Это не папка проекта GM-CurveEdit!</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="613"/>
        <source>Save project on close</source>
        <translation>Сохранение проекта при выходе</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="613"/>
        <source>Do you want to save changes?</source>
        <translation>Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="645"/>
        <source>Create object for add data</source>
        <translation>Создание обекта для добавления данных (проекта)</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="646"/>
        <source>It&apos;s neccessary to create project before add data</source>
        <translation>Необходимо создать проект перед тем, как добавлять данные</translation>
    </message>
    <message>
        <location filename="projectmanager.cpp" line="648"/>
        <source>Do you want to create project?</source>
        <translation>Вы хотите создать проект?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="26"/>
        <source>Standard file CSV GM-Monitor</source>
        <translation>Стандартный CSV-файл GM-Monitor</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="110"/>
        <location filename="../stdcsv/stdcsv.cpp" line="278"/>
        <location filename="../stdcsv/stdcsv.cpp" line="943"/>
        <source>Can&apos;t open indexed file CSV</source>
        <translation>Не открывается индексированный CSV-файл</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="422"/>
        <source>Adjustings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="101"/>
        <location filename="../stdcsv/stdcsv.cpp" line="269"/>
        <location filename="../stdcsv/stdcsv.cpp" line="933"/>
        <source>Error name file indexed</source>
        <translation>Ошибка имени индексированного файла</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="958"/>
        <location filename="../stdcsv/stdcsv.cpp" line="987"/>
        <source>Incorrect file format</source>
        <translation>Неверный формат файла</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="1209"/>
        <source>Error in table header: table marker not found in first 1000 strings</source>
        <translation>Ошибка в заголовке таблицы: маркер table не найден в первых 1000 строках</translation>
    </message>
    <message>
        <source>Error in table header: unresolved symbol found for category label; error in string </source>
        <translation type="vanished">Ошибка в заголовке таблицы: неразрешенный символ найден для метки категории; ошибка в строке </translation>
    </message>
    <message>
        <source>Error in table header: field for functions WMST and EMST not recognized</source>
        <translation type="vanished">Ошибка в заголовке таблицы: поля для функций WMST и EMST не опознаны</translation>
    </message>
    <message>
        <location filename="../stdcsv/stdcsv.cpp" line="1382"/>
        <source>Error in table header: field for function EMST not recognized</source>
        <translation>Ошибка в заголовке таблицы: поле для функции EMST не опознано</translation>
    </message>
    <message>
        <source>Error in table header: field for function WMST not recognized</source>
        <translation type="vanished">Ошибка в заголовке таблицы: поле для функции WMST не опознано</translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1420"/>
        <source>Версия ПО для исходного файла данных: </source>
        <translation type="unfinished">Версия ПО для исходного файла данных: </translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1431"/>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1460"/>
        <source> - номер программы: </source>
        <translation type="unfinished"> - номер программы: </translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1434"/>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1463"/>
        <source> - мажорная версия: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1437"/>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1466"/>
        <source> - минорная версия: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1440"/>
        <source>Тип узла: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1449"/>
        <source>Версия ПО для файла описания при расшифровке: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1469"/>
        <source>Тип узла в файле описания: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1478"/>
        <source>Сведения о компьютере: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1490"/>
        <source> - пользователь: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1493"/>
        <source> - компьютер: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1496"/>
        <source> - домен: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1499"/>
        <source> - дата: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1502"/>
        <source> - время: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1520"/>
        <source>Номер прибора: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1523"/>
        <source>Номер блока: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1526"/>
        <source>Номер узла: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1535"/>
        <source>Тип узла (текст): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1551"/>
        <source>Имя узла: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1554"/>
        <source>Обозначение узла: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1557"/>
        <source>Название прибора: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1560"/>
        <source>Обозначение прибора: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gceLogicLayer/gcedatasource.cpp" line="1566"/>
        <source>Комментарий (~прибор): </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpecialWidgetForPlot</name>
    <message>
        <location filename="gcedataview.cpp" line="2401"/>
        <location filename="gcedataview.cpp" line="3922"/>
        <source>Time = </source>
        <oldsource>UTC_time = </oldsource>
        <translation>Время = </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="2403"/>
        <location filename="gcedataview.cpp" line="3674"/>
        <location filename="gcedataview.cpp" line="3677"/>
        <location filename="gcedataview.cpp" line="3924"/>
        <source> seconds</source>
        <oldsource> seconds GMT</oldsource>
        <translation> сек</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3932"/>
        <source>Values for plot </source>
        <translation>Значения для планшета </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3426"/>
        <location filename="gcedataview.cpp" line="3471"/>
        <location filename="gcedataview.cpp" line="3510"/>
        <location filename="gcedataview.cpp" line="4001"/>
        <location filename="gcedataview.cpp" line="4346"/>
        <location filename="gcedataview.cpp" line="4418"/>
        <location filename="gcedataview.cpp" line="4424"/>
        <location filename="gcedataview.cpp" line="4439"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="1984"/>
        <source>Edit plot header</source>
        <translation>Ввод заголовка планшета</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3427"/>
        <source>You must copy to the same track!</source>
        <translation>Вы должны копирвать в тот же трек!</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3472"/>
        <source>You must move within the same track!</source>
        <translation>Вы должны премещать внутри того же трека!</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3511"/>
        <source>You must copy to another track!</source>
        <translation>Вы должны копирвать в другой трек!</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3536"/>
        <source>Info</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3537"/>
        <source>Label already copied here...</source>
        <translation>Метка уже скопирована сюда...</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3672"/>
        <source>Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3678"/>
        <source> minutes, </source>
        <translation> мин, </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3681"/>
        <source> hours, </source>
        <translation> ч, </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="3684"/>
        <source> days, </source>
        <translation> д, </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4002"/>
        <source>Can&apos;t add graphic with same name from same device to one track twice or more</source>
        <translation>Нельзя добавить график с тем же именем с того же устройства несколько раз</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4003"/>
        <source>Device: </source>
        <translation>Устройство: </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4004"/>
        <source>Graphic curve name: </source>
        <translation>Имя графической кривой: </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4131"/>
        <source>Save to PDF</source>
        <translation>Сохранить в PDF</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4216"/>
        <source>Save to BMP</source>
        <translation>Сохранить в BMP</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4288"/>
        <source>Do you really want to delete track </source>
        <translation>Вы действительно хотите удалить трек </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4290"/>
        <source> from plot </source>
        <translation> из планшета </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4294"/>
        <source>Track removing</source>
        <translation>Удаление трека</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4315"/>
        <source>Do you really want to delete plot </source>
        <translation>Вы действительно хотите удалить планшет </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4318"/>
        <source>Plot removing</source>
        <translation>Удаление планшета</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4337"/>
        <source>Save plot template to file</source>
        <translation>Сохранить шаблон планшета в файл</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4339"/>
        <source>Plot template (*.gmcetplt)</source>
        <translation>Шаблон планшета (*.gmcetplt)</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4346"/>
        <source>You can&apos;t save custom templates to folder with factory templates:</source>
        <translation>Вы не можете сохранять настраиваемые шаблоны в папку заводских шаблонов:</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4358"/>
        <location filename="gcedataview.cpp" line="4365"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4358"/>
        <source>There is no links to any device in plot</source>
        <translation>Нет связки с каким-либо устройством в планшете</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4365"/>
        <source>There is no devices in project</source>
        <translation>Нет устройств в проекте</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4371"/>
        <source>Change device in plot </source>
        <translation>Измененить устройство в планшете </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4379"/>
        <source>Select device in plot: </source>
        <translation>Выбрать устройство в планшете:</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4380"/>
        <source> which will replaced by device from source: </source>
        <translation> которое будет заменено на устройство из источника данных: </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4418"/>
        <source>Both device in plot and other source device must be selected!</source>
        <translation>И устройство в планшете, и устройство в источнике данных должны быть выбраны!</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4424"/>
        <source>You choose the same device. Nothing to do.</source>
        <translation>Вы выбрали то же устройство. Действие не произведено.</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4439"/>
        <source>Fault finding device to link!</source>
        <translation>Срыв поиска устройства для привязки!</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4686"/>
        <location filename="gcedataview.cpp" line="4727"/>
        <source>Before </source>
        <translation>Перед </translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4689"/>
        <location filename="gcedataview.cpp" line="4712"/>
        <source>Move to end</source>
        <translation>Переместить в конец</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4695"/>
        <source>Select place for track</source>
        <translation>Выбор места для трека</translation>
    </message>
    <message>
        <location filename="gcedataview.cpp" line="4767"/>
        <source>Select track for placing moved curve</source>
        <translation>Выбор трека для размещения перемещаемой кривой</translation>
    </message>
</context>
<context>
    <name>Track</name>
    <message>
        <location filename="track.cpp" line="79"/>
        <source>Track</source>
        <translation>Трек</translation>
    </message>
    <message>
        <location filename="track.cpp" line="84"/>
        <source>Load data for selected curves</source>
        <translation>Загрузить данные выбранных кривых</translation>
    </message>
    <message>
        <location filename="track.cpp" line="85"/>
        <source>Reload data for all curves</source>
        <translation>Перезагрузить данные выбранных кривых</translation>
    </message>
    <message>
        <location filename="track.cpp" line="86"/>
        <source>Delete track</source>
        <translation>Удалить трек</translation>
    </message>
    <message>
        <location filename="track.cpp" line="92"/>
        <source>Add text label</source>
        <translation>Добавить текстовую метку</translation>
    </message>
    <message>
        <source>Adjust...</source>
        <translation type="vanished">Настроить...</translation>
    </message>
    <message>
        <location filename="track.cpp" line="229"/>
        <source>Delete curve from track</source>
        <translation>Удалить кривую из трека</translation>
    </message>
    <message>
        <location filename="track.cpp" line="620"/>
        <source>Change curve color</source>
        <translation>Изменить цвет кривой</translation>
    </message>
    <message>
        <location filename="track.cpp" line="265"/>
        <source>Not assigned to device</source>
        <translation>Не привязано к устройству</translation>
    </message>
    <message>
        <location filename="track.cpp" line="606"/>
        <source>Minimum scale value:</source>
        <translation>Минимум шкалы значений:</translation>
    </message>
    <message>
        <location filename="track.cpp" line="608"/>
        <source>Maximum scale value:</source>
        <translation>Максимум шкалы значений:</translation>
    </message>
    <message>
        <source>Rescale another graps too...</source>
        <translation type="vanished">Перемасштабировать другие графики тоже...</translation>
    </message>
    <message>
        <source>Fix values scale (turn off autoscale)</source>
        <translation type="vanished">Фиксировать значения масштаба</translation>
    </message>
    <message>
        <location filename="track.cpp" line="611"/>
        <source>Accept scale settings for another graphs too...</source>
        <translation>Применить настройки масштаба к другим графикам трека...</translation>
    </message>
    <message>
        <location filename="track.cpp" line="618"/>
        <source>Show data as dots</source>
        <translation>Показать точки данных</translation>
    </message>
    <message>
        <location filename="track.cpp" line="619"/>
        <source>... apply for all graphics in track</source>
        <translation>... применить ко всем графикам на треке</translation>
    </message>
    <message>
        <location filename="track.cpp" line="624"/>
        <source>-- Track properties --</source>
        <translation>-- Свойства трека --</translation>
    </message>
    <message>
        <location filename="track.cpp" line="626"/>
        <location filename="track.cpp" line="1390"/>
        <source>Track height, pixels:</source>
        <translation>Высота трека, пиксели:</translation>
    </message>
    <message>
        <location filename="track.cpp" line="88"/>
        <location filename="track.cpp" line="629"/>
        <location filename="track.cpp" line="1355"/>
        <location filename="track.cpp" line="1393"/>
        <source>Hide track</source>
        <translation>Скрыть трек</translation>
    </message>
    <message>
        <location filename="track.cpp" line="90"/>
        <source>Move track</source>
        <translation>Переместить трек</translation>
    </message>
    <message>
        <location filename="track.cpp" line="94"/>
        <location filename="track.cpp" line="1385"/>
        <source>Track properties</source>
        <translation>Свойства трека</translation>
    </message>
    <message>
        <location filename="track.cpp" line="231"/>
        <source>Move curve to another track</source>
        <translation>Переместить кривую на другой трек</translation>
    </message>
    <message>
        <location filename="track.cpp" line="596"/>
        <source>Curve name: </source>
        <translation>Название кривой: </translation>
    </message>
    <message>
        <location filename="track.cpp" line="597"/>
        <source>Device name: </source>
        <translation>Название устройства: </translation>
    </message>
    <message>
        <location filename="track.cpp" line="599"/>
        <source>Pseudoname:</source>
        <translation>Псевдоним:</translation>
    </message>
    <message>
        <location filename="track.cpp" line="634"/>
        <source>Use null value indication</source>
        <oldsource>Use null value</oldsource>
        <translation>Использовать признак отсутствующего значения</translation>
    </message>
    <message>
        <location filename="track.cpp" line="638"/>
        <source>Horizontal sections count:</source>
        <translation>Число горизонтальных полос:</translation>
    </message>
    <message>
        <location filename="track.cpp" line="228"/>
        <location filename="track.cpp" line="713"/>
        <source>Curve properties</source>
        <translation>Свойства кривой</translation>
    </message>
    <message>
        <location filename="track.cpp" line="610"/>
        <source>Fix axis</source>
        <translation>Фиксировать ось</translation>
    </message>
    <message>
        <location filename="track.cpp" line="612"/>
        <source>Scale intreval:</source>
        <translation>Цена деления:</translation>
    </message>
    <message>
        <location filename="track.cpp" line="617"/>
        <source>Fix scale intreval</source>
        <translation>Фиксировать цену деления</translation>
    </message>
    <message>
        <location filename="track.cpp" line="738"/>
        <location filename="track.cpp" line="1424"/>
        <source>Input error</source>
        <translation>Ошибка ввода</translation>
    </message>
    <message>
        <location filename="track.cpp" line="738"/>
        <location filename="track.cpp" line="1424"/>
        <source>Incorrect value(s)!</source>
        <translation>Некорректное(ые) значение(я)!</translation>
    </message>
    <message>
        <location filename="track.cpp" line="886"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="track.cpp" line="886"/>
        <source>Not correct null value!</source>
        <translation>Значение, выбранное для остутствующих данных, не коррректно!</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1174"/>
        <location filename="track.cpp" line="1225"/>
        <source>Select all</source>
        <translation>Выбрать все</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1175"/>
        <source>Deselect all</source>
        <translation>Отменить выбор всех</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1195"/>
        <source>Selected another curves for rescale</source>
        <translation>Выбрать другие кривые для масштаба</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1247"/>
        <source>Do you really want to delete curve </source>
        <translation>Вы действительно хотите удалить кривую </translation>
    </message>
    <message>
        <location filename="track.cpp" line="1249"/>
        <source> from track </source>
        <translation> из трека </translation>
    </message>
    <message>
        <location filename="track.cpp" line="1253"/>
        <source>Curve removing</source>
        <translation>Удаление кривой</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1352"/>
        <location filename="track.cpp" line="1362"/>
        <source>[HIDDEN]</source>
        <translation>[СКРЫТ]</translation>
    </message>
    <message>
        <location filename="track.cpp" line="1364"/>
        <source>Show track</source>
        <translation>Показать трек</translation>
    </message>
</context>
</TS>
