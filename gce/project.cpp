#include "project.h"
#include <QAction>
#include <QMessageBox>

#include <gcelogiclayer.h>
#include <base/variablecontainer.h>

#include "elements/plot.h"
#include "elements/device.h"
#include "gcedataview.h"
#include "projectmanager.h"
#include "mainwindow.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QHBoxLayout>

#include "filesearchernodb.h"

#include "cfg.h"

Project::Project(uint32_t idx, QObject *parent) : QObject(parent)
{
    showDotsOnAllGraphics = false;

    fontPixelSize_UtcHeader = 12;
    fontPointSize_GraphicsMinMaxValuesInHeader =
            fontPointSize_GraphicsNamesInHeader =
            fontPointSize_TimeScale = 8;

    _openingProjectError = OpeningProjectError::None;

    lastElementsKey = 1; // учет элемента самого проекта - его инициализация и вставка в elements прямо тут

    timeOffsetMilliseconds = 0L;
    timeZone = QTimeZone(QByteArray("UTC"));

    saveProject = new QAction(tr("Save project"), this);
    saveProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton));
    saveProjectAs = new QAction(tr("Save project copy to"), this);
    closeProject = new QAction(tr("Close project"), this);
    closeProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogCloseButton));
    renameProject = new QAction(tr("Rename project"), this);
    renameProject->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogApplyButton));
    showDescription = new QAction(tr("View/edit project description"), this);
    showDescription->setIcon(qApp->style()->standardIcon(QStyle::SP_FileDialogInfoView));
    setProjectTime = new QAction(tr("Set time of project"), this);
    setProjectTime->setIcon(QIcon(":/ico1/times"));
    designOfProject = new QAction(tr("Design of project"), this);
    projectPath = new QAction(tr("Project path"));

    connect(saveProject, SIGNAL(triggered(bool)), this, SLOT(slotSaveProject()));
    connect(saveProjectAs, SIGNAL(triggered(bool)), this, SLOT(slotSaveProjectAs()));
    connect(closeProject, SIGNAL(triggered(bool)), this,  SIGNAL(sigCloseProject()));
    connect(renameProject, SIGNAL(triggered(bool)), this, SLOT(slotRenameProject()));
    connect(showDescription, SIGNAL(triggered(bool)), this, SLOT(slotShowDescr()));
    connect(setProjectTime, SIGNAL(triggered(bool)), this, SLOT(slotShowProjectTimeSet()));
    connect(designOfProject, SIGNAL(triggered(bool)), this, SLOT(slotDesignSettings()));
    connect(projectPath, SIGNAL(triggered(bool)), this, SLOT(slotProjectPath()));

    currentIndexProject = idx;

    unsaved = true;
    auto el = new TreeElement(idx, nullptr, qobject_cast<QObject*>(this), true);
    el->setKeyInElementsMap(0);
    elements.insert(0, el);
    el->setType(TreeElement::ElementType::elementProject);
    el->setContextMenuActions(getProjectActionsList());
    rootElem = el;

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
    if (o)
    {
        auto dv = qobject_cast<GceDataView*>(o);
        connect(this, SIGNAL(sigOpenProjectPlots(QString)), dv, SLOT(slotOpenPlots(QString)));
        connect(dv, SIGNAL(sigForAddEmptyPlot()), this, SLOT(slotForAddPlot()));
    }
}

void Project::addDataSource()
{
    devicesList->slotStartImportDialog();
}

void Project::setProjectIndex(uint32_t idx)
{
    currentIndexProject = idx;
}

void Project::fillElementsBase(bool forOpen)
{
    devicesList = new DevicesDatas(currentIndexProject, this, this);
    plotsList = new Plots(currentIndexProject, this, this);
    labelsList = new ListLabels(currentIndexProject, this, this);
    rootElem->addSubElement(devicesList->getItem());
    rootElem->addSubElement(plotsList->getItem());
    rootElem->addSubElement(labelsList->getItem());

    connect(plotsList, SIGNAL(sigAddPlot()), this, SLOT(slotForAddPlot()));
    connect(plotsList, SIGNAL(sigOpenPlotTemplate()), this, SLOT(slotForAddPlotFromSelectedTemplate()));

    Q_UNUSED(forOpen)
//    if (!forOpen)
//    {
//        auto firstPlot = new Plot(currentIndexProject, plotsList->getNextNewPlotIndex(), this, this);
//        plotsList->addPlot(firstPlot);
//        auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
//        if (!o)
//        {
//            return;
//        }
//        auto dv = qobject_cast<GceDataView*>(o);
//        connect(plotsList, SIGNAL(sigOpenPlotTemplate()), dv, SLOT(slotOpenPlotTemplateFromFile()));
//        dv->addPlot(firstPlot, true);
//    }

    auto dataView = qobject_cast<GceDataView*>(
                GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView)
                );

    connect(plotsList, SIGNAL(sigForJoinPlotsByTime()), dataView, SLOT(slotJoinPlotsByTimeDialogStart()));
}

uint32_t Project::getThisProjectIndex()
{
    return currentIndexProject;
}

QString Project::getDescription() const
{
    return description;
}

void Project::setDescription(QString newDescription)
{
    description = newDescription;
}

QString Project::getProjectName() const
{
    return elements.value(0)->getNameElement();
}

void Project::setProjectName(QString newProjectName)
{
    elements.value(0)->setNewNameElement(newProjectName);
}

QString Project::getStringFileNameOfProject() const
{
    return QString();
}

bool Project::unsavedChangesExists() const
{
    return unsaved;
}

void Project::setSaved(bool saved)
{
    unsaved = !saved;
}

void Project::removeOnePlot(Plot *plt)
{
    plotsList->getItem()->removeChild(plt->getItem());
}

bool Project::openProject()
{    
    QFile headerFile(projectHeaderPath);

    if (!headerFile.exists())
    {
        QMessageBox::critical(nullptr, tr("Error"),
                              tr("Project header file not exist!"));
        _openingProjectError = OpeningProjectError::ProjectHeaderNotExists;
        return false;
    }

    //saveCopyToBackupForRecovery();

    auto ok = headerFile.open(QFile::ReadOnly);
    if (!ok)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("Can't open project header file:").append("\n")
                              .append(projectHeaderPath));
        _openingProjectError = OpeningProjectError::ProjectHeaderCantOpenForRead;
        return false;
    }

    emit GceLogicLayer::getInstance().sigOpenStartProgressBarWindow(tr("Opening project..."));

    auto versionProjectMajor = 0;
    auto versionProjectMinor = 0;

    QList<uint32_t> devicesIdsList;

    try
    {
        //
        QDataStream dstreamin(&headerFile);
        QString projectName;
        dstreamin >> projectName;

        auto nameOk = true;
        for (QChar c : projectName)
        {
            if (//!c.isSymbol()&&!c.isSpace()&&!c.isPunct()
                  !c.isPrint()  )
            {
                nameOk = false;
                break;
            }
        }

        if (!nameOk || projectName.isEmpty())
        {
            QMessageBox::critical(nullptr, tr("Error"),
                                  tr("Project header file has ivalid content"));
            _openingProjectError = OpeningProjectError::ProjectHeaderCorrupt;
            emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();
            return false;
        }

        dstreamin >> description;

        dstreamin >> devicesIdsList;

        VariableContainer vcontainer;

        auto data_ = vcontainer.read_container_hash();

        dstreamin >> data_;

        vcontainer.write_container_hash(data_);

        // прочитать из контейнера...
        auto idTimeZone = vcontainer.get_value<decltype(timeZone.id())>("time_zone_id_project", "UTC");
        timeZone = QTimeZone(idTimeZone);
        timeOffsetMilliseconds = vcontainer.get_value<decltype(timeOffsetMilliseconds)>("time_offset_msecs", 0LL);
        versionProjectMajor = vcontainer.get_value<int>("project_version_major", 0);
        versionProjectMinor = vcontainer.get_value<int>("project_version_minor", 0);

//        vcontainer.set_value<int>("font_time_zone", fontPixelSize_UtcHeader);
//        vcontainer.set_value<int>("font_axis_time_labels", fontPointSize_TimeScale);
//        vcontainer.set_value<int>("font_graphics_header_name_graphic", fontPointSize_GraphicsNamesInHeader);
//        vcontainer.set_value<int>("font_graphics_header_scale_min_max",
//                                  fontPointSize_GraphicsMinMaxValuesInHeader);

        if (vcontainer.keyExist("font_time_zone"))
        {
            fontPixelSize_UtcHeader = vcontainer.get_value<int>("font_time_zone");
        }
        if (vcontainer.keyExist("font_axis_time_labels"))
        {
            fontPointSize_TimeScale = vcontainer.get_value<int>("font_axis_time_labels");
        }
        if (vcontainer.keyExist("font_graphics_header_name_graphic"))
        {
            fontPointSize_GraphicsNamesInHeader =
                    vcontainer.get_value<int>("font_graphics_header_name_graphic");
        }
        if (vcontainer.keyExist("font_graphics_header_scale_min_max"))
        {
            fontPointSize_GraphicsMinMaxValuesInHeader =
                    vcontainer.get_value<int>("font_graphics_header_scale_min_max");
        }

        headerFile.close();
        setProjectName(projectName);
    }
    catch (...)
    {
        headerFile.close();
        QMessageBox::critical(nullptr, tr("Error"),
                              tr("Project header file has invalid content"));
        _openingProjectError = OpeningProjectError::ProjectHeaderCorrupt;
        emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();
        return false;
    }


    QString errorStr;
    auto allOk = true;

    auto indexedDataHeaderOk = GceLogicLayer::getInstance().openIndexedDataHeader(projectHeaderPath, errorStr);
    Q_UNUSED(indexedDataHeaderOk)
//    if (!indexedDataHeaderOk)
//    {
//        allOk = false;
//    }
    auto importModulesFound = GceLogicLayer::getInstance().scanImportModules(errorStr);
    Q_UNUSED(importModulesFound)

    // open devices through GceLogicLayer
    auto szDl = devicesIdsList.size();
    for (decltype (szDl) idev = 0; idev < szDl; ++idev)
    {
        uint32_t srcID = devicesIdsList.at(idev);
        auto ok = true;
        if (allOk) ok = GceLogicLayer::getInstance().openSourceData(srcID, projectHeaderPath, errorStr,
                                                              versionProjectMajor, versionProjectMinor);
        if (!ok)
        {
            allOk = false;
            break;
        }
    }

    if (!allOk)
    {
        if (errorStr.contains("project internal directory not found"))
        {
            _openingProjectError = OpeningProjectError::ProjectInternalDataBroken_UnRecoverable;
        }
        else if (errorStr.contains("file i.data"))
        {
            _openingProjectError = OpeningProjectError::ProjectDataSourceIndexCorrupt;
        }
        else if (errorStr.contains("special files are broken"))
        {
            _openingProjectError = OpeningProjectError::ProjectSpecialFilesBroken;
        }
        else if (errorStr.contains("Imported files metadata file")
                  || errorStr.contains("Imported file data file can't read"))
        {
            _openingProjectError = OpeningProjectError::ProjectImportedFilesIndexerFail;
        }

        GceLogicLayer::getInstance().removeAllDataOnClose();
    }

    if (errorStr.size())
    {
        if (allOk)
        {
            QMessageBox::warning(nullptr, tr("Errors encountered"), errorStr);
        }
        else
        {
            QMessageBox::critical(nullptr, tr("Critical errors!"), errorStr);
            emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();
            return false;
        }
    }

    devicesList->extractDataFromSource();

    this->getItemProject()->setExpanded(true);
    devicesList->getItem()->setExpanded(true);
    plotsList->getItem()->setExpanded(true);

    emit sigOpenProjectPlots(projectHeaderPath);

    //saveCopyToBackupForRecovery();

    emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();

    auto o = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameMainWindow);
    if (o)
    {
        QStringList tmp__ = projectHeaderPath.split("/");
        tmp__.removeLast();
        auto mainWnd = qobject_cast<MainWindow*>(o);
        mainWnd->setProjectPathInTitle(tmp__.last());
    }

    return true;
}

void Project::saveCopyToBackupForRecovery()
{
    QString projectDirName = QString(projectHeaderPath).remove(".ceproj");
    //QDir projectDir(projectDirName);
//    QDir indexDir(QString(projectDirName).append("/index"));

//    if (!indexDir.exists())
//    {
//        return; //нечего архивировать
//    }

    QDir backupDir(QString(projectDirName).append("/bkp"));

    if (!backupDir.exists())
    {
        backupDir.cdUp();
        backupDir.mkpath("bkp");
        backupDir.cd("bkp");

        //qDebug() << (b?1:0);
    }

    FileSearcherNoDb fsrchr;
    auto subdirsInBkp = fsrchr.retSubFoldersInFolder(QFileInfo(backupDir.absolutePath()));
    QString newBackupName;
    QString currentDateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss");

    if (backupDir.isEmpty())
    {
        newBackupName = "1";
        newBackupName.append("--").append(currentDateTime);
    }
    else
    {
        qint64 maxNum = 0;
        for (QFileInfo &fiDirBkp : subdirsInBkp)
        {
            QStringList tmp_ = fiDirBkp.fileName().split("--");
            qint64 num = QString(tmp_.first()).toLongLong();
            if (num > maxNum) maxNum = num;
        }
        maxNum++;
        newBackupName = QString::number(maxNum);
        newBackupName.append("--").append(currentDateTime);
    }
    backupDir.mkdir(newBackupName);

    auto subFldrsProject = fsrchr.retSubFoldersInFolder(QFileInfo(projectDirName));
    QFileInfo fiExcludeBackup, fiExcludeIndex;
    auto boolFoundBkcpFi = false;
    auto boolFoundIndexFi = false;
    for (QFileInfo &fi : subFldrsProject)
    {
        if (fi.fileName() == QString("bkp"))
        {
            fiExcludeBackup = fi;
            boolFoundBkcpFi = true;
//            break;
        }
        if (fi.fileName() == QString("index"))
        {
            fiExcludeIndex = fi;
            boolFoundIndexFi = true;
        }
    }
    if (boolFoundBkcpFi) subFldrsProject.removeOne(fiExcludeBackup);
    if (boolFoundIndexFi) subFldrsProject.removeOne(fiExcludeIndex);

    QDir dirCreatingBackupCopy(newBackupName.prepend("/").prepend(backupDir.absolutePath()));
    dirCreatingBackupCopy.mkdir("project.bak");
    dirCreatingBackupCopy.mkdir("header.bak");

    QFile fileCurrentHeader(projectHeaderPath);
    QFileInfo fiCurrentHeader(projectHeaderPath);
    fileCurrentHeader.copy(dirCreatingBackupCopy.absolutePath().append("/header.bak/")
                           .append(fiCurrentHeader.fileName()));
    for (QFileInfo &fiDirHigh : subFldrsProject)
    {
        auto fiListFilesContent = fsrchr.retFilesInFolder(fiDirHigh);
        QString makingPathInBckp = QString("project.bak/").append(fiDirHigh.fileName());
        dirCreatingBackupCopy.mkpath(makingPathInBckp);
        for (QFileInfo &fiFile : fiListFilesContent)
        {
            QFile fileCurrentData(fiFile.absoluteFilePath());
            fileCurrentData.copy(dirCreatingBackupCopy.absolutePath()
                                 .append("/").append(makingPathInBckp).append("/")
                                 .append(fiFile.fileName()));
        }
    }

    subdirsInBkp = fsrchr.retSubFoldersInFolder(QFileInfo(backupDir.absolutePath()));
    if (subdirsInBkp.count() > 3)
    {
        qint64 smallerNum = -1;
        QMap <qint64, QFileInfo> mapBckpSubDirs;
        for (QFileInfo &fiBckpSubDir : subdirsInBkp)
        {
            QStringList tmp2 = fiBckpSubDir.fileName().split("--");
            qint64 num = QString(tmp2.first()).toLongLong();
            if (smallerNum == -1)
            {
                smallerNum = num;
            }
            else
            {
                if (num < smallerNum)
                {
                    smallerNum = num;
                }
            }
            mapBckpSubDirs[num] = fiBckpSubDir;
        }
        auto fiToDeleteOldBackup = mapBckpSubDirs.value(smallerNum);
        QString prjHdrBak = fiToDeleteOldBackup.absoluteFilePath().append("/header.bak");
        auto fiList4Remove = fsrchr.retFilesInFolder(QFileInfo(prjHdrBak));
        for (QFileInfo &fi_fl : fiList4Remove)
        {
            QFile fl(fi_fl.absoluteFilePath());
            fl.remove();
        }
        QString prjBack = fiToDeleteOldBackup.absoluteFilePath().append("/project.bak");
        auto fiListFoldersForRemove = fsrchr.retSubFoldersInFolder(QFileInfo(prjBack));
        auto fiListAllFiles = fsrchr.retFilesInFolderAndSubFolders(QFileInfo(prjBack));
        for (QFileInfo &fi_fl : fiListAllFiles)
        {
            QFile fl(fi_fl.absoluteFilePath());
            fl.remove();
        }
        for (QFileInfo &fi_dir : fiListFoldersForRemove)
        {
            QDir dir_(fi_dir.absolutePath());
            dir_.rmdir(fi_dir.fileName());
        }
        QDir dirToDelOldBackup(fiToDeleteOldBackup.absoluteFilePath());
        dirToDelOldBackup.rmdir("project.bak");
        dirToDelOldBackup.rmdir("header.bak");
        dirToDelOldBackup.cdUp();
        dirToDelOldBackup.rmdir(fiToDeleteOldBackup.fileName());
    }
}

bool Project::restoreFromBackup()
{
    auto result = false;

    QString projectDirName = QString(projectHeaderPath).remove(".ceproj");
    QDir backupDir(QString(projectDirName).append("/bkp"));

    auto dialog = new QDialog;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    dialog->setWindowTitle(tr("Restore from backup. Choose backup copy from list"));

    dialog->setMinimumWidth(600);

    connect(btns, &QDialogButtonBox::accepted, dialog, &QDialog::accept);
    connect(btns, &QDialogButtonBox::rejected, dialog, &QDialog::reject);

    auto lt = new QVBoxLayout;
    auto cmb = new QComboBox;

    lt->addWidget(cmb);
    lt->addWidget(btns);

    dialog->setLayout(lt);

    QMap<QString, QFileInfo> foundBackups;
    FileSearcherNoDb fsrch;

    QFileInfo fi_selected;

    for (QFileInfo &fi_Bak : fsrch.retSubFoldersInFolder(QFileInfo(backupDir.absolutePath())))
    {
        QString nm = fi_Bak.fileName().split("--").last();
        foundBackups[nm] = fi_Bak;
        cmb->addItem(nm);
    }

    auto resAccepted = false;

    if (foundBackups.size())
    {
        resAccepted = dialog->exec() == QDialog::Accepted;
    }

    if (resAccepted)
    {
        fi_selected = foundBackups.value(cmb->currentText());
    }

    dialog->deleteLater();

    if (!resAccepted)
    {
        return result;
    }

    auto subFldrsProject = fsrch.retSubFoldersInFolder(QFileInfo(projectDirName));

    QFileInfo fiExcludeBackup, fiExcludeIndex;
    auto boolFoundBkcpFi = false;
    auto boolFoundIndexFi = false;
    for (QFileInfo &fi : subFldrsProject)
    {
        if (fi.fileName() == QString("bkp"))
        {
            fiExcludeBackup = fi;
            boolFoundBkcpFi = true;
            //break;
        }
        if (fi.fileName() == QString("index"))
        {
            fiExcludeIndex = fi;
            boolFoundIndexFi = true;
        }
    }
    if (boolFoundBkcpFi) subFldrsProject.removeOne(fiExcludeBackup);
    if (boolFoundIndexFi) subFldrsProject.removeOne(fiExcludeIndex);

    for (QFileInfo &subFld : subFldrsProject)
    {
        for (QFileInfo &fi_file : fsrch.retFilesInFolder(subFld))
        {
            QFile fl(fi_file.absoluteFilePath());
            fl.remove();
        }

        QDir dirSF(subFld.absoluteFilePath());
        dirSF.cdUp();
        dirSF.rmdir(subFld.fileName());
    }

//    QDir dirHeaderBak(fi_selected.absoluteFilePath().append("/header.ba"))
    QFileInfo fiCurrentHeader(projectHeaderPath);
    QFile fileHeaderBackuped(fi_selected.absoluteFilePath().append("/header.bak/")
                             .append(fiCurrentHeader.fileName()));
    QFile hdrFileOrig(projectHeaderPath);
    if (hdrFileOrig.isOpen())
    {
        hdrFileOrig.close();
    }
    hdrFileOrig.remove();
    fileHeaderBackuped.copy(projectHeaderPath);
    //auto b1 = fileHeaderBackuped.copy(projectHeaderPath);
    //qDebug() << (b1 ? 1: 0);

    QDir dirProjectBackuped(fi_selected.absoluteFilePath().append("/project.bak"));
    QDir dirProject(projectDirName);
    for (QFileInfo &fiDirHigh : fsrch.retSubFoldersInFolder(QFileInfo(dirProjectBackuped.absolutePath())))
    {
        dirProject.mkdir(fiDirHigh.fileName());
        for (QFileInfo &fi_restore : fsrch.retFilesInFolder(fiDirHigh))
        {
            QFile fl(fi_restore.absoluteFilePath());
            fl.copy(QString(projectDirName).append("/").append(fiDirHigh.fileName())
                    .append("/").append(fi_restore.fileName()));
            result = true;
        }
    }

    return result;
}

bool Project::tryForRecoveryProject()
{
    QString projectDirName = QString(projectHeaderPath).remove(".ceproj");

    QDir indexDir(QString(projectDirName).append("/index"));

    if (!indexDir.exists())
    {
        QMessageBox::critical(nullptr, tr("Recovery error"),
                              tr("Project's internal data lost and can't be recovered."));

        return false;
    }

    QFileInfoList fiListImportedFilesInIndex = indexDir.entryInfoList(QStringList("*.data"), QDir::Files);
    if (fiListImportedFilesInIndex.isEmpty())
    {
        QMessageBox::critical(nullptr, tr("Recovery error"),
                              tr("Project's internal data lost and can't be recovered."));
        return false;
    }

    QStringList tmpSl = projectDirName.split("/");
    QString projectOnlyDirNm = tmpSl.takeLast();

    QString tmpRecoveryFolderPath = tmpSl.join("/");
    tmpRecoveryFolderPath.append("/tmpRecovery");

    QDir tmpDir(tmpRecoveryFolderPath);
    tmpDir.cdUp();

    tmpDir.mkpath("tmpRecovery");

    for (QFileInfo &fiImportedInIndex : fiListImportedFilesInIndex)
    {
        QFile fl_(fiImportedInIndex.absoluteFilePath());
        QString newFlNm = fiImportedInIndex.fileName().prepend("/").prepend(tmpRecoveryFolderPath);
        fl_.copy(newFlNm);
    }

    QString projectDirNameForCmdRd = projectDirName;
    projectDirNameForCmdRd.replace("/", "\\");
    projectDirNameForCmdRd.prepend("\"").append("\"");
    QString cmdRd("rd/s/q ");
    cmdRd.append(projectDirNameForCmdRd);

    QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");
    QByteArray ba = cdc->fromUnicode(cmdRd);
    const char *cdata = ba.constData();
    std::system(cdata);

    tmpDir.mkpath(projectOnlyDirNm);
    tmpDir.cd("tmpRecovery");

    QFileInfoList fiListImportedFilesInTMP = tmpDir.entryInfoList(QStringList("*.data"), QDir::Files);
    auto successAnyFile = false;
    QStringList allErrors;
    for (QFileInfo &fiForImport : fiListImportedFilesInTMP)
    {
        auto successOneFile = false;
        QStringList errorsOneFile;
        GceLogicLayer::getInstance().importByFileName(successOneFile, errorsOneFile,
                                                      projectHeaderPath,
                                                      fiForImport.absoluteFilePath());
        if (successOneFile)
        {
            successAnyFile = true;
        }

        if (errorsOneFile.size())
        {
            allErrors.append(fiForImport.fileName().append(":"));
            allErrors.append(errorsOneFile);
        }
    }

    tmpSl.append("tmpRecovery");
    QString tmpDirNameForCmdRd = tmpSl.join("/");
    tmpDirNameForCmdRd.replace("/", "\\");
    tmpDirNameForCmdRd.prepend("\"").append("\"");
    QString cmdRd2("rd/s/q ");
    cmdRd2.append(tmpDirNameForCmdRd);

    ba = cdc->fromUnicode(cmdRd2);
    cdata = ba.constData();
    std::system(cdata);

    if (allErrors.size())
    {
        QMessageBox::warning(nullptr, tr("Errors"),
                             allErrors.join("\n"));
    }

    if (successAnyFile)
    {
        rootElem->setNewNameElement(tr("Recovered project"));
        devicesList->extractDataFromSource(true);
    }

    return successAnyFile;
}

int32_t Project::getPlotsCount()
{
    return plotsList->getItem()->childCount();
}

Device *Project::getDeviceElemBySourceID(uint32_t srcID)
{
    for (Device *dev : devicesList->getDeviceMapPtr()->values())
    {
        if (dev->getSourceID() == srcID)
        {
            return dev;
            break;
        }
    }
    return nullptr;
}

TreeElement *Project::getItemProject()
{
    return elements.value(0);
}

QVector<TreeElement *> Project::getAllElements()
{
    return elements.values().toVector();
}

void Project::sendElementToMap(TreeElement *addedElem)
{
    auto k = lastElementsKey;
    lastElementsKey++;
    elements.insert(k, addedElem);
    addedElem->setKeyInElementsMap(k);
}

void Project::addPlotToPlotList(Plot *addedPlot)
{
    plotsList->addPlot(addedPlot);
}

QList<QAction *> Project::getProjectActionsList()
{
    QList<QAction *> actionsProject;
    actionsProject << saveProject << saveProjectAs
                   << projectPath
                   << showDescription << setProjectTime
                   << designOfProject
                   << renameProject << closeProject;
    return actionsProject;
}

int64_t Project::getFullOffsetFromUTC_msecs()
{
    auto msecsoffsetFromUTC = 0LL;
    auto negate = false;
    auto baTZ = timeZone.id();
    if (baTZ != QByteArray("UTC"))
    {
        baTZ.remove(0, 3); // first "UTC"
        if (baTZ.at(0) == '-')
        {
            negate = true;
        }
        baTZ.remove(0, 1); // + or -
        QString strTZ(baTZ);
        QString tmp_hours = strTZ.split(":").at(0);
        QString tmp_minutes = strTZ.split(":").at(1);
        msecsoffsetFromUTC += tmp_hours.toLongLong()*3600LL*1000LL;
        msecsoffsetFromUTC += tmp_minutes.toLongLong()*60LL*1000LL;
        if (negate)
        {
            msecsoffsetFromUTC *= -1LL;
        }
    }
    return (msecsoffsetFromUTC + timeOffsetMilliseconds);
}

Label *Project::createLabel()
{
    return labelsList->createLabel();
}

void Project::removeLabelById(uint32_t id)
{
    labelsList->removeLabelById(id);
}

void Project::cleanDataSourceFiles()
{
    int res = QMessageBox::warning(nullptr, tr("Warning"), tr("Are you shure?"), QMessageBox::Yes, QMessageBox::No);
    if (res == QMessageBox::No)
    {
        return;
    }

    QString projectHdrPath = getProjectHeaderPath();
    QStringList tmp = projectHdrPath.split("/");
    tmp.removeLast();
    tmp.append("project");
    tmp.append("index");
    QString projectIndexFolderPath = tmp.join("/");

    QDir idxDir(projectIndexFolderPath);
    for (QFileInfo &fiData : idxDir.entryInfoList(QStringList("*.data"), QDir::Files))
    {
        QFile flData(fiData.absoluteFilePath());
        flData.remove();
    }
}

void Project::closeProjectInternal()
{
    QString projectHdrPath = getProjectHeaderPath();
    QStringList tmp = projectHdrPath.split("/");
    tmp.removeLast();
    QString projectFolderPath = tmp.join("/");

    qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                 .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration))
            ->addProjectPathToListOfLastProjectPaths(projectFolderPath);

    GceLogicLayer::getInstance().removeAllDataOnClose();
    qobject_cast<GceDataView*>
            (GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView))
            ->removeAllPlotsOnClose();
    auto ElementsTreeWgt = elements.first()->treeWidget();
    ElementsTreeWgt->clear();
    elements.clear();
}

void Project::forAddPlotFromTemplateByDevNameInTemplateInternal(QString deviceName, bool question)
{
    QDir templDir(qApp->applicationDirPath().append("/templates"));
    QDir userTmplDir(qApp->applicationDirPath().append("/user.templates"));
    if (templDir.exists() || userTmplDir.exists())
    {
        QFileInfoList tfiList, utfiList;
        if (templDir.exists()) tfiList = templDir.entryInfoList(QStringList("*.cetplt"), QDir::Files);
        if (userTmplDir.exists()) utfiList = userTmplDir.entryInfoList(QStringList("*.cetplt"), QDir::Files);
        if (tfiList.size() || utfiList.size())
        {
            QMap<QString, QString> namesTemplatesByFileNames, namesUserTemplatesByFileNames;
            for (QFileInfo &tfi : tfiList)
            {
                QFile fl(tfi.absoluteFilePath());
                auto ok = fl.open(QFile::ReadOnly);
                if (!ok)
                {
                    //QMessageBox::critical(nullptr, tr("Error"), tr("File can't be read"));
                    continue;
                }

                QDataStream odatastr(&fl);
                QString namePlotElement, namePlotInternal;
                odatastr >> namePlotElement;
                odatastr >> namePlotInternal;

                int32_t devCountInStandalonePlot;
                odatastr >> devCountInStandalonePlot;

                auto mustBeAdded = false;

                QStringList devNamesOnStandalonePlot, devNamesOnStandalonePlotWithinGraphs;

                for (int32_t iDevName = 0; iDevName < devCountInStandalonePlot; ++iDevName)
                {
                    QString devName;
                    odatastr >> devName;
                    devNamesOnStandalonePlot << devName;
                }

                double minX, maxX;

                odatastr >> minX;
                odatastr >> maxX;

                int32_t trackCount;
                odatastr >> trackCount;

                while (trackCount > 0)
                {
                    int32_t graphsCount;
                    odatastr >> graphsCount;

                    while (graphsCount > 0)
                    {
                        QString deviceName__, nameGraph;
                        uint32_t sourceID, curveID;
                        int thicknessLine, arrowOffsetFromPlot;
                        QColor currentPenColor, savedStandardPenColor;
                        QPen colorPen;
                        QBrush colorBrush;
                        double scaleMinimumValue, scaleMaximumValue;

                        odatastr >> deviceName__;
                        odatastr >> sourceID;
                        odatastr >> curveID;
                        odatastr >> thicknessLine;
                        odatastr >> currentPenColor;
                        odatastr >> savedStandardPenColor;
                        odatastr >> colorBrush;
                        odatastr >> colorPen;
                        odatastr >> arrowOffsetFromPlot;
                        odatastr >> scaleMinimumValue;
                        odatastr >> scaleMaximumValue;
                        odatastr >> nameGraph;

                        VariableContainer vcontainer;
                        auto data_ = vcontainer.read_container_hash();
                        odatastr >> data_;

                        devNamesOnStandalonePlotWithinGraphs << deviceName__;

                        graphsCount--;
                    }

                    VariableContainer vcontainer;
                    auto data_ = vcontainer.read_container_hash();
                    odatastr >> data_;

                    trackCount--;
                }

                if ((devNamesOnStandalonePlot.size() && devNamesOnStandalonePlot.contains(deviceName))
                        ||
                        (devNamesOnStandalonePlotWithinGraphs.size()
                         && devNamesOnStandalonePlotWithinGraphs.contains(deviceName)))
                {
                    mustBeAdded = true;
                }

                fl.close();
                if (mustBeAdded) namesTemplatesByFileNames.insert(tfi.absoluteFilePath(),
                                                 namePlotInternal.append(" | ")
                                                 .append(tfi.completeBaseName())
                                                 .prepend(": ")
                                                 .prepend(tr("Factory template")));
            }
            for (QFileInfo &utfi : utfiList)
            {
                QFile fl(utfi.absoluteFilePath());
                auto ok = fl.open(QFile::ReadOnly);
                if (!ok) continue;
                QDataStream odatastr(&fl);
                QString namePElem, namePInternal;
                odatastr >> namePElem;
                odatastr >> namePInternal;

                int32_t devCountInStandalonePlot;
                odatastr >> devCountInStandalonePlot;

                auto mustBeAdded = false;

                QStringList devNamesOnStandalonePlot, devNamesOnStandalonePlotWithinGraphs;

                for (int32_t iDevName = 0; iDevName < devCountInStandalonePlot; ++iDevName)
                {
                    QString devName;
                    odatastr >> devName;
                    devNamesOnStandalonePlot << devName;
                }

                double minX, maxX;

                odatastr >> minX;
                odatastr >> maxX;

                int32_t trackCount;
                odatastr >> trackCount;

                while (trackCount > 0)
                {
                    int32_t graphsCount;
                    odatastr >> graphsCount;

                    while (graphsCount > 0)
                    {
                        QString deviceName__, nameGraph;
                        uint32_t sourceID, curveID;
                        int thicknessLine, arrowOffsetFromPlot;
                        QColor currentPenColor, savedStandardPenColor;
                        QPen colorPen;
                        QBrush colorBrush;
                        double scaleMinimumValue, scaleMaximumValue;

                        odatastr >> deviceName__;
                        odatastr >> sourceID;
                        odatastr >> curveID;
                        odatastr >> thicknessLine;
                        odatastr >> currentPenColor;
                        odatastr >> savedStandardPenColor;
                        odatastr >> colorBrush;
                        odatastr >> colorPen;
                        odatastr >> arrowOffsetFromPlot;
                        odatastr >> scaleMinimumValue;
                        odatastr >> scaleMaximumValue;
                        odatastr >> nameGraph;

                        VariableContainer vcontainer;
                        auto data_ = vcontainer.read_container_hash();
                        odatastr >> data_;

                        devNamesOnStandalonePlotWithinGraphs << deviceName__;

                        graphsCount--;
                    }

                    VariableContainer vcontainer;
                    auto data_ = vcontainer.read_container_hash();
                    odatastr >> data_;

                    trackCount--;
                }

                if ((devNamesOnStandalonePlot.size() && devNamesOnStandalonePlot.contains(deviceName))
                        ||
                        (devNamesOnStandalonePlotWithinGraphs.size()
                         && devNamesOnStandalonePlotWithinGraphs.contains(deviceName)))
                {
                    mustBeAdded = true;
                }

                fl.close();
                if (mustBeAdded) namesUserTemplatesByFileNames.insert(utfi.absoluteFilePath(),
                                                     namePInternal.append(" | ")
                                                     .append(utfi.completeBaseName())
                                                     .prepend(": ")
                                                     .prepend(tr("User template")));
            }
            if (namesTemplatesByFileNames.size() || namesUserTemplatesByFileNames.size())
            {
                if (question)
                {
                    auto res = QMessageBox::question(nullptr, tr("Found compatible plot templates"),
                                                     deviceName.prepend(tr("Found plot templates with "))
                                                     .append(tr(" device")).append("\n")
                                                     .append(tr("Do you want to open this plots?"))
                                                     );

                    if (res != QMessageBox::Yes)
                    {
                        return;
                    }
                }

                auto dlg = new QDialog;
                auto btns = new QDialogButtonBox(
                            QDialogButtonBox::Yes|QDialogButtonBox::No|QDialogButtonBox::Cancel);
                auto lbl = new QLabel(tr("You may choice ready plot template:")
                                       .append("\t").append(tr("(or press No to add empty plot)")));
                auto cmb = new QComboBox;
                dlg->setWindowTitle(tr("Select plot template"));
                auto lt_c = new QVBoxLayout;
                dlg->setLayout(lt_c);
                lt_c->addWidget(lbl);
                lt_c->addWidget(cmb);
                lt_c->addWidget(btns);
                for (QString &name : namesTemplatesByFileNames.values())
                {
                    cmb->addItem(name);
                }
                for (QString &name : namesUserTemplatesByFileNames.values())
                {
                    cmb->addItem(name);
                }
                connect(btns, &QDialogButtonBox::clicked /*SIGNAL(clicked(QAbstractButton*)),*/,
                        this, [&](QAbstractButton *bt){
                    if (bt == btns->button(QDialogButtonBox::Yes))
                    {
                        auto choosedName = cmb->currentText();

                        auto fName = namesTemplatesByFileNames.key(choosedName, "");

                        if (fName.isEmpty())
                        {
                            fName = namesUserTemplatesByFileNames.key(choosedName);
                        }

                        auto o = GceLogicLayer::getInstance()
                                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
                        if (!o)
                        {
                            return;
                        }
                        auto dv = qobject_cast<GceDataView*>(o);

                        dv->openPlotFromTemplateFile(fName);
                    }
                    else if (bt == btns->button(QDialogButtonBox::No))
                    {
                        slotForAddPlot();
                    }
                    dlg->accept();
                });

                dlg->exec();
            }
            else
            {
                if (!question)
                {
                    QMessageBox::information(nullptr, tr("Information"),
                                             tr("There are no plots with device ").append(deviceName));
                }
            }
        }
    }
}

void Project::slotOpenProject()
{
    openProject();
}

void Project::slotSaveProject()
{
    while (_srcIds4removeAtSaving.size())
    {
        auto srcID = _srcIds4removeAtSaving.takeFirst();

        GceLogicLayer::getInstance().removeOneSourceData(srcID);

        QString pathDir = projectHeaderPath;
        pathDir.remove(".ceproj");
        QString pathDir4remove = pathDir;
        pathDir4remove.append("/").append(QString::number(srcID));

        QDir dir4remove(pathDir4remove);
        for (QFileInfo &fi : dir4remove.entryInfoList(QDir::Files))
        {
            QFile fl(fi.absoluteFilePath());
            fl.remove();
        }

        QDir prjDir(pathDir);
        prjDir.remove(QString::number(srcID));
    }

    QFile headerFile(projectHeaderPath);
    auto ok = headerFile.open(QFile::WriteOnly);
    if (!ok)
    {
        QMessageBox::critical(nullptr, tr("Error"), tr("Can't save project header file (may be it is busy):").append("\n")
                              .append(projectHeaderPath));
        return;
    }

    ///!!!saveCopyToBackupForRecovery();

#ifdef Q_OS_WIN

    auto tmp_ = projectHeaderPath.split("/");
    tmp_.removeLast();
    auto svProjectFolder = tmp_.join("/");

    QDir dirProjectFolder(svProjectFolder);
    dirProjectFolder.mkpath("project/index");

    QFile flDesktopIni(QString().append(svProjectFolder).append("/desktop.ini"));
    if (flDesktopIni.open(QFile::WriteOnly|QFile::Text))
    {
        QTextStream tstr(&flDesktopIni);
        tstr << "[.ShellClassInfo]" << "\n";
        tstr << "IconResource=C:\\Windows\\system32\\SHELL32.dll,84" << "\n";
        flDesktopIni.close();
    }

    QString flFullPath = flDesktopIni.fileName();
    flFullPath.replace("/",  "\\");
    QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");

//    QByteArray ba = cdc->fromUnicode(flFullPath);
//    LPCSTR l_fName = (LPCSTR) ba.constData();

//    SetFileAttributesA(l_fName, FILE_ATTRIBUTE_NORMAL);
//    SetFileAttributesA(l_fName, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);

    QString attr_1 = QString().append("attrib -a ").append("\"")
            .append(flFullPath).append("\""); // cmd /k
    QString attr_2 = QString().append("attrib +h +s ").append("\"")
            .append(flFullPath).append("\"");

    auto tmp = flFullPath.split("\\");
    tmp.removeLast();

    QString attr_3 = QString().append("attrib +r ").append("\"")
            .append(tmp.join("\\")).append("\"");

    QByteArray ba = cdc->fromUnicode(attr_1);
    const char *cdata = ba.constData();
    std::system(cdata);

    ba = cdc->fromUnicode(attr_2);
    cdata = ba.constData();
    std::system(cdata);

    ba = cdc->fromUnicode(attr_3);
    cdata = ba.constData();
    std::system(cdata);

#endif

    emit GceLogicLayer::getInstance().sigOpenStartProgressBarWindow(tr("Saving project..."));
    QDataStream dstreamout(&headerFile);
    dstreamout << getProjectName();
    dstreamout << getDescription();
    dstreamout << GceLogicLayer::getInstance().getSourcesIds();
                  //devicesList->getDeviceMapPtr()->keys(); // сохранение списка ID устройств

    VariableContainer vcontainer;

    // что-то записать в контейнер...
    auto idTimeZone = timeZone.id();
    vcontainer.set_value<decltype(idTimeZone)>("time_zone_id_project", idTimeZone);
    vcontainer.set_value<decltype(timeOffsetMilliseconds)>("time_offset_msecs", timeOffsetMilliseconds);
    vcontainer.set_value<int>("project_version_major", CURRENT_PROJECT_VERSION_MAJOR);
    vcontainer.set_value<int>("project_version_minor", CURRENT_PROJECT_VERSION_MINOR);
//    vcontainer.set_value<int>("number_of_devices", devicesList->getDeviceMapPtr()->keys().size());
//    for (int i = 0; i < devicesList->getDeviceMapPtr()->keys().size(); ++i)
//    {
//        QString sign = QString::number(i).append("_index_device");
//        vcontainer.set_value<uint32_t>(sign, devicesList->getDeviceMapPtr()->keys().at(i));
//    }
    vcontainer.set_value<int>("font_time_zone", fontPixelSize_UtcHeader);
    vcontainer.set_value<int>("font_axis_time_labels", fontPointSize_TimeScale);
    vcontainer.set_value<int>("font_graphics_header_name_graphic", fontPointSize_GraphicsNamesInHeader);
    vcontainer.set_value<int>("font_graphics_header_scale_min_max",
                              fontPointSize_GraphicsMinMaxValuesInHeader);

    dstreamout << vcontainer.read_container_hash();

    headerFile.close();

    for (Device *dev : devicesList->getDeviceMapPtr()->values())
    {
        uint32_t srcId = dev->getSourceID();
        GceLogicLayer::getInstance().saveSourceData(srcId, projectHeaderPath);
    }

    GceLogicLayer::getInstance().saveIndexedDataHeader(projectHeaderPath);

    emit sigSaveProjectPlots(projectHeaderPath);

    emit GceLogicLayer::getInstance().sigStopCloseProgressBarWindow();

    auto hdr = projectHeaderPath;
    auto tmp__ = hdr.split("/");
    tmp__.removeLast();
    auto path = tmp__.join("/");

    qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                 .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration))
            ->addProjectPathToListOfLastProjectPaths(path);

    setSaved(true);
}

void Project::slotSaveProjectAs()
{
    QString projectHeaderPathOld = projectHeaderPath;
    QString projectFolderOld = projectHeaderPathOld;
    projectFolderOld.remove(".ceproj");

//    QString svProjectFolderName = QFileDialog::getSaveFileName(
//                nullptr, tr("Save project"),
//                QString(), QString(tr("GM-CurveEdit project header file (*.ceproj)"))
//                );

    QString svProjectFolderName = QFileDialog::getSaveFileName(
                nullptr, tr("Save project"),
                QString(), QString(tr("CurveEdit project folder (*.DIRCE)"))
                );

    if (svProjectFolderName.isEmpty())
    {
        return;
    }

    if (svProjectFolderName.right(6) != QString(".DIRCE"))
    {
        svProjectFolderName.append(".DIRCE");
    }

    QStringList tmp = svProjectFolderName.split("/");
    QString toCreate = tmp.takeLast();
    QDir dirSuper(tmp.join("/"));
    dirSuper.mkdir(toCreate);

    //

    svProjectFolderName.append("/project.ceproj");

    auto projectFolder = svProjectFolderName;
    projectFolder.remove(".ceproj");
    QFileInfo fiSv(projectFolder);
    FileSearcherNoDb fsrchr;
    auto FIlist = fsrchr.retFilesInFolderAndSubFolders(fiSv);
    while (FIlist.size())
    {
        for (QFileInfo &fi : FIlist)
        {
            QFile fl(fi.absoluteFilePath());
            fl.remove();
        }
        FIlist = fsrchr.retFilesInFolderAndSubFolders(fiSv);
    }

    auto saved_PHP = projectHeaderPath;

    projectHeaderPath = svProjectFolderName;
    slotSaveProject();

    QDir projectFolderOld_dir(projectFolderOld);
    if (!projectFolderOld_dir.exists()) return;
    for (QFileInfo &fi : projectFolderOld_dir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot))
    {
        auto isInt = false;
        fi.fileName().toInt(&isInt);
        if (isInt)
        {
            continue;
        }
        QDir elem(fi.absoluteFilePath());
        QString elemNewPath = fi.fileName().prepend("/").prepend(projectFolder);
        elem.mkpath(elemNewPath);
        for (QFileInfo &subfi : elem.entryInfoList(QDir::Files))
        {
            QString subFiNewPath = subfi.fileName().prepend("/").prepend(elemNewPath);
            QFile fl(subfi.absoluteFilePath());
            fl.copy(subFiNewPath);
        }
    }

    projectHeaderPath = saved_PHP;

    return;

    closeProjectInternal();
    this->deleteLater();

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameProjectManager);
    auto prjMgr = qobject_cast<ProjectManager*>(o);

    QStringList _tmp_ = projectFolder.split("/");
    _tmp_.removeLast();
    projectFolder = _tmp_.join("/");

    auto pa = prjMgr->getProjectsArrayPtr();
    pa->remove(pa->lastKey());

    prjMgr->openExistingProject_public(projectFolder, false);
}

void Project::slotCloseProject()
{
    closeProjectInternal();
}

void Project::slotRecoveryDataSourcesOfProject()
{
    auto result = QMessageBox::question(nullptr, tr("Recovery data sources ..."),
                                        tr("Do you want to recovery data sources in index of project?")
                                        .append("\n")
                                        .append(tr("If you say YES, you may recovery indexed data,"))
                                        .append("\n")
                                        .append(tr("but all plots, curves, its adjustings, etc, will be lost!")),
                                        QMessageBox::Yes, QMessageBox::No);
    if (result == QMessageBox::No)
    {
        return;
    }

    GceLogicLayer::getInstance().removeAllDataOnClose();
    qobject_cast<GceDataView*>
            (GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                 ObjectsNames::objNameGceDataView))
            ->removeAllPlotsOnClose();
    //auto ElementsTreeWgt = elements.first()->treeWidget();
    //ElementsTreeWgt->clear();
//    auto prjElem = elements.first();
//    auto kkey = elements.key(prjElem);
//    for (int ch = 0; ch < prjElem->childCount(); ch++)
//    {
//        prjElem->removeChild(prjElem->child(0));
//    }
//    elements.clear();
//    elements.insert(kkey, prjElem);

    devicesList->clearAll();
    plotsList->clearAll();
    labelsList->clearAll();

    //QStringList _tmp_ = projectHeaderPath.split("/");
    //_tmp_.removeLast();

    tryForRecoveryProject();

//    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
//                ObjectsNames::objNameProjectManager);
//    auto prjMgr = qobject_cast<ProjectManager*>(o);

//    prjMgr->getProjectsArrayPtr()->take(currentIndexProject);

//    this->deleteLater();

//    prjMgr->openExistingProject_public(_tmp_.join("/"), false);
}

void Project::slotRestoreProjectFromBackup()
{
    auto result = QMessageBox::question(nullptr, tr("Restore project ..."),
                                        tr("Do you want to restore project from backup?"),
                                        QMessageBox::Yes, QMessageBox::No);
    if (result == QMessageBox::No)
    {
        return;
    }

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                ObjectsNames::objNameProjectManager);
    auto prjMgr = qobject_cast<ProjectManager*>(o);

    prjMgr->getProjectsArrayPtr()->take(currentIndexProject);

    GceLogicLayer::getInstance().removeAllDataOnClose();
    qobject_cast<GceDataView*>
            (GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(
                 ObjectsNames::objNameGceDataView))
            ->removeAllPlotsOnClose();
    auto ElementsTreeWgt = elements.first()->treeWidget();
    ElementsTreeWgt->clear();
    elements.clear();
//    ElementsTreeWgt->clear();
//    ElementsTreeWgt->addTopLevelItem(first_prjElem);
//    elements.insert(0, first_prjElem);

//    auto success = false;
//    do
//    {
//        success = restoreFromBackup();

//    }
//    while (!success);

    QStringList _tmp_ = projectHeaderPath.split("/");
    _tmp_.removeLast();

    restoreFromBackup();

    this->deleteLater();

    //

    prjMgr->openExistingProject_public(_tmp_.join("/"), false);

//    if (resultB)
//    {
//        resultB = openProject();
//    }

//    if (!resultB)
//    {
//        //result = QMessageBox::Yes;
//        while (!resultB)
//        {
//            result = QMessageBox::question(nullptr, tr("Restore project ..."),
//                                                tr("Do you want to restore project from backup with another date and time?"),
//                                                QMessageBox::Yes, QMessageBox::No);
//            if (result == QMessageBox::No)
//            {
//                break;
//            }
//            resultB = restoreFromBackup();
//            if (resultB)
//            {
//                resultB = openProject();
//            }
//        }
//    }

//    if (!resultB)
//    {
//        this->tryForRecoveryProject();
//    }
}

void Project::slotActionsList()
{
    //
}

void Project::slotForAddPlot()
{
    auto nPlot = new Plot(currentIndexProject, plotsList->getNextNewPlotIndex(), this, this);
    plotsList->addPlot(nPlot);
//    firstPlot->rootItem
    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
    if (!o)
    {
        return;
    }
    auto dv = qobject_cast<GceDataView*>(o);
    dv->addPlot(nPlot, true);
}

void Project::slotForAddPlotFromSelectedTemplate()
{
    QDir templDir(qApp->applicationDirPath().append("/templates"));
    QDir userTmplDir(qApp->applicationDirPath().append("/user.templates"));
    if (templDir.exists() || userTmplDir.exists())
    {
        QFileInfoList tfiList, utfiList;
        if (templDir.exists()) tfiList = templDir.entryInfoList(QStringList("*.cetplt"), QDir::Files);
        if (userTmplDir.exists()) utfiList = userTmplDir.entryInfoList(QStringList("*.cetplt"), QDir::Files);
        if (tfiList.size() || utfiList.size())
        {
            QMap<QString, QString> namesTemplatesByFileNames, namesUserTemplatesByFileNames;
            for (QFileInfo &tfi : tfiList)
            {
                QFile fl(tfi.absoluteFilePath());
                auto ok = fl.open(QFile::ReadOnly);
                if (!ok)
                {
                    //QMessageBox::critical(nullptr, tr("Error"), tr("File can't be read"));
                    continue;
                }

                QDataStream odatastr(&fl);
                QString namePlotElement, namePlotInternal;
                odatastr >> namePlotElement;
                odatastr >> namePlotInternal;

                //if (namePlotElement != namePlotInternal)
                //    namePlotInternal = namePlotElement;
                fl.close();
                namesTemplatesByFileNames.insert(tfi.absoluteFilePath(),
                                                 namePlotInternal.append(" | ")
                                                 .append(tfi.completeBaseName())
                                                 .prepend(": ")
                                                 .prepend(tr("Factory template")));
            }
            for (QFileInfo &utfi : utfiList)
            {
                QFile fl(utfi.absoluteFilePath());
                auto ok = fl.open(QFile::ReadOnly);
                if (!ok) continue;
                QDataStream odatastr(&fl);
                QString namePElem, namePInternal;
                odatastr >> namePElem;
                odatastr >> namePInternal;

                fl.close();
                namesUserTemplatesByFileNames.insert(utfi.absoluteFilePath(),
                                                     namePInternal.append(" | ")
                                                     .append(utfi.completeBaseName())
                                                     .prepend(": ")
                                                     .prepend(tr("User template")));
            }
            if (namesTemplatesByFileNames.size() || namesUserTemplatesByFileNames.size())
            {
                auto dlg = new QDialog;
                auto btns = new QDialogButtonBox(
                            QDialogButtonBox::Yes|QDialogButtonBox::No|QDialogButtonBox::Cancel);
                auto lbl = new QLabel(tr("You may choice ready plot template:")
                                       .append("\t").append(tr("(or press No to add empty plot)")));
                auto cmb = new QComboBox;
                dlg->setWindowTitle(tr("Select plot template"));
                auto lt_c = new QVBoxLayout;
                dlg->setLayout(lt_c);
                lt_c->addWidget(lbl);
                lt_c->addWidget(cmb);
                lt_c->addWidget(btns);
                for (QString &name : namesTemplatesByFileNames.values())
                {
                    cmb->addItem(name);
                }
                for (QString &name : namesUserTemplatesByFileNames.values())
                {
                    cmb->addItem(name);
                }
                connect(btns, &QDialogButtonBox::clicked /*SIGNAL(clicked(QAbstractButton*)),*/,
                        this, [&](QAbstractButton *bt){
                    if (bt == btns->button(QDialogButtonBox::Yes))
                    {
                        auto choosedName = cmb->currentText();

                        auto fName = namesTemplatesByFileNames.key(choosedName, "");

                        if (fName.isEmpty())
                        {
                            fName = namesUserTemplatesByFileNames.key(choosedName);
                        }

                        auto o = GceLogicLayer::getInstance()
                                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
                        if (!o)
                        {
                            return;
                        }
                        auto dv = qobject_cast<GceDataView*>(o);

                        dv->openPlotFromTemplateFile(fName);
                    }
                    else if (bt == btns->button(QDialogButtonBox::No))
                    {
                        slotForAddPlot();
                    }
                    dlg->accept();
                });

                dlg->exec();
            }
        }
        else
        {
            //slotForAddPlot();
            auto res = QMessageBox::question(nullptr, tr("Templates not exist"),
                                             tr("There are no any plot templates")
                                  .append("\n").append(tr("Do you want to add empty plot?")));
            if (res == QMessageBox::Yes)
            {
                slotForAddPlot();
            }
        }
    }
    else
    {
        //slotForAddPlot();
        auto res = QMessageBox::question(nullptr, tr("Templates not exist"),
                                         tr("There are no any plot templates")
                              .append("\n").append(tr("Do you want to add empty plot?")));
        if (res == QMessageBox::Yes)
        {
            slotForAddPlot();
        }
    }
}

void Project::slotForAddPlotFromTemplateByDevNameInTemplate(QString deviceName)
{
    auto o = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
    auto gdv = qobject_cast<GceDataView*>(o);

    QStringList allDevsInPlots;
    for (SpecialWidgetForPlot *pw : gdv->getPlotWidgetsList())
    {
        allDevsInPlots.append( pw->getDevNames().toList() );
    }

    if (allDevsInPlots.size() && allDevsInPlots.contains(deviceName))
    {
        return;
    }

    forAddPlotFromTemplateByDevNameInTemplateInternal(deviceName, true);
}

void Project::slotForAddPlotFromTemplateByDevNameInTemplateAtDeviceSubmenu(QString deviceName)
{
    forAddPlotFromTemplateByDevNameInTemplateInternal(deviceName, false);
}

void Project::slotShowDescr()
{
    auto dlgDescription = new QDialog;
    auto lt = new QVBoxLayout;
    auto teDescr = new QTextEdit;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok, dlgDescription);
    dlgDescription->setLayout(lt);
    lt->addWidget(teDescr);
    lt->addWidget(btns);
    teDescr->setPlainText(getDescription());
    auto font = teDescr->font();
    auto pointSize = font.pointSize();
    pointSize += 4;
    font.setPointSize(pointSize);
    teDescr->setFont(font);
    connect(btns, SIGNAL(accepted()), dlgDescription, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlgDescription, SLOT(reject()));
    dlgDescription->setWindowTitle(tr("Description of project"));
    auto result = dlgDescription->exec();
    if (result == QDialog::Accepted && getDescription() != teDescr->toPlainText())
    {
        setDescription(teDescr->toPlainText());
    }

    dlgDescription->deleteLater();
}

void Project::slotShowProjectTimeSet()
{
    auto timeZoneComboBox = new QComboBox;
    // Fill in combo box.
    QList<QByteArray> ids = QTimeZone::availableTimeZoneIds();
    for (QByteArray &id : ids)
    {
        auto zone = QTimeZone(id);
        if (QLocale::countryToString(zone.country()).toLower() != "default")
        {
            continue;
        }
        if (QString(id).left(3) != "UTC")
        {
            continue;
        }
        if (QString(id).right(5) == "00:00")
        {
            continue;
        }
        timeZoneComboBox->addItem(id);
    }
    for (QByteArray &id : ids)
    {
        auto zone = QTimeZone(id);
        if (zone == timeZone)
        {
            timeZoneComboBox->setCurrentIndex(timeZoneComboBox->findText(id));
        }
    }
    //auto shiftTime_TimeEdit = new QTimeEdit;
    //shiftTime_TimeEdit->setDisplayFormat("mm"/*"hh:mm:ss.zzz"*/);
    //auto isForward_Chk = new QCheckBox(tr("Forward time shift?"));
    //isForward_Chk->setChecked(true);
    auto lt = new QVBoxLayout;
    lt->addWidget(new QLabel(tr("Select time zone:")));
    lt->addWidget(timeZoneComboBox);
    lt->addWidget(new QLabel(tr("UTC is coordinated universal time.").append("\n").append(tr("It is the same as Greenvich Mean Time (GMT)"))));
    //lt->addWidget(shiftTime_TimeEdit);
    //lt->addWidget(isForward_Chk);
//    QTime t(0,0,0,0);
//    if (qAbs(timeOffsetMilliseconds) > 0)
//    {
//        if (timeOffsetMilliseconds < 0)
//        {
//            //isForward_Chk->setChecked(false);
//            auto tMs = timeOffsetMilliseconds * -1L;
//            t = QTime::fromMSecsSinceStartOfDay(tMs);
//        }
//        else
//        {
//            t = QTime::fromMSecsSinceStartOfDay(timeOffsetMilliseconds);
//        }
//    }
    //shiftTime_TimeEdit->setTime(t);
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    lt->addWidget(btns);
    auto dlg = new QDialog;
    dlg->setWindowTitle(tr("Project time set"));
    connect(btns, SIGNAL(accepted()), dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlg, SLOT(reject()));
    dlg->setLayout(lt);

    auto result = dlg->exec();
    if (result == QDialog::Accepted)
    {
        timeZone = QTimeZone(timeZoneComboBox->currentText().toLatin1());
        emit sigChangeTimeZone(timeZone);

        auto ids_list = GceLogicLayer::getInstance().getCurvesIdsWithSourcesIds();
        QList< decltype(ids_list.first().second) > srcIdsList;// = QList< decltype(ids_list.first().second) >();
        auto szIL = ids_list.size();
        for (decltype (szIL) i_p = 0; i_p < szIL; ++i_p)
        {
            if (srcIdsList.isEmpty())
            {
                srcIdsList << ids_list.value(i_p).second;
            }
            else if (!srcIdsList.contains(ids_list.value(i_p).second))
            {
                srcIdsList << ids_list.value(i_p).second;
            }
        }
        for (auto srcID : srcIdsList)
        {
            emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(srcID, -1);
        }

        //auto new_t = timeZone.
        //timeOffsetMilliseconds = static_cast<int64_t>(new_t.msecsSinceStartOfDay());
//        if (!isForward_Chk->isChecked())
//        {
//            auto negate = static_cast<int64_t>(-1);
//            timeOffsetMilliseconds *= negate;
//        }
        //
    }

    dlg->deleteLater();
}

void Project::slotRenameProject()
{
    auto dlgRename = new QDialog;
    auto lt = new QHBoxLayout;
    auto lnedRename = new QLineEdit;
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel, dlgRename);
    dlgRename->setLayout(lt);
    lt->addWidget(lnedRename);
    lnedRename->setMinimumWidth(50);
    lnedRename->setText(getProjectName());
    lt->addWidget(btns);
    connect(btns, SIGNAL(accepted()), dlgRename, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlgRename, SLOT(reject()));
    dlgRename->setWindowTitle(tr("Renaming project ..."));
    auto result = dlgRename->exec();
    if (result == QDialog::Accepted && getProjectName() != lnedRename->text())
    {
        auto nameNew = lnedRename->text();
        if (nameNew.isEmpty())
        {
            QMessageBox::critical(nullptr, tr("Error"),
                                  tr("Project name can't be empty"));
        }
        else
        {
            setProjectName(nameNew);
        }
    }

    dlgRename->deleteLater();
}

void Project::slotDeviceRenamed_EmptyPlotDevElements(QString nameNew, QString nameOld, uint32_t sourceID)
{
    Device *device_sender = qobject_cast<Device*>(sender());
    emit sigDeviceRenamedForEmptyPlotDeviceElements(nameNew, nameOld, sourceID, device_sender);
}

void Project::slotRemoveDevice(QString devNm, uint32_t srcID)
{
    devicesList->getDeviceMapPtr()->remove(srcID);

    auto o = GceLogicLayer::getInstance()
            .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
    if (o)
    {
        auto dv = qobject_cast<GceDataView*>(o);
        dv->removeDeviceFromPlots(devNm, srcID);
    }

    GceLogicLayer::getInstance().removeOneSourceData(srcID);

    QString pathDir = projectHeaderPath;
    pathDir.remove(".ceproj");
    QString pathDir4remove = pathDir;
    pathDir4remove.append("/").append(QString::number(srcID));

    QDir dir4remove(pathDir4remove);
    for (QFileInfo &fi : dir4remove.entryInfoList(QDir::Files))
    {
        QFile fl(fi.absoluteFilePath());
        fl.remove();
    }

    QDir prjDir(pathDir);
    prjDir.remove(QString::number(srcID));

    slotSaveProject();
}

void Project::slotDesignSettings()
{
    QDialog dlg;
    dlg.setWindowTitle(tr("Design of project"));
    QDialogButtonBox *btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    connect(btns, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), &dlg, SLOT(reject()));
    auto lt = new QVBoxLayout;
    auto ltFontUtcHeader = new QHBoxLayout;
    auto ltFontTimeScale = new QHBoxLayout;
    auto ltFontGrHdrNames = new QHBoxLayout;
    auto ltFontGrHdrMinMax = new QHBoxLayout;
    lt->addLayout(ltFontUtcHeader);
    lt->addLayout(ltFontTimeScale);
    lt->addLayout(ltFontGrHdrNames);
    lt->addLayout(ltFontGrHdrMinMax);
    lt->addWidget(btns);
    auto lnedFontUtcHeader = new QLineEdit;
    lnedFontUtcHeader->setText(QString::number(fontPixelSize_UtcHeader));
    auto ivldUtcHdr = new QIntValidator(4, 50);
    lnedFontUtcHeader->setValidator(ivldUtcHdr);
    auto lnedFontTimeScale = new QLineEdit;
    lnedFontTimeScale->setText(QString::number(fontPointSize_TimeScale));
    auto ivldTmSc = new QIntValidator(4, 50);
    lnedFontTimeScale->setValidator(ivldTmSc);
    auto lnedFontGrHdrNames =new QLineEdit;
    lnedFontGrHdrNames->setText(QString::number(fontPointSize_GraphicsNamesInHeader));
    auto ivldGHN = new QIntValidator(4, 50);
    lnedFontGrHdrNames->setValidator(ivldGHN);
    auto lnedFontGrHdrMinMax = new QLineEdit;
    lnedFontGrHdrMinMax->setText(QString::number(fontPointSize_GraphicsMinMaxValuesInHeader));
    auto ivldGHMM = new QIntValidator(4, 50);
    lnedFontGrHdrMinMax->setValidator(ivldGHMM);
    auto lblFontUtcHeader = new QLabel(tr("Timezone information header font size"));
    auto lblFontTimeScale = new QLabel(tr("Time axis labels font size"));
    auto lblFontGrHdrNames = new QLabel(tr("Graphics names in tracks headers font size"));
    auto lblFontGrHdrMinMax = new QLabel(tr("Graphics min/max scale values labels font size"));
    ltFontUtcHeader->addWidget(lblFontUtcHeader);
    ltFontUtcHeader->addWidget(lnedFontUtcHeader, 0, Qt::AlignRight);
    ltFontTimeScale->addWidget(lblFontTimeScale);
    ltFontTimeScale->addWidget(lnedFontTimeScale, 0, Qt::AlignRight);
    ltFontGrHdrNames->addWidget(lblFontGrHdrNames);
    ltFontGrHdrNames->addWidget(lnedFontGrHdrNames, 0, Qt::AlignRight);
    ltFontGrHdrMinMax->addWidget(lblFontGrHdrMinMax);
    ltFontGrHdrMinMax->addWidget(lnedFontGrHdrMinMax, 0, Qt::AlignRight);

    lnedFontTimeScale->hide();
    lblFontTimeScale->hide();

    dlg.setLayout(lt);

    if (dlg.exec() != QDialog::Accepted)
    {
        return;
    }

    // accepted:
    auto fontPS_UtcHdr = lnedFontUtcHeader->text().toInt();
    auto fontPS_GHN = lnedFontGrHdrNames->text().toInt();
    auto fontPS_GHMM= lnedFontGrHdrMinMax->text().toInt();

    if (fontPixelSize_UtcHeader != fontPS_UtcHdr)
    {
        fontPixelSize_UtcHeader = fontPS_UtcHdr;
        emit sigChangeFontUtcHeaderSize(fontPixelSize_UtcHeader);
    }

    if (fontPointSize_GraphicsNamesInHeader != fontPS_GHN)
    {
        fontPointSize_GraphicsNamesInHeader = fontPS_GHN;
        emit sigChangeFontGraphicsHeaderNamesSize(fontPointSize_GraphicsNamesInHeader);
    }

    if (fontPointSize_GraphicsMinMaxValuesInHeader != fontPS_GHMM)
    {
        fontPointSize_GraphicsMinMaxValuesInHeader = fontPS_GHMM;
        emit sigChangeFontGraphicsHeaderMinMaxSize(fontPointSize_GraphicsMinMaxValuesInHeader);
    }
}

void Project::slotProjectPath()
{
    QDialog dlg;
    dlg.setWindowTitle(tr("Project path"));
    auto btnOk = new QPushButton("OK");
    connect(btnOk, SIGNAL(clicked(bool)), &dlg, SLOT(accept()));
    auto tePath = new QTextEdit;
    auto lt = new QVBoxLayout;
    dlg.setLayout(lt);
    lt->addWidget(tePath);
    lt->addWidget(btnOk);

    QStringList tmp__ = projectHeaderPath.split("/");
    tmp__.removeLast();

    QString path__;
#ifdef LNX
    path__ = tmp__.join("/").prepend("/");
#else
    path__ = tmp__.join("\\");
#endif

    tePath->append(path__);
    tePath->setReadOnly(true);

    dlg.exec();
}
