#ifndef TRACK_H
#define TRACK_H

#include <QObject>
#include <QWidget>
#include "qcustomplot.h"
#include "treeelement.h"

class Curve;
class ObjectsPack;

constexpr int32_t LINE_DEFAULT_THICKNESS = 1;
constexpr int32_t TRACK_HEADER_WIDTH = 26;

struct GraphWithSettings
{
    uint32_t sourceID;
    uint32_t curveID;
    bool     loaded;
    bool     useNullValues;
    bool     frozenValuesScaleForAutoScale;
    double   nullValueIfUsed;
    double   scaleMinimumValue;
    double   scaleMaximumValue;
    double   divisionValueForAxis; // цена деления шкалы
    bool     divisionValueIsFixed; // фиксация цены деления
    int32_t  thicknessLine;
    QBrush   colorBrush;
    QPen     colorPen;
    QColor   currentPenColor;
    QColor   savedStandardPenColor;
    QCPGraph *graphic;
    bool     graphicScatterStyleIsSquare;
    QString  nameGraph;
    QString  pseudoNameGraph;
    QString  deviceName;
    QCPAxis  *axisSpec;
    QCPItemText *labelNameGraphic;
    QCPItemTracer *tracerForArrowUpper;
    QCPItemTracer *tracerForArrowLower;
    QCPItemTracer *tracerForArrowMiddle;
    QCPItemLine *arrow;
    int         arrowOffsetFromPlot;
    QCPItemText *lMaximum;
    QCPItemText *lMinimum;
    void setScaleMinValue(double newMinimum)
    {
        scaleMinimumValue = newMinimum;
        lMinimum->setText(QString::number(scaleMinimumValue).append(" ").prepend(" "));
    }
    void setScaleMaxValue(double newMaximum)
    {
        scaleMaximumValue = newMaximum;
        lMaximum->setText(QString::number(scaleMaximumValue).append(" ").prepend(" "));
    }
    void updateLabelGraphicName()
    {
        if (pseudoNameGraph.isEmpty())
        {
            labelNameGraphic->setText(nameGraph);
        }
        else
        {
            labelNameGraphic->setText(pseudoNameGraph);
        }
    }
};

class DataLabel;
class SpecialWidgetForPlot;
struct LoggingDatasBlock;
class Plot;

class Track : public QCustomPlot
{
    Q_OBJECT
public:
    explicit Track(Plot *plotParent, int indexInListOfTracks, QObject *prjPtr, QWidget *parent = nullptr);
    void        setTrackID(uint32_t trId) { trackID = trId; }
    uint32_t    getTrackID() { return trackID; }
    void        setPlotPtr(SpecialWidgetForPlot *plotPtr) { plotPointer = plotPtr; }
    SpecialWidgetForPlot *getPLotPtr() { return plotPointer; }
    TreeElement *getItem();
    void        setParentForItem(TreeElement *prntElem);
    QVector<GraphWithSettings*> graphicsArray;
    QVector<DataLabel*> labelsArray;
    QVector<QCPItemRect*> noDataAreas;
    std::vector<std::pair<double, double>> datasBlocks;
    GraphWithSettings    *addGraphicWithSettings();

    QList<QAction*> getTrackActionsList();
    void            drawCustomPlot(QCPPainter *painter);
    Curve           *copyCurveFromDeviceToTrack(Curve *sourceCurve);
    Curve           *makeCurveToTrackWithoutDevice(QString baseCurveName, QString deviceName);
    void            specialAutoRescaleValuesAxes();
    void            relocateLabelsTextsWithoutReplot();
    static void     autoRoundRangeValues(GraphWithSettings *g, bool roundOnlyMinAndSaveScale = false);
    void            removeAllLabelsOnTrack();
    void            deleteCurveFromTrack(Curve *curve, GraphWithSettings *accordingGraph);
    int             getHorizontalSectionsCount();
    void            setHorizontalSectionsCount(int countNew);

protected:
    virtual void dragEnterEvent(QDragEnterEvent *dragEntEvt) /*override*/;
    virtual void dropEvent(QDropEvent *dropEvt) /*override*/;
    virtual void resizeEvent(QResizeEvent *rszEvt);

private:
    SpecialWidgetForPlot *plotPointer;
    uint32_t  idxProject;
    uint32_t  trackID;
    TreeElement *rootItem;
    TreeElement *parentItem;
    QAction     *actionLoadDataForSelectedCurves;
    QAction     *actionReloadDataForAllCurves;
    QAction     *actionRemoveTrack;
    QAction     *actionHideOrShowTrack;
    QAction     *actionMoveTrack_withinPlot;
    QAction     *actionAddLabel;
    QAction     *actionTrackProperties;

    bool        hidden_track;

    QSharedPointer<QCPAxisTicker> commonTicker;
    QSharedPointer<QCPAxisTickerDateTime> datetimeTicker;

    QTimer      forMenuOfItemClickTimer;
    QPoint      forMenuOfItemClickPos;
    GraphWithSettings *forMenuOfItemClickMouseGWS;

    // для множественного выбора кривых для рескайлинга
    GraphWithSettings *gws_alreadySelected;
    ObjectsPack *pack_;
    bool wasSelectedAnotherGraphs;
    QVector<QCheckBox*> chks;

    QPoint      posMousePressed;

    void    loadGraphicsDataFromMap(QMap<uint32_t, QStringList> map_graphs);
    void    showRescaleValuesAxisDialogForGraph(GraphWithSettings *grws);

signals:
    void    sigAddGraphToTrack(Curve *curve);
    void    sigTrackResized();
    void    sigRescaleAbcissAxis(double min, double max);
    void    sigRemoveTrack();
    void    sigCurveRemoved();
    void    sigGoToLabel(double xCoord);
    void    sigPrepareCopyLabel(DataLabel *copyOfDL);
    void    sigPrepareCopyToAnotherTrack(DataLabel *copiedDL);
    void    sigPrepareMoveLabel(DataLabel *movedDL);
    void    sigTrackHeightChanged(int h);
    void    sigMoveTrack();
    void    sigForMoveCurve(Curve *mayBeMovedCurve);

public slots:
    void    slotLoadDataForSelectedCurves();
    void    slotRemoveLabel();
    void    slotCopyLabelToAnotherTrack();
    void    slotMoveLabel();
    void    slotCopyLabel();

private slots:
    void    slotReloadDataForAllCurves();
    void    slotSomeItemClicked(QCPAbstractItem *clickedItem, QMouseEvent *mouseEvent);
    void    slotRescaleValueAxisForCurve();
    void    slotRescaleValueAxisAndChangeOtherPropertiesByClick(QCPAbstractItem *clickedItem, QMouseEvent *mouseEvent);
    void    slotRescaleValueAxisForSeveralGraphsOnTrack();
    void    slotSelectUnselectAllForRescalingSeveralGraphs();
    void    slotDeleteCurveFromTrack();
    void    slotChangeCurveColor();
    void    slotChangeCurveColorInProperties(GraphWithSettings *gws);
    void    slotChangeCurveColorInPropertiesPrepare();
    void    slotForMenuOfItemClick();
    void    slotAddLabel();
    void    slotMousePressed(QMouseEvent *pressEvt);
    void    slotHideOrShowTrack();
    void    slotForMoveCurveToAnotherTrack();
    void    slotTrackPropertiesDialog();
};

#endif // TRACK_H
