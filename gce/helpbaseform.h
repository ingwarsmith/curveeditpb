#ifndef HELPBASEFORM_H
#define HELPBASEFORM_H

#include <QWidget>

namespace Ui {
class HelpBaseForm;
}

class HelpBaseForm : public QWidget
{
    Q_OBJECT

public:
    explicit HelpBaseForm(QWidget *parent = 0);
    ~HelpBaseForm();

private:
    Ui::HelpBaseForm *ui;
};

#endif // HELPBASEFORM_H
