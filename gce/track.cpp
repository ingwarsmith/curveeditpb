#include "track.h"
#include "elements/curve.h"
#include "elements/plot.h"
#include "project.h"
#include "projectmanager.h"
#include "gcelogiclayer.h"
#include "gcedataview.h"

#include "datalabel.h"

#include "cfg.h"

#include <QColorDialog>

Track::Track(Plot *plotParent, int indexInListOfTracks, QObject *prjPtr, QWidget *parent) : QCustomPlot(parent)
{
    hidden_track = false;

    pack_ = new ObjectsPack;
    auto sd = this->axisRect()->autoMargins();
    sd = QCP::msNone;
    axisRect()->setAutoMargins(sd);
    QMargins mrg;
    mrg.setLeft(40);
    mrg.setBottom(30);
    mrg.setTop(5);
    mrg.setRight(15);
    axisRect()->setMargins(mrg);

    commonTicker = xAxis->ticker();
    datetimeTicker = QSharedPointer<QCPAxisTickerDateTime>(new QCPAxisTickerDateTime);
    xAxis->setTicker(datetimeTicker);
    datetimeTicker.data()->setDateTimeSpec(Qt::UTC);
    auto utcTime = QDateTime::currentDateTimeUtc().time().msecsSinceStartOfDay();
    auto msecsFromDayBegin = static_cast<double>(utcTime);//(QDateTime::currentDateTimeUtc().time().msecsSinceStartOfDay());//
    xAxis->setRange(0.001*msecsFromDayBegin, 0.001*(msecsFromDayBegin+3600));
    datetimeTicker.data()->setDateTimeFormat("hh:mm:ss\ndd.MM.yyyy");

    if (indexInListOfTracks == -1) // trackTimeOnly
    {
        xAxis2->setTicker(datetimeTicker);
        xAxis2->setRange(xAxis->range());
        xAxis->setVisible(false);
        xAxis2->setVisible(true);

        //auto px = xAxis2->tickLabelFont().pointSize();

        mrg.setBottom(5);
        mrg.setTop(30);
        axisRect()->setMargins(mrg);

        return;
    }



    setFixedHeight(250);

    auto config = qobject_cast<Configuration*>(GceLogicLayer::getInstance()
                                                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameConfiguration));
    auto defHt = config->dataMembers.tracksDefaultHeight;
    if (defHt)
    {
        setFixedHeight(defHt);
    }

    auto szPolicy = sizePolicy();
    szPolicy.setVerticalPolicy(QSizePolicy::Fixed);
    setSizePolicy(szPolicy);

    QCPMarginGroup *marginGroup = new QCPMarginGroup(this);
    axisRect()->setMarginGroup(QCP::msLeft | QCP::msRight, marginGroup);

    idxProject = plotParent->getThisProjectIndex();
    rootItem = new TreeElement(plotParent->getThisProjectIndex(), nullptr, prjPtr);
    rootItem->setType(TreeElement::ElementType::elementTrack);

    auto num = indexInListOfTracks + 1;
    rootItem->setNewNameElement(tr("Track").append("_").append(QString::number(num)));
    //axisRect()->setupFullAxesBox(true);

    setAcceptDrops(true);

    actionLoadDataForSelectedCurves = new QAction(tr("Load data for selected curves"));
    actionReloadDataForAllCurves = new QAction(tr("Reload data for all curves"));
    actionRemoveTrack = new QAction(tr("Delete track"));

    actionHideOrShowTrack = new QAction(tr("Hide track"));

    actionMoveTrack_withinPlot = new QAction(tr("Move track"));

    actionAddLabel = new QAction(tr("Add text label"));

    actionTrackProperties = new QAction(tr("Track properties"));

    connect(actionLoadDataForSelectedCurves, SIGNAL(triggered(bool)),
            this, SLOT(slotLoadDataForSelectedCurves()));
    connect(actionReloadDataForAllCurves,SIGNAL(triggered(bool)),
            this, SLOT(slotReloadDataForAllCurves()));
    connect(actionRemoveTrack, SIGNAL(triggered(bool)), this, SIGNAL(sigRemoveTrack()));
    connect(actionMoveTrack_withinPlot, SIGNAL(triggered(bool)),
            this, SIGNAL(sigMoveTrack()));

    connect(actionHideOrShowTrack, SIGNAL(triggered(bool)),
            this, SLOT(slotHideOrShowTrack()));

    connect(actionAddLabel, SIGNAL(triggered(bool)), this, SLOT(slotAddLabel()));

    connect(actionTrackProperties, SIGNAL(triggered(bool)),
             this, SLOT(slotTrackPropertiesDialog()));

    //this->setContextMenuPolicy(Qt::ActionsContextMenu);
    //this->addActions(getTrackActionsList());

    rootItem->setContextMenuActions(getTrackActionsList());

    connect(this, SIGNAL(itemClick(QCPAbstractItem*,QMouseEvent*)),
            this, SLOT(slotSomeItemClicked(QCPAbstractItem*,QMouseEvent*)));
    connect(this, SIGNAL(itemDoubleClick(QCPAbstractItem*,QMouseEvent*)),
            this, SLOT(slotRescaleValueAxisAndChangeOtherPropertiesByClick(QCPAbstractItem*,QMouseEvent*)));
    connect(this, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(slotMousePressed(QMouseEvent*)));
    //connect(this, SIGNAL(itemClick(QCPAbstractItem*,QMouseEvent*)), plotParent, SIGNAL())



#ifdef LNX
//    QFont fnt = xAxis->labelFont();
//    fnt.setPointSize(5);
//    xAxis->setLabelFont(fnt);
    datetimeTicker.data()->setDateTimeFormat("hh:mm:ss dd.MM.yyyy");
    datetimeTicker.data()->setTickStepStrategy(QCPAxisTickerDateTime::tssReadability);
    datetimeTicker.data()->setTickCount(3);
#endif

    forMenuOfItemClickTimer.setSingleShot(true);
    connect(&forMenuOfItemClickTimer, SIGNAL(timeout()), this, SLOT(slotForMenuOfItemClick()));
}

TreeElement *Track::getItem()
{
    return rootItem;
}

void Track::setParentForItem(TreeElement *prntElem)
{
    parentItem = prntElem;
    //connect(this, SIGNAL())
}

GraphWithSettings *Track::addGraphicWithSettings()
{
    if (graphCount() == 0)
    {
        yAxis->setTickLabels(false);
        yAxis->setTickLength(0);
        yAxis->setSubTickLength(0);

        //axisRect()->axis(QCPAxis::atLeft, 0)->setPadding(30);
    }
    GraphWithSettings *newGraph = new GraphWithSettings;
    auto axisLeftCount = axisRect()->axisCount(QCPAxis::atLeft);
    auto nextAxisLeftIndex = axisLeftCount;
    axisRect()->addAxis(QCPAxis::atLeft);
    newGraph->useNullValues = false;
    newGraph->frozenValuesScaleForAutoScale = false;
    newGraph->nullValueIfUsed = 0.0;
    newGraph->axisSpec = axisRect()->axis(QCPAxis::atLeft, nextAxisLeftIndex);
    newGraph->axisSpec->setTickLabels(false);
    newGraph->axisSpec->setTickLength(0);
    newGraph->axisSpec->setSubTickLength(0);
    newGraph->axisSpec->setVisible(false);
    newGraph->arrowOffsetFromPlot = -axisLeftCount*TRACK_HEADER_WIDTH;
    newGraph->graphic = addGraph(xAxis, newGraph->axisSpec );
    newGraph->graphicScatterStyleIsSquare = false;

    newGraph->divisionValueIsFixed = false;
    newGraph->divisionValueForAxis = //yAxis->range().size() / newGraph->axisSpec->range().size();
            newGraph->axisSpec->range().size() / yAxis->range().size();

    //newGraph->setScaleMaxValue(5000);
    //newGraph->setScaleMinValue(0);

    return newGraph;
}

QList<QAction *> Track::getTrackActionsList()
{
    QList<QAction *> lst;
//#ifndef LNX
    //lst //<< actionLoadDataForSelectedCurves
        //<< actionReloadDataForAllCurves
        //    << actionAddLabel
    //    << actionRemoveTrack;
    lst << actionTrackProperties
        << actionMoveTrack_withinPlot
        << actionHideOrShowTrack
        << actionRemoveTrack;
//#endif
    return lst;
}

void Track::drawCustomPlot(QCPPainter *painter)
{
    draw(painter);
}

Curve *Track::copyCurveFromDeviceToTrack(Curve *sourceCurve)
{
    auto curveID = sourceCurve->getCurveID();
    auto sourceID = sourceCurve->getSourceID();

    auto copiedCurve = new Curve(sourceCurve->getThisProjectIndex(), sourceID,
                                  sourceCurve->getItem()->getProjectPointer(), this);
    copiedCurve->setSourceID(sourceID);
    copiedCurve->setCurveID(curveID);

    copiedCurve->setCurveName(
                QString().append(sourceCurve->getCurveName()).append(" / ")
                .append(sourceCurve->getParentForItem()->getNameElement())
                );
    copiedCurve->setBaseCurveName_withinTrack(sourceCurve->getCurveName());
    copiedCurve->setParentForItem(sourceCurve->getParentForItem());
    copiedCurve->setLinkingToDeviceName(true);
    //rootItem->addSubElement(copiedCurve->getItem());
    rootItem->insertSubElement(copiedCurve->getItem(), 0);

    // actions for curve in track:
    auto     *actionCurve_RescaleValuesAxisForCurve = new QAction(tr("Curve properties"));
    auto     *actionCurve_DeleteCurveFromTrack = new QAction(tr("Delete curve from track"));
    //auto     *actionCurve_ChangeCurveColor = new QAction(tr("Change curve color"));
    auto     *actionCurve_MoveCurveToAnotherTrack = new QAction(tr("Move curve to another track"));
    connect(actionCurve_DeleteCurveFromTrack, SIGNAL(triggered(bool)),
            copiedCurve, SIGNAL(sigDeleteFromTrack()));
    connect(actionCurve_RescaleValuesAxisForCurve, SIGNAL(triggered(bool)),
            copiedCurve, SIGNAL(sigRescaleValuesAxis()));
//    connect(actionCurve_ChangeCurveColor, SIGNAL(triggered(bool)),
//            copiedCurve, SIGNAL(sigChangeCurveColor()));
    connect(copiedCurve, SIGNAL(sigDeleteFromTrack()), this, SLOT(slotDeleteCurveFromTrack()));
    connect(copiedCurve, SIGNAL(sigRescaleValuesAxis()), this, SLOT(slotRescaleValueAxisForCurve()));
    connect(copiedCurve, SIGNAL(sigChangeCurveColor()), this,  SLOT(slotChangeCurveColor()));
    connect(actionCurve_MoveCurveToAnotherTrack, SIGNAL(triggered(bool)),
            copiedCurve, SIGNAL(sigForMoveToAnotherTrack()));
    connect(copiedCurve, SIGNAL(sigForMoveToAnotherTrack()), this, SLOT(slotForMoveCurveToAnotherTrack()));
    QList<QAction*> actionCurveLst;
    actionCurveLst << actionCurve_RescaleValuesAxisForCurve
                   //<< actionCurve_ChangeCurveColor
                   << actionCurve_MoveCurveToAnotherTrack
                   << actionCurve_DeleteCurveFromTrack;
    copiedCurve->getItem()->setContextMenuActions(actionCurveLst);

    return copiedCurve;
}

Curve *Track::makeCurveToTrackWithoutDevice(QString baseCurveName, QString deviceName)
{
    auto project = qobject_cast<Project*>(getItem()->getProjectPointer());
    auto makedCurve = new Curve(project->getThisProjectIndex(), 0, getItem()->getProjectPointer(), this);
    makedCurve->setSourceID(NULL_ID);
    makedCurve->setCurveID(NULL_ID);

    makedCurve->setCurveName(QString().append(baseCurveName).append(" / ").append(deviceName));
    //rootItem->addSubElement(makedCurve->getItem());
    rootItem->insertSubElement(makedCurve->getItem(), 0);
    //makedCurve->getItem()->setToolTip(tr("Not assigned to device"));
    makedCurve->getItem()->QTreeWidgetItem::setToolTip(0, tr("Not assigned to device"));

    return makedCurve;
}

void Track::specialAutoRescaleValuesAxes()
{
    auto oldRangeSizeForValues = 0.0, oldRangeSizeForAxisMain = 0.0, divisionValueForAxis = 0.0;
    QCPAxis *valueAxis_dv = nullptr;
    auto fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated = false;
    //GraphWithSettings *gws__ = nullptr;

    for (GraphWithSettings *gws : graphicsArray)
    {
        if (gws->divisionValueIsFixed)
        {
            auto ok = false;
            if (fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
            {
                auto dvCntPrev = oldRangeSizeForValues / divisionValueForAxis;
                //auto t_tmp = gws->scaleMaximumValue - gws->scaleMinimumValue;

                auto t_tmp = gws->graphic->getValueRange(ok, QCP::sdBoth,
                                                         gws->graphic->keyAxis()->range()).size();
                auto dvCntCurrent = t_tmp / gws->divisionValueForAxis;

                if (dvCntCurrent < dvCntPrev)
                {
                    gws->graphic->rescaleValueAxis(false, true);
                    continue;
                }
            }

            fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated = true;

            valueAxis_dv = gws->axisSpec;
            oldRangeSizeForValues = gws->graphic->getValueRange(ok, QCP::sdBoth,
                                                                gws->graphic->keyAxis()->range()).size();
                    //gws->scaleMaximumValue - gws->scaleMinimumValue; //valueAxis_dv->range().size();
            divisionValueForAxis = gws->divisionValueForAxis;
            oldRangeSizeForAxisMain = valueAxis_dv->range().size();
            //gws__ = gws;
            gws->graphic->rescaleValueAxis(false, true);
        }
    }

//    if (fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
//    {
//        gws__->graphic->rescaleValueAxis(false, true);
//    }

    for (GraphWithSettings *graphWS : graphicsArray)
    {
        if (graphWS->frozenValuesScaleForAutoScale)
        {
            continue;
        }
        //graphWS->graphic->rescaleValueAxis(false, true);
        if (graphWS->divisionValueIsFixed)
        {
            if (graphWS->axisSpec != valueAxis_dv)
            {
                auto rrngSz = valueAxis_dv->range().size();
                auto newRangeSizeGws = rrngSz * (graphWS->divisionValueForAxis / divisionValueForAxis);
                auto rngGws = graphWS->axisSpec->range();
                rngGws.upper = rngGws.lower + newRangeSizeGws;

                graphWS->axisSpec->setRange(rngGws);

                graphWS->setScaleMaxValue(graphWS->axisSpec->range().upper);

                autoRoundRangeValues(graphWS);
            }
        }
        else
        {
            graphWS->graphic->rescaleValueAxis(false, true);
        }

        graphWS->setScaleMaxValue(graphWS->axisSpec->range().upper);
        graphWS->setScaleMinValue(graphWS->axisSpec->range().lower);
        if (graphWS->graphic->dataCount() == 1)
        {
            graphWS->setScaleMaxValue(graphWS->axisSpec->range().upper + 1);
            graphWS->setScaleMinValue(graphWS->axisSpec->range().lower - 1);
        }
        autoRoundRangeValues(graphWS);
    }

    if (fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
    {
        auto newRangeSizeForAxis = valueAxis_dv->range().size();
        auto newRangeCommonAxis = yAxis->range().size() * newRangeSizeForAxis / oldRangeSizeForAxisMain;

        yAxis->setRange(0.0, newRangeCommonAxis);
        auto ticker = yAxis->ticker();
        auto intCountOfTicks = qRound(newRangeSizeForAxis / divisionValueForAxis);
        ticker.data()->setTickCount(intCountOfTicks);
    }

    for (GraphWithSettings *gws : graphicsArray)
    {
        if (!gws->divisionValueIsFixed ) //&& fixedDivValueGraphicFoundAndRangeOfCommonAxisCalculated)
        {
            gws->divisionValueForAxis = gws->axisSpec->range().size() / yAxis->range().size();
        }
        else
        {
            //if (gws->axisSpec == valueAxis_dv) continue;

            //auto dvCntMax = oldRangeSizeForAxis / divisionValueForAxis;
            //auto dvCntCurrent = gws->axisSpec->range().size() / gws->divisionValueForAxis;

//            auto comprValuesCoeff = dvCntCurrent / dvCntMax;

//            auto newRngSz = gws->axisSpec->range().size() * comprValuesCoeff;
//            auto rngGws = gws->axisSpec->range();
//            auto diff_ = newRngSz - rngGws.size();
//            rngGws.upper -= diff_;

            //auto rrngSz = yAxis->range().size();

        }
    }
}

void Track::dragEnterEvent(QDragEnterEvent *dragEntEvt)
{
//    dragEntEvt->acceptProposedAction();
    if (dragEntEvt->mimeData()->hasFormat("specialcopy/curve")
            || dragEntEvt->mimeData()->hasFormat("specialcopy/curves_array"))
    {
        dragEntEvt->setDropAction(Qt::CopyAction);
        //dragEntEvt->accept();
        dragEntEvt->acceptProposedAction();
    }
}

void Track::dropEvent(QDropEvent *dropEvt)
{
    if (dropEvt->mimeData()->hasFormat("specialcopy/curve"))
    {
        dropEvt->acceptProposedAction();
        //dropEvt->accept();
        auto o = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameAddedCurve);
        auto idx = GceLogicLayer::getInstance().getObjectsContainerPointer()->indexOf(o);
        GceLogicLayer::getInstance().getObjectsContainerPointer()->takeAt(idx);
        auto curve = qobject_cast<Curve*>(o);

        curve->setCurveType_RAW();

        auto o2 = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
        auto dataview = qobject_cast<GceDataView*>(o2);
        SpecialWidgetForPlot *plotWidget = nullptr;
        for (SpecialWidgetForPlot *plotWidget1 : dataview->getPlotWidgetsList())
        {
            if (plotWidget1->getTracksVectorPtr()->contains(this)) // теперь планшеты могут совпадать по именам
            {
                plotWidget = plotWidget1;
                break;
            }
        }
        auto curveInTrack = copyCurveFromDeviceToTrack(curve);
        if (plotWidget)
        {
            connect(curveInTrack, SIGNAL(sigDeviceNameChanged(QString,QString)),
                    plotWidget, SLOT(slotDeviceNameChanged(QString,QString)));
        }

        emit sigAddGraphToTrack(curveInTrack);
        return;
    }

    if (dropEvt->mimeData()->hasFormat("specialcopy/curves_array"))
    {
        dropEvt->acceptProposedAction();

        auto o = GceLogicLayer::getInstance()
                .getObjectFromObjectContainerByObjName(ObjectsNames::objNameAddedCurves_);
        auto idx = GceLogicLayer::getInstance().getObjectsContainerPointer()->indexOf(o);
        GceLogicLayer::getInstance().getObjectsContainerPointer()->takeAt(idx);

        auto objectsPack = qobject_cast<ObjectsPack*>(o);

        for (auto o : objectsPack->pack.values())
        {
            auto curve = qobject_cast<Curve*>(o);

            curve->setCurveType_RAW();

            auto o2 = GceLogicLayer::getInstance()
                    .getObjectFromObjectContainerByObjName(ObjectsNames::objNameGceDataView);
            auto dataview = qobject_cast<GceDataView*>(o2);
            SpecialWidgetForPlot *plotWidget = nullptr;
            for (SpecialWidgetForPlot *plotWidget1 : dataview->getPlotWidgetsList())
            {
                if (plotWidget1->getTracksVectorPtr()->contains(this)) // теперь планшеты могут совпадать по именам
                {
                    plotWidget = plotWidget1;
                    break;
                }
            }
            auto curveInTrack = copyCurveFromDeviceToTrack(curve);
            if (plotWidget)
            {
                connect(curveInTrack, SIGNAL(sigDeviceNameChanged(QString,QString)),
                        plotWidget, SLOT(slotDeviceNameChanged(QString,QString)));
            }

            emit sigAddGraphToTrack(curveInTrack);
        }
        return;
    }
}

void Track::resizeEvent(QResizeEvent *rszEvt)
{
    QCustomPlot::resizeEvent(rszEvt);
    emit sigTrackResized();
}

void Track::loadGraphicsDataFromMap(QMap<uint32_t, QStringList> map_graphs)
{
    if (map_graphs.isEmpty())
    {
        return;
    }

    for (uint32_t srcId : map_graphs.keys())
    {
        QStringList ignoredCurveNames;
        GceLogicLayer::getInstance()
                .loadGraphicsDataFromImportedFiles(srcId, map_graphs.value(srcId), ignoredCurveNames);

        QStringList cvnms = map_graphs.value(srcId);
        for (QString &ignoredCurve : ignoredCurveNames)
        {
            cvnms.removeOne(ignoredCurve);
        }
        map_graphs[srcId] = cvnms;
    }

//    auto emptyGraphicsOnTrackBeforeLoading = true;
//    for (int igr = 0; igr < graphCount(); ++igr)
//    {
//        auto gr = graph(igr);
//        if (gr->dataCount() > 0)
//        {
//            emptyGraphicsOnTrackBeforeLoading = false;
//            break;
//        }
//    }

    auto o = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameProjectManager);
    auto prjMgr = qobject_cast<ProjectManager*>(o);
    auto project = prjMgr->getProjectsArrayPtr()->values().last();

    auto min = 0.0, max = 1.0;
    auto szGA = graphicsArray.size();
    for (decltype (szGA) igr = 0; igr < szGA; ++igr)
    {
        GraphWithSettings *gws = graphicsArray[igr];
        QString nm = gws->nameGraph;
        auto ok = map_graphs.value(gws->sourceID).contains(nm);
        if (!ok)
        {
            continue;
        }
        auto curveLoaded = GceLogicLayer::getInstance()
                .isLoggingCurveLoaded(gws->sourceID, gws->curveID, ok);
        if (ok && curveLoaded)
        {
            auto x_n = GceLogicLayer::getInstance().getValuesX(gws->sourceID, gws->curveID, project->getFullOffsetFromUTC_msecs(),
                                                               GceLogicLayer::getInstance().getTimeShiftOfDataSource(gws->sourceID), ok);
            auto y_n = GceLogicLayer::getInstance().getValuesY(gws->sourceID, gws->curveID, ok);
            gws->loaded = true;
            gws->graphic->setData(x_n, y_n);
            //emit GceLogicLayer::getInstance().sigDrawOrRedrawGraphic(gws->sourceID, gws->curveID);
            if (igr == 0)
            {
                min = x_n.first();
                max = x_n.last();
            }
            else
            {
                auto first = x_n.first(), last = x_n.last();
                if (first < min) min = first;
                if (last > max) max = last;
            }
            auto min_value = *std::min_element(y_n.begin(), y_n.end());
            auto max_value = *std::max_element(y_n.begin(), y_n.end());
            auto range_values_simple = max_value - min_value;
            auto margin_values = range_values_simple * 0.1;
            min_value -= margin_values;
            max_value += margin_values;

            gws->axisSpec->setRange(min_value, max_value);
            gws->setScaleMaxValue(max_value);
            gws->setScaleMinValue(min_value);

            autoRoundRangeValues(gws);
        }
    }

    auto range_simple = max - min;
    auto margin = range_simple * 0.1;
    min -= margin;
    max += margin;

    replot();

//    if (emptyGraphicsOnTrackBeforeLoading)
//    {
//        emit sigRescaleAbcissAxis(min, max);
//    }
}

void Track::showRescaleValuesAxisDialogForGraph(GraphWithSettings *grws)
{
    QCPScatterStyle square(QCPScatterStyle::ssSquare, 4);
    auto project = qobject_cast<Project*>(getItem()->getProjectPointer());
    auto dotsShow = !grws->graphic->scatterStyle().isNone();
    if (project->showDotsOnAllGraphics)
    {
        dotsShow = grws->graphicScatterStyleIsSquare;
    }

    auto dlg = new QDialog;
    auto lt = new QVBoxLayout;
    dlg->setLayout(lt);
    auto lblInfo = new QLabel; //(tr("Change scale for ").append(grws->nameGraph));
    QString str = QString(grws->nameGraph).prepend(tr("Curve name: ")).append("\n")
            .append(tr("Device name: ")).append(grws->deviceName);
    lblInfo->setText(str);
    auto lblPseudoName = new QLabel(tr("Pseudoname:"));
    auto lnedPseudoName = new QLineEdit;
    lnedPseudoName->setText(grws->pseudoNameGraph);
    if (grws->pseudoNameGraph.isEmpty())
    {
        lnedPseudoName->setText(grws->nameGraph);
    }
    auto lblMin = new QLabel(tr("Minimum scale value:"));
    auto lnedMin = new QLineEdit;
    auto lblMax = new QLabel(tr("Maximum scale value:"));
    auto lnedMax = new QLineEdit;
    auto chkFrozenScale = new QCheckBox(tr("Fix axis"));
    auto btnApplyRescaleForOtherGraphicsOnTrack = new QPushButton(tr("Accept scale settings for another graphs too..."));
    auto lblDivisionValue = new QLabel(tr("Scale intreval:"));
    auto lnedDivisionValue = new QLineEdit;
    grws->divisionValueForAxis = //yAxis->range().size() / grws->axisSpec->range().size();
            grws->axisSpec->range().size() / yAxis->range().size();
    QString dvalue;
    dvalue.setNum(grws->divisionValueForAxis);
    lnedDivisionValue->setText(dvalue);
    auto chkFixedDivisionValue = new QCheckBox(tr("Fix scale intreval"));
    auto chkDots = new QCheckBox(tr("Show data as dots"));
    auto chkDotsAll = new QCheckBox(tr("... apply for all graphics in track"));
    auto btnChangeColor = new QPushButton(tr("Change curve color"));
    chkDots->setChecked(dotsShow);
    chkDotsAll->setChecked(false);
    auto heightSaved = height();
    auto lblTrackAdj = new QLabel(tr("-- Track properties --"));
    auto lblTrackName = new QLabel(getItem()->getNameElement());
    auto lblTrackHeight = new QLabel(tr("Track height, pixels:"));
    auto lnedTrackHeight = new QLineEdit;
    lnedTrackHeight->setText(QString::number(heightSaved));
    auto chkHideTrack = new QCheckBox(tr("Hide track"));
    chkHideTrack->setChecked(this->isHidden());
    auto settedNullValue = false;
    double null_value = 0.0;
    GceLogicLayer::getInstance().getSettingOfNullValue(grws->sourceID, grws->curveID, settedNullValue, null_value);
    auto chkSetValueForNull = new QCheckBox(tr("Use null value indication"));
    chkSetValueForNull->setChecked(settedNullValue);
    auto lnedNullValue = new QLineEdit;
    lnedNullValue->setText(QString::number(null_value));
    auto lblCountTrackHorizontalSections = new QLabel(tr("Horizontal sections count:"));
    auto lnedCountTrackHorizontalSections = new QLineEdit;
    auto ivalidCTHS = new QIntValidator(1, 500);
    lnedCountTrackHorizontalSections->setValidator(ivalidCTHS);
    auto hsectionscount = getHorizontalSectionsCount();
    lnedCountTrackHorizontalSections->setText(QString::number(hsectionscount));

    connect(chkFrozenScale, &QCheckBox::clicked, this, [&](bool checked)
    {
        if (checked)
        {
            chkFixedDivisionValue->setChecked(false);
        }
    });
    connect(chkFixedDivisionValue, &QCheckBox::clicked, this, [&](bool checked)
    {
        if (checked)
        {
            chkFrozenScale->setChecked(false);
        }
    });

    chkFixedDivisionValue->setChecked(grws->divisionValueIsFixed);

    lt->addWidget(lblInfo);
    lt->addWidget(lblPseudoName);
    lt->addWidget(lnedPseudoName);
    lt->addWidget(lblMax);
    lt->addWidget(lnedMax);
    lt->addWidget(lblMin);
    lt->addWidget(lnedMin);
    lt->addWidget(chkFrozenScale);
    lt->addWidget(btnApplyRescaleForOtherGraphicsOnTrack);
    lt->addWidget(lblDivisionValue);
    lt->addWidget(lnedDivisionValue);
    lt->addWidget(chkFixedDivisionValue);
    lt->addWidget(chkDots);
    lt->addWidget(chkDotsAll);
    lt->addWidget(btnChangeColor);
    lt->addWidget(chkSetValueForNull);
    lt->addWidget(lnedNullValue);
    lt->addWidget(lblTrackAdj);
    lt->addWidget(lblTrackName);
    lt->addWidget(lblTrackHeight);
    lt->addWidget(lnedTrackHeight);
    lt->addWidget(chkHideTrack);
    lt->addWidget(lblCountTrackHorizontalSections);
    lt->addWidget(lnedCountTrackHorizontalSections);

    chkHideTrack->setVisible(false); // убрать потом??
    lblTrackAdj->hide();
    lblTrackName->hide();
    lblTrackHeight->hide();
    lnedTrackHeight->hide();
    lblCountTrackHorizontalSections->hide();
    lnedCountTrackHorizontalSections->hide();

    auto objectsPack_SeveralCurvesIDs = new ObjectsPack;
    objectsPack_SeveralCurvesIDs->setObjectName(ObjectsNames::objNameSeveralCurvesIDs4pack);
    gws_alreadySelected = grws;

    auto objCurveIDs = new QObject;
    objCurveIDs->setObjectName(ObjectsNames::objNameCurveIDs);
    objCurveIDs->setProperty("curveID", QVariant(grws->curveID));
    objCurveIDs->setProperty("sourceID", QVariant(grws->sourceID));

    GceLogicLayer::getInstance().getInstance().getObjectsContainerPointer()->append(objCurveIDs);
    //lnedMax->setText(QString::number(grws->scaleMaximumValue));
    //lnedMin->setText(QString::number(grws->scaleMinimumValue));
    QString rngvalue;
    rngvalue.setNum(grws->axisSpec->range().upper);
    lnedMax->setText(rngvalue);
    rngvalue.setNum(grws->axisSpec->range().lower);
    lnedMin->setText(rngvalue);
    chkFrozenScale->setChecked(grws->frozenValuesScaleForAutoScale);
    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    lt->addWidget(btns);
    //dlg->setWindowTitle(tr("Value axis scale change"));
    dlg->setWindowTitle(tr("Curve properties"));
    connect(btns, SIGNAL(accepted()), dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlg, SLOT(reject()));
    connect(btnChangeColor, SIGNAL(clicked(bool)), this, SLOT(slotChangeCurveColorInPropertiesPrepare()));
    connect(btnApplyRescaleForOtherGraphicsOnTrack, SIGNAL(clicked(bool)), this,
            SLOT(slotRescaleValueAxisForSeveralGraphsOnTrack()));
    auto result = dlg->exec();
    if (result == QDialog::Accepted)
    {
        auto pnm = lnedPseudoName->text();
        grws->pseudoNameGraph = pnm;
        if (grws->pseudoNameGraph == grws->nameGraph)
        {
            grws->pseudoNameGraph.clear();
        }
        grws->updateLabelGraphicName();
        auto isDouble = false, isInt = false;
        lnedTrackHeight->text().toInt(&isInt);
        lnedMax->text().toDouble(&isDouble);
        if (isDouble)
        {
            lnedMin->text().toDouble(&isDouble);
        }
        if (!isDouble || !isInt)
        {
            QMessageBox::critical(nullptr, tr("Input error"), tr("Incorrect value(s)!"));
        }
        else
        {
            auto min = lnedMin->text().toDouble(),
                    max = lnedMax->text().toDouble();
            auto frozenScale = grws->frozenValuesScaleForAutoScale =
                    chkFrozenScale->checkState() == Qt::Checked;
            auto trackHeight = lnedTrackHeight->text().toInt();
            auto trackHeightChanged = trackHeight != heightSaved;
            if (trackHeightChanged)
            {
                trackHeight = (trackHeight >= 10 ? trackHeight : 10);
                setFixedHeight(trackHeight);
                emit sigTrackHeightChanged(trackHeight);
            }
            if (this->isHidden() && (chkHideTrack->checkState() == Qt::Unchecked))
            {
                this->show();
            }
            if (!this->isHidden() && (chkHideTrack->checkState() == Qt::Checked))
            {
                this->hide();
            }

            auto hsectionscountnew = lnedCountTrackHorizontalSections->text().toInt();
            if (hsectionscountnew == 0) hsectionscountnew = 1;
            if (hsectionscount != hsectionscountnew)
            {
                setHorizontalSectionsCount(hsectionscountnew);
            }

            //
            grws->setScaleMaxValue(max);
            grws->setScaleMinValue(min);
            grws->frozenValuesScaleForAutoScale = frozenScale;
            auto oldRangeSizeForCommonAxis = yAxis->range().size();
            auto oldRangeSizeForAxis = grws->axisSpec->range().size();
            grws->axisSpec->setRange(min, max);
            auto newRangeSizeForAxis = grws->axisSpec->range().size();
            grws->divisionValueIsFixed = chkFixedDivisionValue->checkState() == Qt::Checked;
            if (grws->divisionValueIsFixed)
            {
                auto newRangeSizeForCommonAxis = oldRangeSizeForCommonAxis
                        * newRangeSizeForAxis / oldRangeSizeForAxis;

                auto intCountOfTicks = qRound(grws->axisSpec->range().size() / grws->divisionValueForAxis);
                auto ticker = yAxis->ticker();
                ticker.data()->setTickCount(intCountOfTicks);
                yAxis->setRange(0.0, newRangeSizeForCommonAxis);
            }

            autoRoundRangeValues(grws);
            if (wasSelectedAnotherGraphs)
            {
                for (int key : pack_->pack.keys())
                {
                    for (GraphWithSettings *g : graphicsArray)
                    {
                        if (g->sourceID == pack_->pack.value(key)->property("sourceID").toUInt())
                        {
                            if (g->curveID == pack_->pack.value(key)->property("curveID").toUInt())
                            {
                                g->setScaleMaxValue(max);
                                g->setScaleMinValue(min);
                                g->frozenValuesScaleForAutoScale = frozenScale;
                                g->axisSpec->setRange(min, max);
                                autoRoundRangeValues(g);
                            }
                        }

                    }
                }
                pack_->pack.clear();
            }

            auto newDivisionValue = lnedDivisionValue->text().toDouble();
            if (true || !qFuzzyCompare(static_cast<float>(newDivisionValue), static_cast<float>(grws->divisionValueForAxis)))
            {
                auto oldDivisionValue = grws->divisionValueForAxis;
                grws->divisionValueForAxis = newDivisionValue;
                auto oldRangeSizeForCommonAxis = yAxis->range().size();
                auto newRangeSizeForCommonAxis = oldRangeSizeForCommonAxis * oldDivisionValue / newDivisionValue;
                auto intCountOfTicks = qRound(grws->axisSpec->range().size() / newDivisionValue);
                auto ticker = yAxis->ticker();
                ticker.data()->setTickCount(intCountOfTicks);
                yAxis->setRange(0.0, newRangeSizeForCommonAxis);
                autoRoundRangeValues(grws);

                for (GraphWithSettings *g : graphicsArray)
                {
                    if (g != grws)
                    {
                        if (g->divisionValueIsFixed)
                        {
                            auto minimumSaved = g->scaleMinimumValue;
                            auto rangeNew = newRangeSizeForCommonAxis * g->divisionValueForAxis;
                            auto maximumNew = rangeNew + minimumSaved;
                            g->setScaleMaxValue(maximumNew);
                            g->axisSpec->setRange(minimumSaved, maximumNew);
                            autoRoundRangeValues(g);
                        }
                        else
                        {
                            g->divisionValueForAxis = g->axisSpec->range().size() / newRangeSizeForCommonAxis;
                        }
                    }
                }
            }

            //

            this->replot();
            //emit sigTrackResized();
            if (trackHeightChanged)
            {
                emit sigTrackResized();
            }
        }
        if (dotsShow != (chkDots->checkState() == Qt::Checked))
        {
            QCPScatterStyle newStyle =
                    ( (chkDots->checkState() == Qt::Checked || project->showDotsOnAllGraphics) ?
                         square :
                         QCPScatterStyle(QCPScatterStyle::ssNone));

            grws->graphic->setScatterStyle(newStyle);
            grws->graphicScatterStyleIsSquare = chkDots->checkState() == Qt::Checked;
            this->replot();
        }
        if (chkDotsAll->checkState() == Qt::Checked)
        {
            QCPScatterStyle newStyle =
                    ( (chkDots->checkState() == Qt::Checked || project->showDotsOnAllGraphics) ?
                         square :
                         QCPScatterStyle(QCPScatterStyle::ssNone));
            for (GraphWithSettings *g : graphicsArray)
            {
                g->graphic->setScatterStyle(newStyle);
                g->graphicScatterStyleIsSquare = chkDots->checkState() == Qt::Checked;
            }
            this->replot();
        }
        auto settedNullValueNew = chkSetValueForNull->checkState() == Qt::Checked ? true : false;
        auto nv_toDoubleOk = false;
        auto nv_value = lnedNullValue->text().toDouble(&nv_toDoubleOk);
        if (!nv_toDoubleOk)
        {
            QMessageBox::critical(nullptr, tr("Error"), tr("Not correct null value!"));
        }
        else if (settedNullValueNew != settedNullValue)
        {
            GceLogicLayer::getInstance().setExcludedTimesForNullValues(grws->sourceID, grws->curveID, settedNullValueNew, nv_value);
            grws->useNullValues = settedNullValueNew;
            grws->nullValueIfUsed = nv_value;
        }
    }

    if (objCurveIDs)
    {
        GceLogicLayer::getInstance().getObjectsContainerPointer()->removeOne(objCurveIDs);
    }

    objCurveIDs->deleteLater();
    objectsPack_SeveralCurvesIDs->deleteLater();

    dlg->deleteLater();
}

void Track::relocateLabelsTextsWithoutReplot()
{
    if (this->labelsArray.size() > 1)
    {
        QMap<int64_t, DataLabel*> labelsShowed;
        auto min = xAxis->range().lower, max = xAxis->range().upper;
        for (DataLabel *lbl : this->labelsArray)
        {
            if (!lbl->isTextVisible()||!lbl->isVisible())
            {
                continue;
            }
            auto dblX = lbl->getLabel_Xcoord();
            if (dblX > min && dblX < max)
            {
                auto timeVal = dblX*1000.0;
                auto tm_Int = (int64_t)timeVal;
                labelsShowed.insert(tm_Int, lbl);
                //lbl->hide(false);
            }
            else
            {
                //lbl->hide(true);
            }
        }
        if (labelsShowed.size())
        {
            auto keys = labelsShowed.keys();
            for (decltype (keys.size()) i_key = 1; i_key < keys.size(); ++i_key)
            {
                DataLabel *current = labelsShowed.value(keys.at(i_key));
                DataLabel *previos = labelsShowed.value(keys.at(i_key-1));
                auto prevTextEndCoordX = previos->getTextBlockLastCoordX();
                auto currTextBeginCoordX = current->getLabel_Xcoord();
                if (prevTextEndCoordX > currTextBeginCoordX)
                {
                    auto prevBottomY = previos->getTextBlockBottomCoordY();
                    auto textHeight = previos->getTextBlockHeightInCoordUnits();
                    auto delta = yAxis->range().upper - prevBottomY;
                    auto c_delta = delta + textHeight; //yAxis->coordToPixel(delta);
                    current->setMargineFromAnotherTextAndFromBottom(c_delta);
                }
                else
                {
                    current->resetMargineFromAnotherTextAndFromBottom();
                }
            }
        }
    }
}

void Track::autoRoundRangeValues(GraphWithSettings *g, bool roundOnlyMinAndSaveScale)
{
    (void)roundOnlyMinAndSaveScale;
    QString dvalue;
    auto max = g->axisSpec->range().upper, min = g->axisSpec->range().lower;
    auto prec = 4;
    if (g->graphic->parentPlot()->height()<150)
    {
        prec = 2;
    }
    dvalue.setNum(max, 'g', prec);
    g->lMaximum->setText(dvalue);
    dvalue.setNum(min, 'g', prec);
    g->lMinimum->setText(dvalue);
}

void Track::removeAllLabelsOnTrack()
{
    while (labelsArray.size())
    {
        auto datalabel = labelsArray.takeFirst();
        datalabel->removeMe();
        datalabel->deleteLater();
    }
}

void Track::deleteCurveFromTrack(Curve *curve, GraphWithSettings *accordingGraph)
{
    auto gws = accordingGraph;

    getItem()->removeChild(curve->getItem());

    this->removeGraph(gws->graphic);
    this->removeItem(gws->arrow);
    this->removeItem(gws->labelNameGraphic);
    this->removeItem(gws->lMaximum);
    this->removeItem(gws->lMinimum);
    this->removeItem(gws->tracerForArrowLower);
    this->removeItem(gws->tracerForArrowMiddle);
    this->removeItem(gws->tracerForArrowUpper);
    graphicsArray.removeOne(gws);
    delete gws;

    emit sigCurveRemoved();
    emit sigTrackResized();
}

int Track::getHorizontalSectionsCount()
{
    return qRound(this->yAxis->range().size());
}

void Track::setHorizontalSectionsCount(int countNew)
{
    this->yAxis->setSubTicks(false);
    auto ticker = this->yAxis->ticker();
    ticker.data()->setTickCount(countNew);
    auto hsectionscountnewdbl = static_cast<double>(countNew);
    auto rng = this->yAxis->range();
    rng.upper = hsectionscountnewdbl;
    this->yAxis->setRange(rng);
}

void Track::slotLoadDataForSelectedCurves()
{
    QMap<uint32_t, QStringList> forLoad_CurveNamesBySrcId;
    for (GraphWithSettings *gws : graphicsArray)
    {
        if (!gws->loaded)
        {
            if (gws->sourceID == NULL_ID && gws->curveID == NULL_ID)
            {
                continue;
            }
            if (forLoad_CurveNamesBySrcId.keys().contains(gws->sourceID))
            {
                QStringList crvNames = forLoad_CurveNamesBySrcId.take(gws->sourceID);
                crvNames.append(gws->nameGraph);
                forLoad_CurveNamesBySrcId.insert(gws->sourceID, crvNames);
            }
            else
            {
                QStringList crvNames(gws->nameGraph);
                forLoad_CurveNamesBySrcId.insert(gws->sourceID, crvNames);
            }
        }
    }
    loadGraphicsDataFromMap(forLoad_CurveNamesBySrcId);
}

void Track::slotRemoveLabel()
{
    auto datalabel = qobject_cast<DataLabel*>(sender());
    labelsArray.removeOne(datalabel);
    datalabel->removeMe();
    datalabel->deleteLater();
    replot();
}

void Track::slotCopyLabelToAnotherTrack()
{
    DataLabel *dl = qobject_cast<DataLabel*>(sender());
    emit sigPrepareCopyToAnotherTrack(dl);
}

void Track::slotMoveLabel()
{
    DataLabel *dl = qobject_cast<DataLabel*>(sender());
    emit sigPrepareMoveLabel(dl);
}

void Track::slotCopyLabel()
{
    DataLabel *dl = qobject_cast<DataLabel*>(sender());
    emit sigPrepareCopyLabel(dl);
}

void Track::slotReloadDataForAllCurves()
{
    QMap<uint32_t, QStringList> forLoad_CurveNamesBySrcId;
    for (GraphWithSettings *gws : graphicsArray)
    {
        if (forLoad_CurveNamesBySrcId.keys().contains(gws->sourceID))
        {
            QStringList crvNames = forLoad_CurveNamesBySrcId.take(gws->sourceID);
            crvNames.append(gws->nameGraph);
            forLoad_CurveNamesBySrcId.insert(gws->sourceID, crvNames);
        }
        else
        {
            QStringList crvNames(gws->nameGraph);
            forLoad_CurveNamesBySrcId.insert(gws->sourceID, crvNames);
        }
    }
    loadGraphicsDataFromMap(forLoad_CurveNamesBySrcId);
}

void Track::slotSomeItemClicked(QCPAbstractItem *clickedItem, QMouseEvent *mouseEvent)
{
    Q_UNUSED(mouseEvent)
    for (GraphWithSettings *gws : graphicsArray)
    {
        if (clickedItem == gws->labelNameGraphic)
        {
            if (forMenuOfItemClickTimer.isActive())
            {
                forMenuOfItemClickTimer.stop();
                forMenuOfItemClickTimer.start(1000);
            }
            else
            {
                forMenuOfItemClickTimer.start(1000);
            }
            forMenuOfItemClickPos = mouseEvent->globalPos();
            forMenuOfItemClickMouseGWS = gws;

            break;
        }
    }
}

void Track::slotRescaleValueAxisForCurve()
{
    auto curve = qobject_cast<Curve*>(sender());

    if (curve)
    {
        auto curveID = curve->getCurveID();
        auto sourceID = curve->getSourceID();

        for (GraphWithSettings *gws : graphicsArray)
        {
            if (gws->curveID == curveID && gws->sourceID == sourceID)
            {
                showRescaleValuesAxisDialogForGraph(gws);
                break;
            }
        }
    }
}

void Track::slotRescaleValueAxisAndChangeOtherPropertiesByClick(QCPAbstractItem *clickedItem, QMouseEvent *mouseEvent)
{
    if (forMenuOfItemClickTimer.isActive())
    {
        forMenuOfItemClickTimer.stop();
    }
    Q_UNUSED(mouseEvent);
    for (GraphWithSettings *gws : graphicsArray)
    {
        if (clickedItem == gws->labelNameGraphic)
        {
            showRescaleValuesAxisDialogForGraph(gws);
            break;
        }
    }
}

void Track::slotRescaleValueAxisForSeveralGraphsOnTrack()
{
    wasSelectedAnotherGraphs = false;
    QDialog dlg;
    auto lt = new QVBoxLayout;
    dlg.setLayout(lt);

    QVector<GraphWithSettings*> Gs;

    auto btnSelectAll = new QPushButton(tr("Select all")),
            btnDeselectAll = new QPushButton(tr("Deselect all"));
    connect(btnSelectAll, SIGNAL(clicked(bool)), this, SLOT(slotSelectUnselectAllForRescalingSeveralGraphs()));
    connect(btnDeselectAll, SIGNAL(clicked(bool)), this, SLOT(slotSelectUnselectAllForRescalingSeveralGraphs()));

    lt->addWidget(btnSelectAll);
    lt->addWidget(btnDeselectAll);

    for (GraphWithSettings *g : graphicsArray)
    {
        if (g == gws_alreadySelected) continue;
        auto chk_graph = new QCheckBox;
        chk_graph->setChecked(false);
        chk_graph->setText(g->nameGraph);
        lt->addWidget(chk_graph);
        chks << chk_graph;
        Gs << g;
    }

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    lt->addWidget(btns);
    dlg.setWindowTitle(tr("Selected another curves for rescale"));
    connect(btns, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), &dlg, SLOT(reject()));
    auto result = dlg.exec();
    if (result == QDialog::Accepted)
    {
        for (QCheckBox *c : chks)
        {
            if (c->isChecked())
            {
                wasSelectedAnotherGraphs = true;
                auto idx = chks.indexOf(c);
                auto objCurveIDs = new QObject;
                auto grws = Gs.at(idx);
                objCurveIDs->setProperty("curveID", QVariant(grws->curveID));
                objCurveIDs->setProperty("sourceID", QVariant(grws->sourceID));
                pack_->pack.insert(idx, objCurveIDs);
            }
        }
    }

    while (chks.size())
    {
        auto chk_ = chks.takeFirst();
        chk_->deleteLater();
    }
}

void Track::slotSelectUnselectAllForRescalingSeveralGraphs()
{
    auto checkedAll = qobject_cast<QPushButton*>(sender())->text() == tr("Select all") ? true : false;
    for (QCheckBox *c : chks)
    {
        c->setChecked(checkedAll);
    }
}

void Track::slotDeleteCurveFromTrack()
{
    auto curve = qobject_cast<Curve*>(sender());
    auto curveID = curve->getCurveID();
    auto sourceID = curve->getSourceID();

    GraphWithSettings *gws = nullptr;
    for (GraphWithSettings *gws_ : graphicsArray)
    {
        if (gws_->curveID == curveID && gws_->sourceID == sourceID)
        {
            gws = gws_;
            break;
        }
    }
    if (gws == nullptr)
    {
        return;
    }

    QString msg = tr("Do you really want to delete curve ")
            .append(gws->nameGraph)
            .append(tr(" from track "))
            .append(this->getItem()->getNameElement())
            .append("?");

    auto result = QMessageBox::question(nullptr, tr("Curve removing"), msg);
    if (result == QMessageBox::Yes)
    {
        deleteCurveFromTrack(curve, gws);
    }
}

void Track::slotChangeCurveColor()
{
    auto curve = qobject_cast<Curve*>(sender());
    auto curveID = curve->getCurveID();
    auto sourceID = curve->getSourceID();

    GraphWithSettings *gws = nullptr;
    for (GraphWithSettings *gws_ : graphicsArray)
    {
        if (gws_->curveID == curveID && gws_->sourceID == sourceID)
        {
            gws = gws_;
            break;
        }
    }

    if (gws) slotChangeCurveColorInProperties(gws);
}

void Track::slotChangeCurveColorInProperties(GraphWithSettings *gws)
{
    QColor newColor = QColorDialog::getColor(gws->currentPenColor);
    if (newColor.isValid())
    {
        gws->savedStandardPenColor = gws->currentPenColor;
        gws->currentPenColor = newColor;
        gws->colorPen.setColor(newColor);
        gws->colorBrush.setColor(newColor);

        gws->graphic->setPen(gws->colorPen);

        auto pen = gws->colorPen;
        pen.setWidth(2.0);
        gws->arrow->setPen(pen);
        gws->lMaximum->setColor(gws->currentPenColor);
        gws->lMinimum->setColor(gws->currentPenColor);
        gws->lMaximum->setBrush(QBrush(Qt::white));
        gws->lMinimum->setBrush(QBrush(Qt::white));

        replot();
    }
}

void Track::slotChangeCurveColorInPropertiesPrepare()
{
    auto objCurveIDs = GceLogicLayer::getInstance().getObjectFromObjectContainerByObjName(ObjectsNames::objNameCurveIDs);
    if (!objCurveIDs)
    {
        return;
    }
    uint32_t curveID = objCurveIDs->property("curveID").toUInt();
    uint32_t sourceID = objCurveIDs->property("sourceID").toUInt();

    GraphWithSettings *gws = nullptr;
    for (GraphWithSettings *gws_ : graphicsArray)
    {
        if (gws_->curveID == curveID && gws_->sourceID == sourceID)
        {
            gws = gws_;
            break;
        }
    }

    if (gws) slotChangeCurveColorInProperties(gws);
}

void Track::slotForMenuOfItemClick()
{
    this->setToolTipDuration(8000);
    QToolTip::showText(forMenuOfItemClickPos/*->globalPos()*/,
                       QString().append(forMenuOfItemClickMouseGWS->nameGraph).append("\n").append(forMenuOfItemClickMouseGWS->deviceName));
}

void Track::slotAddLabel()
{
    //auto labelText = DataLabel::makeDataLabel(this, posMousePressed, "ANY TEXT");
    //labelsArray << labelText;
    //replot();
}

void Track::slotMousePressed(QMouseEvent *pressEvt)
{
    posMousePressed = pressEvt->pos();
}

void Track::slotHideOrShowTrack()
{
    if (hidden_track)
    {
        hidden_track = false;
        this->show();
        QString nm = getItem()->getNameElement();
        QStringList _tmp_ = nm.split(" ");
        _tmp_.removeOne(tr("[HIDDEN]"));
        nm = _tmp_.join(" ");
        getItem()->setNewNameElement(nm);
        actionHideOrShowTrack->setText(tr("Hide track"));
    }
    else
    {
        hidden_track = true;
        this->hide();
        QString nm = getItem()->getNameElement();
        nm.append(" ").append(tr("[HIDDEN]"));
        getItem()->setNewNameElement(nm);
        actionHideOrShowTrack->setText(tr("Show track"));
    }

    emit sigTrackResized();
}

void Track::slotForMoveCurveToAnotherTrack()
{
    auto curve = qobject_cast<Curve*>(sender());
    emit sigForMoveCurve(curve);
}

void Track::slotTrackPropertiesDialog()
{
    auto dlg = new QDialog;
    auto lt = new QVBoxLayout;
    dlg->setLayout(lt);
    dlg->setWindowTitle(tr("Track properties"));

    auto heightSaved = height();
    auto lblTrackName = new QLabel(getItem()->getNameElement());
    auto lblTrackHeight = new QLabel(tr("Track height, pixels:"));
    auto lnedTrackHeight = new QLineEdit;
    lnedTrackHeight->setText(QString::number(heightSaved));
    auto chkHideTrack = new QCheckBox(tr("Hide track"));
    chkHideTrack->setChecked(this->isHidden());

    lt->addWidget(lblTrackName);
    lt->addWidget(lblTrackHeight);
    lt->addWidget(lnedTrackHeight);
    lt->addWidget(chkHideTrack);

    chkHideTrack->setVisible(false); // убрать потом

    auto btns = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    lt->addWidget(btns);
    connect(btns, SIGNAL(accepted()), dlg, SLOT(accept()));
    connect(btns, SIGNAL(rejected()), dlg, SLOT(reject()));
    auto result = dlg->exec();
    if (result == QDialog::Accepted)
    {
        auto isInt = false;
        lnedTrackHeight->text().toInt(&isInt);

        if (!isInt)
        {
            QMessageBox::critical(nullptr, tr("Input error"), tr("Incorrect value(s)!"));
        }
        else
        {
            auto trackHeight = lnedTrackHeight->text().toInt();
            auto trackHeightChanged = trackHeight != heightSaved;
            if (trackHeightChanged)
            {
                trackHeight = (trackHeight >= 10 ? trackHeight : 10);
                setFixedHeight(trackHeight);
                emit sigTrackHeightChanged(trackHeight);
            }
            if (this->isHidden() && (chkHideTrack->checkState() == Qt::Unchecked))
            {
                this->show();
            }
            if (!this->isHidden() && (chkHideTrack->checkState() == Qt::Checked))
            {
                this->hide();
            }
            this->replot();

            if (trackHeightChanged)
            {
                emit sigTrackResized();
            }
        }
    }

    dlg->deleteLater();
}


