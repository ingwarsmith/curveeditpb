#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>
#include <QVector>

#include "cfg.h"

class AppManager : public QObject
{
    Q_OBJECT
public:
    explicit AppManager(QObject *parent = nullptr);
    void    updateChecking(bool &needToUpdate);
    void    viewUpdateErrorIfExist();
    void    updateStart();
    bool    checkConfiguration();
    void    loadConfiguration();
    void    saveConfiguration();
    void    init();
    void    run();
    QString getLanguageFromCgf() const;

private:
    QVector<QObject*> objectsContainer;
    QString updaterExecutablePath;
    QString updateServerName;
    QString errorUpdateInfo;
    Configuration configuration;
    bool isConnectedToNetwork();

signals:

public slots:
};

#endif // APPMANAGER_H
