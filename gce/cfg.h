#ifndef CFG_H
#define CFG_H

#include <QObject>

class Configuration : public QObject
{
    // класс, описывающий конфигурацию программы
    Q_OBJECT

    const QString DELETE = "delete";
    const QString SAVE = "save";

    const QString AUTOLOAD = "autoload";
    const QString MANUALLOAD = "manualload";

    const QString DELIMETER = "#;^%";
    const QString LAST_PROJECTS_MARKER = "LastProjects";
    const QString LANGUAGE_MARKER = "Language";
    const QString LANGUAGE_RUSSIAN = "ru";
    const QString LANGUAGE_ENGLISH = "en";
    const QString TIME_LOOPS_MARKER = "TimeLoops";

    const QString AUTO_LOAD_GRAPH_DATA_MARKER = "AutoLoadGraphData";

    const QString TRACK_HEIGHT_DATA_MARKER = "DefaultTrackHeight";

public:
    Configuration();
    void    checkCfg();
    void    setDefaultSettings();
    void    addProjectPathToListOfLastProjectPaths(QString projectPathNew);
    void    setLastProjectsCount(int count);
    void    saveSettingsToCfg();
    QStringList getLanguages();
    struct DataMembers
    {
        QString     configFileFullPath;
        QStringList lastProjectsPaths;
        int         lastProjectsCount;
        QString     language;
        bool        removeTimeLoops;            // удалять немонотонные интервалы
        int         timeLoopPeriodMilliseconds; // считать немонотонными интервалы с длительностью не менее ... миллисекунд
        bool        loadDataAutomaticallyThenAddGraphicsToTrack; // загружать данные автоматически при добавлении графиков на трек
        int         tracksDefaultHeight;

        struct
        {
            int         lastProjectsCountDEFAULT;
            QString     languageDEFAULT;
            bool        removeTimeLoopsDEFAULT;
            int         timeLoopPeriodMillisecondsDEFAULT;
            bool        loadDataAutomaticallyThenAddGraphicsToTrackDEFAULT;
            int         tracksDefaultHeightDEFAULT;
        } defaultValues;
    } dataMembers;

private:
    QString delimeter;
};

#endif // CFG_H
