#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include <QMap>
#include <QList>
#include <QDateTime>
#include "project.h"
#include "projecttreewidget.h"

class ProjectManager : public QObject
{
    Q_OBJECT
    const QString SPRJ_SaveProject = QString("save__");
    const QString SPRJ_SaveProjectAs = QString("save_as__");
    const QString SPRJ_TimeZone = QString("tz__");
    const QString SPRJ_CloseProject = QString("close__");
    const QString SPRJ_RenameProject = QString("rename__");
    const QString SPRJ_ProjectInfo = QString("p_i__");
    const QString SPRJ_AddDataSource = QString("add_ds__");
    const QString SPRJ_AddPlot = QString("add_plot__");
    const QString SPRJ_RestoreFromBackup = QString("restore_from_backup__");
    const QString SPRJ_Design = QString("design__");


public:
    explicit    ProjectManager(QObject *parent = nullptr);
    void        selectCurrentProjectByIndex(int projectIndex);
    QMap<uint32_t, Project*>   *getProjectsArrayPtr()
    {
        return &projectsArray;
    }

    QString     getLast_Current_ProjectFolderPath();

    Project     *createNewProject(bool forOpen = false);
    void        onlyCreateProjectWithoutOpeningPlotFromTemplate()
    {
        creatingProjectInternal();
    }
    ProjectTreeWidget *projectTreeWidget;
    QList<QAction*> getCommonActions();
    QList<QAction*> getProjectActions();
    void        setCurrentProjectByID(uint32_t projectID)
    {
        currentProjectID = projectID;
    }
    void            openExistingProject_public(QString projectFolderPath, bool withBackup = true)
    {
        openExistingProject(projectFolderPath, withBackup);
    }
    bool            openedProjectExist()
    {
        return workWithProjectAlready();
    }
    QAction         *actionCreateProject;
    QAction         *actionOpenProject;
    //------
    QAction         *actionAddDataSource;
    QAction         *actionAddPlot;
    QAction         *actionSaveCurrentProject;
    QAction         *actionSaveCurrentProjectAs;
    QAction         *actionViewEditProjectProperties;
    QAction         *actionSetProjectTimeZone;
    QAction         *actionRenameProject;
    QAction         *actionCloseCurrentProject;
    QAction         *actionRestoreProjectFromBackup;
    QAction         *actionRecoveryDataSourcesOfProject;
    QAction         *actionCleanProjectFromDataSources;
    QAction         *actionDesignOfProject;

private:
    QMap<uint32_t, Project*> projectsArray;
    uint32_t        currentProjectID;

    bool            workWithProjectAlready();
    void            openExistingProject(QString projectFolderPath, bool withBackup = true);
    void            creatingProjectInternal();

signals:
    void            sigProjectCreate();
    void            sigCurrentProjectSave(int projectID);
    void            sigCurrentProjectClose(int projectID);
    void            sigAllProjectsSave();
    void            sigAllProjectsClose();

public slots:

private slots:
    void            slotCreateProject();
    void            slotOpenProject();
    void            slotCurrentProjectSave();
    void            slotCloseCurrentProject();
    void            slotCurrentProjectActionProcessing();
};

#endif // PROJECTMANAGER_H
