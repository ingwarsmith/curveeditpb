#ifndef TREEELEMENT_H
#define TREEELEMENT_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QMap>
#include <QAction>
#include <QMenu>
#include <QMouseEvent>

class TreeElement : public QWidget, public QTreeWidgetItem
{
    Q_OBJECT
public:
    enum class ElementType
    {
        //
        elementNone = 0,
        elementProject,
        elementDevicesData,
        elementCalculatedData,
        elementViews,
        elementDevice = 10,     // -> elementDevicesData
        //elementDeviceInformation, // -> elementDevice
        elementNotLoadedCurve,
        elementRawCurve,        // -> elementDevice
        elementChangedCurve,    // -> elementDevice
        elementSavedRawCurve,   // -> elementChangedCurve
        elementCalcCurve,       // -> elementCalculatedData
        elementFormula,         // -> elementCalcCurve
        elementView,            // -> elementView
        elementTrack,           // -> elementTrack
        elementCurveLinkInTrack, // -> elementTrack
        elementHierarchy,        // -> elementDevice или вложен в него
        elementListLabels,
        elementLabel,
        elementArrayType = 40
    };

    explicit TreeElement(uint32_t indexProject_, QWidget *parent = nullptr,
                         QObject *projectPtr = nullptr, bool isProjectElement = false);
    TreeElement(TreeElement *another);
    ElementType type;

    bool    isGroup;

    void    setIndexProject(uint32_t idx)
    {
        indexProject = idx;
    }
    uint32_t    getIndexProject() { return indexProject; }
    QString getProjectHeaderPath();

    void    setType(ElementType tp);
    ElementType getType() { return type; }
    void    setMenuParent(QWidget *parent);
    QWidget *getMenuParent();

    void    setNewNameElement(QString nm);
    QString getNameElement();
    void    insertSubElement(TreeElement *subElem, int32_t index = -1);
    void    addSubElement(TreeElement *subElem);
    void    removeSubElement(TreeElement *subElemDel);

    void    setKeyInElementsMap(uint32_t key) { keyElem = key; }
    uint32_t    getKeyInElementsMap() { return keyElem; }

    void    setContextMenuActions(QList<QAction*> actions);

    QMenu    *getCustomMenu() { return menuCustom; }
    QTreeWidgetItem    *baseItem()
    {
        return static_cast<QTreeWidgetItem*>(this);
    }
    QObject    *getProjectPointer()
    {
        return ptrProject;
    }

    void    setToolTip(const QString &toolTip);

private:
    uint32_t    keyElem;
    QString     txtNm;
    QMenu       *menuCustom;
    uint32_t    indexProject;
    QObject     *ptrProject;

public slots:
    void    slotShowCustomContextMenu(QPoint pos);

signals:
    void    sigElementNameChanged(QString newName);
    void    sigClicked();
};

#endif // TREEELEMENT_H
