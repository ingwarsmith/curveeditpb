#ifndef BASEDATATYPES_H
#define BASEDATATYPES_H

#include <QVector>
#include <QMap>

const long TS_NONE = -1L;
const double UTC_NONE = -111777.999;

struct LoggingDataTimeElement
{
    ulong   id_element;
    long    timestamp_raw;  // для привязки к елементу данных, = -1 для точек интерполяции
    int     isInterpolation : 1;
    double  UTC_orExactTimeAny;
};

enum SourceFileKind // тип файла-источника данных
{
    Binary = 1,     // двоичный
    Text            // текстовый
};

enum AbscissParameter // описатель параметра по оси абсцисс
{
    ExactThrougthTimeStamp = 1,      // через таймстампу (относительное время)
    AbsoluteTime_UTC,   // абсолютное время (UTC)
    Depth,              // глубина, м
    ParameterValue      // значение параметра
};

struct LoggingDatasBlock
{
    uint            isShifted : 1; // производился сдвиг по оси времени
    QVector<double> shiftings_time;
    int             blockID;
    uint            sourceID;
    QVector<ulong>  timeElementsIds;
    QVector<ulong>  ids_interpolated_values;
    bool            linkedByTime;
};

struct LoggingCurve
{
    uint            curveID;
    uint            sourceID;
    uint            isRawData : 1; // сырые данные (первичные, необработанные)
    QString         curveNameString;
    AbscissParameter x_type;
    QMap<ulong, double> timesOrAnotherX;
    QMap<ulong, double> values;
};

struct SourceData
{
    uint            sourceID;
    QVector<LoggingDatasBlock> blocks;
    QMap<uint, LoggingCurve*> curves;   // набор кривых
    QVector<LoggingDataTimeElement> times; // набор данных по времени
};

#endif // BASEDATATYPES_H
