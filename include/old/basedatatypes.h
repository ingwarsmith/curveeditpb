#ifndef BASEDATATYPES_H
#define BASEDATATYPES_H

#include <QString>
#include <QVector>

enum AbscissParameter // описатель параметра по оси абсцисс
{
    TimeStamp = 1,      // таймстампа (относительное время)
    AbsoluteTime_UTC,   // абсолютное время (UTC)
    Depth,              // глубина, м
    ParameterValue      // значение параметра
};

enum SourceFileKind // тип файла-источника данных
{
    Binary = 1,     // двоичный
    Text            // текстовый
};

struct LoggingDataElementBase // базовый тип (структура) логического элемента данных
{
    ulong       id_element;             // идентифиактор элемента в текущем наборе данных
    double      UTC;                    // целочисленное значение UTC в вещественном формате - для отображения на графиках
    long        timestamp_raw;          // таймстампа, миллисекунды
    double      timestamp_raw_seconds;  // сырая таймстампа, переведенная в секунды (учет тысячных долей - double) ЗАРЕЗЕРВИРОВАНО!
    double      x_value;                // значение параметра по оси абсцисс
    double      y_value_Parameter;      // значение параметра по оси ординат
};

struct LoggingDataElement : public LoggingDataElementBase // расширение типа логического элемента данных
{
    uint    interpolated                : 1; // полученные путём интерполяции
    bool    isEmpty;                        // признак отсутствия значения
};

struct LoggingDatasBase // базовый тип (структура) массива логических элементов данных
{
    uint    isRawData                   : 1; // сырые данные
    uint    isShifted                   : 1; // смещенные по абсцисс данные
    uint    fromFormulaCalculation      : 1; // рассчитанные по формуле данные
    QVector<LoggingDataElement>     datas;          // сам массив
    AbscissParameter                xType;          // тип параметра по оси абсцисс
    QVector<double>                 shiftings;      // сдвиги (смещения) по оси абсцисс (накопленный вектор)
    int size() const                                // число логических элементов данных
    {
        return datas.size();
    }
};

struct LoggingDatasBlock : LoggingDatasBase // расширение типа массива логических элементов данных (БЛОК ДАННЫХ)
{
    QString                         curveIDstring;                      // строковый идентификатор кривой
    QString                         curveIDstring_if_X_isParameter;     // --"-- --"-- --"-- в случае, если X - параметр
    uint                            blockID;                            // идентификатор блока (последовательный!!!)
    uint                            sourceID;                           // идентификатор источника данных
    bool                            linkedByTime;

    LoggingDataElement              firstElement() const                // первый элемент
    {
        return datas.first();
    }

    LoggingDataElement              lastElement() const                 // последний элемент
    {
        return datas.last();
    }

    LoggingDataElement              elementAt(int pos) const            // произвольный элемент массива в позиции pos
    {
        return datas.at(pos);
    }

    LoggingDataElement              elementByItsID(ulong ID) const      // произвольный элемент массива по его идентификатору
    {
        LoggingDataElement elRet;
        elRet.interpolated = 0;
        elRet.id_element = 0L;
        elRet.UTC = 0.0;
        elRet.timestamp_raw = 0L;
        elRet.timestamp_raw_seconds = 0.0;
        elRet.x_value = 0.0;
        elRet.y_value_Parameter = 0.0;
        elRet.isEmpty = true;

        for (QVector<LoggingDataElement>::const_iterator it = datas.begin();
             it != datas.end(); it++)
        {
            if (it->id_element == ID)
            {
                elRet = *it;
                break;
            }
        }

        return elRet;
    }

    QList<LoggingDataElement>       elementsWithSameTimestamp(ulong timestamp) const // список элементов с заданной таймстампой
    {
        QList<LoggingDataElement> retVal;
        ulong ts = (ulong)datas.first().timestamp_raw_seconds;
        for (QVector<LoggingDataElement>::const_iterator it = datas.begin();
             it != datas.end(); it++)
        {
            //ulong ts = (ulong)it->timestamp_raw_seconds;
            if (ts == timestamp)
            {
                retVal.append(*it);
            }
        }

        return retVal;
    }

    void                            shiftingValues(double X_offsetDouble) // сдвиг значений абсцисс всех элементов блока
    {
        bool markShift = (shiftings.isEmpty() ? false : true);
        for (QVector<LoggingDataElement>::iterator it = datas.begin();
             it != datas.end(); it++)
        {
            it->x_value += X_offsetDouble;
            //if (markShift)
            //    it->isShifted = 1;
        }
        if (!markShift)
            isShifted = 1;
        shiftings.push_back(X_offsetDouble);
    }

    void                            insertELementIntoPosition(int posBefore, LoggingDataElement newElem) // вставка элемента
    {
        datas.insert(posBefore, newElem);
    }
};

struct DependenciesCalc // зависимости при вычислении
{
    QVector<uint>           parents_curvesIDs; // родители (наборы данных, от которых зависит текущий набор данных)
    QVector<uint>           children_curvesIDs; // дети (наборы данных, заисимые от текущего)
};

struct LoggingCurve // объект данных "кривая" (кусочно-непрерывный набор данных - состоит из блоков)
{
    QVector<LoggingDatasBlock> blocks;  // набор блоков данных (последовательный)
    uint                      sourceID; // идентификатор источника
    uint                      curveID;  // идентификатор кривой

    QString                   curveIDstring;                    // строковый идентификатор кривой
    QString                   curveIDstring_if_X_isParameter;   // --"-- --"-- --"-- в случае, если X - параметр
    DependenciesCalc          dependencies;                     // зависимости в расчётах

    void shiftAllBlocks(double xOffset) // сдвиг всех блоков данных
    {
        for (QVector<LoggingDatasBlock>::iterator it = blocks.begin();
             it != blocks.end(); it++)
        {
            it->shiftingValues(xOffset);
        }
    }

    void shiftSomeUnlinkedBlock(int blockIndex, double xOffset, bool &success) // сдвиг отдельного блока, он должен быть не увязан по времени
    {
        success = false;
        if (blocks[blockIndex].linkedByTime) // а блок-то увязан по времени!
        {
            return;
        }
        if (blockIndex < blocksCount()) // индекс блока меньше количества блоков
        {
            // проверка на отсутствие наслоений смещаемых данных на данные соседних блоков ("в рамках ...")
            if (xOffset >= 0.0) // при смещении вперёд - проверка, чтоб не получилось наслоения на следующий блок
            {
                double diffWithNextBlock = blocks[blockIndex+1].firstElement().x_value - blocks[blockIndex].lastElement().x_value;
                if (diffWithNextBlock < xOffset)
                {
                    return;
                }
            }
            else // xOffset < 0.0 _ при смещении назад - проверка, чтоб не получилось наслоения на предыдущий блок
            {
                double diffWithPreviosBlock = blocks[blockIndex].firstElement().x_value - blocks[blockIndex-1].lastElement().x_value;
                if (diffWithPreviosBlock < xOffset)
                {
                    return;
                }
            }
            blocks[blockIndex].shiftingValues(xOffset); // всё проверено, сдвигаем.
            success = true;
        }
    }

    int  getBlockIndexForValueX(double x, bool &valueInsideSomeBlock)
    {
        valueInsideSomeBlock = false;
        int idx = -1;
        int cntr = 0;
        foreach (LoggingDatasBlock bl, blocks)
        {
            if (x >= bl.firstElement().x_value)
            {
                if (x <= bl.lastElement().x_value)
                {
                    idx = cntr;
                    valueInsideSomeBlock = true;
                    break;
                }
            }
            if (x < bl.firstElement().x_value)
            {
                break;
            }
            cntr++;
        }
        return idx;
    }

    void setSourceID(uint srcId)        // присвоить идентификатор источника
    {
        sourceID = srcId;
        for (QVector<LoggingDatasBlock>::iterator it = blocks.begin();
             it != blocks.end(); it++)
        {
            it->sourceID = srcId;
        }
    }

    QVector<uint>           depending_on_curves_IDs()       // возвращает идентификаторы кривых, от которых зависит
    {
        return              dependencies.parents_curvesIDs;
    }

    QVector<uint>           depended_curves_IDs()           // возвращает идентификаторы зависимых кривых
    {
        return              dependencies.children_curvesIDs;
    }

    int                     blocksCount()
    {
        return              blocks.size();
    }
};

struct SourceInfo   // информация об источнике данных
{
    uint                    sourceID;               // идентификатор
    QString                 DateString_yyyy_mm_dd;  // строка даты (гггг-мм-дд)
    QString                 nameFileImported;       // имя импортированного файла
    SourceFileKind          kindFileImported;       // вид импортированного файла
    QString                 exstensionFileImported; // расширение --"-- --"--
    QString                 information;            // прочая информация
};

#endif // BASEDATATYPES_H
