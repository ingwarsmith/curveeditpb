#include "standardcsvimportmodule.h"
#include "stdcsv.h"

#include <QString>

StdCsv *wo;

StandardCsvModule::StandardCsvModule(QObject *)
{
    wo = new StdCsv;
}

StandardCsvModule::~StandardCsvModule()
{
    delete wo;
}

void StandardCsvModule::getFormatName(QString &dataSourceFormatName)
{
    wo->getFormatName(dataSourceFormatName);
}

void StandardCsvModule::getFileExtensionsList(QStringList &dataSourceFileExtensions)
{
    wo->getFileExtensionsList(dataSourceFileExtensions);
}

void StandardCsvModule::getAbscissType(AbscissParameter &abscissType)
{
    abscissType = AbscissParameter::AbsoluteTime_UTC;
}

void StandardCsvModule::setMaximumPeriodOfNonmonotonicIntervalMSesc(int periodMSecs)
{
    wo->setTimeLoopMaxPeriod(periodMSecs);
}

void StandardCsvModule::setDataFileIndexed(QString dataSourceFilePath)
{
    wo->setDataFileIndexed(dataSourceFilePath);
}

void StandardCsvModule::getDeviceName(QString &dev_name)
{
    dev_name = wo->getDeviceName();
}

void StandardCsvModule::getParametersOfDevice(QString &data_dev)
{
    data_dev = wo->getDeviceInformation();
}

void StandardCsvModule::getNamesOfCurves(QStringList &crvNames, QStringList &crvDescriptions)
{
    wo->getNamesOfCurves(crvNames, crvDescriptions);
}

void StandardCsvModule::getTimesData(std::unordered_map<uint32_t, TimeLogElement> &times, std::vector<uint32_t> &excludedTimeIds)
{
    wo->getTimesData(times, excludedTimeIds);
}

void StandardCsvModule::getDepthsData(std::unordered_map<uint32_t, DepthLogElement> &)
{}

void StandardCsvModule::getParameterValues(std::vector<double> &, QString &)
{}

void StandardCsvModule::getCurvesData(std::vector<LoggingCurve*> &curves, QStringList selectedCurvesNames)
{
    wo->getCurvesData(curves, selectedCurvesNames);
}

void StandardCsvModule::getHierarchyInfo(QVector<HierarchyMember> &hierarchyMembers)
{
    wo->getHierarchyInfo(hierarchyMembers);
}

void StandardCsvModule::getLastOperationResult(bool &success)
{
    success = wo->lastOperationSuccessfull();
}

void StandardCsvModule::getErrors(QStringList &errors)
{
    errors = wo->lastErrors();
}


