#-------------------------------------------------
#
# Project created by QtCreator 2019-04-24T11:37:32
#
#-------------------------------------------------

QT       -= gui

CONFIG += c++17

TARGET = stdcsv
TEMPLATE = lib

CONFIG += plugin

DEFINES += STDCSV_LIBRARY

CONFIG += skip_target_version_ext

NAME = CurveEditor Import Module Plugin for GeoMash CSV

QMAKE_TARGET_DESCRIPTION = CurveEditor Import Module Plugin for GeoMash CSV

#VERSION = 0.5.1.0 #
VERSION = 1.1.6.1
DEFINES += VERSION=\\\"$$VERSION\\\"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += stdcsv.cpp \
    standardcsvimportmodule.cpp

HEADERS += stdcsv.h\
    ../gceLogicLayer/base/importmoduleinterface.h \
        stdcsv_global.h \
    standardcsvimportmodule.h

#unix {
#    target.path = /usr/lib
#    INSTALLS += target
#}

INCLUDEPATH += ../gceLogicLayer/base


unix {
    TARGET_EXT = .so
#    CONFIG(release, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/release/libstdcsv.so $$OUT_PWD/../sys/import.modules/libstdcsv.so
#    else: CONFIG(debug, debug|release): QMAKE_POST_LINK = \
#            cp $$OUT_PWD/../gce/debug/libstdcsv.so $$OUT_PWD/../sys/import.modules/libstdcsv.so
    QMAKE_POST_LINK = \
            mkdir -p $$OUT_PWD/../sys/import.modules &&\
            cp $$OUT_PWD/libstdcsv.so $$OUT_PWD/../sys/import.modules/libstdcsv.so.1.1.5 && \
#            if ! [ -f $$OUT_PWD/../sys/import.modules/libstdcsv.so.1 ]; then && \
#            ln -s $$OUT_PWD/../sys/import.modules/libstdcsv.so.1.1.5 $$OUT_PWD/../sys/import.modules/libstdcsv.so.1; && \
#            fi
            mv $$OUT_PWD/../sys/import.modules/libstdcsv.so.1.1.5 $$OUT_PWD/../sys/import.modules/libstdcsv.so
}
win32 {
    DLLDESTDIR = $$OUT_PWD/../sys/import.modules
}

