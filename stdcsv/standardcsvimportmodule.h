#ifndef STANDARDCSVIMPORTMODULE_H
#define STANDARDCSVIMPORTMODULE_H

#include <QObject>
#include "importmoduleinterface.h"

class StandardCsvModule : public ImportModuleInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID importModuleInterface_iid)
    Q_INTERFACES(ImportModuleInterface)

public:
    explicit StandardCsvModule(QObject *parent = nullptr);

    virtual ~StandardCsvModule();
    virtual void getFormatName(QString &dataSourceFormatName);
    virtual void getFileExtensionsList(QStringList &dataSourceFileExtensions);
    virtual void getAbscissType(AbscissParameter &abscissType);
    virtual void setDataFileIndexed(QString dataSourceFilePath);
    virtual void setMaximumPeriodOfNonmonotonicIntervalMSesc(int periodMSecs);
    // после успешной загрузки:
    virtual void getDeviceName(QString &dev_name);
    virtual void getParametersOfDevice(QString &data_dev);
    virtual void getNamesOfCurves(QStringList &crvNames, QStringList &crvDescriptions);
    virtual void getTimesData(std::unordered_map<uint32_t, TimeLogElement> &times, std::vector<uint32_t> &excludedTimeIds);
    virtual void getDepthsData(std::unordered_map<uint32_t, DepthLogElement> &depths);
    virtual void getParameterValues(std::vector<double> &param_values, QString &parameter_name);
    virtual void getCurvesData(std::vector<LoggingCurve*> &curves, QStringList selectedCurvesNames = QStringList());
    virtual void getHierarchyInfo(QVector<HierarchyMember> &hierarchyMembers);
    virtual void getLastOperationResult(bool &success);
    virtual void getErrors(QStringList &errors);
};

#endif // STANDARDCSVIMPORTMODULE_H
