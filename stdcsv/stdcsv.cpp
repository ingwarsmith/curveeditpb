#include "stdcsv.h"

#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QTextCodec>
#include <QDateTime>

#include <QDebug>

#include <QCoreApplication>

//constexpr int32_t TS_NONE = -1; // это может означать только интерполяцию
////constexpr double UTC_NONE = -111777.99932523;
//constexpr int64_t UTC_NONE = -1;

StdCsv::StdCsv()
{
    loadingSuccess = true;
    onceLoadDataHeader = false;
    msecsPeriodMaxOfTimeLoop = 0;
    positionDataBegin = 0LL;
    useCodePage1251_onLinux = false;
    codepageChecked = false;
}

void StdCsv::getFormatName(QString &dataSourceFormatName)
{
    dataSourceFormatName = QObject::tr("Standard file CSV GM-Monitor");
}

void StdCsv::getFileExtensionsList(QStringList &dataSourceFileExtensions)
{
    dataSourceFileExtensions.clear();
    dataSourceFileExtensions.append("Files CSV (*.csv)");
}

void StdCsv::setDataFileIndexed(QString dataSourceFilePath)
{
    fnameIndexed = dataSourceFilePath;
    onceLoadDataHeader = false;

    loadStandardTextCsvFileHeader();

    onceLoadDataHeader = true;
}

void StdCsv::getNamesOfCurves(QStringList &crvNames, QStringList &crvDescriptions)
{
    if (onceLoadDataHeader)
    {
        if (formatVersion > 0)
        {
            crvNames = tableHeaderProperties.dataHeaderElementsWithoutEMST;
            crvDescriptions = tableHeaderProperties.descriptionElements;
            crvDescriptions.removeAt(tableHeaderProperties.indexEMST); //without EMST
        }
        else
        {
            crvNames = tableHeaderProperties.dataHeaderElements;
            crvDescriptions.clear();
        }
    }
    else
    {
        loadStandardTextCsvFileHeader();
        if (loadingSuccess)
        {
            if (formatVersion > 0)
            {
                crvNames = tableHeaderProperties.dataHeaderElementsWithoutEMST;
                crvDescriptions = tableHeaderProperties.descriptionElements;
                crvDescriptions.removeAt(tableHeaderProperties.indexEMST); //without EMST
            }
            else
            {
                crvNames = tableHeaderProperties.dataHeaderElements;
                crvDescriptions.clear();
            }
            onceLoadDataHeader = true;
        }
    }
}

void StdCsv::getTimesData(std::unordered_map<uint32_t, TimeLogElement> &times, std::vector<uint32_t> &excludedTimeIds)
{
    if (!onceLoadDataHeader)
    {
        loadStandardTextCsvFileHeader();
        if (loadingSuccess)
        {
            onceLoadDataHeader = true;
        }
        else
        {
            return;
        }
    }
    loadingSuccess = false;
    QFile fl_MAIN(fnameIndexed);
    if (fnameIndexed.isEmpty())
    {
        lastOperationErrors.append(
                    QObject::tr("Error name file indexed")
                    //.append(":\n").append(fnameIndexed)
                    );
        return;
    }
    bool ok = fl_MAIN.open(QFile::ReadOnly|QFile::Text);
    if (!ok)
    {
        lastOperationErrors.append(
                    QObject::tr("Can't open indexed file CSV")
                    .append(":\n").append(fnameIndexed)
                    );
        return;
    }

    uint32_t id_row_in_data = 0; // индекс строки в данных

    struct TimeCompareElement
    {
        uint32_t    id;         // line in file in data part
        int32_t     timestamp_raw;  // для привязки к елементу данных
        int64_t     utc;
    };

    std::vector<TimeCompareElement> times_compare;

    QTextStream stream(&fl_MAIN);
    auto dataBegin = false, utcPresent = false;
    if (formatVersion > 0)
    {
        stream.seek(positionDataBegin);
    }
    auto no_utc_none = true;

    while (!stream.atEnd())
    {
        auto line = stream.readLine();
        auto ts = TS_NONE;
        auto utc = UTC_NONE;
        auto processTimeData = false;
        QString timestampStr;

        LoggingDatasBlock current_block;
        LoggingDatasBlock previos_block;

        if (formatVersion == 0)
        {
            if (dataBegin)
            {
                QStringList tmp = line.split(";");
                timestampStr = tmp.at(0);
                QString utcStr = tmp.at(1);
                processTimeData = true;

                if (utcPresent)
                {
                    if (!utcStr.isEmpty())
                    {
                        utc = qRound64( utcStr.toDouble()*1000.0 );
                    }
                }
            }
            else
            {
                if (line.split(";").at(0) == "ts")
                {
                    dataBegin = true;
                    if (line.split(";").at(1) == "UTC")
                    {
                        utcPresent = true; // есть колонка UTC
                    }
                }
                else continue; // далее эту строку не анализируем - это еще в заголовке что-то
            }
        }
        else //formatVersion > 0
        {
            QStringList tmp = line.split(";");
            timestampStr = (tableHeaderProperties.indexWMST >= 0 ? tmp.at(tableHeaderProperties.indexWMST) : "");
            QString utcStr = tmp.at(tableHeaderProperties.indexEMST);
            processTimeData = true;

            if (!utcStr.isEmpty())
            {
                utc = utcStr.toLongLong();
            }
        }

        if (!timestampStr.isEmpty())
        {
            ts = timestampStr.toLong();
        }

        if (processTimeData)
        {
            TimeCompareElement compareTimeElement;
            if (times_compare.empty())
            {
                if (utc == UTC_NONE) no_utc_none = false;
                compareTimeElement.id = id_row_in_data;
                compareTimeElement.timestamp_raw = ts;
                compareTimeElement.utc = utc;
                times_compare.push_back(compareTimeElement);
            }
            else
            {
                if (utc == UTC_NONE) no_utc_none = false;
                auto lastTimeElementCmpr = times_compare.back();
                if ( ts != TS_NONE && ts < lastTimeElementCmpr.timestamp_raw
                        && msecsPeriodMaxOfTimeLoop > 0
                        && (lastTimeElementCmpr.timestamp_raw - ts) <= msecsPeriodMaxOfTimeLoop
                     && ts > 10000
                     )
                {
                    excludedTimeIds.push_back(id_row_in_data);
                    id_row_in_data++;
                    continue;
                }

                compareTimeElement.id = id_row_in_data;
                compareTimeElement.timestamp_raw = ts;
                compareTimeElement.utc = utc;
                // внутри каждого отдельного блока с непрерывным возрастанием ts:
                // -----|--------@**********-->*******|--------
                if (utc == UTC_NONE && ts >= lastTimeElementCmpr.timestamp_raw
                        && lastTimeElementCmpr.utc != UTC_NONE)
                {
                    auto diff = compareTimeElement.timestamp_raw - lastTimeElementCmpr.timestamp_raw;
                    compareTimeElement.utc = lastTimeElementCmpr.utc + static_cast<int64_t>(diff);
                }
                times_compare.push_back(compareTimeElement);
            }

            id_row_in_data++;
        }
    }

    if (!no_utc_none) // встречены данные с нерассчитанными utc
    {
        std::map<int64_t, TimeCompareElement> times_by_utc;
        auto came_to_beginning = false;
        std::size_t idx = 1; // 2й элемент

        for (; idx < times_compare.size(); ++idx)
        {
            auto idx_prev = idx - 1;
            // для текущего элемента задано utc, а для предыдущего нет:
            if (times_compare[idx].utc != UTC_NONE && times_compare[idx_prev].utc == UTC_NONE)
            {
                // идём назад до границы с предыдущим блоком:
                // -----|********@************************|--------
                //       <-------
                auto idx_saved = idx;
                while (times_compare[idx_prev].timestamp_raw < times_compare[idx].timestamp_raw && idx_prev > 0)
                {
                    auto diff_back = //it_cmp0->timestamp_raw - it_prev0->timestamp_raw;
                            times_compare[idx].timestamp_raw - times_compare[idx_prev].timestamp_raw;
                    times_compare[idx_prev].utc = times_compare[idx].utc - static_cast<int64_t>(diff_back);
                    --idx_prev;
                    --idx;
                    if (idx_prev == 0)
                    {
                        // начало данных
                        came_to_beginning = true;
                        times_compare[idx_prev].utc = times_compare[idx].utc -
                                static_cast<int64_t>(times_compare[idx].timestamp_raw - times_compare[idx_prev].timestamp_raw);
                    }
                }
                if (!came_to_beginning)
                {
                    //if (it_prev0->utc != UTC_NONE)
                    if (times_compare[idx_prev].utc != UTC_NONE)
                    {
                        idx = idx_saved;
                        continue;
                    }
                    else
                    {
                        auto cond_spec = idx_prev > 0 && times_compare[idx_prev].utc == UTC_NONE;

                        while (cond_spec)
                        {
                            if (times_compare[idx_prev].timestamp_raw < times_compare[idx].timestamp_raw)
                            {
                                auto diff_back =
                                        times_compare[idx].timestamp_raw - times_compare[idx_prev].timestamp_raw;
                                times_compare[idx_prev].utc =
                                        times_compare[idx].utc - static_cast<int64_t>(diff_back);
                            }
                            else // снова граница блока и в конце предыдущего длока нет метки utc
                            {
                                times_compare[idx_prev].utc =
                                        times_compare[idx].utc - static_cast<int64_t>(times_compare[idx].timestamp_raw);
                            }

                            --idx;
                            --idx_prev;

                            cond_spec = idx_prev > 0 && times_compare[idx_prev].utc == UTC_NONE;
                        }
                    }
                }
            }
        }

        // здесь имеем набор блоков с рассчитанными либо нерасчитанными utc
        // пристыкуем нерассчитанные сразу за рассчитанными:
        idx = 1;
        auto idx_nxt = idx + 1;
        for (; idx_nxt < times_compare.size(); ++idx, ++idx_nxt)
        {
            if (times_compare[idx_nxt].utc == UTC_NONE)
            {
                if (times_compare[idx_nxt].timestamp_raw < times_compare[idx].timestamp_raw)
                {
                    times_compare[idx_nxt].utc = times_compare[idx].utc
                            + static_cast<int64_t>(times_compare[idx_nxt].timestamp_raw);
                }
                else // next.ts >= curr.ts
                {
                    times_compare[idx_nxt].utc = times_compare[idx].utc
                            + static_cast<int64_t>(times_compare[idx_nxt].timestamp_raw - times_compare[idx].timestamp_raw);
                }
            }
        }
        // 1я точка:
        times_compare[0].utc = times_compare[1].utc
                - static_cast<int64_t>(times_compare[1].timestamp_raw - times_compare[0].timestamp_raw);

        // отсортируем по utc между блоками:
        real_id_by_row_idx.clear();
        for (auto & tc : times_compare)
        {
            times_by_utc.insert(std::make_pair(tc.utc, tc));
        }

        uint32_t id_new = 0;
        times.clear();
        times.reserve(times_by_utc.size());
        for (auto & tbu : times_by_utc)
        {
            times[id_new] = tbu.second.utc;
            real_id_by_row_idx.insert(std::make_pair(tbu.second.id, id_new));
            ++id_new;
        }
    }
    else // no utc-synchros found
    {
        std::size_t idx_curr = times_compare.size() - 1;
        times_compare[idx_curr].utc = QDateTime::currentMSecsSinceEpoch();
        auto idx_prev = idx_curr - 1;
        while (idx_prev > 0)
        {
            if (times_compare[idx_prev].timestamp_raw > times_compare[idx_curr].timestamp_raw)
            {
                times_compare[idx_prev].utc = times_compare[idx_curr].utc - times_compare[idx_curr].timestamp_raw;
            }
            else
            {
                times_compare[idx_prev].utc = times_compare[idx_curr].utc
                        - (times_compare[idx_curr].timestamp_raw - times_compare[idx_prev].timestamp_raw);
            }
            idx_curr--;
            idx_prev--;
            if (idx_prev == 0)
            {
                times_compare[idx_prev].utc = times_compare[idx_curr].utc
                        - (times_compare[idx_curr].timestamp_raw - times_compare[idx_prev].timestamp_raw);
            }
        }

        times.clear();
        times.reserve(times_compare.size());
        real_id_by_row_idx.clear();
        for (auto & tcmp : times_compare)
        {
            real_id_by_row_idx.insert(std::make_pair(tcmp.id, tcmp.id));
            times[tcmp.id] = tcmp.utc;
        }
    }

    fl_MAIN.close();

    loadingSuccess = true;
}

void StdCsv::getCurvesData(std::vector<LoggingCurve*> &curves, QStringList selectedCurvesNames)
{
    loadingSuccess = false;
    QFile fl_MAIN(fnameIndexed);
    if (fnameIndexed.isEmpty())
    {
        lastOperationErrors.append(
                    QObject::tr("Error name file indexed")
                    //.append(":\n").append(fnameIndexed)
                    );
        return;
    }
    bool ok = fl_MAIN.open(QFile::ReadOnly|QFile::Text);
    if (!ok)
    {
        lastOperationErrors.append(
                    QObject::tr("Can't open indexed file CSV")
                    .append(":\n").append(fnameIndexed)
                    );
        return;
    }

    for (auto &curve : curves)
    {
        curve->ids_x.clear();
        curve->values.clear();
    }

    QTextStream stream(&fl_MAIN);
    #ifdef Q_OS_LINUX
    if (useCodePage1251_onLinux)
    {
        QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");
        stream.setCodec(cdc);
    }
    #endif

    QList<int32_t> indexes;

    uint32_t id_row_in_data = 0;

    auto dataBegin = false, utcPresent = false;

    if (formatVersion > 0)
    {
        for (auto it = curves.begin(); it != curves.end(); ++it)
        {
            if (!selectedCurvesNames.contains(QString::fromStdString((*it)->name))) continue;
            indexes << tableHeaderProperties.dataHeaderElements.indexOf(
                           QString::fromStdString((*it)->name)
                           );
            (*it)->loaded = true;
        }

        stream.seek(positionDataBegin);
    }

    while (!stream.atEnd())
    {
        auto line = stream.readLine();
        if (formatVersion == 0)
        {
            if (dataBegin)
            {
                QStringList tmp = line.split(";");
                if (utcPresent)
                {
                    tmp.removeAt(1); // UTC
                }
                for (size_t icrv = 0; icrv < curves.size(); ++icrv)
                {
                    int32_t indexCurve = indexes.at(icrv);
                    QString value = tmp.at(indexCurve);
                    if (!value.isEmpty())
                    {
                        if (real_id_by_row_idx.find(id_row_in_data) != real_id_by_row_idx.end())
                        {
                            curves[icrv]->values.push_back( value.toDouble() );
                            curves[icrv]->ids_x.push_back( real_id_by_row_idx[id_row_in_data] /*id_row_in_data*/ );
                        }
                    }
                }
                id_row_in_data++;
            }
            else
            {
                if (line.split(";").at(0) == "ts")
                {
                    dataBegin = true;
                    if (line.split(";").at(1) == "UTC")
                    {
                        utcPresent = true;
                    }
                    if (tableHeaderProperties.dataHeaderElements.isEmpty()) // считывание заголовков
                    {
                        QString dataHeader = line;
                        tableHeaderProperties.dataHeaderElements = dataHeader.split(";");
                        if (tableHeaderProperties.dataHeaderElements.at(1) == "UTC")
                        {
                            tableHeaderProperties.dataHeaderElements.removeAt(1);
                        }
                    }
//                    for (int icrv = 0; icrv < curves.size(); ++icrv) // индексирование
//                    {
//                        if ( !selectedCurvesNames.contains(curves.at(icrv)->curveNameString) )
//                        {
//                            continue;
//                        }
//                        indexes << tableHeaderProperties.dataHeaderElements.indexOf(curves.at(icrv)->curveNameString);
//                        curves[icrv]->loaded = true;
//                    }
                    for (auto it = curves.begin(); it != curves.end(); ++it) // индексирование
                    {
                        if (!selectedCurvesNames.contains(QString::fromStdString((*it)->name))) continue;
                        indexes << tableHeaderProperties.dataHeaderElements.indexOf(
                                       QString::fromStdString((*it)->name)
                                       );
                        (*it)->loaded = true;
                    }
                }
            }
        }
        else //formatVersion > 0
        {
            QStringList tmp = line.split(";");
            for (size_t icrv = 0; icrv < curves.size(); ++icrv)
            {
                int32_t indexCurve = indexes.at(icrv);
                QString value = tmp.at(indexCurve);
                if (!value.isEmpty())
                {
                    if (real_id_by_row_idx.find(id_row_in_data) != real_id_by_row_idx.end())
                    {
                        curves[icrv]->values.push_back( value.toDouble() );
                        curves[icrv]->ids_x.push_back( real_id_by_row_idx[id_row_in_data] );
                    }
                }
            }
            id_row_in_data++;
        }
    }
    loadingSuccess = true;
}

void StdCsv::getHierarchyInfo(QVector<HierarchyMember> &hierarchyMembers)
{
    switch (formatVersion)
    {
    case 0:
    {
        HierarchyMember rootMember;
        rootMember.curves_names.clear();
        for (auto &dhe : tableHeaderProperties.dataHeaderElements)
        {
            rootMember.curves_names.push_back(dhe.toStdString());
        }
        rootMember.member_id = 0;
        rootMember.depend_on_id = -1;
        rootMember.name = "Root";
        hierarchyMembers.append(rootMember);

        break;
    }
    case 1:
    {
        QString adjustingsName = QObject::tr("Adjustings");

        HierarchyMember rootMember;
        rootMember.member_id = 0;
        rootMember.depend_on_id = -1;
        rootMember.name = "Root";

        HierarchyMember adjustingsMember;
        adjustingsMember.member_id = 100;
        adjustingsMember.depend_on_id = 0;
        adjustingsMember.name = adjustingsName.toStdString();

        QMap<QString, HierarchyMember> membersForSortsByName;
        QMap<QString, HierarchyMember> membersForAdjustingsSortsByName;
        QStringList curvesNamesRoot, curvesNamesAdjustings;

        auto allSortElementsAreEmpty = true;
        for (QString& sort : tableHeaderProperties.sortElements)
        {
            if (!sort.isEmpty())
            {
                allSortElementsAreEmpty = false;
                break;
            }
        }

        if (allSortElementsAreEmpty)
        {
            for (decltype (tableHeaderProperties.dataHeaderElements.size()) i = 0;
                 i < tableHeaderProperties.dataHeaderElements.size(); ++i)
            {
                if (tableHeaderProperties.indexesOfWriteable.size() > 0
                        && tableHeaderProperties.indexesOfWriteable.contains(i)) // это настройка
                {
                    if (tableHeaderProperties.structureKindElements.size() == 0)
                    {
                        curvesNamesAdjustings << tableHeaderProperties.dataHeaderElements.at(i);
                    }
                    if (tableHeaderProperties.structureKindElements.size() > 0)
                    {
                        if (tableHeaderProperties.structureKindElements.at(i) == "ARRAY")
                        {
                            // это - начало массива
                            if (tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.keys().size() > 0
                                 && tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.keys().contains(i))
                            {
                                QString arrBeg = tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                                curvesNamesAdjustings << arrBeg;
                            }
                        }
                        else
                        {
                            curvesNamesAdjustings << tableHeaderProperties.dataHeaderElements.at(i);
                        }
                    }
                }
                else // это не настройка (~канал)
                {
                    if (tableHeaderProperties.structureKindElements.size() == 0)
                    {
                        curvesNamesRoot << tableHeaderProperties.dataHeaderElements.at(i);
                    }
                    if (tableHeaderProperties.structureKindElements.size() > 0)
                    {
                        if (tableHeaderProperties.structureKindElements.at(i) == "ARRAY")
                        {
                            // это - начало массива
                            if (tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.keys().size() > 0
                                 && tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.keys().contains(i))
                            {
                                QString arrBeg = tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                                curvesNamesRoot << arrBeg;
                            }
                        }
                        else
                        {
                            curvesNamesRoot << tableHeaderProperties.dataHeaderElements.at(i);
                        }
                    }
                }
            }
        }
        else // есть сортировка на Основные, Системные, Сервисные, ...
        {
            QStringList sl_sorts, sl_sorts_adjustings;
            QMap<int, QString> map_sorts, map_sorts_adjustings;
            for (decltype (tableHeaderProperties.sortElements.size()) i = 0;
                 i < tableHeaderProperties.sortElements.size(); ++i)
            {
                QString sort = tableHeaderProperties.sortElements.at(i);
                if (tableHeaderProperties.indexesOfWriteable.size() > 0
                        && tableHeaderProperties.indexesOfWriteable.contains(i)) // это настройка
                {
                    if (sort.isEmpty())
                    {
                        if (tableHeaderProperties.structureKindElements.size() == 0)
                        {
                            curvesNamesAdjustings << tableHeaderProperties.dataHeaderElements.at(i);
                        }
                        if (tableHeaderProperties.structureKindElements.size() > 0)
                        {
                            curvesNamesAdjustings << tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                        }
                    }
                    else
                    {
                        if (sl_sorts_adjustings.size() == 0)
                        {
                            sl_sorts_adjustings << sort;
                            map_sorts_adjustings.insert(100001, sort);
                        }
                        else if (!sl_sorts_adjustings.contains(sort))
                        {
                            sl_sorts_adjustings << sort;
                            auto k = map_sorts_adjustings.keys().size() + 100001;
                            map_sorts_adjustings.insert(k, sort);
                        }
                    }
                }
                else // это не настройка (~канал)
                {
                    if (sort.isEmpty())
                    {
                        if (tableHeaderProperties.structureKindElements.size() == 0)
                        {
                            curvesNamesRoot << tableHeaderProperties.dataHeaderElements.at(i);
                        }
                        if (tableHeaderProperties.structureKindElements.size() > 0)
                        {
                            curvesNamesRoot << tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                        }
                    }
                    else
                    {
                        if (sl_sorts.size() == 0)
                        {
                            sl_sorts << sort;
                            map_sorts.insert(1001, sort);
                        }
                        else if (!sl_sorts.contains(sort))
                        {
                            sl_sorts << sort;
                            auto k = map_sorts.keys().size() + 1001;
                            map_sorts.insert(k, sort);
                        }
                    }
                }
            }

            for (QString &sortUnique : sl_sorts)
            {
                HierarchyMember mmbSort;
                mmbSort.name = sortUnique.toStdString();
                mmbSort.depend_on_id = rootMember.member_id;
                mmbSort.member_id = map_sorts.key(sortUnique);

                for (decltype (tableHeaderProperties.sortElements.size()) i = 0;
                     i < tableHeaderProperties.sortElements.size(); ++i)
                {
                    if (tableHeaderProperties.indexesOfWriteable.size() > 0
                            && tableHeaderProperties.indexesOfWriteable.contains(i))
                    {
                        continue; // это настройка, а не канал
                    }
                    QString sort = tableHeaderProperties.sortElements.at(i);
                    if (sort == sortUnique)
                    {
                        if (tableHeaderProperties.structureKindElements.size() > 0)
                        {
                            QString cName = tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                            if (cName.size())
                            {
                                mmbSort.curves_names.push_back( cName.toStdString() );
                            }
                        }
                        else
                        {
                            mmbSort.curves_names.push_back( tableHeaderProperties.dataHeaderElements.at(i).toStdString() );
                        }
                    }
                }

                membersForSortsByName.insert(sortUnique, mmbSort);
            }

            adjustingsMember.curves_names.clear();
            for (auto &cn : curvesNamesAdjustings)
            {
                adjustingsMember.curves_names.push_back(cn.toStdString());
            }

            for (QString &sortUnique : sl_sorts_adjustings)
            {
                HierarchyMember mmbSortAdjusting;
                mmbSortAdjusting.name = sortUnique.toStdString();
                mmbSortAdjusting.depend_on_id = adjustingsMember.member_id;
                mmbSortAdjusting.member_id = map_sorts_adjustings.key(sortUnique);

                for (decltype (tableHeaderProperties.sortElements.size()) i = 0;
                     i < tableHeaderProperties.sortElements.size(); ++i)
                {
                    if (tableHeaderProperties.indexesOfWriteable.size() > 0
                            && !tableHeaderProperties.indexesOfWriteable.contains(i))
                    {
                        continue; // это - не настройка, а канал
                    }
                    QString sort = tableHeaderProperties.sortElements.at(i);
                    if (sort == sortUnique )
                    {
                        if (tableHeaderProperties.structureKindElements.size() > 0)
                        {
                            QString cName = tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.value(i);
                            if (cName.size())
                            {
                                mmbSortAdjusting.curves_names.push_back(cName.toStdString());
                            }
                        }
                        else
                        {
                            mmbSortAdjusting.curves_names.push_back( tableHeaderProperties.dataHeaderElements.at(i).toStdString() );
                        }
                    }
                }

                membersForAdjustingsSortsByName.insert(sortUnique, mmbSortAdjusting);
            }
        }

        rootMember.curves_names.clear();
        for (auto &cnr : curvesNamesRoot)
        {
            rootMember.curves_names.push_back(cnr.toStdString());
        }

        hierarchyMembers.append(rootMember);
        for (QString &sort : membersForSortsByName.keys())
        {
            hierarchyMembers.append(membersForSortsByName.value(sort));
        }
        hierarchyMembers.append(adjustingsMember);
        for (QString &sort : membersForAdjustingsSortsByName.keys())
        {
            hierarchyMembers.append(membersForAdjustingsSortsByName.value(sort));
        }

        for (decltype (tableHeaderProperties.arrays.size()) i_array = 0;
             i_array < tableHeaderProperties.arrays.size(); ++i_array)
        {
            array_instance arr_one = tableHeaderProperties.arrays.at(i_array);
            HierarchyMember newArrayMember;
            for (auto &cna : arr_one.dataHeaderElements)
            {
                newArrayMember.curves_names.push_back(cna.toStdString());
            }
            newArrayMember.name = arr_one.arrayName.toStdString();
            newArrayMember.member_id = 1000000000 + i_array;
            newArrayMember.depend_on_id = -1;
            newArrayMember.is_flag = arr_one.isFlag;
            newArrayMember.can_drag = true;
            newArrayMember.structure_number = arr_one.numberInStructuresList.toStdString();

            {
                for (QString &sort : membersForSortsByName.keys())
                {
                    if (membersForSortsByName.value(sort).curves_names.size())
                    {
                        auto mmb = membersForSortsByName.value(sort);
                        if (std::find(
                                    mmb.curves_names.begin(),
                                    mmb.curves_names.end(),
                                    arr_one.arrayName.toStdString()
                                    ) != mmb.curves_names.end() )
                        {
                            newArrayMember.depend_on_id = mmb.member_id;
                            break;
                        }
                    }
                }
                if (newArrayMember.depend_on_id == -1)
                {
                    for (QString &sort : membersForAdjustingsSortsByName.keys())
                    {
                        if (membersForAdjustingsSortsByName.value(sort).curves_names.size())
                        {
                            auto mmb = membersForAdjustingsSortsByName.value(sort);
                            if (std::find(
                                        mmb.curves_names.begin(),
                                        mmb.curves_names.end(),
                                        arr_one.arrayName.toStdString()
                                        ) != mmb.curves_names.end() )
                            {
                                newArrayMember.depend_on_id = mmb.member_id;
                                break;
                            }
                        }
                    }
                }
                if (newArrayMember.depend_on_id == -1)
                {
                    for (QString &cvrNm : curvesNamesAdjustings)
                    {
                        if (cvrNm == arr_one.arrayName)
                        {
                            newArrayMember.depend_on_id = adjustingsMember.member_id;
                            break;
                        }
                    }
                }
                if (newArrayMember.depend_on_id)
                {
                    for (QString &cvrNm : curvesNamesRoot)
                    {
                        if (cvrNm == arr_one.arrayName)
                        {
                            newArrayMember.depend_on_id = rootMember.member_id;
                            break;
                        }
                    }
                }
                if (newArrayMember.depend_on_id == -1)
                {
                    newArrayMember.depend_on_id = 0;
                }
            }

            hierarchyMembers.append(newArrayMember);
        }

        break;
    }
    default:
        break;
    }

    loadingSuccess = true;
}

void StdCsv::loadStandardTextCsvFileHeader()
{
    loadingSuccess = false;
    lastOperationErrors.clear();
    tableHeaderProperties.arrays.clear();
    tableHeaderProperties.categoryElements.clear();
    tableHeaderProperties.categoryStringShift = -1;
    tableHeaderProperties.descriptionStringShift = -1;
    tableHeaderProperties.dataHeaderAkaNameStringShift = -1;
    tableHeaderProperties.dataHeaderElements.clear();
    tableHeaderProperties.dataHeaderElementsWithoutEMST.clear();
    tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.clear();
    tableHeaderProperties.functionElements.clear();
    tableHeaderProperties.functionStringShift = -1;
    tableHeaderProperties.indexEMST = -1;
    tableHeaderProperties.indexWMST = -1;
    tableHeaderProperties.indexesOfReadable.clear();
    tableHeaderProperties.indexesOfWriteable.clear();
    tableHeaderProperties.sortElements.clear();
    tableHeaderProperties.sortStringShift = -1;
    tableHeaderProperties.structureComponentElements.clear();
    tableHeaderProperties.structureComponentStringShift = -1;
    tableHeaderProperties.structureKindElements.clear();
    tableHeaderProperties.structureKindStringShift = -1;
    tableHeaderProperties.structureNumberLabelsElements.clear();
    tableHeaderProperties.structureNumberLabelsStringShift = -1;

    devInfoBlock.binaryFile_SWversionInfo.MegaID.clear();
    devInfoBlock.binaryFile_SWversionInfo.sw_number.clear();
    devInfoBlock.binaryFile_SWversionInfo.sw_version_major.clear();
    devInfoBlock.binaryFile_SWversionInfo.sw_version_minor.clear();
    devInfoBlock.descriptionFile_SWversionInfo.MegaID.clear();
    devInfoBlock.descriptionFile_SWversionInfo.sw_number.clear();
    devInfoBlock.descriptionFile_SWversionInfo.sw_version_major.clear();
    devInfoBlock.descriptionFile_SWversionInfo.sw_version_minor.clear();
    devInfoBlock.computerInfo.computer.clear();
    devInfoBlock.computerInfo.date.clear();
    devInfoBlock.computerInfo.time.clear();
    devInfoBlock.computerInfo.domain.clear();
    devInfoBlock.computerInfo.username.clear();
    devInfoBlock.device.commentDevice.clear();
    devInfoBlock.device.deviceName.clear();
    devInfoBlock.device.deviceSign.clear();
    devInfoBlock.device.nodeName.clear();
    devInfoBlock.device.nodeSign.clear();
    devInfoBlock.numbers.block.clear();
    devInfoBlock.numbers.device.clear();
    devInfoBlock.numbers.node.clear();
    devInfoBlock.nodeTypeText.clear();
    devInfoBlock.commentFree.clear();

    QFile fl_MAIN(fnameIndexed);
    if (fnameIndexed.isEmpty())
    {
        lastOperationErrors.append(
                    QObject::tr("Error name file indexed")
                    //.append(":\n").append(fnameIndexed)
                    );
        return;
    }
    bool ok = fl_MAIN.open(QFile::ReadOnly|QFile::Text);
    if (!ok)
    {
        //QMessageBox::critical(nullptr, tr("Error"), tr("Can't open file CSV "); //.append("\n").append(absPathToImport));
        lastOperationErrors.append(
                    QObject::tr("Can't open indexed file CSV")
                    .append(":\n").append(fnameIndexed)
                    );
        return;
    }

    QString dataHeader;

    QTextStream stream(&fl_MAIN);

    auto firstLineMayBeFormatVersion = stream.readLine();

    if (firstLineMayBeFormatVersion.isEmpty())
    {
        lastOperationErrors.append(
                    QObject::tr("Incorrect file format")
                    );
        return;
    }

    stream.seek(0LL);

    #ifdef Q_OS_LINUX
    if (useCodePage1251_onLinux)
    {
        QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");
        stream.setCodec(cdc);
    }
    #endif

    auto formatVerIs_1 = false;

    if (firstLineMayBeFormatVersion.left(6) == "format")
    {
        auto sl_tmp = firstLineMayBeFormatVersion.split(";");
        if (sl_tmp.size() > 1)
        {
            auto fmtVerStr = sl_tmp[1];
            formatVersion = fmtVerStr.toInt();
            formatVerIs_1 = true;
        }
    }

    if (!formatVerIs_1)
    {
        if (firstLineMayBeFormatVersion.left(3) == "ts;"
                || firstLineMayBeFormatVersion.left(4) == "bin;")
        {
            formatVersion = 0;
        }
        else
        {
            lastOperationErrors.append(
                        QObject::tr("Incorrect file format")
                        );
            return;
        }
    }

    auto dataBegin = false;
    auto tableMarkerFound = false;
    //auto commentFound = false;

    auto shiftStringInTableHeader = 0,
            maxShiftBeforeDataBegin = 0,
            stringsCounter = 0;
    qint64 saved_pos = 0;
    while (!stream.atEnd())
    {
        saved_pos = stream.pos();
        QString line = stream.readLine();
        stringsCounter++;

        if (line.split(";").at(0) == "bin")
        {
            auto sz = line.split(";").size();
            if (sz > 1) devInfoBlock.binaryFile_SWversionInfo.sw_number = line.split(";").at(1).toStdString();
            if (sz > 2) devInfoBlock.binaryFile_SWversionInfo.sw_version_major = line.split(";").at(2).toStdString();
            if (sz > 3) devInfoBlock.binaryFile_SWversionInfo.sw_version_minor = line.split(";").at(3).toStdString();
            if (sz > 4) devInfoBlock.binaryFile_SWversionInfo.MegaID = line.split(";").at(4).toStdString();
        }

        if (line.split(";").at(0) == "desc")
        {
            auto sz = line.split(";").size();
            if (sz > 1) devInfoBlock.descriptionFile_SWversionInfo.sw_number = line.split(";").at(1).toStdString();
            if (sz > 2) devInfoBlock.descriptionFile_SWversionInfo.sw_version_major = line.split(";").at(2).toStdString();
            if (sz > 3) devInfoBlock.descriptionFile_SWversionInfo.sw_version_minor = line.split(";").at(3).toStdString();
            if (sz > 4) devInfoBlock.descriptionFile_SWversionInfo.MegaID = line.split(";").at(4).toStdString();
        }

        if (line.split(";").at(0) == "comp")
        {
            auto sz = line.split(";").size();
            if (sz > 1) devInfoBlock.computerInfo.date = line.split(";").at(1).toStdString();
            if (sz > 2) devInfoBlock.computerInfo.time = line.split(";").at(2).toStdString();
            if (sz > 3) devInfoBlock.computerInfo.username = line.split(";").at(3).toStdString();
            if (sz > 4) devInfoBlock.computerInfo.domain = line.split(";").at(4).toStdString();
            if (sz > 5) devInfoBlock.computerInfo.computer = line.split(";").at(5).toStdString();
        }

        if (line.split(";").at(0) == "number")
        {
            auto sz = line.split(";").size();
            if (sz > 1) devInfoBlock.numbers.device = line.split(";").at(1).toStdString();
            if (sz > 2) devInfoBlock.numbers.block = line.split(";").at(2).toStdString();
            if (sz > 3) devInfoBlock.numbers.node = line.split(";").at(3).toStdString();
        }

        if (line.split(";").at(0) == "type")
        {
            auto sz = line.split(";").size();
            if (sz > 1) /// devInfoBlock.nodeTypeText = line.split(";").at(1).toStdString();
                devInfoBlock.device.deviceName = line.split(";").at(1).toStdString();
        }

        if (line.split(";").at(0) == "device")
        {
            auto sz = line.split(";").size();
            if (sz > 1) devInfoBlock.device.nodeName = line.split(";").at(1).toStdString();
            if (sz > 2) devInfoBlock.device.nodeSign = line.split(";").at(2).toStdString();
            /// if (sz > 3) devInfoBlock.device.deviceName = line.split(";").at(3).toStdString();
            if (sz > 3) devInfoBlock.device.deviceSign = line.split(";").at(4).toStdString();
            if (sz > 4) devInfoBlock.device.commentDevice = line.split(";").at(5).toStdString();
        }

#ifdef Q_OS_LINUX
    auto checkStringIsCyrillicOrLatinOnly = [](const QString &str)
    {
        for (auto &qch : str)
        {
            auto uc = qch.unicode();
            // 'Ё' и 'ё' из расширенной кириллицы
            auto isEdotdot = uc == 0x0401 || uc == 0x0451;
            // стандартный русский алфавит
            auto isStdRus = uc >= 0x0410 && uc <= 0x044f;
            // символы, знаки препинания ASCII и цифры ASCII
            auto isPunctuationOrDigitAscii = (uc >= 0x0020 && uc <= 0x0040)
                    || (uc >= 0x005b && uc <= 0x0060)
                    || (uc >= 0x007b && uc <= 0x007e);
            // символы и знаки препинания Latin-1 и простые дроби 1/4, 1/2, 3/4,
            // матем-е операторы умнож-я и деления
            auto isPunctuationLatin1 = (uc >= 0x00a0 && uc <= 0x00be) || uc == 0x00d7 || uc ==  0x00f7;
            // латинские буквы в верхнем регистре
            auto isUpperLetter = uc >= 0x0041 && uc <= 0x005a;
            // латинские буквы в нижнем регистре
            auto isLowerLetter = uc >= 0x0061 && uc <= 0x007a;
            if (!(isStdRus || isEdotdot || isPunctuationLatin1 || isPunctuationOrDigitAscii || isUpperLetter || isLowerLetter))
            {
                return false;
            }
        }
        return true;
    };
#endif

        if (formatVersion == 0)
        {
            if (dataBegin)
            {
                break;
            }
            else
            {
                fileHeaderStrings << line;
                if (line.split(";").at(0) == "ts")
                {
                    #ifdef Q_OS_LINUX
                    if (!codepageChecked)
                    {
                        codepageChecked = true;
                        auto only_cyr_or_lat = checkStringIsCyrillicOrLatinOnly(line);
                        if (!only_cyr_or_lat) // возможно, кодировка Win cp-1251
                        {
                            useCodePage1251_onLinux = true;
                            stream.seek(saved_pos);
                            QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");
                            stream.setCodec(cdc);
                            line = stream.readLine(); // заново прочитать строку - с кодировкой cp1251
                            fileHeaderStrings.removeLast();
                            fileHeaderStrings << line;
                        }
                    }

                    #endif
                    dataHeader = line;
                    dataBegin = true;
                    tableHeaderProperties.dataHeaderElements = dataHeader.split(";");
                    if (tableHeaderProperties.dataHeaderElements.at(1) == "UTC")
                    {
                        tableHeaderProperties.dataHeaderElements.removeAt(1);
                    }
                }
            }
        }
        // formatVersion > 0:
        else
        {
            //
            if (dataBegin)
            {
                break;
            }
            else
            {
                #ifdef Q_OS_LINUX
                if (!codepageChecked)
                {
                    codepageChecked = true;
                    auto only_cyr_or_lat = checkStringIsCyrillicOrLatinOnly(line);
                    if (!only_cyr_or_lat) // возможно, кодировка Win cp-1251
                    {
                        useCodePage1251_onLinux = true;
                        stream.seek(saved_pos);
                        QTextCodec *cdc = QTextCodec::codecForName("Windows-1251");
                        stream.setCodec(cdc);
                        // заново прочитать строку - с кодировкой cp1251
                        line = stream.readLine();
                    }
                }
                #endif
                fileHeaderStrings << line;

                if (line.left(7) == "comment" && !tableMarkerFound)
                {
                    QStringList sl_tmp = line.split(";");
                    sl_tmp.removeFirst();
                    while (sl_tmp.last() == "")
                    {
                        sl_tmp.removeLast();
                    }
                    QString commentLine = sl_tmp.join(";");
                    comment << commentLine;
                }

                if (tableMarkerFound)
                {
                    if (shiftStringInTableHeader == tableHeaderProperties.categoryStringShift)
                    {
                        tableHeaderProperties.categoryElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.descriptionStringShift)
                    {
                        tableHeaderProperties.descriptionElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.dataHeaderAkaNameStringShift)
                    {
                        QStringList lst = line.split(";");
                        while (lst.last() == "")
                        {
                            lst.removeLast();
                        }
                        tableHeaderProperties.dataHeaderElements = lst;
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.functionStringShift)
                    {
                        tableHeaderProperties.functionElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.sortStringShift)
                    {
                        tableHeaderProperties.sortElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.structureComponentStringShift)
                    {
                        tableHeaderProperties.structureComponentElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.structureKindStringShift)
                    {
                        tableHeaderProperties.structureKindElements = line.split(";");
                    }
                    if (shiftStringInTableHeader == tableHeaderProperties.structureNumberLabelsStringShift)
                    {
                        tableHeaderProperties.structureNumberLabelsElements = line.split(";");
                    }
                }

                if (line.left(5) == "table")
                {
                    tableMarkerFound = true;
                    tableHeaderProperties.tableShiftInHeader = stringsCounter;
                    QStringList sl_tableMarkerString = line.split(";");
                    QString v;
                    for (decltype (sl_tableMarkerString.size()) i = 0; i < sl_tableMarkerString.size(); ++i)
                    {
                        v = sl_tableMarkerString[i];
                        v.remove(" ");
                        sl_tableMarkerString[i] = v;
                    }
                    if (sl_tableMarkerString.last() == "")
                    {
                        while (sl_tableMarkerString.last() == "")
                        {
                            sl_tableMarkerString.removeLast();
                        }
                    }
                    tableHeaderProperties.categoryStringShift = sl_tableMarkerString.toVector().indexOf(QString("category"));
                    tableHeaderProperties.descriptionStringShift = sl_tableMarkerString.toVector().indexOf(QString("description"));
                    tableHeaderProperties.dataHeaderAkaNameStringShift = sl_tableMarkerString.toVector().indexOf(QString("name"));
                    tableHeaderProperties.functionStringShift = sl_tableMarkerString.toVector().indexOf(QString("function"));
                    tableHeaderProperties.sortStringShift = sl_tableMarkerString.toVector().indexOf(QString("sort"));
                    tableHeaderProperties.structureComponentStringShift = sl_tableMarkerString.toVector().indexOf(QString("component"));
                    tableHeaderProperties.structureKindStringShift = sl_tableMarkerString.toVector().indexOf(QString("kind"));
                    tableHeaderProperties.structureNumberLabelsStringShift = sl_tableMarkerString.toVector().indexOf(QString("structure"));
                    maxShiftBeforeDataBegin = sl_tableMarkerString.size();
                }

                if (tableMarkerFound)
                {
                    shiftStringInTableHeader++;
                }
                if (!tableMarkerFound && stringsCounter > 1000)
                {
                    loadingSuccess = false;
                    QString msg = QObject::tr("Error in table header: table marker not found in first 1000 strings");
                    lastOperationErrors.append(msg);
                    return;
                }
                if (shiftStringInTableHeader > 0 && shiftStringInTableHeader == maxShiftBeforeDataBegin)
                {
                    dataBegin = true;
                    positionDataBegin = stream.pos();
                }
            }
        }
    }

    fl_MAIN.close();

    if (formatVersion > 0)
    {
        for (decltype (tableHeaderProperties.categoryElements.size()) i = 0;
             i < tableHeaderProperties.categoryElements.size(); ++i)
        {
            QString elem = tableHeaderProperties.categoryElements.value(i);
            if (elem == "R")
            {
                tableHeaderProperties.indexesOfReadable.append(i);
            }
            else if (elem == "W")
            {
                tableHeaderProperties.indexesOfWriteable.append(i);
            }
            else
            {
                //loadingSuccess = false;
                //QString msg = QObject::tr("Error in table header: unresolved symbol found for category label; error in string ");
                //msg.append(" ").append(QString::number(tableHeaderProperties.tableShiftInHeader + tableHeaderProperties.categoryStringShift));
                //lastOperationErrors.append(msg);
                //return;
            }
        }

        //for (int i = 0; i < tableHeaderProperties.dataHeaderElements.size(); ++i)
        //int lastStructureNumber = -1;
        auto lastStructureNumber = QString();
        for (decltype (tableHeaderProperties.dataHeaderElements.size()) i = 0;
             i < tableHeaderProperties.dataHeaderElements.size(); ++i)
        {
            QString elem = tableHeaderProperties.structureNumberLabelsElements.value(i);
            if (elem.isEmpty())
            {
                tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.insert(i, tableHeaderProperties.dataHeaderElements.at(i));
                continue;
            }
            auto numberStructure = elem; //elem.toInt();
            if (tableHeaderProperties.structureKindElements.at(i) == "ARRAY")
            {
                if (numberStructure == lastStructureNumber)
                {
                    tableHeaderProperties.arrays.last().dataHeaderElements << tableHeaderProperties.dataHeaderElements.at(i);
                }
                else
                {                    
                    lastStructureNumber = numberStructure;
                    array_instance newArray;
                    QString comp = tableHeaderProperties.structureComponentElements.at(i);
                    QString dataEl = tableHeaderProperties.dataHeaderElements.at(i);
                    auto idx = dataEl.lastIndexOf(comp);
                    dataEl.remove(idx, 1); // "Название_массива[]"
                    tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.insert(i, numberStructure);
                    newArray.dataHeaderElements << tableHeaderProperties.dataHeaderElements.at(i);
                    newArray.indexOfFirstElementInDataHeader = i;
                    newArray.numberInStructuresList = numberStructure;
                    newArray.arrayName = numberStructure;
                    tableHeaderProperties.arrays << newArray;
                }
            }
            else if (tableHeaderProperties.structureKindElements.at(i) == "FLAG")
            {
                if (numberStructure == lastStructureNumber)
                {
                    QString dataEl = tableHeaderProperties.dataHeaderElements.at(i);
                    tableHeaderProperties.arrays.last().dataHeaderElements << dataEl;
                }
                else
                {
                    lastStructureNumber = numberStructure;
                    array_instance newFlag;
                    newFlag.isFlag = true;
                    QString dataEl = tableHeaderProperties.dataHeaderElements.at(i);
                    tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.insert(i, numberStructure);
                    newFlag.dataHeaderElements << dataEl;
                    newFlag.indexOfFirstElementInDataHeader = i;
                    newFlag.numberInStructuresList = numberStructure;
                    newFlag.arrayName = numberStructure;
                    tableHeaderProperties.arrays << newFlag;
                    tableHeaderProperties.indexesOfFlags << i;
                }
            }
            else //if (tableHeaderProperties.structureKindElements.at(i) == "MINIMAX")
            {
                if (numberStructure == lastStructureNumber)
                {
                    tableHeaderProperties.arrays.last().dataHeaderElements << tableHeaderProperties.dataHeaderElements.at(i);
                }
                else
                {
                    lastStructureNumber = numberStructure;
                    array_instance newMinimax;
                    QString dataEl = tableHeaderProperties.dataHeaderElements.at(i);
                    //newMinimax.arrayName = dataEl;
                    tableHeaderProperties.dataHeaderElementsNOTFLAT_byIndex.insert(i, numberStructure);
                    newMinimax.dataHeaderElements << dataEl;
                    newMinimax.indexOfFirstElementInDataHeader = i;
                    newMinimax.numberInStructuresList = numberStructure;
                    newMinimax.arrayName = numberStructure;
                    tableHeaderProperties.arrays << newMinimax;
                }
            }
        }

        if (comment.size() > 0)
        {
            QString commentFull = comment.join("\n");
            devInfoBlock.commentFree = commentFull.toStdString();
        }

        if (tableHeaderProperties.functionElements.size() == 0)
        {
            for (decltype (tableHeaderProperties.dataHeaderElements.size()) i = 0;
                 i < tableHeaderProperties.dataHeaderElements.size(); ++i)
            {
                QString elem = tableHeaderProperties.dataHeaderElements.at(i);
                if (elem == "WMST")
                {
                    tableHeaderProperties.indexWMST = i;
                }
                if (elem == "EMST")
                {
                    tableHeaderProperties.indexEMST = i;
                }
            }
        }

        for (decltype (tableHeaderProperties.functionElements.size()) i = 0;
             i < tableHeaderProperties.functionElements.size(); ++i)
        {
            QString elem = tableHeaderProperties.functionElements.at(i);
            if (elem.isEmpty())
            {
                continue;
            }
            if (elem == "WMST")
            {
                tableHeaderProperties.indexWMST = i;
            }
            if (elem == "EMST")
            {
                tableHeaderProperties.indexEMST = i;
            }
        }

        if (tableHeaderProperties.indexEMST == -1)
        {
            loadingSuccess = false;
            QString msg = QObject::tr("Error in table header: field for function EMST not recognized");
            lastOperationErrors.append(msg);
            return;
        }

        for (decltype (tableHeaderProperties.dataHeaderElements.size()) i = 0;
             i < tableHeaderProperties.dataHeaderElements.size(); ++i)
        {
            if (i == tableHeaderProperties.indexEMST)
            {
                continue;
            }
            tableHeaderProperties.dataHeaderElementsWithoutEMST.push_back(tableHeaderProperties.dataHeaderElements.at(i));
        }
    }

    loadingSuccess = true;
}


