#ifndef STDCSV_GLOBAL_H
#define STDCSV_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(STDCSV_LIBRARY)
#  define STDCSVSHARED_EXPORT Q_DECL_EXPORT
#else
#  define STDCSVSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // STDCSV_GLOBAL_H
