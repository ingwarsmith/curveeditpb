#ifndef STDCSV_H
#define STDCSV_H

#include "types.h"
#include <QStringList>
#include <QMap>
#include <QObject>
#include <QString>
#include <memory>

class StdCsv
{

public:
    StdCsv();

    void getFormatName(QString &dataSourceFormatName);
    void getFileExtensionsList(QStringList &dataSourceFileExtensions);
    void setDataFileIndexed(QString dataSourceFilePath);
    void setTimeLoopMaxPeriod(int period)
    {
        msecsPeriodMaxOfTimeLoop = period;
    }
    // после успешной привязки:
    void getNamesOfCurves(QStringList &crvNames, QStringList &crvDescriptions);
    void getTimesData(std::unordered_map<uint32_t, TimeLogElement> &times, std::vector<uint32_t> &excludedTimeIds);
    void getCurvesData(std::vector<LoggingCurve *> &curves, QStringList selectedCurvesNames = QStringList());
    void getHierarchyInfo(QVector<HierarchyMember> &hierarchyMembers);
    bool lastOperationSuccessfull()
    {
        return loadingSuccess;
    }
    QStringList lastErrors()
    {
        auto ret = lastOperationErrors;
        lastOperationErrors.clear();
        return ret;
    }
    QString getDeviceName() const
    {
        return QString::fromStdString(devInfoBlock.device.deviceName);
    }
    QString getDeviceInformation() const
    {
        QString information;
        QString binary;
        auto binary_ok = true;
        binary.append(QObject::trUtf8("Версия ПО для исходного файла данных: "));
        if (devInfoBlock.binaryFile_SWversionInfo.sw_number.empty()
                && devInfoBlock.binaryFile_SWversionInfo.sw_version_major.empty()
                && devInfoBlock.binaryFile_SWversionInfo.sw_version_minor.empty()
                && devInfoBlock.binaryFile_SWversionInfo.MegaID.empty())
        {
            binary_ok = false;
        }
        if (binary_ok)
        {
            binary.append(QString("\n%1%2\n%3%4\n%5%6\n%7%8")
                          .arg(QObject::trUtf8(" - номер программы: "))
                          .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_number.c_str())
                          .arg(QObject::trUtf8(" - мажорная версия: "))
                          .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_version_major.c_str())
                          .arg(QObject::trUtf8(" - минорная версия: "))
                          .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_version_minor.c_str())
                          .arg(QObject::trUtf8("Тип узла в файле описания: "))
                          .arg(devInfoBlock.descriptionFile_SWversionInfo.MegaID.c_str()));

            information.append(binary);
        }

        QString decripted;
        auto dec_ok = true;
        decripted.append(QObject::trUtf8("Версия ПО для файла описания при расшифровке: "));
        if (devInfoBlock.descriptionFile_SWversionInfo.sw_number.empty()
                && devInfoBlock.descriptionFile_SWversionInfo.sw_version_major.empty()
                && devInfoBlock.descriptionFile_SWversionInfo.sw_version_minor.empty()
                && devInfoBlock.descriptionFile_SWversionInfo.MegaID.empty())
        {
            dec_ok = false;
        }
        if (dec_ok)
        {
            decripted.append(QString("\n%1%2\n%3%4\n%5%6\n%7%8\n")
                             .arg(QObject::trUtf8(" - номер программы: "))
                             .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_number.c_str())
                             .arg(QObject::trUtf8(" - мажорная версия: "))
                             .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_version_major.c_str())
                             .arg(QObject::trUtf8(" - минорная версия: "))
                             .arg(devInfoBlock.descriptionFile_SWversionInfo.sw_version_minor.c_str())
                             .arg(QObject::trUtf8("Тип узла в файле описания: "))
                             .arg(devInfoBlock.descriptionFile_SWversionInfo.MegaID.c_str()));

            information.append(decripted);
        }

        QString computer;
        auto comp_ok = true;
        computer.append(QObject::trUtf8("Сведения о компьютере: "));
        if (devInfoBlock.computerInfo.date.empty()
                && devInfoBlock.computerInfo.time.empty()
                && devInfoBlock.computerInfo.username.empty()
                && devInfoBlock.computerInfo.domain.empty()
                && devInfoBlock.computerInfo.computer.empty())
        {
            comp_ok = false;
        }
        if (comp_ok)
        {
            computer.append(QString("\n%1%2\n%3%4\n%5%6\n%7%8\n%9%10\n")
                    .append(QObject::trUtf8(" - пользователь: "))
                    .append(devInfoBlock.computerInfo.username.c_str())
                    .append(QObject::trUtf8(" - компьютер: "))
                    .append(devInfoBlock.computerInfo.computer.c_str())
                    .append(QObject::trUtf8(" - домен: "))
                    .append(devInfoBlock.computerInfo.domain.c_str())
                    .append(QObject::trUtf8(" - дата: "))
                    .append(devInfoBlock.computerInfo.date.c_str())
                    .append(QObject::trUtf8(" - время: "))
                    .append(devInfoBlock.computerInfo.time.c_str()));

            information.append(computer);
        }

        QString numbers;
        auto nums_ok = true;
        if (devInfoBlock.numbers.block.empty()
                && devInfoBlock.numbers.device.empty()
                && devInfoBlock.numbers.node.empty())
        {
            nums_ok = false;
        }
        if (nums_ok)
        {
            numbers.append(QString("\n%1%2\n%3%4\n%5%6\n")
                    .append(QObject::trUtf8("Номер прибора: "))
                    .append(devInfoBlock.numbers.device.c_str())
                    .append(QObject::trUtf8("Номер блока: "))
                    .append(devInfoBlock.numbers.block.c_str())
                    .append(QObject::trUtf8("Номер узла: "))
                    .append(devInfoBlock.numbers.node.c_str()));

            information.append(numbers);
        }

        if (!devInfoBlock.nodeTypeText.empty())
        {
            information.append(QObject::trUtf8("Тип узла (текст): "))
                    .append(devInfoBlock.nodeTypeText.c_str())
                    .append("\n");
        }

        QString device;
        auto dev_ok = true;
        if (devInfoBlock.device.nodeName.empty()
                && devInfoBlock.device.nodeSign.empty()
                && devInfoBlock.device.deviceName.empty()
                && devInfoBlock.device.deviceSign.empty())
        {
            dev_ok = false;
        }
        if (dev_ok)
        {
            device.append(QObject::trUtf8("Имя узла: "))
                    .append(devInfoBlock.device.nodeName.c_str())
                    .append("\n");
            device.append(QObject::trUtf8("Обозначение узла: "))
                    .append(devInfoBlock.device.nodeSign.c_str())
                    .append("\n");
            device.append(QObject::trUtf8("Название прибора: "))
                    .append(devInfoBlock.device.deviceName.c_str())
                    .append("\n");
            device.append(QObject::trUtf8("Обозначение прибора: "))
                    .append(devInfoBlock.device.deviceSign.c_str())
                    .append("\n");

            if (!devInfoBlock.device.commentDevice.empty())
            {
                device.append("\n").append(QObject::trUtf8("Комментарий (~прибор): "))
                        .append(devInfoBlock.device.commentDevice.c_str())
                        .append("\n");
            }

            information.append(device);
        }

        if (!devInfoBlock.commentFree.empty())
        {
            information.append("\n\n").append(QString::fromStdString(devInfoBlock.commentFree));
        }
        return information;
    }

private:
    struct parametersOfDevice
    {
        // bin:
        struct
        {
            std::string sw_number;
            std::string sw_version_major;
            std::string sw_version_minor;
            std::string MegaID;
        } binaryFile_SWversionInfo;

        // desc:
        struct
        {
            std::string sw_number;
            std::string sw_version_major;
            std::string sw_version_minor;
            std::string MegaID;
        } descriptionFile_SWversionInfo;

        // comp:
        struct
        {
            std::string date;
            std::string time;
            std::string username;
            std::string domain;
            std::string computer;
        } computerInfo;

        // number:
        struct
        {
            std::string device; // sndev – серийный номер прибора [данные] в поле 10:Функция(особое назначение) таблиц каналов(и настроек?) в ФО узла
            std::string block; // sn_block – серийный номер блока [данные] в поле 10:Функция(особое назначение) таблиц каналов(и настроек?) в ФО узла
            std::string node; // sn_node – серийный номер узла [данные] в поле 10:Функция(особое назначение) таблиц каналов(и настроек?) в ФО узла
        } numbers;

        // type:
        std::string nodeTypeText; // поле 10 таблицы общих данных в ФО узла

        // device:
        struct
        {
            std::string nodeName;   // поле 4 таблицы общих данных в ФО узла
            std::string nodeSign;   // поле 5 таблицы общих данных в ФО узла
            std::string deviceName;
            std::string deviceSign;
            std::string commentDevice;
        } device;

        // free comment at description process:
        std::string commentFree;

        //
    };

    void loadStandardTextCsvFileHeader();

    parametersOfDevice devInfoBlock;

    bool onceLoadDataHeader;
    bool loadingSuccess;
    QStringList lastOperationErrors;
    QString fnameIndexed;
    QStringList fileHeaderStrings;
    int  msecsPeriodMaxOfTimeLoop;
    int  formatVersion;
    int64_t positionDataBegin;

    struct array_instance
    {
        int         indexOfFirstElementInDataHeader;
        QString     numberInStructuresList;
        QString     arrayName;
        QStringList dataHeaderElements;
        bool        isFlag;
        array_instance() { isFlag = false; }
    };

    struct
    {
        int         tableShiftInHeader;
        int         functionStringShift;
        QStringList functionElements; //v
        int         categoryStringShift;
        QStringList categoryElements; //v
        int         descriptionStringShift;
        QStringList descriptionElements;
        int         sortStringShift;
        QStringList sortElements; //v
        int         structureNumberLabelsStringShift;
        QStringList structureNumberLabelsElements; //v
        int         structureKindStringShift;
        QStringList structureKindElements; //v
        int         structureComponentStringShift;
        QStringList structureComponentElements; //v
        int         dataHeaderAkaNameStringShift;
        QStringList dataHeaderElements; //v
        QStringList dataHeaderElementsWithoutEMST;
        QMap<int, QString> dataHeaderElementsNOTFLAT_byIndex; //v // кроме массивов, только их первые элементы
        QVector<array_instance> arrays; //v
        QVector<int> indexesOfReadable; //v
        QVector<int> indexesOfWriteable; //v
        QVector<int> indexesOfFlags;
        int         indexWMST; //v
        int         indexEMST; //v
    } tableHeaderProperties;

    QStringList     comment;
    std::map<uint32_t, uint32_t> real_id_by_row_idx;

    bool            useCodePage1251_onLinux;
    bool            codepageChecked;
};

#endif // STDCSV_H
